#include<bits/stdc++.h>
using namespace std;

#define MAXP 1000000
set<int> criba[MAXP+1];
int sv[100000];
int nums[100000];

int main(void){
	int t;
	cin>>t;
	while(t--){
		for(int i = 1; i <= 1000000; i++) criba[i] = set<int>();
		memset(sv,0,sizeof(sv));
		int n;
		cin>>n;
		for(int i = 0; i < n; i++) cin>>nums[i];
		for(int i = n-1; i >= 0; i--){
			int num = nums[i];
			if(criba[num].size()){
				for(auto j:criba[num])
					sv[j]++;
			}
			else{
				for(int j = num; j <= MAXP; j+=num){
					criba[j].insert(i);
				}
			}
			
		}
		int mx = 0;
		for(int i = 0; i < n; i++){
			mx = max(mx, sv[i]);
		}
		cout<<mx<<'\n';
	}

	return 0;
}
