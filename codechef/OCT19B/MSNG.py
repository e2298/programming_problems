def is_int(s,b):
    try: 
        int(s,b)
        return True
    except ValueError:
        return False

t = int(input())
for _ in range(t):
    n = int(input())
    inp = [input().split() for _ in range(n)]
    bases = [int(i[0]) for i in inp]
    nums = [i[1] for i in inp]
    free = True
    for i in range(n):
        if(bases[i] != -1):
            free = int(nums[i],bases[i])
            break
    if(free == True):
        cool = True
        for base in range(2,37):
            cool = True
            if(not is_int(nums[0],base)):
                continue
            num = int(nums[0],base)
            if(num > 10**12):
                cool = False
                break
            for i in range(1,n):
                can = False
                for bas in range(2,37):
                    if(is_int(nums[i],bas) and int(nums[i],bas) == num):
                        can = True
                        break
                if(not can):
                    cool = False
                    break
            if(cool):
                print(num)
                break
        if(not cool):
            print(-1)
            
    else:
        cool = True
        for i in range(n):
            if(bases[i] == -1):
                can = False
                for bas in range(2,37):
                    if(is_int(nums[i],bas) and int(nums[i],bas) == free):
                        can = True
                        break
                if(not can):
                    cool = False
                    break
            else:
                if(int(nums[i],bases[i]) != free):
                    cool = False
                    break
        if(cool and free <= 10**12):
            print(free)
        else:
            print(-1)
        

