#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		deque<int> q;
		int r = 0;
		while(n--){
			int i;
			cin>>i;
			int gud = 1;
			for(auto j:q){
				if(j <= i) gud = 0;
			}
			r += gud;
			q.push_front(i);
			if(q.size() > 5) q.pop_back();
		}
		cout<<r<<'\n';
	}

	return 0;
}
