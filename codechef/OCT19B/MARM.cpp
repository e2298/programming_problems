#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int arry[10000];
int lft [2][2][6] = {{{0,0,0,0,0,0},{0,1,1,1,1,0}},{{1,1,1,0,0,1},{1,0,0,1,1,1}}};
int rgt [2][2][6] = {{{0,0,0,0,0,0},{1,1,0,0,1,1}},{{0,0,1,1,1,1},{1,1,1,1,0,0}}};


int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n;
		ll k;
		cin>>n>>k;
		for(int i = 0; i < n; i++){
			cin>>arry[i]; 
		}
		for(int i = 0; i < (n/2); i++){
			int a = arry[i];
			int b = arry[n-i-1];
			arry[i] = 0;
			arry[n-i-1] = 0;
			ll times = k/n;
			times *=2ll;
			if((k%n) > i) times ++;
			if((k%n) > (n-i-1)) times ++;
			for(int bit = 0; bit < 30; bit++){
				arry[i] |= lft[!!(a&(1<<bit))][!!(b&(1<<bit))][times%6ll]<<bit;
				arry[n-i-1] |= rgt[!!(a&(1<<bit))][!!(b&(1<<bit))][times%6ll]<<bit;
			}
		}
		if(n&1 && k > (n/2)){
			arry[n/2] = 0;	
		}
		for(int i = 0; i < n; i++)cout<<arry[i]<<' ';
		cout<<'\n';
	}

	return 0;
}
