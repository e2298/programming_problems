#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll rows[100000];
ll cols[100000];

int main(void){
	int t;
	cin>>t;
	while(t--){
		memset(rows,0,sizeof(rows));
		memset(cols,0,sizeof(cols));
		ll n,m,q;
		cin>>n>>m>>q;
		while(q--){
			int x,y;
			cin>>x>>y;
			x--;
			y--;
			rows[x]++;
			cols[y]++;
		}
		ll r = 0, c = 0;
		for(int i = 0; i < n; i++){
			r += rows[i]&1ll;
		}
		for(int i = 0; i < m; i++){
			c += cols[i]&1ll;
		}
		ll res = 0;
		res += r *(m-c);
		res += (n-r)* c;
		cout<<res<<'\n';
	}
	return 0;
}
