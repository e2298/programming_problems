#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<vi> vvi;

int a[100000];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t = 0;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		vvi b(n);
		for(int i = 0; i < n; i++){
			cin>>a[i];
			a[i]--;
		}
		for(int i = 0; i < n; i++){
			b[a[a[i]]].push_back(a[i]);
		}			
		int r = 0;
		for(int i = 0; i < n; i++){
			sort(b[i].begin(), b[i].end());
			if(b[i].size()){
				int lst = b[i][0];
				for(int j = 1; j < b[i].size(); j++) if(b[i][j] != lst ){ r = 1; break;}
			}
			if(r) break;
		}
		if(r) cout <<"Truly Happy\n";
		else cout<<"Poor Chef\n";
	}

	return 0;
}
