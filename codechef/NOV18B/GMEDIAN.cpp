#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int in[1001];
ll comb[1010][1010];
ll mod = 1000000007;

ll expmod(ll b, ll e){
	if(!e) return 1;
	ll q = expmod(b,e/2); q = (q*q) % mod;
	return e%2? (b*q)%m:q;
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);

	for(int i = 1; i < 1005; i++){
		comb[i][1] = 1;
		for(int j = 2; j <= i; j++){
			comb[i][j] = (comb[i-1][j] + comb[i-1][j-1]) % mod;
		}
	}

	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		in[n] = -1;
		for(int i = 0; i < n; i++) cin>>in[i];
		sort(in, in+n);
		ll r = 0;
		for(int i = 1; i <= n; i++, i++){
			r += comb[n][i];
		}
		r %= mod;
		for(int i = 0; i < n; i++){
			int curr = in[n];
			int j = i;
			while(in[++j] == in[i]);
			i = j;
			i--;
			for(; j < n; j++){
				if(!(in[i] ^ in[j])){
					ll c = min(i, n-j-1);
					r += expmod(2, c+1);
				}
				while((j < n) && (in[j] == in[++j]));
				j--;
			}

		}

	}

	return 0;
}
