#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	deque<ii> q;
	multiset<int> h;
	int n, Q, k;
	cin>>n>>Q>>k;
	int lst = -1;
	int c1=0;
	for(int i = 0; i < n; i++){
		int in;
		cin>>in;
		if(in) c1++;
		if(lst == in){
			((q.end()-1)->first)++;
		}
		else{
			q.push_back(ii(1,in));
			lst = in;
		}
	}
	if(!c1){
		while(Q--){
			char in;
			cin>>in;
			if(in == '?')cout<<0<<'\n';
		}
		return 0;
	}
	for(auto i:q){
		if(i.second) h.insert(i.first);
	}

	while(Q--){
		char in;		
		cin>>in;
		if(in == '?'){
			auto i = h.lower_bound(k);
			if(i == h.end()) i--;
			if(*i > k) cout<<k<<'\n';
			else cout<<*i<<'\n';
				
		}
		else{
			if(q.back().second){
				h.erase(h.find(q.back().first));
				if(q.back().first == 1){
					q.pop_back();
				}
				else{
					((q.end() - 1)->first)--;
					h.insert(q.back().first);
				}
				if(q.front().second){
					h.erase(h.find(q.front().first));
					(q.begin() -> first)++;
					h.insert(q.front().first);
				}
				else{
					q.push_front(ii(1,1));
					h.insert(1);
				}
			}
			else{
				if(q.back().first == 1){
					q.pop_back();
				}
				else{
					((q.end()-1)->first)--;
				}
				if(q.front().second){
					q.push_front(ii(1,0));
				}
				else{
					(q.begin() -> first) ++;
				}
			}
			/*
			for(auto i:q) cout<<i.first<<' '<<i.second<<endl;
			for(auto i:h) cout<<i<<"**"<<endl;
			cout<<"*********************"<<endl;
			*/
		}
	}

	return 0;
}
