#include<bits/stdc++.h>
using namespace std;

int r1[30];
int r2[30];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	
	r1[1] = 1;
	r2[1] = 2;
	r1[2] = 1;
	r2[2] = 4;
	for(int i = 3 ; i < 30; i++){
		r2[i] = r2[i-1] <<1;
		r1[i] = r1[i-1] <<1;
		r1[i] += i&1? 1:-1;
	}
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		cout<<r1[n]<<' '<<r2[n]<<' ';
	}

	return 0;
}
