#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll f(ll n){
	if((n & 3 == 0)) return n>>1;
	else return (n>>1) + 1;
}

ll solve(ll n, ll k){
	if(!k) return n;
	if(n == 2ll) return 2ll;
	if((n == 3ll || n == 4ll) && (k == 1ll)) return 2;
	if((n == 3ll) || (n == 4ll)) return 1;
	if(k == 1) return f(n);
	return solve(f(n), k-1);
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);
	int t;
	cin>>t;
	while(t--){
		ll n,k;
		cin>>n>>k;
		double r = 1;
		printf("%lf\n", r/(double)solve(n,k));
		//cout<<r/(long double)solve(n,k)<<'\n';
	}
}
