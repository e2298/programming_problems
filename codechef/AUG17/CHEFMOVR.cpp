#include <bits/stdc++.h>
using namespace std;

int main(void){
	int t,n,d,p,x,y;
	long sum,avg,cnt;
	vector<int>a;
	cin>>t;
	while(t--){
		p = 1;
		cin>>n>>d;
		a = vector<int>(n);
		sum = 0;
		for(int i =0;i<n;i++){
			cin>>a[i];
			sum+=a[i];
		}
		if((sum%n)) {
			cout<<-1<<'\n'; 
			continue;
		}
		
		avg = sum/n;
		cnt = 0;

		for(int i = 0; i< d; i++){
			sum = 0;
			y = 0;

			for(int j = i; j<n; j+=d){
				sum+=a[j];
				y++;
			}
			if((sum/y)!=avg) {
				cout<<i;
				p = 0;
				break;
			}
			for(int j = i+d; j<n; j+=d){
				x = a[j-d];
				
				a[j]+= x-avg;
				cnt += abs(x-avg);
			}
		}

		if(p){
			cout<<cnt<<'\n';
		}
		else cout<<"-1\n";

	}

	return 0;
}
