#include <bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	int n;
	int tmp;
	vector<int> a;

	cin>>t;
	while(t--){
		cin>>n;
		a = vector<int>(n);

		while(n--){
			cin>>a[n];
		}
		n = a.size();

		int i;
		for(i = 0; (i<n) && (a[i]==a[--n]);i++);
		if(i < n){
			cout<<"no\n";
			continue;
		}
		i--;
		
		//cout<<a[i] <<' ';

		if(is_sorted(a.begin(),a.begin()+i) && (a[0]==1) && (a[i]==7) ) cout<<"yes\n";
		else cout<<"no\n";

	}

	return 0;
}
