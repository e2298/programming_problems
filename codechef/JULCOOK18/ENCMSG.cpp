#include<bits/stdc++.h>
using namespace std;

int main(void){
	string in;
	int t;
	cin>>t;
	char l[26];
	for(int i = 'a'; i <= 'z'; i++) l[i-'a'] = 'z'-i;
	while(t--){
		int n;
		cin>>n;
		cin>>in;
		for(int i = 1; i < in.size(); i+=2) {
			char t = in[i];
			in[i] = in[i-1];
			in[i-1] = t;
		}	
		for(int i = 0; i < in.size(); i++){
			in[i] = l[in[i]-'a']+'a';
		}
		cout<<in<<'\n';
	}
}
