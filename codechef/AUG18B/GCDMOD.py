def gcd(a, b):
    if b != 0:
        return gcd(b, a%b)
    else:
        return a

t = int(input())
while t != 0:
    t -= 1
    inp = input().split(" ")
    a = int(inp[0])
    b = int(inp[1])
    n = int(inp[2])

    c = abs(a-b);
    if c:
        a = pow(a, n, c)
        b = pow(b, n, c)
        r = gcd(a + b, c)
    else:
        a = pow(a, n, 1000000007)
        b = pow(b, n, 1000000007)
        r = a+b
    print(r % 1000000007)
