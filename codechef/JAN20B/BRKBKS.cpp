#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int s,a,b,c;
		cin>>s>>a>>b>>c;
		if(a+b+c <= s) cout<<"1\n";
		else if(a+b <= s || b+c <= s) cout<<"2\n";
		else cout<<"3\n";
	}	

	return 0;
}
