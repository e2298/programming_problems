#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;


int main(void){
	int t;
	cin>>t;
	while(t--){
		int n,m;
		cin>>n>>m;
		vvi a(n,vi(m));
		for(auto& i:a) for(auto& j:i) cin>>j;
		ll r = 0;
		for(int i = 0; i < n; i++){
			for(int j = 0; j < m; j++){
				for(int k = 0; i-k >= 0 && i+k <n && j-k >= 0 && j+k < m; k++){
					if(a[i-k][j] != a[i+k][j] || a[i][j-k] != a[i][j+k]) break;
					else r++;
				}
			}
		}
		cout<<r<<'\n';
	}

	return 0;
}
