#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

ll pow(ll b, ll e){
	if(!e) return 1ll;
	ll q = pow(b/2,e);
	q = q*q;
	return e&1?q*b:q;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n,a,b,c,d,e;
		cin>>n;
		cin>>a;
		ll m = pow(10,n)-1;
		ll s = a + 2ll + 2ll*m;
		cout<<s<<endl;
		cin>>b;
		cout<<m+1-b<<endl;
		cin>>d;
		cout<<m+1-d<<endl;
		int r;
		cin>>r;
		if(r == -1) exit(0);
	}	

	return 0;
}
