#include<bits/stdc++.h>
using namespace std;

struct fenwick{
	static const int sz = 100010;
	int t[sz];
	void adjust(int p, int v){
		for(int i = p; i < sz; i+=(i&-i)) t[i] += v;
	}
	int sum(int p){
		int s = 0;
		for(int i = p; i; i-=(i&-i)) s+=t[i];
		return s;
	}
	int sum(int a, int b){return sum(b) - sum(a-1);}
}incst, incen, decst, decen;

int a[100010];

int main(void){
	int n,q;
	cin>>n>>q;
	for(int i = 1; i <= n; i++){
		cin>>a[i];
	}
	int lst = a[1];
	int decing =  a[1] > a[2], incing = a[1] < a[2];
	int dec = decing;
	int inc = incing;
	incst.adjust(1,a[1] < a[2]);
	decst.adjust(1,a[1] > a[2]);
	incen.adjust(n,a[n-1] < a[n]);
	decen.adjust(n,a[n-1] > a[n]);
	for(int i = 2; i < n; i++){
		if(a[i] < lst){
			if(incing)
				incen.adjust(i-1,1);
			incing = 0;
			if(a[i] < a[i+1]){
				incst.adjust(i,1);
				inc++;
				incing = 1;
			}
		}
		if(a[i] > lst){
			if(decing)
				decen.adjust(i-1,1);
			decing = 0;
			if(a[i] > a[i+1]) {
				decst.adjust(i,1);
				dec++;
				decing = 1;
			}
		}
		lst = a[i];
	}
	while(q--){
		int l,r;
		cin>>l>>r;
		int d = dec - decen.sum(l) - decst.sum(r,n);
		int i = inc - incen.sum(l) - incst.sum(r,n);
		cout<<(d-i ? "NO\n":"YES\n");
	}

	return 0;
}
