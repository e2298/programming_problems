#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n;
		string s;
		char c;
		cin>>n>>s>>c;
		ll curr = 1;
		for(int i = 0; i < n; i++)s[i] = s[i] == c;
		int lst = s[0];
		ll res = (n* (n+1)) / 2;
		for(int i = 1; i < n; i++){
			if(lst != s[i]){
				if(lst != 1) res -= (curr*(curr+1)) / 2;
				curr = 1;
				lst = s[i];
			}
			else curr ++;
		}
		if(lst != 1) res -= (curr*(curr+1)) / 2;
		cout<<res<<'\n';
	}
	return 0;
}
