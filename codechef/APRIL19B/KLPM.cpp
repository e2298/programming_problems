#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
ll b = 331;
ll bpow[1005];
ll hs[1005];
map<ll,int> m;

int main(void){
	bpow[0] = 1;
	for(int i = 1; i < 1003; i++) bpow[i] = b*bpow[i-1];
	string s;
	cin>>s;
	ll n = s.size();
	ll h = 0;
	for(ll i = 0; i <= n; i++){
		h*= b;
		h += s[i];
		hs[i] = h;
		m[h]++;
		for(ll j = 0; j < i; j++){
			ll hh = hs[i] - (hs[j] * bpow[i-j]);	
			m[hh] ++;
		}
	}
	reverse(s.begin(), s.end());
	ll r = 0;
	h = 0;
	for(ll i = 0; i <= n; i++){
		h*= b;
		h += s[i];
		hs[i] = h;
		r += m[h];
		for(ll j = 0; j < i; j++){
			ll hh = hs[i] - (hs[j] * bpow[i-j]);	
			r += m[hh];
		}
	}
	cout<<r<<'\n';

}
