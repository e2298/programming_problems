#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;
typedef set<ii> sp;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n,m,k;
		sp plants;
		cin>>n>>m>>k;
		int r = 4*k;
		while(k--){
			int i,j;
			cin>>i>>j;
			if(plants.find(ii(i-1, j)) != plants.end()) r -= 2;
			if(plants.find(ii(i+1, j)) != plants.end()) r -= 2;
			if(plants.find(ii(i, j-1)) != plants.end()) r -= 2;
			if(plants.find(ii(i, j+1)) != plants.end()) r -= 2;
			plants.insert(ii(i,j));
		}
		cout<<r<<'\n';

	}

	return 0;
}

