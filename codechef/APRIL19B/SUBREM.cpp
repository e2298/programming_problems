#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;
typedef set<ii> sp;
typedef vector<int> vi;
typedef vector<vi> vvi;

ll x;
int cas;
ll v[100000];
int seen[100000];
ll dp[100000];
vvi graph;

ll sum(int i){
	dp[i] = v[i];
	seen[i] = cas;
	for(auto j:graph[i]){
		if(seen[j] != cas) dp[i] += sum(j);
	}
	return dp[i];
}

ll solve(int i){
	ll r = dp[i];
	ll b = v[i];
	seen[i] = cas;
	for(auto j:graph[i]){
		if(seen[j] != cas) b += solve(j);
	}

	return max(r,max(b, -x)); 
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	cas = 1;
	while(t--){
		int n;
		cin>>n>>x;
		graph = vvi(n);
		for(int i = 0;i<n;i++) cin>>v[i];			
		for(int i = 1; i < n; i++){
			int u,v;
			cin>>u>>v;
			u--;
			v--;
			graph[u].push_back(v);
			graph[v].push_back(u);
		}
		sum(0);
		cas++;

		cout<<max(-x, solve(0))<<'\n';

		cas++;
	}
	return 0;
}

