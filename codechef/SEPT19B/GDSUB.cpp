#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll MOD = 1000000007;
ll items[9000];
ll times[9000];
ll cnt;
ll dp[8001][8001];
int seen[8001][8001];

ll solve(ll start, ll zeroes){
	if(start >= cnt)
	       if(zeroes == 0ll) return 1ll;
	       else return 0ll;
	if(seen[start][zeroes]) return dp[start][zeroes];
	seen[start][zeroes] = 1;
	if(zeroes == 0ll) return dp[start][zeroes] = (items[start] * solve(start+1, 0)) % MOD;
	ll r = items[start];
	r *= solve(start+1, zeroes);
	r += solve(start+1, zeroes-1);
	return dp[start][zeroes] = r % MOD;
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	ll n, k;
	cin>>n>>k;
	for(int i = 0; i < n; i++){
		int a;
		cin>>a;
		times[a]++;
	}
	for(int j = 0; j <=8500; j++){
		if(times[j]){
			items[cnt++]= times[j];
		}
	}

	ll r = 0;
	for(ll zeroes = max(0ll,cnt-k); zeroes < cnt; zeroes++){
		r += solve(0, zeroes);
	}
	r++;
	r %= MOD;
	cout<<r<<'\n';
	return 0;
}
