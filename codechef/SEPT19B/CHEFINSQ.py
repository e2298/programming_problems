t = int(input())
for case in range(t):
    inp =[int(i) for i in  input().split()]
    n = inp[0]
    k = inp[1]
    inp =[int(i) for i in  input().split()]
    times = [0 for i in range(150)]
    for i in inp:
        times[i] += 1
    i = 0
    while i < 150:
        if(times[i] < k):
            k -= times[i]
        else:
            break
        i += 1
    r = 1
    for j in range(k+1, times[i]+1):
        r = r * j
    for j in range(2, times[i]-k+1):
        r = r // j

    print(r)

