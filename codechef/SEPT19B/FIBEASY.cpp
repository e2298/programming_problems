#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll fib[60];

int main(void){
	fib[1] = 1ll;
	for(int i = 2; i < 60; i++){
		fib[i] = fib[i-1] + fib[i-2];
		fib[i] %= 10ll;
	}
	int t = 0;
	cin>>t;
	while(t--){
		ll n;
		cin>>n;
		n |= n>>32;
		n |= n>>16;
		n |= n>>8;
		n |= n>>4;
		n |= n>>2;
		n |= n>>1;
		n +=1ll;
		n >>=1;
		n--;
		cout<<fib[n%60ll]<<'\n';
	}


}
