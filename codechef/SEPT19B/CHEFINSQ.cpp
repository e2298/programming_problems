#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int times[200];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		memset(times,0,sizeof(times));
		int n,k;
		cin>>n>>k;
		while(n--){
			int a;
			cin>>a;
			times[a]++;
		}
		int i;
		for(i = 0; i < 200; i++){
			if(times[i] < k) k -= times[i];
			else break;
		}
		ll r = 1;
		k = max(k, times[i]-k);
		for(ll j = k+1; j <= times[i]; j++) r *= j;
		for(ll j = 2; j <= times[i]-k; j++) r /= j;
		
		cout<<r<<'\n';
	}


	return 0;
}
