#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

#define MAXN (ll)1e7
ll criba[MAXN];

void cribar(){
	ll w[] = {4,2,4,2,4,6,2,6};
	for(ll p = 25; p<=MAXN; p+=10) criba[p]=5;
	for(ll p = 9; p<=MAXN; p+=6) criba[p]=3;
	for(ll p = 4; p<=MAXN; p+=2) criba[p]=2;
	for(ll p=7,cur=0;p*p<=MAXN;p+=w[cur++&7])if(!criba[p])
		for(ll j = p*p; j <= MAXN; j+=(p<<1))
			if(!criba[j]) criba[j]=p;
}

ll mulmod(ll a, ll b, ll c){
	ll x = 0, y = a%c;
	while(b > 0){
		if(b %2 ==1)x = (x+y)%c;
		y = (y*2)%c;
		b /= 2;
	}
	return x % c;
}

ll expmod(ll b, ll e, ll m){
	if(!e) return 1;
	ll q = expmod(b,e/2,m); q = mulmod(q,q,m);
	return e%2?mulmod(b,q,m) : q;
}

bool es_primo_prob(ll n, ll a){
	if(n == a) return true;
	ll s = 0, d = n-1;
	while(d%2 == 0) s++,d/=2;

	ll x = expmod(a,d,n);
	if((x==1)||(x+1 == n)) return true;

	for(ll i = 0; i < (s-1); i++){
		x = mulmod(x,x,n);
		if(x == 1) return false;
		if(x+1 == n) return true;
	}
	return false;
}

bool rabin(ll n){
	if(n==1) return false;
	const ll ar[] = {2,3,5,7,11,13,17,19,23};
	for(ll j = 0; j < 9; j++){
		if(!es_primo_prob(n,ar[j])) return false;
	}
	return true;
}

ll rho(ll n){
	if((n&1) == 0) return 2;
	ll x = 2,  y = 2, d = 1;
	ll c = rand() %n+1;
	while(d==1){
		x = (mulmod(x,x,n)+c)%n;
		y = (mulmod(y,y,n)+c)%n;
		y = (mulmod(y,y,n)+c)%n;
		if(x - y >= 0) d = __gcd(x-y,n);
		else d = __gcd(y-x,n);
	}
	return d==n?rho(n):d;
}

void factRho(ll n, map<ll,ll> &prim){
	if(n == 1) return;
	if(rabin(n)){
		prim[n]++;
		return;
	}
	ll factor = rho(n);
	factRho(factor,prim);
	factRho(n/factor,prim);
	
}

void fact(ll n, map<ll,ll> &prim){
	if(n < MAXN){
		while(n != 1 && criba[n]){
			prim[criba[n]]++;
			n/=criba[n];
		}	
		if(n != 1) prim[n]++;
	}
	else factRho(n, prim);
}

int main(){
	ll t;
	srand(time(NULL));
	cin>>t;
	string s;
	while(t--){
		cout<<"1 31627"<<endl;
		ll res;
		cin>>res;
		if(res == 0){
			cout<<"2 31627"<<endl;
			cin>>s;
			if(s=="no")exit(0);
			continue;
		}
		else res = 31627*31627 - res;
		map<ll,ll> factors;
		fact(res,factors);
		for(auto i:factors){
			cout<<i.first<<' '<<i.second<<'\n';
		}
		
	}	


	return 0;
}
