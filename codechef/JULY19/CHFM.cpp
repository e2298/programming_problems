#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int a[100000];

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n;
		cin>>n;
		ll tot = 0;
		for(int i = 0; i < n; i++){
			cin>>a[i];
			tot += a[i];
		}	
		if(tot % n == 0){
			tot /= n;
			int i = 0;
			for(; i < n; i++){
				if(a[i] == tot){
					break;
				}
			}
			if(i == n) cout<<"Impossible\n";
			else cout << i+1<<'\n';

		}
		else cout<<"Impossible\n";
	}

	return 0;
}
