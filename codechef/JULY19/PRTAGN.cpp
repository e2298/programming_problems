#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef set<int> si;

int main(void){
	int t;
	cin>>t;
	while(t--){
		si a[2];
		int n;
		cin>>n;
		while(n--){
			int i;
			cin>>i;
			if(!a[__builtin_popcount(i)&1].count(i)){
				si b[2];
				for(auto j:a[0]){
					int tt = i ^ j;
					b[__builtin_popcount(tt)&1].insert(tt);
				}
				for(auto j:a[1]){
					int tt = i ^ j;
					b[__builtin_popcount(tt)&1].insert(tt);
				}

				a[__builtin_popcount(i)&1].insert(i);
				a[0].insert(b[0].begin(), b[0].end());
				a[1].insert(b[1].begin(), b[1].end());
			}
			cout<<a[0].size()<<' '<<a[1].size()<<'\n';
		}


	}

	return 0;
}
