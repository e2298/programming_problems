#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	scanf("%d\n", &t);
	while(t--){
		int n;
		scanf("%d\n", &n);
		ll k = 0;
		char c = 0;
		while(scanf("%c",&c) && (c != '\n')){
			k *= 10;
			k += c - '0';
			k %= n;
		}
		int r = min(k, n-k);
		if(n == 2) cout<<r<<'\n';
		else {
			int rest = max(k,n-k);
			if(rest >= r+1) cout<<2*r<<'\n';
			else cout<<2*r-1<<'\n';
		}

	}

	return 0;
}
