#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

int gr[1000][1000];


int main(void){
	ios::sync_with_stdio(0);
    	cin.tie(0);
	int t, n,m;
	cin>>t;
	while(t--){
		cin>>n>>m;
		for(int i = 0; i < n; i++) for(int j = 0; j < m; j++){ char in; cin>>in; gr[i][j] = (in == '#'? 0:1);}
		int minn = 1000000;
		int can = 1;
		if(gr[0][0]) can *= gr[1][1] * gr[1][0] * gr[0][1];
		for(int i = 1; i < n; i++) if(gr[i][0]) can *= gr[i-1][0] * gr[i-1][1] * gr[i][1];
		for(int j = 1; j < m; j++) if(gr[0][j]) can *= gr[0][j-1] * gr[1][j-1] * gr[1][j];
		for(int i = 1; i < n; i++){
			for(int j = 1; j < m; j++){ 
				int tt = gr[i-1][j] * gr[i-1][j-1] * gr[i][j-1];
				tt += 
				if(gr[i][j]) can *= !!tt;
			}
		}
		cout<<can<<'\n';
		if(can) cout<<"YES\n";
		else cout<<"NO\n";
	}

}
