#include<bits/stdc++.h>
using namespace std;

typedef map<int,int> mi;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		mi times;
		while(n--){int in; cin>>in; times[in]++;}
		int r = 0;
		for(auto i:times){
			for(int j = 1; j <= i.second; j++){
				auto ot = times.find(j);
				if(ot != times.end() && ot -> second >= i.first) r++;
			}
		}
		cout<<r<<'\n';
	}
	return 0;
}
