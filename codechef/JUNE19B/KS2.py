base = [0,19,28,37,46,55,64,73,82,91]
def solve(n):
    if(n < 10):
        return base[n]
    mod = n%10
    quo = n // 10
    r = solve(quo)
    r = (r//10) * 100 + r%10
    while(mod):
        if(r % 10):
            r += 9
        else:
            r += 19
        mod -= 1

    return r


t = int(input())
for i in range (t):
    n = int(input())
    print(solve(n))
