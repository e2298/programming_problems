#include<bits/stdc++.h>
using namespace std;

int a[100000];

int gcd(int a, int b){
	return a? gcd(b%a, a) : b;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		int mx = 0;
		for(int i = 0; i < n; i++){
			cin>>a[i];
			mx = max(mx, a[i]);
		}
		int g = 0;
		for(int i = 0; i < n; i++){
			if(a[i] != mx) g = gcd(a[i], g); 	
		}
		if(g){
			cout<<mx+g<<'\n';
		}
		else{
			cout<<2*mx<<'\n';
		}

	}
	return 0;
}
