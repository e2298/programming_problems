#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n,m,r=0;
		cin>>n>>m;
		int in;
		while(n--){
			cin>>in;
			if(!(in%m)){
				r++;
			}
		}
		cout<<(1<<r)-1<<'\n';
	}

	return 0;
}
