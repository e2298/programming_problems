#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		int pos=0,neg=0,z=0;
		while(n--){
			int i;
			cin>>i;
			if( i < 0) neg++;
			if(i == 0) z++;
			if(i > 0) pos++;
		}
		cout<<max(pos,neg)+z<<' ';
		if(z){
			cout<<1<<'\n';
		}
		else{
			if(pos && neg)
				cout<<min(pos,neg)<<'\n';
			else cout<<(pos|neg)<<'\n';
		}
	}

	return 0;
}
