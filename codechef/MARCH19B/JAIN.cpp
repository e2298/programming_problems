#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int q[100];
ll seen[100];
int num[200];
int ins[100000];

int main(void){
	num['a'] = 1;
	num['e'] = 2;
	num['i'] = 4;
	num['o'] = 8;
	num['u'] = 16;

	int t;
	cin>>t;
	ll curr =1;
	while(t--){
		memset(q, 0,sizeof(q));
		int n;
		cin>>n;
		for(int i = 0; i < n; i++){
			string in;
			int m =0;
			cin>>in;
			for(auto j:in) m |= num[j];
			q[m]++;
			ins[i] = m;
		}
		ll r = 0;
		for(int i = 0; i < n; i++){
			int con = ((1<<5)-1) ^ ins[i];
			for(int j = 0; j <= (1<<5)-1; j++){
				if((con & j) == con && seen[j] != curr){
					seen[j] = curr;
					r += q[j];
				}
			}
			curr++;
		}
		cout<<r/2<<'\n';
	}	

	return 0;
}
