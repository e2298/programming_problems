#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

int main(void){
	int t;
	cin>>t;
	while(t--){
		string n;
		int d;
		cin>>n>>d;
		d += '0';
		int curr = 0;
		char m;
		while(curr < n.size()){
			m = d;
			for(int i = curr; i < n.size(); i++) m = min(m, n[i]);
			if(m == d){
				for(int i = curr; i < n.size(); i++) n[i] = d;
				break;
			}
			else{
				while(n[curr] != m){
					for(int i = curr; i < n.size() -1; i++) n[i] = n[i+1];
					n[n.size()-1] = d;
				}
			}
			curr++;
		}
		cout<<n<<'\n';
	}

	return 0;
}
