#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll times[2005][2005];
ll les[2005][2005];
ll a[2005];

ll get(ll arry[2005][2005], ll r, ll l, ll q){
	return arry[q][r] - (l > 0? arry[q][l-1] : 0);
}

int main(void){
	ll t;
	cin>>t;
	while(t--){
		memset(times, 0, sizeof(times));
		memset(les, 0, sizeof(les));
		ll n,k;
		cin>>n>>k;
		cin>>a[0];
		times[a[0]][0]++;
		ll r = 0;
		for(ll i = a[0]; i <= 2000; i++) les[i][0]++;
		for(ll i = 1; i < n; i++){
			for(ll j = 0; j <= 2000; j++) times[j][i] = times[j][i-1];
			for(ll j = 0; j <= 2000; j++) les[j][i] = les[j][i-1];
			cin>>a[i];
			times[a[i]][i]++;
			for(ll j = a[i]; j < 2000; j++) les[j][i]++;
		}
		for(ll i = 0; i < n; i++) for(ll j = i; j < n; j++){
			ll sz = j - i + 1;
			ll m = (k + sz - 1) / (sz);
			ll d = (m * sz) % k;
			ll pos = j - i - d/m;
			ll lo = 0, up = 2000;
			while(lo < up){
				ll curr = lo + up;
				curr /= 2;
				if(get(les, j, i, curr) <= pos) lo = curr +1;
				else up = curr;
			}
			ll cant = get(times, j, i, lo);
			if(get(times, j, i, cant)) r++; 
			
		}
		cout<<r<<'\n';

	}

	return 0;
}
