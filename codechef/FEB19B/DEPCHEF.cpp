#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int a[100];

int main(void){
	int t;
	cin>>t;
	while(t--){
		int r = -1;
		int n;
		cin>>n;
		for(int i = 0; i < n; i++){
			cin>>a[i];
		}
		int d;
		cin>>d;
		if(d > a[n-1] + a[0]) r = d;

		for(int i = 1; i < n-1; i++){
			cin>>d;
			if(d > a[i-1] + a[i+1]) r = max(r,d);
		}
		cin>>d;
		if(d > a[n-2] + a[0]) r = max(r,d);
		cout<<r<<'\n';

	}
	return 0;
}
