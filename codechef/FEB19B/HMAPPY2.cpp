#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n,a,b,k;
		cin>>n>>a>>b>>k;
		if(a == b){
			if(k == 0) cout<<"Win\n";
			else cout<<"Lose\n";
			continue;
		}
		if(a > b){
			a ^= b;
			b ^= a;
			a ^= b;
		}
		if(b % a == 0){
			if((n/a - n/b) >= k) cout<<"Win\n";
			else cout<<"Lose\n";
			continue;
		}

		if((n/a + n/b - 2*n/(a*b)) >= k) cout<<"Win\n";
		else cout<<"Lose\n";
	}
	return 0;
}
