#include<bits/stdc++.h>
using namespace std;


int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		string s;
		cin>>n;
		int tot = ~0;
		for(int i = 0; i < n; i++){
			int curr = 0;
			cin>>s;
			for(auto j:s){
				curr |= 1<<(j-'a');
			}
			tot &= curr;
		}
		cout<<__builtin_popcount(tot)<<'\n';	
	}

	return 0;
}
