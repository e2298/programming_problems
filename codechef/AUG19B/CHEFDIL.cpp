#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		string s;
		cin>>s;
		int r = 0;
		for(auto i:s) r += i=='1';
		if(r&1)cout<<"WIN\n";
		else cout<<"LOSE\n";
	}

	return 0;
}
