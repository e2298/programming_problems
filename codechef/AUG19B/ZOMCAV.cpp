#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		vi r(n,0);
		vi l(n,0);
		vi c(n,0);
		vi z(n,0);
		for(int i = 0; i < n; i++){
			int tt;
			cin>>tt;
			r[min(n-1,i+tt)]++;
			l[max(0,i-tt)]++;
		}
		for(auto &i:z) cin>>i;
		int curr = 0;
		for(int i = 0; i < n; i++){
			curr += l[i];
			c[i] = curr;
			curr -= r[i];
		}
		sort(c.begin(),c.end());
		sort(z.begin(),z.end());
		int res = 1;
		for(int i = 0; i < n && res; i++){
			res = z[i]==c[i];
		}
		if(res) cout<<"YES\n";
		else cout<<"NO\n";
	
	}

	return 0;
}
