#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n,k;
		cin>>n>>k;
		if((n/k) % k == 0) cout<<"NO\n";
		else cout<<"YES\n";
	}
	return 0;
}

