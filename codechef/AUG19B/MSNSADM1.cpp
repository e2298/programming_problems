#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int a[150];

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		for(int i = 0; i < n; i++){
			 cin>>a[i];
		}	
		int tm, mx=0;
		for(int i = 0; i < n; i++){
			cin>>tm;
			mx = max(mx, (a[i]<<1) - tm);
		}
		cout<<mx*10<<'\n';
	}

	return 0;
}
