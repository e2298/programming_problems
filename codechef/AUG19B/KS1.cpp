#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> ii;
typedef pair<ll,ii> iii;
typedef map<int,iii> m;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		ll res = 0;
		m times;
		int curr = 0;
		times[0].first = -1;
		times[0].second.first = 0;
		times[0].second.second = 0;
		for(int i = 0; i < n; i++){
			int in;
			cin>>in;
			curr ^= in;
			ll ne;
			if(times.find(curr) != times.end()){
				ne = times[curr].second.first + times[curr].second.second * (i - times[curr].first) + (i - times[curr].first -1);
				times[curr].second.second++;
			}
			else {
				ne = 0;
				times[curr].second.second = 0;
			}
			res += ne;

			times[curr].second.first = ne;
			times[curr].first = i;
		}
		cout<<res<<'\n';
	}

	return 0;
}
