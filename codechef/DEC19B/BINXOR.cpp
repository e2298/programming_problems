#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
ll MOD = 1000000007;
ll fact[100005];
ll inv[100005];

ll pow(ll b, ll e){
	if(!e) return 1ll;
	ll q = pow(b, e/2); q = (q*q)%MOD;
	return e%2?(b*q)%MOD:q;	
}

ll perm(ll a, ll b){
	ll r = fact[a];
	r *= inv[b];
	r %= MOD;
	r *= inv[a-b];
	return r% MOD;
}

int main(void){
	fact[0] = 1ll;
	inv[0] = 1ll;
	for(int i = 1; i < 100005; i++){
		fact[i] = i*fact[i-1];
		fact[i] %= MOD;
		inv[i] = pow(fact[i], MOD-2);
	}
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		int a=0,b = 0;
		string in;
		cin>>in;
		for(auto i: in){
			if(i == '1') a++;
		}
		cin>>in;
		for(auto i:in){
			if(i == '1') b++;
		}
		if(b > a) {
			a = n-a;
			b = n-b;
		}
		int ones = a-b;
		int leftover = min(a-ones, n-a);
		ll r = perm(n, ones);
		while(leftover--){
			ones += 2;
			r += perm(n,ones);
			r %= MOD;
		}
		cout<<r<<'\n';
	}

	return 0;
}
