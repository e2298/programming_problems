#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		ll z=0,t=0;
		while(n--){
			int i;
			cin>>i;
			switch(i){
				case 0: z++; break;
				case 2: t++; break;
				default: break;
			}
		}
		ll r = 0;
		r += z*(z-1ll)/2ll;
		r += t*(t-1ll)/2ll;
		cout<<r<<'\n';
	}

	return 0;
}
