#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;

ii r[100000];
ii s[100000];
ii e[100000];
ii ev[200000];

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		int a,b;
		for(int i = 0; i < n; i++){
			cin>>a>>b;
			r[i] = ii(a,b);
			s[i] = ii(a,i);
			e[i] = ii(b,i);
			ev[2*i] = ii(a,0);
			ev[2*i+1] = ii(b,1);
		}
		sort(r,r+n);
		int mxs = 0;
		int i = 0;
		while(r[i].first == r[0].first) mxs = max(mxs, r[i++].second);
		int mne = 2e9;
		i = n-1;
		while(r[i].second == r[n-1].second) mne = min(mne, r[i--].first);
		if(mne <= mxs ) {
			cout<<"-1\n";
			continue;
		}

		sort(ev,ev+2*n);
		i = 0;
		int curr = 0;
		int mn = 2e9;
		while(i < 2*n){
			a = ev[i].first;
			while(ev[i].first == a && ev[i].second == 0){
				curr++;
				i++;
			}
			while(ev[i].first == a && ev[i].second == 1 && i < 2*n){
				curr--;
				i++;
			}
			if(i < 2*n) mn = min(mn, curr);
		}
		if(mn <= 0) cout<<"0\n";
		else cout<<mn<<'\n';
	}

	return 0;
}
