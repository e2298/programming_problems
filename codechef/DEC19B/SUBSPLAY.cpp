#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		string s;
		cin>>n>>s;
		vi last(256, -1000000);
		vi d(256, 1<<30);
		for(int j = 0; j < s.size(); j++){
			char i = s[j];
			d[i] = min(d[i], j-last[i]);
		       	last[i] = j;	
		}
		int r = 1<<30;
		for(auto i:d) r = min(r, i);
		if(r > 100010) r = n;
		cout<<n-r<<'\n';
	}

	return 0;
}
