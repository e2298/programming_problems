#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		string a,b;
		cin>>a>>b;
		reverse(a.begin(), a.end());
		reverse(b.begin(), b.end());
		for(auto& i:a){
			i -= '0';
		}
		for(auto& i:b){
			i -= '0';
		}
		int mx = 1;
		if(b.size() == 1 && b[0] == 0){
			cout<<"0\n";
			continue;
		}
		while(b.size() < a.size()) b.push_back('\0');
		while(a.size() < b.size()) a.push_back('\0');
		for(int i = 0; i < b.size(); i++){
			if(i >= a.size()){
				break;
			}
			if(a[i] & b[i]){
				int cycles = 1;
				for(int j = i+1;; j++){
					cycles++;
					if(j == b.size()) {
						break;
					}
					if(j == a.size()){
						break;
					}
					if(!(a[j] ^ b[j])){
						break;
					}
				}
				mx = max(mx, cycles);
			}
		}
		cout<<mx<<'\n';
	}

	return 0;
}
