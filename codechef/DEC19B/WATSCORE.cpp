#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		vector<int> scores(12,0);
		int n;
		cin>>n;
		while(n--){
			int p,s;
			cin>>p>>s;
			scores[p] = max(scores[p],s);
		}
		int r = 0;
		for(int i = 1; i <=8; i++) r += scores[i];
		cout<<r<<'\n';
	}

	return 0;
}
