#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int main(void){
	int t;
	vi in(4);
	cin>>t;
	while(t--){
		for(auto& i:in) cin>>i;
		sort(in.begin(),in.end());

		if((in[0] == in[1]) && (in[2] == in[3])){
			cout<<"YES\n";
		}
		else cout<<"NO\n";
	}

	return 0;
}
