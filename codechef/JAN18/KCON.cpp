#include<bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;

int merge(string a, string b){
	int rn = 1, al = a.size(), bl = b.size();
	string r(al+bl,'\0');
	r[0] = a[0];

	int i,ai,bi;
	for(i = 1, ai = 1, bi = 0; (ai<al) && (bi<bl); i++){
		if(a[ai] == r[i-1]){
			r[i] = a[ai];
			ai++;
		}
		else{
			r[i] = b[bi];
			bi++;
		}	
	}

	while(ai < al){
		r[i] = a[ai];
		ai++;
		i++;
	}
	while(bi < bl){
		r[i] = b[bi];
		bi++;
		i++;
	}

	for(i = 1; i < (al+bl); i++){
		if(r[i] != r[i-1]) rn++;
	}

	return rn;
}

int main(void){
	int t, al, bl;
	char tr;
	string a,b,r;
	
	cin>>t;
	while(t--){
		cin>>al>>bl;
		a = string(al,'\0');
		b = string(bl,'\0');

		for(auto& i:a) cin>>i;
		for(auto& i:b) cin>>i;

		int rn = min(merge(a,b),merge(b,a));

		cout<<rn<<'\n';
	}

	return 0;
}
