#include<bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;

int main(void){
	long t,x,n;
	cin>>t;
	while(t--){
		cin>>x>>n;
		long sum = (n*(n+1))/2;
		sum -= x;
		if(sum & 1){
			cout<<"impossible\n";
			continue;
		}

		sum /= 2;

		vi taken(n+1);
		taken[x]=2;

		for(int i = n; i; i--){
			if(i != x){
				if((sum-i)>=0){
					taken[i] = 1;
					sum -= i;
				}
			}
		}

		if(sum) cout<<"impossible";
		else for(int i = 1; i<=n; i++) cout<<taken[i];
		cout<<'\n';
	
	}
	return 0;
}
