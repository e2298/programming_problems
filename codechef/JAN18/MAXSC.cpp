#include<bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;

int main(void){
	long t;
	cin>>t;
	while(t--){
		long n;
		cin>>n;
		vvi nums(n,vi(n));
		for(auto& i:nums){
		       	for(auto& j:i)cin>>j;
			sort(i.begin(), i.end());
		}
		long c=nums[n-1][n-1],r=c,i=n-1;
		int fail = 0;
		while(i>0){
			i--;	
			auto b = lower_bound(nums[i].begin(),nums[i].end(),c);
			if(b == nums[i].begin()){
				fail = 1;
				break;
			}
			else{
				c = *(b-1);
				r +=c;
			}

		}

		if(fail) cout<<"-1\n";
		else cout<<r<<'\n';
	}
	return 0;
}
