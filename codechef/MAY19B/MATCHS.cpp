#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int solve(ll a, ll b){
	if(a %b == 0) return 1;
	if(a/b >= 2) return 1;
	return !solve(b, a-b);
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll a,b;
		cin>>a>>b;
		if(a < b) {
			a ^=b;
			b ^=a;
			a ^=b;
		}
		cout<<(solve(a,b)?"Ari\n":"Rich\n");
	}
	return 0;
}
