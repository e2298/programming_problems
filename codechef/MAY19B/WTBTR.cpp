#include<bits/stdc++.h>
using namespace std;

double p[10000][2];

int main(void){
	int t;
	cin>>t;
	while(t--){
		double m = 10000000000.0;
		int n;
		cin>>n;
		for(int i = 0; i < n; i++){
			cin>>p[i][0]>>p[i][1];
			for(int j = 0; j < i; j++){
				double d = abs(abs(p[i][0]-p[j][0]) - abs(p[i][1] - p[j][1]))/2; 
				m = min(d,m);
			}
		}
		cout<<m<<'\n';
	};

	return 0;
}
