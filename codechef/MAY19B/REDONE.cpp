#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll r[1000001];

int main(void){
	r[1] = 1;
	for(int i = 2; i <= 1000000; i++){
		r[i] = (i + r[i-1] + i*r[i-1]) % 1000000007;
	}
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		cout<<r[n]<<'\n';
	}
	return 0;
}
