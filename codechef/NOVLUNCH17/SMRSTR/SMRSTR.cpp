#include <bits/stdc++.h>

using namespace std;

int main(void){
	int t, n , q;
	long r;
	long tmp;
	
	cin>>t;

	while(t--){
		cin>>n>>q;
		r = 1;
		while(n--){
			cin>>tmp;
			r *= tmp;
		}

		while(q--){
			cin>>tmp;
			cout<<(tmp/r)<<' ';
		}

		cout<<'\n';
	}
}
