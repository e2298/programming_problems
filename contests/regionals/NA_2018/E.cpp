#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> ii;
ll dp[300][300];
ll s[300];
ii c[300];
ll n,m,k,r;

ll solve(ll sp, ll l){
	if(dp[sp][l]) return dp[sp][l];
	if(sp == n-1){
		if(k*(l-1) <= s[sp]){
			return dp[sp][l] = (k+r)*(l-1) + s[sp] - k*(l-1);
		}	
		else return dp[sp][l] = 1ll<<60;
	}
	ll res = 1ll<<62;
	for(ll la = 0;( la * (k) <= s[sp]) &&( la + l <= m); la++){
		res = min(res, s[sp] - k*la + (k+r) * la + c[sp].first + c[sp].second * (la + l) + solve(sp+1, l+la));
	}
	for(ll la = 1;( la * (k) <= s[sp]) &&( l-la >0); la++){
		res = min(res, s[sp] - k*la + (k+r) * la + c[sp].first + c[sp].second * (l-la) + solve(sp+1, l-la));
	}
	return dp[sp][l] = res;
}

int main(void){
	cin>>n>>m>>k>>r;
	for(int i = 0; i < n; i++){
		cin>>s[i];
	}
	for(int i = 0; i < n-1; i++){
		cin>>c[i].first>>c[i].second;
	}
	cout<<solve(0,1)<<'\n';

	return 0;
}
