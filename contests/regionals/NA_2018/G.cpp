#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int h[200010];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	ll n,c;
	cin>>n>>c;
	for(int i =0 ; i < c; i++){
		int t;
		cin>>t;
		h[t]++;
	}
	ll lst = n+1;
	ll r = 0;
	for(ll i = n; i >0; i--){
		if(h[i]) lst = i;
		r += n-lst + 1;	
	}
	cout<<r<<'\n';

	return 0;
}
