#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> ii;
typedef pair<ll, ii> iii;
typedef vector<iii> v;

v g[345];


int main(void){
	int msk[200];
	msk['R'] = 1;
	msk['O'] = 1<<1;
	msk['Y'] = 1<<2;
	msk['G'] = 1<<3;
	msk['B'] = 1<<4;
	msk['I'] = 1<<5;
	msk['V'] = 1<<6;
	int n,m;
	cin>>n>>m;
	while(m--){
		ll a,b,d;
		char c;
		cin>>a>>b>>d>>c;
		a--;
		b--;
		g[a].push_back(iii(b,ii(d,msk[c])));
		g[b].push_back(iii(a,ii(d,msk[c])));		
	}
	priority_queue<iii, vector<iii>, greater<iii> > Q;
	vector<vector<ll> >dist(n, vector<ll>(1<<7, 1ll<<60));
	dist[0][0] = 0;
	Q.push(iii(0, ii(0,0)));
	while(Q.size()){
		iii p = Q.top(); Q.pop();
		if(p.second == ii(0, (1<<7)-1)) break;
		ll cs = p.first;
		ll nod = p.second.first;
		ll ms = p.second.second;
		for(auto it:g[nod]){
			ll nodi = it.first;
			ll di = it.second.first;
			ll mi = it.second.second;
			if(cs + di < dist[nodi][ms | mi]){
				dist[nodi][ms|mi] = cs + di;
				Q.push(iii(dist[nodi][ms|mi], ii(nodi, ms|mi)));
			}
		}
	}
	cout<<dist[0][(1<<7)-1]<<'\n';
	

	return 0;
}
