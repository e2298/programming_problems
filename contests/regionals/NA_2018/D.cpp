#include<bits/stdc++.h>
using namespace std;


int main(void){
	int n;
	cin>>n;
	map<int,int> a;
	for(int i = 0; i < n; i++){
		int t;
		cin>>t;
		a[t]++;
	}
	int cit = 0;
	int h = 0;
	int tot = a.size();
	for(auto i = --(a.end()); tot; i--,tot--){
		cit += (*i).second;
		h = max(h, min(cit, (*i).first));
	}
	cout<<h<<'\n';

	return 0;
}
