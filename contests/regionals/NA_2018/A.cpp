#include<bits/stdc++.h>
using namespace std;

int main(void){
	int n;
	double r = 0.0;
	cin>>n;
	while(n--){
		double a,b;
		cin>>a>>b;
		r += a*b;
	}
	printf("%.4lf", r);
	return 0;
}
