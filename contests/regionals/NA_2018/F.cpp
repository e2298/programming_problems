#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> ii;
typedef complex<ll> cl;

cl pos[1010];
cl dir[1010];
cl w1[1010];
cl w2[1010];

ll dp(cl a,cl b){
	return a.real() * b.real() + a.imag() * b.imag();
}

int main(void){
	int w,p;
	cin>>w>>p;
	ll x, y;
	cin>>x>>y;
	cl t(x,y);
	for(int i = 0; i < w; i++){
		ll a,b,c,d;
		cin>>a>>b>>c>>d;
		w1[i] = cl(a,b);
		w2[i] = cl(c,d);
	}
	for(int i = 0; i < p; i++){
		ll a,b,c,d;
		cin>>a>>b>>c>>d;
		pos[i] = cl(a,b);
		dir[i] = cl(c,d);
	}

	for(int i = 0; i < p; i++){
		cl aa = dir[i] - pos[i];
		cl bb = t - pos[i];
		if((aa.real() * bb.real() + aa.imag()* bb.imag()) < 0ll) {
			cout<<"not visible\n";
			continue;
		}
		ll t1,t2,p1,p2,d1,d2;
		t1 = t.real();
		t2 = t.imag();
		p1 = pos[i].real();
		p2 = pos[i].imag();
		d1 = dir[i].real();
		d2 = dir[i].imag();
		if(((t1-p1)*(t1-p1) + (t2-p2)*(t2-p2)) > ((p1-d1)*(p1-d1) + (p2-d2)*(p2-d2))){
			cout<<"not visible\n";
			continue;
		}
		int can = 1;
		for(int j = 0; j < w; j++){
			cl a,b,c,d;
			a =pos[i];
			b = t;
			c = w1[j];
			d = w2[j];
			cl e,f;
			e = d-c;
			f = b-a;
			ll dd = e.real() * f.imag() - e.imag() * f.real();
			if(!dd){
				if(e.real() * (b-c).imag() - e.imag() * (b-c).real() == 0ll){
					if(dp(c-a,c-a) < dp(a-b,a-b) && dp(c-b,c-b) < dp(a-b,a-b)){
						can = 0;
					}
					if(dp(d-a,d-a) < dp(a-b,a-b) && dp(d-b,d-b) < dp(a-b,a-b)){
						can = 0;
					}
				}
				if(can) continue;
				else break;
			}
			f = c-a;
			ll tt = e.real() * f.imag() - e.imag() * f.real();
			e = b-a;
			f = c-a;
			ll ss = e.real() * f.imag() - e.imag() * f.real();
			if(tt > 0ll && tt < dd && ss > 0ll && ss < dd){
				can = 0;
				break;
			}

		}
		if(!can){
			cout<<"not visible\n";
			continue;
		}
		for(int j = 0; j < p; j++){
			if(j == i) continue;
			cl a = t - pos[i];
			cl b = pos[j] - pos[i];
			if((a.real()*b.imag() - b.real()*a.imag() == 0ll) && (a.real()*b.real() + a.imag()*b.imag() > 0ll)){
				if((a.real()*a.real() + a.imag()*a.imag())  > ( b.real()*b.real() + b.imag()*b.imag())){
					can = 0;
					break;
				}
			}
		}
		if(!can){
			cout<<"not visible\n";
			continue;
		}
		cout<<"visible\n";
	}	

	return 0;
}
