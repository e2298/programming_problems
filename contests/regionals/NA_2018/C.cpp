#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll gcd(ll a, ll b){
	if(!a) return b;
	return gcd(b%a, a);
}

int main(void){
	ll x,y;
	ll x1,y1,x2,y2;
	cin>>x>>y>>x1>>y1>>x2>>y2;

	ll d = gcd(x,y);
	if(d == 1) {
		cout<<"Yes\n";
		return 0;
	}
	if(x/d < x1 || x/d > x2 || y/d < y1 || y/d > y2){
		cout<<"No\n"<<x/d<<' '<<y/d<<'\n';
		return 0;
	}
	if(x-x/d < x1 || x-x/d > x2 || y-y/d < y1 || y-y/d > y2){
		ll a = x/d;
		ll b = y/d;
		ll ka = (x2)/a;
		ll kb = (y2)/b;
		ka++;
		kb++;
		
		cout<<"No\n"<<a*min(ka,kb)<<' '<<b*min(ka,kb)<<'\n';
		return 0;
	}
	cout<<"Yes\n";

	return 0;
}
