#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())
typedef long long ll;
typedef pair<int,int>ii;

struct fenwick{
	static const int sz = 100010;
	int t[sz];
	void adjust(int p, int v){
		for(int i = p; i < sz; i+=(i&-i)) t[i] +=v;
	}
	int sum(int p){
		int s = 0;
		for(int i =p; i; i-=(i&-i)) s+=t[i];
		return s;
	}
	int sum(int a, int b){return sum(b)-sum(a-1);}
	int getind(int x){
		int summ = 0;
		int pos = 0;
		for(int i = 17; i>=0; i--){
			if(pos + (i<<i) < sz && summ + t[pos+(1<<i)]<x){
				summ += t[pos+(1<<i)];
				pos += (1<<i);
			}
		}
		return pos+1;
	}
}fen;
ll ar[100010];
ii srt[100010];
bool used[100010];

int main(void){
	int n,k;
	while(cin>>n>>k){
		memset(fen.t,0,(n+5)*sizeof(ll));
		memset(used,0,(n+1));
		for(int i = 1; i <= n; i++){
			cin>>ar[i];
			srt[i-1] = ii(ar[i],i);
		}
		int uses = n/(k+1);
		for(int i = k+1; i <= n; i+= k+1){
			fen.adjust(i,1);
		}
		sort(srt,srt+n,greater<ii>());
		for(int i = 0; i < n && uses; i++){
			int cur = srt[i].second;
			int s = fen.sum(cur);
			if(s){
				int use = fen.getind(s);
				used[cur] = 1;
				fen.adjust(use,-1);
				uses--;	
			}
		}
		ll r = 0;
		for(int i = 1; i <= n; i++){
			if(!used[i]) r+= ar[i];
		}
		cout<<r<<'\n';
	}
	return 0;
}
