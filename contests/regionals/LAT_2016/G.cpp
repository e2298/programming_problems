#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())
vector<int> T;
vector<int> P;
int last[300];
int b[500010];
void kmppre(){
    int n = (int)P.size();
    for (int i = 1; i < n; i++) {
        int j = b[i-1];
        while (j > 0 && P[i] != P[j])
            j = b[j-1];
        if (P[i] == P[j])
            j++;
        b[i] = j;
    }
}
int r;

void kmp(){
	int i = 0, j = 0;
	while(i < sz(T)){
		while(j >= 0 && (T[i] > j? -1 : T[i]) != P[j]) j = b[j];
		i++,j++;
		if(j == sz(P)){
			r++;
			j = b[j];
		}
	}
}

int main(void){
	string t;
	while(cin>>t){
		int n;
		cin>>n;
		memset(b,0,sizeof(int)*(n));
		vector<int> p(n);
		T = vector<int>(t.size());
		P = vector<int>(n);
		for(int i = 0; i < n; i++){
			int j;
			cin>>j;
			p[i] = j;
		}
		memset(last,-1,sizeof(last));
		for(int i = 0; i < n; i++){
			if(last[p[i]] != -1)P[i] = i - last[p[i]];
			else P[i] = -1;
			last[p[i]] = i;
		}
		memset(last,-1,sizeof(last));
		for(int i = 0; i < sz(t); i++){
			if(last[t[i]] != -1) {
				T[i] = i - last[t[i]];
			}
			else {
				T[i] = -1;
			}
			last[t[i]] = i;
		}
		kmppre();
		for(int i = 0; i < n; i++){
			cout<<b[i]<<' ';
			b[i]--;
		}
		cout<<'\n';
		for(int i = 0; i < n; i++){
			cout<<P[i]<<' ';
		}
		cout<<'\n';
		for(int i = 0; i < sz(t); i++){
			cout<<T[i]<<' ';
		}
		cout<<'\n';
		r = 0;
		kmp();
		cout<<r<<'\n';
	}

	return 0;
}
