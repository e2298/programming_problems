#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())

int main(void){
	int n,c,s;
	while(cin>>n>>c>>s){
		int times = 0;
		int curr=0;
		s--;
		if(!s) times++;
		while(c--){
			int dir;
			cin>>dir;
			curr += dir;
			if(curr < 0) curr = n-1;
			if(curr == n) curr = 0;
			if(curr == s) times++;
		}
		cout<<times<<'\n';
	}
	return 0;
}
