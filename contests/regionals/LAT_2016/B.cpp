#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;

int main(void){
	int n,m,a,b;
	while(cin>>n>>m>>a>>b){
		vector<set<int> >G(n);
		set<ii> deg;
		while(m--){
			int x,y;
			cin>>x>>y;
			x--;
			y--;
			G[x].insert(y);
			G[y].insert(x);
		}
		for(int i = 0; i < n; i++){
			deg.insert(ii(G[i].size(), i));
		}
		int end = 0;
		while(!end && deg.size()){
			end = 1;
			if(deg.size() && (deg.begin()->first < a)){
				ii tmp = *(deg.begin());
				deg.erase(tmp);
				for(auto i:G[tmp.second]){
					deg.erase(ii(G[i].size(), i));
					G[i].erase(tmp.second);
					deg.insert(ii(G[i].size(), i));
				}
				end = 0;
			}
			if(deg.size()){
				auto pt = --deg.end();
				ii tmp = *pt;
				if(deg.size() < (tmp.first +1 + b)){
					end = 0;
					deg.erase(tmp);
					for(auto i:G[tmp.second]){
						deg.erase(ii(G[i].size(), i));
						G[i].erase(tmp.second);
						deg.insert(ii(G[i].size(), i));
					}
				}
				else break;
			}
			
		}
		cout<<deg.size()<<'\n';
	}
	return 0;
}
