#include<bits/stdc++.h>
using namespace std;

int times[41];

int main(void){
	int n,m;
	int c = 0;
	while(cin>>n>>m){
		if(c) cout<<'\n';
		c = 1;
		memset(times,0,sizeof(times));
		for(int i = 1; i <= n; i++) for(int j = 1; j <= m; j++){
			times[i+j]++;
		}
		int mm = 0;
		for(int i = 2; i <= n+m; i++) mm = max(mm, times[i]);
		for(int i = 2; i <= n+m; i++) if(times[i] == mm) cout<<i<<'\n';
	}

	return 0;
}
