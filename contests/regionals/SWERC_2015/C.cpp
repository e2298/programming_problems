#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	while(cin>>t){
	while(t--){
		int n;
		ll r = 0;
		priority_queue<ll,vector<ll>,greater<ll>> Q;
		cin>>n;
		for(int i = 0; i < n; i++){
			ll in;
			cin>>in;
			Q.push(in);
		}
		while(Q.size() > 1){
			ll a,b;
			a = Q.top(); Q.pop();
			b = Q.top(); Q.pop();
			r += a+b;
			Q.push(a+b);
		}
		cout<<r<<'\n';

	}
	}
	return 0;
}
