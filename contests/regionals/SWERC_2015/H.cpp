#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

ll pot[70];

int digits(ll a){
	int r = 0;
	while(a){
		r ++;
		a >>=1;
	}
	return r;
}

int lones(ll a){
	while((a+1) & a) a>>=1;
	return digits(a);
}

ll solve(ll a){
	if(a < 1) return 0;
	if(a&(a-1)){
		ll b = a;
		b |= b>>32;	
		b |= b>>16;	
		b |= b>>8;	
		b |= b>>4;	
		b |= b>>2;	
		b |= b>>1;	
		b ++;
		ll r = pot[digits(b)-2];
		int ones = lones(a);
		
		int c = ~a;
		c ^= (~0)<<digits(a);
		int zerorg = lones(c);
		int dig = digits(a);

		for(int zeros = zerorg; zeros <= dig-ones; zeros++){
			if(dig%(ones+zeros) == 0) {r++;}
			else if((dig-ones) % (ones+zeros) == 0) {r++;}
		}	
		for(ones--; ones; ones--)
		for(int zeros = 1; zeros <= dig-ones; zeros++){
			if(dig%(ones+zeros) == 0) {r++;}
			else if((dig-ones) % (ones+zeros) == 0) {r++;}
		}	
		return r;
	}
	else return pot[digits(a)-1]+1;
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	pot[1] = 1;
	for(int i = 2; i <= 64; i++){
		pot[i] += pot[i-1];
		for(int j = 2; j < i; j++){
			if(i%j == 0){
				pot[i] += j-1;
			}	
		}
		for(int a = 1; a < i; a++){
			for(int b = 1; b <= i-a; b++){
				if((i-a) % (a+b) == 0) pot[i]++;
			}
		}
		pot[i] += i;
	}

	ll a,b;
	int seen = 0;
	while(cin>>a>>b){
		//if(seen) cout<<'\n';
		seen = 1;
		ll r = solve(b);
		if(a > 0)r -= solve(a-1);	
		cout<<r;
		cout<<'\n';
	}
	return 0;
}
