#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

vi G[5000];
vi H[5000];
int seen[5000];
int need[5000];
int can_reach[5000];
int cas = 1;

int needs(int i){
	if(seen[i] == cas) return need[i];
	seen[i] = cas;
	need[i] = 0;
	for(auto j:H[i])need[i] += needs(j);
	need[i]++;
	return need[i];
}

int reach(int i){
	if(seen[i] == cas) return can_reach[i];
	seen[i] = cas;
	can_reach[i] = 0;
	for(auto j:G[i]) if(seen[j] != cas)can_reach[i] += reach(j);
	can_reach[i]++;
	return can_reach[i];
}



int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int a,b,e,p;
	while(cin>>a>>b>>e>>p){
		for(int i = 0; i < e; i++) G[i] = vi();
		for(int i = 0; i < e; i++) H[i] = vi();
		while(p--){
			int x,y;
			cin>>x>>y;
			G[x].push_back(y);
			H[y].push_back(x);
		}
		int q1 = 0, q2 = 0, q3 = 0;

		for(int i = 0; i < e; i++){
			cas++;
			//cout<<reach(i)<<'\n';
			if(e-reach(i) < a) q1++;
			if(e-can_reach[i] < b) q2++;
			cas++;
			if(needs(i) > b) q3++;
		}
		cout<<q1<<'\n'<<q2<<'\n'<<q3<<'\n';
	}


	return 0;
}
