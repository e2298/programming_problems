import heapq
t = int(input())
for _ in range(t):
    n = int(input())
    Q = [int(i) for i in input().split()]
    heapq.heapify(Q)
    r = 0
    while len(Q) > 1:
        a = heapq.heappop(Q)
        b = heapq.heappop(Q)
        r += a+b
        heapq.heappush(Q,a+b)
    print(r)

    
