#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef complex<ll> c;
typedef vector<c> vc;
bool compare_complex(c a, c b){
	pair<ll,ll> aa(real(a), imag(a));
	pair<ll,ll> bb(real(b), imag(b));
	return aa < bb;
}

long cross(c a, c b){
	return a.real()*b.imag() - b.real() * a.imag();
}

//a left of c-b
int left(c a, c b, c c){
	return cross(b-a, c-a) > 0;
}

bool inPolygon(c p, const vc &pt){
	int n = pt.size();
	if(left(p,pt[0],pt[1]) || left(p, pt[n-1], pt[0])) return false;
	int a = 1, b = n-1;
	while(b - a > 1){
		int c = (a+b)/2;
		if(!left(p,pt[0],pt[c])) a = c;
		else b = c;
	}
	return !left(p,pt[a],pt[a+1]);
}


int main(void){
	int l;

	while(cin>>l){
		vc points(l);
		for(int i = 0; i < l; i++){
			int x,y;
			cin>>x>>y;
			points[i] = c(x,y);
		}
		sort(points.begin(), points.end(), compare_complex);
		vc hull(l+1);
		int idx = 0, idxo = 0;
		for(int i = 0; i < l; i++){
			while(idx>1 && cross(points[i] - hull[idx-2], hull[idx-1] - hull[idx-2]) <= 0) idx--;
			hull[idx++] = points[i];
		}
		idxo = idx;
		for(int i = l-1; i+1; i--){
			while(idx > idxo && cross(points[i] - hull[idx-2], hull[idx-1] - hull[idx-2]) <= 0) idx--;
			hull[idx++] = points[i];
		}
		hull.resize(idx-1);
		
		int r = 0;
		int s;
		cin>>s;
		while(s--){
			int x,y;
			cin>>x>>y;
			c p(x,y);
			if(inPolygon(p,hull)) r++;
		}
		cout<<r<<'\n';
	}
	return 0;
}
