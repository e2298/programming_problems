#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

vector<int> cards({1,10,100,1000,10000});

int main(void){
	int n,m;
	while(cin>>n>>m){
		ll r = 0;
		while(m--){
			ll budget,d;
			cin>>budget>>d;
			for(int i = 1; i < n; i++){
				ll c;
				cin>>c;
				budget -= c;
			}
			int mx = 0;
			for(auto i:cards){
				if(budget >= i) mx = i;
			}
			if(d > budget) r += mx;
			else r += mx-d;
			
		}
		cout<<r<<'\n';
	}
	
	return 0;
}
