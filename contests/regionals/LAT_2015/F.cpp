#include<bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,std::less<int>,rb_tree_tag,tree_order_statistics_node_update> set_t;

using namespace std;
typedef long long ll;
typedef pair <ll,ll> ii;
typedef pair<ll,ii> iii;

int sorted[200005];
int x[200005];
int type[200005];
int y[200005];

int cmp(int a, int b){
	return iii(x[a],ii(type[a],y[a])) < iii(x[b],ii(type[b], y[b]));
}

int main(void){
	int p,v;
	while(cin>>p>>v){
		map<ii, ll> value;
		int events = 0;
		for(int i = 0; i < p; i++){
			sorted[events] = events;
			cin>>x[events]>>y[events];
			value[ii(x[events],y[events])] = i+1;
			type[events] = 0;
			events++;
		}
		int frstx, frsty;
		int lstx,lsty;
		cin>>frstx>>frsty;
		lstx = frstx;
		lsty = frsty;
		for(int i = 1; i < v; i++){
			int xx,yy;
			cin>>xx>>yy;
			if(lsty == yy){
				sorted[events] = events;
				x[events] = min(xx,lstx);
				type[events] = 1;
				y[events] = yy;
				events++;
				sorted[events] = events;
				x[events] = max(xx,lstx);
				type[events] = 2;
				y[events] = yy;
				events++;
			}
			lstx = xx;
			lsty = yy;
		}
		if(frsty == lsty){
			sorted[events] = events;
			x[events] = min(frstx, lstx);
			type[events] = 1;
			y[events] = lsty;
			events++;
			sorted[events] = events;
			x[events] = max(frstx, lstx);
			type[events] = 2;
			y[events] = lsty;
			events++;
		}
		sort(sorted,sorted+events, cmp);
		set_t st;
		ll r = 0;
		for(int ev = 0; ev < events; ev++){
			int xx = x[sorted[ev]];
			int yy = y[sorted[ev]];
			int ty = type[sorted[ev]];
			int i = 0;
			//cout<<xx<<' '<<yy<<' '<<ty<<'\n';
			switch(ty){
				case 0:
					i = st.order_of_key(yy);
					if(!(i&1))
						r += value[ii(xx,yy)];
					break;
				case 1:
					st.insert(yy);
					break;
				case 2: 
					st.erase(yy);
					break;
			}
		}
		cout<<r<<'\n';

	}
	return 0;
}
