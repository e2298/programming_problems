#include<bits/stdc++.h>
using namespace std;
#define check(n,i) (n&(1ll<<i))
#define set(n,i) (n|(1ll<<i))
#define can(m,d) !(check(m,d*2)&&check(m,d*2+1))
typedef long long ll;

int use(ll msk, int d){
	if(check(msk,d*2)) return set(msk,d*2+1);
	else return set(msk,d*2);
}

int dig;
int digits[20];
ll poww[20];
ll rest(ll msk, int k){
	ll r =0;
	for(int d = 19; k>=0; d--){
		if(!check(msk,d)){
			r*= 10;
			r += d/2;
			k--;
		}
	}	
	return r;
}

ll solve(ll msk, int k){
	if(k < 0) return 0ll;
	ll r = -(1ll<<62);
	int algo = 0;
	for(int d = 0; d < digits[k]; d++){
		if(can(msk,d)){
			r = max(r, d*poww[k] + rest(use(msk, d), k-1));
			algo = 1;
		}
	}
	if(can(msk,digits[k])){
		r = max(r, digits[k]*poww[k] + solve(use(msk,digits[k]), k-1));
		algo = 1;
	}
	
	return algo? r: -(1ll<<61);
}

int main(void){
	ll n;
	poww[0] = 1ll;
	for(int i = 1; i <=20; i++){
		poww[i] = poww[i-1]*10ll;
	}
	while(cin>>n){
		dig = 0;
		while(n){
			digits[dig++] = n%10;
			n /= 10;
		}
		ll r = solve(0,dig-1);
		r = max(r, rest(0,dig-2));
		cout<<r<<'\n';

	}
	return 0;
}
