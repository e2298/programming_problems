#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

int cost[500];
int dad[500];
int w[500];
int c[500];
int s[500];
int cas = 1;
int dp[10501][500];
int seen[10501][500];
vi comps;

int solve1(int b, int i){
	if(i < 0) return 0;
	if(b < 0) return -(1<<29);
	if(seen[b][i]== cas) return dp[b][i];
	seen[b][i] = cas;
	int r;
	if(i)
		r = max(s[comps[i]] + solve1(b-c[comps[i]],i-1), solve1(b, i-1));
	else
		r = b >= c[comps[i]]? max(s[comps[i]],0) : 0;
	return dp[b][i] = r;
}
int solve2(int b, int i){
	if(i < 0) return 0;
	if(b < 0) return (1<<29);
	if(seen[b][i]== cas) return dp[b][i];
	seen[b][i] = cas;
	int r;
	if(i)
		r = min(s[comps[i]] + solve2(b-c[comps[i]],i-1), solve2(b, i-1));
	else
		r = b >= c[comps[i]]? min(s[comps[i]],0) : 0;
	return dp[b][i] = r;
}


int find(int a){
	if(dad[a] == a) return a;
	else return dad[a] = find(dad[a]);
}
void join(int a, int b){
	a = find(a);
	b = find(b);
	if(w[a] > w[b]){
		dad[b] = a;
	}
	else if(w[b] > w[a]) dad[a] = b;
	else {
		dad[a] = b;
		w[a]++;
	}
}

int main(void){
	int d,p,r,b;
	while(cin>>d>>p>>r>>b){
		for(int i = 0; i < d+p; i++) dad[i] = i;
		memset(w,0,(d+p)*sizeof(int));
		memset(c,0,(d+p)*sizeof(int));
		memset(s,0,(d+p)*sizeof(int));
		comps = vi();
		for(int i = 0; i < d; i++){
			cin>>cost[i];
		}
		for(int i = 0; i < p; i++){
			cin>>cost[i+d];
		}
		for(int i = 0; i < r; i++){
			int a,bb;
			cin>>a>>bb;
			a--;bb--;
			join(a,bb+d);
		}
		for(int i = 0; i < d+p; i++){
			if(dad[i] == i) {
				comps.push_back(i);
			}
			c[find(i)] += cost[i];
			if(i < d){
				s[find(i)]--;
			}
			else{
				s[find(i)]++;
			}
		}
		cout<<max(d, d+solve1(b, comps.size()-1))<<' ';
		cas++;
		cout<<max(p, p-solve2(b, comps.size()-1))<<'\n';
		cas++;
	}
	return 0;
}
