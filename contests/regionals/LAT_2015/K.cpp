#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

#define MAXN 100005
#define operacion(x,y) min(x,y)
#define tipo ll
#define forr(i,a,b) for(ll i = (a); i <(b); i++)
#define forn(i,n) forr(i,0,n)
#define dforn(i,n) for(ll i = n-1; i >=0; i--)

const ll neutro = 1ll<<50;
struct RMQ{
	ll sz;
	tipo t[4*MAXN];
	tipo &operator[] (ll p){return t[sz+p];}
	void init(ll n){
		sz = 1 << (32-__builtin_clz(n));
		forn(i,2*sz) t[i] = neutro;
	}
	void updall(){
		dforn(i,sz) t[i] = operacion(t[2*i],t[2*i+1]);
	}
	tipo get(ll i, ll j){return get(i,j,1,0,sz);}
	tipo get(ll i, ll j, ll n, ll a, ll b){
		if(j<=a || i>=b) return neutro;
		if(i<=a && b<= j)return t[n];
		ll c = (a+b)/2;
		return operacion(get(i,j,2*n,a,c),get(i,j,2*n+1,c,b));
	}
	void set(ll p, tipo val){
	  for(p+=sz;p>0 && t[p] != val;){
	    t[p] = val;
	    p/=2;
	    val = operacion(t[p*2],t[p*2+1]);
	  }
	}
}st;

ll E[100005];
ll L[100005];
ll S[100005];
ll C[100005];
ll dist[100005];
ll sorted[100005];
bool cmp(ll a, ll b){
	return L[a] < L[b];
}

int main(){
	ll N,M;
	while(cin >> N>>M){
		for(ll i = 0; i < N; i++)
			cin>> E[i];
		for(ll i = 0; i < M; i++){
			sorted[i] = i;
			cin>> L[i]>> S[i]>> C[i];      
			L[i]--;
		}
		L[M] = N;
		sorted[M] = M;
		S[M] = C[M] = 0;
		M++;
		sort(sorted, sorted+M, cmp);
		if(L[sorted[0]] != 0){
			cout<<"-1\n";
			continue;
		}
		st.init(M);
		st.updall();
		ll lst = 0;
		for(ll i = 1; i < M; i++){
			ll d=0;
			for(ll j = lst; j < L[sorted[i]]; j++)d+=E[j];
			dist[i] = d + dist[i-1];
			lst = L[sorted[i]];
		}
		st.set(M-1, 0);
		for(ll i = M-1; i >=0; i--){
			ll get = upper_bound(dist, dist+M, dist[i] + S[sorted[i]]) - 1 - dist;
			if(get > i){
				st.set(i, st.get(i, get+1) + C[sorted[i]]);
			}
		}
		ll r = neutro;
		for(int i = 0; L[sorted[i]] == 0; i++) 
			r = min(r, st.get(i,i+1));
		if(r >= neutro) cout<<"-1\n";
		else cout<<r<<'\n';
	}
	return 0;
}
