#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef complex<ll> cl;
typedef pair<ll, ll> ii;

ll cros(cl a, cl b){
	return a.real() * b.imag() - a.imag() * b.real();
}
ll cross(cl a, cl b){
	return b.real() * a.imag() - b.imag() * a.real();
}

int comp(cl a, cl b){
	return ii(real(a),imag(a)) < ii(real(b),imag(b));
}

ll res[100000];

int main(void){
	int n;
	while(cin>>n){
		cout<<'a'<<endl;
		vector<cl> points(n);
		cout<<'b'<<endl;
		for(int i =0; i < n; i++){
			ll x,y;
			cin>>x>>y;
			points[i] = cl(x,y);
			res[i] = -1ll;
		}
		reverse(points.begin(),points.end());
		int i = 0;
		int j = 1;
		int k = 0;	
		for(int id = 0; id < n; id++){
			if(comp(points[id],points[i])) i = id;
		}
		j = i+2;
		j %= n;
		for(int id = (i+2) % n; id != (i?i-1:n-1); id++, id%=n){
			if(comp(points[j],points[id])) j = id;
		}
		ll left = 0, right = 0;
		for(int id = i; id != j; id++, id%= n){
			left += cross(points[id],points[(id+1)%n]);
		}
		left += cross(points[j], points[i]);
		for(int id = j; id != i; id++, id%= n){
			right += cross(points[id], points[(id+1)%n]);
		}
		right += cross(points[i], points[j]);
		ll r = 0;
		if((i+1%n) != j && (j+1)%n != i)
			r = abs(right-left);
		while(k < n){
			//pair
			k++;
			if(cros(points[(j+1)%n] - points[j], points[(i+1) % n] - points[i]) < 0){
				ll change = cross(points[i]-points[j], points[(i+1)%n] - points[j]);
				left -= change;
				right += change;
				i++;
				if((i+1%n) != j && (j+1)%n != i){
					ll a = 0,b=0;
					if(res[i] != -1){
						a = res[i];
					}
					if(res[j] != -1){
						b = res[j];
					}
					ll t = max(a,b);
					if(t == 0) t = (1ll<<60);
					res[i] = min(t, abs(right-left));
					res[j] = min(t, abs(right-left));
				}
			}
			else if(cros(points[(j+1)%n] - points[j], points[(i+1) % n] - points[i]) == 0){
				//pairs
				ll change1 = cross(points[i]-points[j], points[(i+1)%n] - points[j]);
				if((i+2%n) != j && (j+1)%n != (i+1)%n){
					ll a = 0,b=0;
					if(res[(i+1)%n] != -1){
						a = res[i];
					}
					if(res[j] != -1){
						b = res[j];
					}
					ll t = max(a,b);
					if(t == 0) t = (1ll<<60);
					res[i] = min(t, abs((right + change1)-(left-change1)));
					res[j] = min(t, abs((right + change1)-(left-change1)));
				}
					

				ll change2 = cross(points[j]-points[i], points[(j+1)%n] - points[i]);
				if((i+1%n) != (j+1)%n && (j+2)%n != i){
					ll a = 0,b=0;
					if(res[(i+1)%n] != -1){
						a = res[i];
					}
					if(res[j] != -1){
						b = res[j];
					}
					ll t = max(a,b);
					if(t == 0) t = (1ll<<60);
					res[i] = min(t, abs((right - change2)-(left+change2)));
					res[j] = min(t, abs((right - change2)-(left+change2)));
				}
					

				left -= change1;
				left += change2;
				right += change1;
				right -= change2;

				i++;
				j++;
				k++;
				if((i+1%n) != j && (j+1)%n != i){
					ll a = 0,b=0;
					if(res[i] != -1){
						a = res[i];
					}
					if(res[j] != -1){
						b = res[j];
					}
					ll t = max(a,b);
					if(t == 0) t = (1ll<<60);
					res[i] = min(t, abs(right-left));
					res[j] = min(t, abs(right-left));
				}
			}
			else{
				ll change = cross(points[j]-points[i], points[(j+1)%n] - points[i]);
				left += change;
				right -= change;
			       	j++;
				if((i+1%n) != j && (j+1)%n != i){
					ll a = 0,b=0;
					if(res[i] != -1){
						a = res[i];
					}
					if(res[j] != -1){
						b = res[j];
					}
					ll t = max(a,b);
					if(t == 0) t = (1ll<<60);
					res[i] = min(t, abs(right-left));
					res[j] = min(t, abs(right-left));
				}
			}
			i %= n;
			j %= n;
		}
		ll t = right + left;
		cout<<t<<'\n';
		cout<<(r+t)/2<<' '<<(t-r)/2<<'\n';
	}
	return 0;
}
