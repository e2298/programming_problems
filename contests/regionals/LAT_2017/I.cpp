#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
#define forr(i,a,b) for(int i=(a); i<(b); i++)
#define forn(i,n) forr(i,0,n)
struct{
	#define LVL 17
	int vec[LVL][1<<(LVL+1)];
	int &operator[](int p){return vec[0][p];}
	int get(int i, int j) {//intervalo [i,j)
		int p = 31-__builtin_clz(j-i);
		return max(vec[p][i],vec[p][j-(1<<p)]);
	}
	void build(int n) {//O(nlogn)
		int mp = 31-__builtin_clz(n);
		forn(p, mp) forn(x, n-(1<<p))
			vec[p+1][x] = max(vec[p][x], vec[p][x+(1<<p)]);
	}
}rmq;

vector<ii> G[100010];
ii edges[200020];
vector<ii> edw;

const int MAXN = 100010;
const int LOGN = 17;
int N, f[MAXN][LOGN], L[MAXN];
int treesz[MAXN];
int dad[MAXN];
void dfs(int v, int fa=-1, int lvl = 0){
	f[v][0] = fa, L[v] = lvl;
	dad[v] = fa;
	treesz[v] = 1;
	for(auto it:G[v]) if(it.first != fa){
		dfs(it.first, v, lvl+1);
		treesz[v] += treesz[it.first];
	}
}

void build(){
	forn(k,LOGN-1) forn(i,N) f[i][k+1]=f[f[i][k]][k];
}
#define dforn(i,n) for(int i=n-1; i>=0; i--)
#define lg(x) (31-__builtin_clz(x))
int climb(int a, int d){
	if(!d) return a;
	dforn(i, lg(L[a])+1) if(1<<i<=d) a = f[a][i],d-=1<<i;
	return a;	
}

int lca(int a, int b){
	if(L[a]<L[b]){
		a^=b;
		b^=a;
		a^=b;
	}
	a = climb(a,L[a]-L[b]);
	if(a==b) return a;
	dforn(i,lg(L[a])+1)
		if(f[a][i]!=f[b][i])
			a = f[a][i],b=f[b][i];
	return f[a][0];
}
int dist(int a, int b){
	return L[a]+L[b] - 2*L[lca(a,b)];
}

/*uso:
 *rmq.init(n)
 *dfs1(inicial);
 *heavylight(inicial);
 *dfs(); (o poner la llamada adentro de build)
 *rmq[pos[v]] = peso entre dad[v] y v para todo v (aqui se hace como parte de heavylight, si la lista de adjacencia tiene pares(hijo,peso);
 *build(inicial);
 *rmq.updall();
*/
int pos[MAXN], q;//pos[v]=posicion del nodo v en el recorrido de la dfs
//Las cadenas aparecen continuas en el recorrido!
int cantcad;
int homecad[MAXN];//dada una cadena devuelve su nodo inicial
int cad[MAXN];//cad[v]=cadena a la que pertenece el nodo
void heavylight(int v, int cur=-1, int peso = -1){
	if(cur==-1) homecad[cur=cantcad++]=v;
	pos[v]=q++;
	rmq[pos[v]]=peso;
	cad[v]=cur;
	int mx=-1;
	for(int i = 0; i < G[v].size(); i++) if(G[v][i].first!=dad[v])
		if(mx==-1 || treesz[G[v][mx].first]<treesz[G[v][i].first]) mx=i;
	if(mx!=-1) heavylight(G[v][mx].first, cur, G[v][mx].second);
	for(int i = 0; i < G[v].size(); i++) if(i!=mx && G[v][i].first!=dad[v])
		heavylight(G[v][i].first, -1, G[v][i].second);
}
// r = max(query(lca(v,u), v), query(lca(u,v),u));
int query(int an, int v){//O(logn)
	//si estan en la misma cadena:
	if(an == v) return 0;
	if(cad[an]==cad[v]) return rmq.get(pos[an]+1, pos[v]+1);
	return max(query(an, dad[homecad[cad[v]]]),
			   rmq.get(pos[homecad[cad[v]]], pos[v]+1));
}

int disj[100010];
int w[100010];

int find(int a){
	if(disj[a] ==a)return a;
	else return disj[a] = find(disj[a]);
}
void join(int a, int b){
	a = find(a);
	b = find(b);
	if(w[a] < w[b]){
		disj[a] = b;
	}
	else if (w[b] < w[a]){
		disj[b] = a;
	}
	else{
		disj[b] = a;
		w[a]++;
	}
}


int main(void){
	cin.tie(0);
	ios::sync_with_stdio(0);
	int n,r;
	cin>>n>>r;
	map<ii,int> ed;
	for(int i = 0; i<r; i++){
		int a,b,c;
		cin>>a>>b>>c;
		a--;
		b--;
		edges[i] = ii(a,b);
		edw.push_back(ii(c,i));
		ed[ii(a,b)] = c;
		ed[ii(b,a)] = c;
	}	
	sort(edw.begin(),edw.end());
	for(int i = 0; i < n; i++){
		disj[i] = i;
	}
	r = 0;
	for(auto i:edw){
		int ww = i.first;
		int j = edges[i.second].first;
		int k = edges[i.second].second;
		if(find(j) != find(k)){
			r += ww;
			join(j,k);
			G[j].push_back(ii(k,ww));
			G[k].push_back(ii(j,ww));
		}
	}
	N = n+1;
	dfs(0);
	build();
	heavylight(0);
	rmq.build(n+1);
	int q;
	cin>>q;
	while(q--){
		int a,b;
		cin>>a>>b;
		a--;
		b--;
		cout<<r+ed[ii(a,b)]-max(query(lca(a,b),a), query(lca(a,b),b))<<'\n';
	}

	return 0;
}
