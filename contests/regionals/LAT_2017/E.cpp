#include<bits/stdc++.h>
using namespace std;

string k;
int n;
int dp[1010][1010];
bool seen[1010][1010];
int solve(int dig, int mod){
	if(seen[dig][mod]) return dp[dig][mod];
	seen[dig][mod] = 1;
	if(dig == k.size()) return dp[dig][mod] = mod? -1: 0;
	if(k[dig] != '?'){
		return dp[dig][mod] = solve(dig+1, (mod*10+(int)k[dig])%n) != -1? k[dig] : -1;
	}
	int r = -1;
	for(int i = 0; i < 10; i++){
		if(solve(dig+1, (mod*10+i)%n) != -1){
			r = i;
			break;
		}
	}
	return dp[dig][mod] = r;
}

int main(void){
	cin>>k>>n;
	for(auto& i:k){
		if(i != '?') i-='0';
	}
	int r = -1;
	if(k[0] == '?'){
		for(int i = 1; i < 10; i++){
			if(solve(1,i%n) != -1){
				r = i;
				break;
			}	
		}
	}
	else{
		r = solve(1,k[0]%n) == -1? -1 : k[0];
	}
	if(r == -1){
		cout<<"*\n";
	}
	else{
		cout<<r;
		int mod = r %n;
		for(int i = 1; i < k.size(); i++){
			int dig = dp[i][mod];
			cout<<dig;	
			mod = mod*10 + dig;
			mod %= n;
		}
		cout<<'\n';
	}
	return 0;
}
