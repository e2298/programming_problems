#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;

int se_pichasean(ii a, ii b){
	if(a> b){
		swap(a,b);
	}
	if(a.first < b.first) return b.second <= a.second;
	if(a.first == b.first) return b.second > a.second;
}

struct arbol{
	arbol* der;
	arbol* izq;
	ii ele;
	ll val;
	arbol(ii v, int c):val(c), der(NULL), izq(NULL), ele(v){}
	void insertar(ii elem, int c){
		if(se_pichasean(elem,ele)){
			if(izq == NULL)izq = new arbol(elem,c);
			else izq->insertar(elem,c);
		}
		else{
			if(der == NULL)der = new arbol(elem,c);
			else der->insertar(elem,c);
		}
	}
	ll acc(){
		if(izq == NULL && der == NULL)
			return val;
		else if(izq == NULL)
			return val + der->acc();
		else if(der == NULL)
			return max(izq->acc(), val);
		return max(izq->acc(), val+der->acc());
	}
};
     
typedef pair<ii, int> iii;

int main(void){
	cin.tie(0);
	ios::sync_with_stdio(0);
	int n;
	cin>>n;
	int a,b,c;
	vector<iii> p;
	for(int i = 0; i < n; i++){
		cin>>a>>b>>c;
		p.push_back(iii(ii(a,b),c));
	}
	sort(p.begin(),p.end());
	arbol t(p[0].first,p[0].second);
	for(int i = 1; i < n; i++){
		t.insertar(p[i].first,p[i].second);
	}
	cout<<t.acc()<<'\n';
	return 0;
}
