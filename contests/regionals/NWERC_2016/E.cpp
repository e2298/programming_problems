#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;

int main(void){
	ll n;
	cin >> n;
	vector<ii> s(n);
	for(int i = 0; i < n; ++i){
		cin >> s[i].first;
		s[i].second = i;
	}
	sort(s.rbegin(), s.rend());
	int fallo = 0;
	int otraVuelta = 0;
	ll examenes = 0;
	for(int i = 0; i < n && !fallo; ++i ){
			if(i == 0){
				examenes += s[i].first;
			}else{
				examenes -= s[i].first;
				if(examenes < 0){
					fallo = 1;
				}else{
					examenes += s[i].first;
				}
			}
	}

	if(!fallo){
		ll sumaTodos = 0;
		for(int i = 1; i < n; ++i){
				sumaTodos += s[i].first;
		}
		if(s[0].first <= sumaTodos){
			for(int i = 0; i < n; ++i){
				if(i != n-1){
					cout << s[i].second + 1 << " ";
				}else{
					cout << s[i].second + 1 << '\n';
				}
			}
		}else{
			cout << "impossible\n";
		}
	}else{
		cout << "impossible\n";
	}
	return 0;
}
