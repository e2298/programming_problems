#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,ii> iii;
typedef map<int, iii> mi;
typedef vector<int> vi;
#define forr(i,a,b) for(int i=(a); i<(b); i++)
#define tipo int
#define forn(i,n) forr(i,0,n)

struct RMQ{
	#define LVL 20
	tipo vec[LVL][1<<(LVL+1)];
	tipo &operator[](int p){return vec[0][p];}
	tipo get(int i, int j){
		int p = 31-__builtin_clz(j-i);
		return max(vec[p][i], vec[p][j-(1<<p)]);
	}
	void build(int n){
		int mp = 31-__builtin_clz(n);
		forn(p, mp) forn (x, n-(1<<p))
			vec[p+1][x] = max(vec[p][x], vec[p][x+(1<<p)]);
	}
};

struct RMQ ra[2];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	mi ma;
	set<int> seen;
	cin>>n;
	for(int i = 0; i < n; i++) {
		cin>>ra[0][i];
		if(seen.count(ra[0][i])){
			ma[ra[0][i]].second = ii(0,i);
		}
		else{
			seen.insert(ra[0][i]);
			ma[ra[0][i]].first = ii(0,i);
		}
	}
	for(int i = 0; i < n; i++) {
		cin>>ra[1][i];
		if(seen.count(ra[1][i])){
			ma[ra[1][i]].second = ii(1,i);
		}
		else{
			seen.insert(ra[1][i]);
			ma[ra[1][i]].first = ii(1,i);
		}
	}
	ra[0].build(n+10);
	ra[1].build(n+10);
	
	int r = 0;
	for(auto i: ma){
		ii p1 = i.second.first, p2 = i.second.second;
		int v = i.first;
		if(p1.first != p2.first){
			r = max(r, v);
			continue;
		}
		if(p2.second - p1.second > 1){
			int rack = p1.first;
			r = max( r, min(v, ra[rack].get(p1.second + 1, p2.second)));
		}
	}
	cout<<r<<'\n';

	return 0;
}
