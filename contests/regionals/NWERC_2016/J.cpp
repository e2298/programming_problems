#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, ll> ii;
#define INF (1ll<<60ll)
#define MAX_V 2000
#define SRC 0
#define SNK 1999
ll f, p[MAX_V];
map<int,ll> g[MAX_V];
void augment(int v, ll minE){
	if(v== SRC) f = minE;
	else if(p[v] != -1){
		augment(p[v], min(minE, g[p[v]][v]));
		g[p[v]][v]-=f, g[v][p[v]]+=f;
	}

}

ll maxflow(){
	ll Mf = 0;
	do{
		f = 0;
		char used[MAX_V]; queue<int> q; q.push(SRC);
		memset(used, 0, sizeof(used)), memset(p,-1,sizeof(p));
		while(q.size()){
			int u = q.front(); q.pop();
			if(u ==SNK) break;
			for(auto it:g[u]){
				if((it.second > 0) && !used[it.first]) used[it.first]=true, q.push(it.first), p[it.first] = u;
			}
		}
		augment(SNK, INF);
		Mf += f;
	
	}while(f);
	return Mf;
}

int qr[100];
int qs[30];


//S
//windows
//sensors
//queues
int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	int n,q,s;
	cin>>n>>q>>s;
	for(int i = 0; i < s; i++){
		cin>>qr[i];
		qr[i]--;
	}
	for(int i = 0; i < q; i++){
		cin>>qs[i];
	}
	ll tot =0;
	for(int i = 0; i < n; i++){
		ll d;
		cin>>d;
		for(int j = 0; j < q; j++){
			g[1 + n +(n+1)*j + i][1 + n +(n+1)*q + n*j + i] = qs[j];
			g[1 + n +(n+1)*q + n*j + i][1+i] = qs[j];
			g[1 + n +(n+1)*q + n*j + i][1 + n +(n+1)*j + i + 1] = qs[j];
		}
		for(int j =0;j < s; j++){
			ll sz;
			cin>>sz;
			tot += sz;
			g[0][1 + n + (n+1)*qr[j] +i] += sz;
		}		
		g[1+i][1999]  = d;
	}

	ll fl = maxflow();
	if(fl >= tot) cout<<"possible\n";
	else cout<<"impossible\n";




	return 0;
}
