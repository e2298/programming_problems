#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;


ll solve(int n, ll a){
	if(n==1){
		return a + 1;
	}
	ll t = solve(n-1, a>>1);
	ll r = t<<1;
	if(((t)&1) != (a&1)){
		r--;
	}
	return r;
}


int main(void){
	int n;
	cin>>n;
	ll a=0ll,b=0ll;
	for(int i = 0;i < n; i++){
		char x;
		cin>>x;
		a<<=1;
		a += (x=='1');
	}
	for(int i = 0;i < n; i++){
		char x;
		cin>>x;
		b<<=1;
		b += (x=='1');
	}

	cout<<solve(n,b) - solve(n,a) - 1<<'\n';

	return 0;
}
