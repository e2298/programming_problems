#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef complex<ll> cl;
typedef complex<double> cd;


double PI = 2.0*acos(0.0);

cd incenter(cl x, cl y, cl z){
	cd aa(x.real(), x.imag());
	cd bb(y.real(), y.imag());
	cd cc(z.real(), z.imag());
	double c = abs(aa-bb);
	double b = abs(aa-cc);
	double a = abs(bb-cc);
	return (a*aa + b*bb + c*cc) / (a+b+c);
}



void solve3(){
	double x,y;
	cin>>x>>y;
	cd a(x,y); 
	cin>>x>>y;
	cd b(x,y); 
	cin>>x>>y;
	cd c(x,y); 

	double anga = arg((b-a)/(c-a));
	if(anga > 180.0){
		anga = -anga + 2.0*PI;
	}
	anga /= 2.0;
	double angb = arg((c-b)/(a-b));
	if(angb > 180.0){
		angb = -angb + 2.0*PI;
	}
	angb /= 2.0;
	double angc = arg((a-c)/(b-c));
	if(angc > 180.0){
		angc = -angc + 2.0*PI;
	}
	angc /= 2.0;

	cd ca = (c-a) * polar(1.0, anga);
	ca /= abs(ca);
	ca *= 4000.0 / sin(anga); 	
	ca += a;
	cd cb = (a-b) * polar(1.0, angb);
	cb /= abs(cb);
	cb *= 4000.0 / sin(angb); 	
	cb += b;
	cd cc = (b-c) * polar(1.0, angc);
	cc /= abs(cc);
	cc *= 4000.0 / sin(angc); 	
	cc += c;
	//cout<<anga<<' '<<angb<<' '<<angc<<'\n';
	//cout<<ca<<cb<<cc<<'\n';

	if(abs(ca - cc) >= 8000.0){
		printf("%lf %lf\n", ca.real(), ca.imag());
		printf("%lf %lf\n", cc.real(), cc.imag());
	}
	else if(abs(ca - cb) >= 8000.0){
		printf("%lf %lf\n", ca.real(), ca.imag());
		printf("%lf %lf\n", cb.real(), cb.imag());
	}
	else if(abs(cc - cb) >= 8000.0){
		printf("%lf %lf\n", cc.real(), cc.imag());
		printf("%lf %lf\n", cb.real(), cb.imag());
	}
	else printf("impossible\n");

}

ll cross(cl a, cl b){
	return a.real() * b.imag() - a.imag() * b.real();
}

ll in_triangle(cl a, cl b, cl c, cl d){
	return (cross(b-a, d-a)) < 0 && (cross(c-b, d-b) < 0) && (cross(a-c, d-c) < 0 );
}

int main(void){
	int n;
	cin>>n;
	if(n == 3){
		solve3();
		return 0;
	}
	int e1,e2= -1;
	vector<cl> points(n);
	for(int i = 0; i <  n; i++){
		ll x,y;
		cin>>x>>y;
		points[i] = cl(x,y);
	}
	int i = 1;
	for(; i < n-1; i++){
		int can = 1;
		if(cross(points[i]-points[i-1], points[i+1]-points[i-1]) > 0ll){
			continue;
		}
		for(int j = 2; j <= n-2; j++){
			if(in_triangle(points[i-1],points[i],points[i+1],points[(j+i)%n])){
				can = 0;
				break;
			}
		}
		if(can){
			e1 = i;
			i += 2;
			break;
		}
	}
	for(; i < n-1; i++){
		int can = 1;
		if(cross(points[i]-points[i-1], points[i+1]-points[i-1]) > 0ll){
			continue;
		}
		for(int j = 2; j <= n-2; j++){
			if(in_triangle(points[i-1],points[i],points[i+1],points[(j+i)%n])){
				can = 0;
				break;
			}
		}
		if(can){
			e2 = i;
			break;
		}
	}
	if(e2 == -1){
		if(e1 == n-2){ e2 = 0;}
		else{
			int can = 1;
			if(cross(points[n-1]-points[n-2], points[0]-points[n-2]) > 0ll){
				can = 0;
			}
			for(int j = 2; j <= n-2; j++){
				if(in_triangle(points[n-2],points[n-1],points[0],points[(j+n-1)%n])){
					can = 0;
					break;
				}
			}
			if(can){
				e2 = n-1;
			}
			else{
				e2 = 0;	
			}
		}
	}
	cd ccc = incenter(points[e1],points[e1+1],points[e1-1]);
	printf("%lf %lf\n", ccc.real(), ccc.imag());
	if(!e2)
		ccc = incenter(points[0],points[n-1],points[1]);
	else if(e2 == n-1)
		ccc = incenter(points[0],points[n-1],points[n-2]);
	else 
		ccc = incenter(points[e2],points[e2+1],points[e2-1]);
	printf("%lf %lf\n", ccc.real(), ccc.imag());

	return 0;
}
