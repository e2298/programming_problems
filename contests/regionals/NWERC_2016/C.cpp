#include<bits/stdc++.h>
using namespace std;
typedef long long ll;



int main(void){
	double x,y;
	cin>>x>>y;
	double sum = 0.0;
	int n;
	cin>>n;
	while(n--){
		double l,v,f;
		cin>>l>>v>>f;
		sum += (v-l)*(f-1.0);
	}
	double r = x/(y+sum);
	printf("%.10f", r);
	return 0;
}
