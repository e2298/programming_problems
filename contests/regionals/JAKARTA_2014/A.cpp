#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int dad[200];
int nums[200];
int sz[200];

int find(int a){
	if(dad[a] == a) return a;
	else return dad[a] = find(dad[a]);
}
void join(int a, int b){
	a = find(a);
	b = find(b);
	if(sz[a] < sz[b]){
		dad[a] = b;
		sz[b] += sz[a];
	}
	else{
		dad[b] = a;
		sz[a] += sz[b];
	}
}


int main(void){
	int t;
	cin>>t;
	int cas = 1;
	while(t--){
		int n,k;
		cin>>n>>k;
		for(int i = 0; i < n; i++){
			dad[i] = i;
			sz[i] = 1;
			cin>>nums[i];
		}
		for(int i = 0; i < n-1; i++){
			for(int j=i+1; j < n; j++){
				if(abs(nums[i]-nums[j]) <= k){
					join(i,j);
				}
			}
		}
		set<int> clusters;
		for(int i = 0; i < n; i++){
			clusters.insert(find(i));
		}
		printf("Case #%d: %d\n", cas++,clusters.size());
		
	}

	return 0;
}
