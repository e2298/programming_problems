#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll st[1010];
ll ass[] = {0,4,8,11};
ll dp[1010][11][4][51];
ll seen[1010][11][4][51];
int cas;

ll solve(int n, int k, int i, int e){
	if(n < 0) return 0ll;
	if(seen[n][k][i][e] == cas) return dp[n][k][i][e];
	seen[n][k][i][e] = cas;
	
	ll r;
	int eorg = e;
	int iorg = i;
	if(e < i){
		i = 0;
		e = 0;
	}	
	r = solve(n-1, k, i, e-i);
	if(k && (i != 0)){
		r = min(r, solve(n-1,k-1,0,e-i));
	}
	if(k && (i != 1) && (e-i)>=1){
		r = min(r, solve(n-1,k-1,1,e-i));
	}
	if(k && (i != 2) && (e-i)>=2){
		r = min(r, solve(n-1,k-1,2,e-i));
	}
	if(k && (i != 3) && (e-i)>=3){
		r = min(r, solve(n-1,k-1,3,e-i));
	}
	r += max(0ll,st[n] - ass[i]);
	return dp[n][k][iorg][eorg] = r;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		cas++;
		int n,k,e;
		cin>>n>>k>>e;
		for(int i = n-1; i >=0; i--){
			cin>>st[i];
		}
		ll r = 0;
		r = solve(n-1,k,0,e);
		if(e >= 1 && k) r = min(r,solve(n-1,k-1,1,e));
		if(e >= 2 && k) r = min(r,solve(n-1,k-1,2,e));
		if(e >= 3 && k) r = min(r,solve(n-1,k-1,3,e));
		cout<<"Case #"<<cas<<": "<<r<<'\n';
	}
	return 0;
}
