#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int uf[20100];
int sz[20100];
int dad[20100];
int org[20100];
char qtype[5010];
int qa[5010];
int qb[5010];
int ans[5010];
char seen[20010];

int find(int a){
	if(uf[a] == a) return a;
	else return uf[a] = find(uf[a]);
}

void join(int a, int b){
	a = find(a);
	b = find(b);
	if(sz[a] < sz[b]){
		uf[a] = b;
		sz[b] += sz[a];
	}
	else{
		uf[b] = a;
		sz[a] += sz[b];
	}
}

int main(void){
	int t;
	cin>>t;
	int cas = 1;
	while(t--){
		int n,k;
		cin>>n>>k;
		//memset(sz, 0, n*sizeof(int));
		memset(seen,0,n);
		for(int i = 0; i < n; i++){
			uf[i] = i;
			sz[i] = 1;
			cin>>dad[i];
			dad[i]--;
			org[i] = dad[i];
		}
		for(int i = 0; i < k; i++){
			char type;
			cin>>type;
			if(type=='Q'){
				qtype[i] = 0;
				cin>>qa[i]>>qb[i];
				qb[i]--;
				qa[i]--;
			}
			else{
				qtype[i] = 1;
				cin>>qa[i];
				qa[i]--;
				if(seen[qa[i]]){
					qa[i] = -1;
				}
				seen[qa[i]] = 1;
				dad[qa[i]] = -1;
			}
		}
		for(int i = 0; i < n; i++){
			if(dad[i] != -1) join(i,dad[i]);
		}
		cout<<"Case #"<<cas++<<":\n";
		int answers = 0;
		for(int i = k-1; i >=0; i--){
			if(qtype[i]){
				if(qa[i] == -1) continue;
				if(org[qa[i]] != -1){
					join(qa[i],org[qa[i]]);
				}
			}
			else{
				if(find(qa[i]) == find(qb[i])) ans[answers++] = 1;
				else ans[answers++]=0;;
			}
		}
		for(answers--; answers >= 0; answers--){
			cout<<(ans[answers]?"YES\n":"NO\n");
		}


		
	}

	return 0;
}
