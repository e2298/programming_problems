#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

int seen[200][200];
int d[200][200];

int main(void){
	int h,t;
	int cas = 1;
	while(cin>>h>>t && h){
		int r = 0;
		queue<ii> q;
		q.push(ii(h,t));
		d[h][t] = 0;
		seen[h][t] = cas;
		while(q.size()){
			ii c = q.front(); q.pop();
			h = c.first, t = c.second;
			if(h == 0 && t == 0){
				cout<<d[h][t]<<'\n';
				break;
			}
			if(t-1 >= 0 && t+1 < 200 && seen[h][t+1] != cas){
				seen[h][t+1] = cas;
				d[h][t+1] = d[h][t] + 1;
				q.push(ii(h,t+1));
			}
			if(t-2 >= 0 && h+1 < 200 && seen[h+1][t-2] != cas){
				seen[h+1][t-2] = cas;
				d[h+1][t-2] = d[h][t] +1;
				q.push(ii(h+1,t-2));
			}
			if(h-2 >= 0 && seen[h-2][t] != cas){
				seen[h-2][t] = cas;
				d[h-2][t] = d[h][t] +1;
				q.push(ii(h-2,t));	
			}

		}
		cas++;
	}

	return 0;
}

/*
int main(void){
	int h,t;
	
	while(cin>>h>>t && h){
		if((h&1)^1 && !(t&3)){
			cout<<(t>>1) + (t>>2) + (h>>1)<<'\n';
		}
		else if((h&1)^1 && (t&1)^1){
			cout<<2 + (h>>1) + ((t+2)>>1)+ ((t+2)>>2)<<'\n';
		}
		else if((h&1)^1 && (t&1))
		else if(h&1 && !(t&3)){
			cout<< ((h + (t>>1))>>1)<<'\n';
		}
		else if(h&1 && (t&1)^1){
			cout<< ((h + (t>>1) + 1) >>1) + 2<<'\n';
		}
		else if(t&2){
			cout<<( 4 + (h-1 + (t>>1) + 1)>>1)<<'\n';
		}
		else{
			cout<< (2 +( h+1 + (t>>1))>>2)<<'\n';
		}
	}
	return 0;
}
*/
