#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll, ll> ii;
typedef complex<ll> cl;
typedef vector<cl> vc;

ll cross(cl a, cl b){
	return a.real() * b.imag() - a.imag() * b.real();
}

ll dot(cl a, cl b){
	return a.real() * b.real() + a.imag() * b.imag();
}

int collinear(cl a, cl b, cl c){
	return !cross(b-a, c-a);
}

cl cmparg;
int cmp(cl a, cl b){
	a = a-cmparg;
	b = b-cmparg;
	if(cross(a,b) > 0ll){
		if(a.imag() < 0ll && b.imag() >= 0ll) return 0;
		else return 1;
	}
	else if (cross(a,b) == 0ll){
		if(dot(a,b) > 0) return 0;
		if(a.imag() < 0) return 0;
		if(a.imag() > 0) return 1;
		if(a.real() > 0) return 1;
		return 0;
	}
	else{
		return !cmp(b+cmparg,a+cmparg);
	}
}

int complex_cmp(cl a, cl b){
	return ii(a.real(), a.imag()) < ii(b.real(), b.imag());
}
struct less_complex {
  bool operator() (const cl& x, const cl& y) const {return complex_cmp(x,y);}
};
typedef set<cl, less_complex> sc;

cl sorted[100000];

int main(void){
	int n;
	while(cin>>n && n){
		sc line1;
		sc line2;
		sc line3;
		sc rest;
		for(int i = 0; i < n; i++){
			ll x,y;
			cin>>x>>y;
			sorted[i] = cl(x,y);
		}
		cmparg = sorted[0];
		sort(sorted+1, sorted+n,cmp);
		int i;
		for(i = 1; i < n-1; i++){
			if(collinear(sorted[0], sorted[i], sorted[i+1])) break;
			else rest.insert(sorted[i]);
		}
		line1.insert(sorted[0]);
		if(i != n-1)line1.insert(sorted[i]);
		for(i++; i < n && collinear(sorted[0], sorted[i-1],sorted[i]); i++){
			line1.insert(sorted[i]);
		}
		for(; i < n; i++) rest.insert(sorted[i]);

		n = rest.size();
		if(n == 0){
			cout<<"YES\n";
			continue;
		}
		i = 0;
		for(auto j:rest){
			sorted[i++] = j;
		}
		rest.clear();
		cmparg = sorted[0];
		sort(sorted+1, sorted+n,cmp);
		for(i = 1; i < n-1; i++){
			if(collinear(sorted[0], sorted[i], sorted[i+1])) break;
			else rest.insert(sorted[i]);
		}
		line2.insert(sorted[0]);
		if(i != n-1)line2.insert(sorted[i]);
		for(i++; i < n && collinear(sorted[0], sorted[i-1],sorted[i]); i++){
			line2.insert(sorted[i]);
		}
		for(; i < n; i++) rest.insert(sorted[i]);

		n = rest.size();
		if(n != 0){
			i = 0;
			for(auto j:rest){
				sorted[i++] = j;
			}
			rest.clear();
			cmparg = sorted[0];
			sort(sorted+1, sorted+n,cmp);
			for(i = 1; i < n-1; i++){
				if(collinear(sorted[0], sorted[i], sorted[i+1])) break;
				else rest.insert(sorted[i]);
			}
			line3.insert(sorted[0]);
			if(i != n-1)line3.insert(sorted[i]);
			for(i++; i < n && collinear(sorted[0], sorted[i-1],sorted[i]); i++){
				line3.insert(sorted[i]);
			}
			for(; i < n; i++) rest.insert(sorted[i]);
		}
		if(rest.size()){
			cout<<"NO\n";
			continue;
		}
		int cool = 1;
		cout<<line1.size()<<'\n';
		for(auto j: line2) cout<<j<<' ';
		cout<<'\n';
		cout<<line2.size()<<'\n';
		cout<<line3.size()<<'\n';
		if(line1.size() > 1){
			cl a1 = *line1.begin();
			cl a2 = *(--line1.end());

			cl b1 = *line2.begin();
			cl b2 = *(--line2.end());
			if(cross(b1-a1, a2-a1) > 0ll && cross(b2-a1, a2-a1) < 0ll) cool = 0;
			if(cross(b1-a1, a2-a1) < 0ll && cross(b2-a1, a2-a1) > 0ll) cool = 0;
			if(line2.size() > 1 && cross(b1-b2, a1-a2) == 0ll) cool = 0;

			if(line3.size()){
				b1 = *line3.begin();
				b2 = *(--line3.end());
				if(cross(b1-a1, a2-a1) > 0ll && cross(b2-a1, a2-a1) < 0ll) cool = 0;
				if(cross(b1-a1, a2-a1) < 0ll && cross(b2-a1, a2-a1) > 0ll) cool = 0;
				if(line3.size() > 1 && cross(b1-b2, a1-a2) == 0ll) cool = 0;
			}

		}
		if(line2.size() > 1){
			cl a1 = *line2.begin();
			cl a2 = *(--line2.end());

			if(line1.size()){
				cl b1 = *line1.begin();
				cl b2 = *(--line1.end());
				if(cross(b1-a1, a2-a1) > 0ll && cross(b2-a1, a2-a1) < 0ll) cool = 0;
				if(cross(b1-a1, a2-a1) < 0ll && cross(b2-a1, a2-a1) > 0ll) cool = 0;
				if(line1.size() > 1 && cross(b1-b2, a1-a2) == 0ll) cool = 0;
			}

			if(line3.size()){
				cl b1 = *line3.begin();
				cl b2 = *(--line3.end());
				if(cross(b1-a1, a2-a1) > 0ll && cross(b2-a1, a2-a1) < 0ll) cool = 0;
				if(cross(b1-a1, a2-a1) < 0ll && cross(b2-a1, a2-a1) > 0ll) cool = 0;
				if(line3.size() > 1 && cross(b1-b2, a1-a2) == 0ll) cool = 0;
			}

		}
		if(line3.size() > 1){
			cl a1 = *line3.begin();
			cl a2 = *(--line3.end());

			if(line1.size()){
				cl b1 = *line1.begin();
				cl b2 = *(--line1.end());
				if(cross(b1-a1, a2-a1) > 0ll && cross(b2-a1, a2-a1) < 0ll) cool = 0;
				if(cross(b1-a1, a2-a1) < 0ll && cross(b2-a1, a2-a1) > 0ll) cool = 0;
				if(line1.size() > 1 && cross(b1-b2, a1-a2) == 0ll) cool = 0;
			}

			if(line2.size()){
				cl b1 = *line2.begin();
				cl b2 = *(--line2.end());
				if(cross(b1-a1, a2-a1) > 0ll && cross(b2-a1, a2-a1) < 0ll) cool = 0;
				if(cross(b1-a1, a2-a1) < 0ll && cross(b2-a1, a2-a1) > 0ll) cool = 0;
				if(line2.size() > 1 && cross(b1-b2, a1-a2) == 0ll) cool = 0;
			}

		}
		if(cool) cout<<"YES\n";
		else cout<<"NO\n";


	}
	return 0;
}
