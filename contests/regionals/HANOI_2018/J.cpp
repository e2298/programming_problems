#include<bits/stdc++.h>
using namespace std;

int main(void){
	int n,m;
	while(cin>>n>>m && m!=-1){
		if(m == n){
			cout<<"YES\n";
			for(int i = 1; i < n; i++){
				cout<<i<<' '<<i+1<<'\n';
			}
			cout<<n<<' '<<1<<'\n';
		}
		else if ((n*(n-1))/2 == m){
			cout<<"YES\n";
			for(int i = 1; i < n; i++){
				for(int j = i + 1; j <= n; j++){
					cout<<i<<' '<<j<<'\n';
				}
			}
		}
		else cout<<"NO\n";
	}
	return 0;
}
