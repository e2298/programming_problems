#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;

int seen[100][100];
int r,c,n;

int gud(int x,int y){
	return x >=0 && x <r && y >=0 && y < c;
}


int main(void){
	cin>>r>>c>>n;
	set<ii> weak;
	int x,y;
	while(n--){
		cin>>x>>y;
		x--;
		y--;
		weak.insert(ii(x,y));
	}
	queue<ii> q;
	for(auto i:weak){
		q.push(i);
		seen[i.first][i.second] = 1;
	}
	int r = 0;
	int left = weak.size();
	int next = 0;
	while(q.size()){
		ii c = q.front(); q.pop();
		x = c.first;
		y = c.second;
		left --;
		int xx[] = {1,0,-1,0};
		int yy[] = {0,1,0,-1};
		for(int i = 0; i < 4; i++){
			int nx = x+xx[i];
			int ny = y+yy[i];
			if(gud(nx,ny) && !seen[nx][ny]){
				next++;
				q.push(ii(nx,ny));
				seen[nx][ny] = 1;
			}
		}
		if(!left){
			r++;
			left = next;
			next = 0;
		}	
	}

	cout<<r<<'\n';


	return 0;
}
