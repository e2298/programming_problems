#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

vi graph[1000000];
int seen[1000000];
int seen2[1000000];
int kanc[1000000];
int kanc2[1000000];
int d[1000000];
int r[1000000];
int r2[1000000];
int indepth[1000000];
int k;
int n;

void solve(int i){
	int rr = 1;
	for(auto j: graph[i]){
		solve(j);
		rr += r[j];
		rr-= kanc[j];
	}
	r[i] = rr;
}

void solve2(int i){
	if(r2[i]) return;
	int rr = 1;
	for(auto j: graph[i]){
		solve2(j);
		rr += r2[j];
		rr-= kanc2[j];
	}
	r2[i] = rr;
}

void anc(int i, int depth){
	indepth[depth] = i;
	if(depth-k >= 0) kanc[indepth[depth-k]]++;
	for(auto j:graph[i]) anc(j, depth+1);
}

void anc2(int i, int depth){
	if(seen2[i]) return;
	seen2[i] = 1;
	indepth[depth] = i;
	if(depth-k >= 0) kanc2[indepth[depth-k]]++;
	for(auto j:graph[i]) anc2(j, depth+1);
}


void dfs(int i, int run, int depth){
	d[depth] = i;
	seen[i] = run;
	for(auto j: graph[i]){
		if(seen[j] == run){
			graph[i].erase(find(graph[i].begin(), graph[i].end(), j));
			int art = min(depth, k);
			graph[n].push_back(j);
			for(int ii = 1; ii < art; ii++) graph[n+ii].push_back(n+ii-1);
			anc(n+art-1, 0);
			solve(n+art-1);
			if(depth < k){
				int tk = k;
				k = tk - depth -1;
				for(int ii = 0; ii < depth; ii++){
					anc2(d[depth-ii], 0);				
					solve2(d[depth-ii]);
					r[d[depth-ii]] += r[n+ii] - ((ii+1)  + r2[d[depth-ii]]);
				}

				k = tk;	
			}
			else{
				for(int ii = 0; ii < k; ii++){
					r[d[depth-ii]] += r[n+ii]- (ii+1);
				}	
			}
			for(int ii = 0; ii < art; ii++) graph[n+ii].clear();
			return;
		}
		else if (!seen[j]) dfs(j, run, depth+1);
	}

}

int main(void){
	cin>>n>>k;
	for(int i = 0; i < n; i++){
		int t;
		cin>>t;
		t--;
		graph[t].push_back(i);
	}

	int run = 1;
	for(int i = 0; i < n; i++){
		if(!seen[i]) dfs(i, run++,0);
		/*cout<<"*****************\n";
		for(int j = 0; j < n; j++){
			cout<<j<<": ";
			for(auto k:graph[j])cout<<k<<' ';	
			cout<<'\n';
		}
		cout<<'\n';*/
	}
	for(int i = 0; i < n; i++) cout<<r[i]<<'\n';			

	return 0;
}
