#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

int s1[500000];
int a1[500000];
int b1[500000];
int s2[500000];
int a2[500000];
int b2[500000];
int r2[500000];
int breaks1[500000];
int breaks2[500000];
int capacity[500000];
int ties1[500000];
int ties2[500000];

bool sort1(int i, int j){
	if(a1[i] == a1[j]) return b1[i] < b1[j];
	return a1[i] < a1[j];
}
bool so1(int i, int j){
	return a1[i] < a1[j];
}
bool sort2(int i, int j){
	if(a2[i] == a2[j]) return b2[i] < b2[j];
	return a2[i] < a2[j];
}
bool so2(int i, int j){
	return a2[i] < a2[j];
}

int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		s1[i] =i;
		cin>>a1[i];	
	}
	for(int i = 0; i < n; i++){
		cin>>b1[i];	
	}
	for(int i = 0; i < n; i++){
		s2[i] =i;
		cin>>a2[i];	
	}
	for(int i = 0; i < n; i++){
		cin>>b2[i];	
	}

	sort(s1, s1+n, sort1);
	sort(s2, s2+n, sort2);

	int breaksn1 = 0;
	int breaksn2 = 0;
	int lst = 0;
	for(int i = 0; i < n; i++){
		if(a1[s1[i]] != lst){
			lst = a1[s1[i]];
			capacity[breaksn1]++;
			breaks1[breaksn1] = i;
			breaksn1++;
		}	
		else capacity[breaksn1-1]++;
	}
	breaks1[breaksn1++] = n;
	lst = 0;
	for(int i = 0; i < n; i++){
		if(a2[s2[i]] != lst){
			lst = a2[s2[i]];
			breaks2[breaksn2++] = i;
		}	
	}
	breaks2[breaksn2++] = n;
	set<ii>s;
	int prv=0, nxt=0;
	int prev = 0;
	int ties = 0;
	int valid = 1;
	for(int k = 0; k < breaksn2 && valid; k++){
		prv = nxt;
		while(breaks1[nxt] < breaks2[k]) nxt++;
		for(int i = breaks1[prv]; i < breaks1[nxt]; i++) s.insert(ii(b1[s1[i]], i));
		for(int i = prev; i < breaks2[k]; i++){
			auto t = s.upper_bound(ii(b2[s2[i]], n));
			if(t == s.end()){
				valid = 0;
				break;
			}
			ties1[ties] = s1[(*t).second];
			ties2[ties] = lower_bound(breaks1, breaks1+breaksn1, (*t).second)- breaks1;	
			if(breaks1[ties2[ties]] != (*t).second) ties2[ties]--;
			s.erase(t);
			ties++;
		}

		prev = breaks2[k];
	}

	memset(s1, 0, n*sizeof(int));
	for(int i = 0; i < n; i++){
		int br = ties2[i];	
		if(!capacity[br]){valid = 0; break;}
		int plc = breaks1[br+1] - capacity[br];
		capacity[br]--;
		s1[plc] = ties1[i];
		r2[plc] = s2[i];
	}

	if(!is_sorted(s1, s1+n, so1)) valid = 0;
	if(!is_sorted(r2, r2+n, so2)) valid = 0;
	int cz = 0;
	for(int i =0; i < n; i++){
		if(!s1[i]){cz++;}
		if(!r2[i]){cz++;}
		if(!(b1[s1[i]] > b2[r2[i]])) valid = 0;
	}
	if(cz != 2) valid = 0;

	if(valid){
		for(int i = 0; i < n; i++){
			cout<<s1[i]+1<<' ';
		}
		cout<<'\n';
		for(int i = 0; i < n; i++){
			cout<<r2[i]+1<<' ';
		}
		cout<<'\n';
	}
	else cout<<"impossible";
	return 0;
}
