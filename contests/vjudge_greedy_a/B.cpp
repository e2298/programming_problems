#include<bits/stdc++.h>
using namespace std;
typedef pair<double, double> pi;
typedef set<pi> sp;


int main(void){
	int n;
	double d;
	int cas = 1;
	while(cin>>n>>d && n != 0){
		sp points;
		for(int i = 0; i < n; i++){
			pi in;
			cin>>in.first>>in.second;
			points.insert(in);
		}
		int r = 0;
		int can = 1;
		for(sp::iterator i = points.begin(); i != points.end(); i++){
			r++;
			sp to_del;
			double y = (*i).second;
			double x = sqrt(d*d - y*y);
			if(y > d){ can = 0; break;}
			x += (*i).first;
			sp::iterator j = i;
			j++;
			while(j != points.end()){
				double xx = (*j).first;
				double yy = (*j).second;
				if(xx > x+d) break;
				if(sqrt((xx - x)*(xx-x) + yy * yy) <= d) to_del.insert((*j));
				j++;
			}
			for(auto k:to_del) points.erase(k);
		}
		if(can)
			cout<<"Case "<<cas++<<": "<<r<<'\n';
		else cout<<"-1\n";
		
	}

	return 0;
}
