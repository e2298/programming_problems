#include<bits/stdc++.h>
using namespace std;

int w[10];

int main(void){
	int c,s;
	int cas = 1;
	while(cin>>c>>s){
		double tot = 0;
		for(int i = 0; i < s; i++){
			cin>>w[i];
			tot += w[i];
		}
		tot /= (double)c;
		for(int i = s; i < 2*c; i++) w[i] = 0;
		sort(w,w+2*c);

		double inv = 0;
		printf("Set #%d\n", cas++);
		for(int i = 0; i < c; i++){
			inv += abs(w[i] + w[2*c-1-i] - tot);
			printf(" %d:", i);
			if(!w[i]){
				if(w[2*c-1-i])
					printf(" %d\n", w[2*c-1-i]);
				else printf("\n");
			}
			else printf(" %d %d\n",w[i], w[2*c-1-i]);
		}
		printf("IMBALANCE = %.5lf\n\n", inv);
	}
	return 0;
}
