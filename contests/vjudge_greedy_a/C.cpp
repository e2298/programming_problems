#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

ii c[100000];
ii r[100000];

int main(void){
	int t;
	cin>>t;
	while(t--){
		int m;
		cin>>m;
		int a = -1, b = -1;
		int n = 0;
		cin>>a>>b;
		while(a || b){
			c[n++] = ii(a,b);
			cin>>a>>b;
		}
		sort(c, c+n);
		
		int can = 1;
		int lst = 0;
		int mx = -1;
		int mxx = 0;
		int nn = 0;
		int i = 0;
		while(i < n && c[i].first <= 0){
			if(c[i].second > mx){
				mx = c[i].second;
				mxx = i;
			}
			i++;
		}
		if(i){
			lst = mx;
			if(mx < 0) can = 0;
			r[nn++] = c[mxx];
		}
		while(i < n && lst < m){
			mx = -1;
			while((i < n) && c[i].first <= lst){
				if(c[i].second > mx){mx = c[i].second; mxx = i;}
				i++;
			}
			if(mx <= lst) {
				can = 0;
				break;
			}
			lst = c[mxx].second;
			r[nn++] = c[mxx];
		}
		if(can) {
			cout<<mxx<<'\n';
			for(i = 0; i < mxx; i++){
				cout<<r[i].first<<' '<<r[i].second<<'\n';
			}	
		}
		else cout<<"0\n";
		if(t) cout<<'\n';
	}
	return 0;
}
