#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll c[1000];


int main(void){
	ll t;
	cin>>t;
	while(t--){
		ll n;
		cin>>n;
		for(int i = 0; i < n; i++){
			cin>>c[i];
		}
		ll r = 0;
		ll t = 0;
		for(int i = 0; i < n; i++){
			int can = 1;
			if(t + c[i] >= 2*c[n-1]) break;
			ll curr = c[i];
			for(int j = i-1; j > -1; j--){
				can = 0;
				if(c[j] < curr){
					if(2*c[j] <= curr){
						can = 1;
						break;
					}
					else curr -= c[j];
				}
			}
			if(can){
				r++;
				t += c[i];
			}
		}


		cout<<r<<'\n';
	}

	return 0;
}
