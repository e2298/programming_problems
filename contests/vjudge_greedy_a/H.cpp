#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	cin>>t;
	int cas = 1;
	while(t--){
		int n;
		cin>>n;
		ll r = 0;
		string in;
		cin>>in;
		for(int i = 0; i < n; i++){
			if(in[i] == '.'){
				r++;
				i+=2;
			}
		}
		printf("Case %d: %ld\n", cas++, r);
	}
}
