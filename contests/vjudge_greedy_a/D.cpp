#include<bits/stdc++.h>
using namespace std;
typedef pair<double, double> pi;

pi a[10000];

int main(void){
	int n;
	double l,w;
	while(cin>>n>>l>>w){
		double ww = w;
		for(int i = 0; i < n; i++){
			double r;
			double lw,c;
			cin>>c>>r;
			if(r < ww/2) r = ww/2;
			else{
				lw = sqrt(r*r - (ww/2)*(ww/2));
				a[i].first = max(0.0, (c-lw));
				a[i].second = -min(l, (c+lw));
			}
		}	
		sort(a,a+n);
		for(int i = 0; i < n; i++)a[i].second = -a[i].second;
		int can = 1;
		int i = 0;
		double lst;
		int r = 0;
		while(lst < l && i < n){
			int m = -1;
			double mx = a[i].second;
			for(int j = 0; j < n && a[j].first <= a[i].second; j++){
				if(a[j].second >= mx){
					m = j;
					mx = a[j].second;
				}
			}	
			if(m == -1){
				can = 0;
				break;
			}
			r++;
			i = m;
			lst = a[i].second;
		}
		if(lst < l) can = 0;


		if(can) cout<<r<<'\n';
		else cout<<"-1\n";
	}

	return 0;
}
