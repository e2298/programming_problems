#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int dp[100][100*500+1];
int seen[100][100*500+1];
int v[100];

int solve(int i, int n){
	if(n < 0) return 0;
	if(n == 0) return 1;
	if(i < 0) return 0;
	if(seen[i][n]) return dp[i][n];
	seen[i][n] = 1;

	return dp[i][n] = max(solve(i-1, n - v[i]), solve(i-1, n));
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		memset(dp, 0, sizeof(dp));
		memset(seen, 0, sizeof(seen));
		int n;
		cin>>n;
		int tot = 0;
		for(int i = 0; i < n; i++){
			cin>>v[i];
			tot += v[i];
		}
		int md = 1000000000;
		for(int i = 0; i <= tot; i++){
			if(solve(n-1, i)){
				md = min(md, abs(tot- i - i));
			}
		}
		cout<<md<<'\n';

	}
	return 0;
}
