#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
int casos;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		ll r = 0;
		ll a,b,c;
		while(n--){
			cin>>a>>b>>c;
			r += a*c;
		}
		cout<<r<<'\n';
	}
	return 0;
	
}
