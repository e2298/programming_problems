#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int t;
	cin>>t;
	string in;
	ll tot = 0;
	while(t--){
		cin>>in;
		if(in == "report") cout<<tot<<'\n';
		else {
			ll tt;
			cin>>tt;
			tot += tt;
		}
	}
	return 0;
}
