#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	
	int n,x;
	while(scanf("%i",&n) != EOF && n != 0){
		int r,s; r = s = 0;
		for(int i = 0; i < n; i++){
			scanf("%i",&x);
			s += x;
			if(s>r) r = s;
			if(s < 0) s = 0;

		}
		if(!r) printf("Losing streak.\n");
		else printf("The maximum winning streak is %d.\n",r);

	}
	
	return 0;
}
