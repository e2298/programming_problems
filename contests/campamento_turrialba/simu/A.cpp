#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll xi[100010];
int n,c;

int solve(ll d){
	ll lst = xi[0];
	ll g = c-1;
	for(int i = 1; i < n && g; i++){
		if(xi[i] - lst >= d){
			lst = xi[i];
			g--;
		}
	}
	return !g;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		cin>>n>>c;
		for(int i = 0; i < n; i++){
			cin>>xi[i];
		}
		sort(xi, xi+n);

		ll lo = 0, up = xi[n-1];
		while(lo < up){
			ll cu = (up + lo + 1)/2;
			if(solve(cu)){
				lo = cu;
			}
			else up = cu-1;
		}
		cout<<lo<<'\n';
	}
	return 0;
}
