#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
int comer[50050];
int procesado[50050];
int n, k;
int d, x, y;
int malas;

struct UnionFind{
	vector<int>f;
	void init(int n){f.clear(); f.insert(f.begin(), n, -1);}
	int comp(int x){return (f[x]==-1?x:f[x]=comp(f[x]));}
	bool join(int i, int j){
		bool con=comp(i)==comp(j);
		if(!con){
		        f[comp(i)]=comp(j);
		        if(comer[comp(i)]==-1)comer[comp(i)]=comer[comp(j)];
		}
		return con;
	}
	bool sameset(int i, int j){
		return comp(i)==comp(j);
	}
};

int bloques, lineas;
UnionFind uf;
int main(void){
        cin>>bloques;
        while(bloques--){
                cin>>n>>k;
                malas=0;
                memset(procesado, -1, sizeof procesado);
                memset(comer, -1, sizeof comer);
                uf.init(n+10);
                while(k--){
                        cin>>d>>x>>y;
                        if(x>n || y>n){
                                malas++;
                                continue;                
                        }
                        if(d==1){
                                if(procesado[x]!=-1 && procesado[y]!=-1){
                                        if(!uf.join(x, y))malas++;
                                }
                                else{
                                        procesado[x]=0;
                                        procesado[y]=0;
                                        uf.join(x, y);
                                }
                        }
                        else{
                                if(x==y){malas++; continue;}
                                if(uf.sameset(x, y)){malas++; continue;}
                                if(procesado[x]==-1 || procesado[y]==-1){
                                        comer[x]=y;
                                        procesado[x]=0;
                                        procesado[y]=-1;
                                }
                                else{
                                        int a=uf.comp(x);
                                        int b=uf.comp(y);
                                        if(comer[uf.comp(y)]==uf.comp(x))malas++;
                                        
                                        else if(comer[uf.comp(x)]!=-1){
                                                if(comer[uf.comp(x)]!=uf.comp(y))malas++;
                                        }
                                        else comer[uf.comp(x)]=uf.comp(y);
                                }
                        }
                
                }
                cout<<malas<<"\n";
        }
	return 0;
}
