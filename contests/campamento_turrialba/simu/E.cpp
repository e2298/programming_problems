#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

bool dp[20][1<<25];
bool seen[20][1<<25];
ll b, l;
ll ng;

ll pow(ll b, ll e){
	if(e){
		ll r = pow(b, e/2);
		return (e&1)?r*r*b: r*r;

	}else return 1;
}

int solve(int i, ll n){
	if(n == 0) return 1;
	if(i < 0) return 0;
	if(seen[i][n+((1<<24))]) return dp[i][n+((1<<24))];
	seen [i][n+((1<<24))] =1;
	int r = 0;

	for(int j = 0; j < b; j ++){
		ll t = pow(b, i);
		t *= (l + j);
		if(abs(n-t) >= 1<<24-1) continue;
		r = r || solve(i-1, n-t);
	}

	return dp[i][n+((1<<24))] = r;
}

int main(void){
	ll n;
	int cas = 1;
	while(cin>>b>>l>>n){
	ng = n;
		memset(dp,0,sizeof(dp));
		memset(seen,0, sizeof(seen));
		int r = 0;
		for(int i = 0; i < 15; i++){
			r = r || solve(i, n);
		}
		cout<<"CASE# "<< cas<<":\n";
		cas++;
		if(r){
			int i = -1;
			while(!dp[++i][n+((1<<24))]);
			int nn = n;
			while(i){
				int j;
				for(j = 0; j < b; j ++){
					ll t = pow(b, i);
					t *= (l + j);
					if(dp[i-1][nn-t+(1<<24)]) {
						nn -= t;
						break;
					}
				}
				if(l + j >= 0) cout<<'+'<<l+j;
				else cout << l+j;
				cout<<'*'<<b<<'^'<<i;
				i--;
			}
			if(nn >= 0) cout<<'+'<<nn;
			else cout<<nn;
			cout<<'*'<<b<<"^0 = "<<n<<'\n';
		}
		else{
			cout<<"NOT REPRESENTABLE\n";
		}
	}
	return 0;
}
