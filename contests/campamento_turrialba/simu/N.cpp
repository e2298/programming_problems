#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll dp [60][60];
int seen [60][60];
ll in [60];
int n;
int cas = 1;

ll solve(ll a, ll b){
	if(seen[a][b] == cas) return dp[a][b];
	seen[a][b] = cas;
	int i = a+1;
	if(i == b ) return dp[a][b] = 0;
	ll r = 1ll<<62;
	while(i < b){
		r = min(r, solve(a, i)+ solve(i, b) + in[b] - in[a]);
		i++;
	}
	//cout<<a<<' '<<b<<' '<<r<<'\n';

	return dp[a][b] = r;	
}
	
int main(void){
	ll l;
	in[0] = 0;
	while(cin>>l && l){
		cas++;
		cin>>n;
		in[n+1] = l;
		for(int i = 1; i <= n; i++){
			cin>>in[i];
		}		
		cout<<"The minimum cutting is "<<solve(0,n+1)<<".\n";
	}

	return 0;
}
