#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int n;
	int o = 0;
	int r = 0;
	cin>>n;
	while(n--){
		int i;
		cin>>i;
		if(i < 0 && o == 0) r++;
		else if( i < 0) o--;
		else o += i;
	}
	cout<<r;
	return 0;
}
