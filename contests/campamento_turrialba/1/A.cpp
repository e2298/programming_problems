#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int n,h;
	cin>>n>>h;
	int t;
	int r = 0;
	while(n--){
		cin>>t;
		if(t > h) r++;
		r++;
	}
	cout<<r;
	return 0;
}
