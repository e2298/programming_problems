#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	ll n, t, r;
	cin>>n;
	r = 0ll;
	while(n--){
		cin>>t;
		r += t;
	}
	cout<<r;
	return 0;
}
