#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	string s;
	cin>>s;
	int r = 0;
	int lst = 'a';
	for(auto i:s){
		r += min(min(abs(lst-i), 'z'-lst + i-'a'+1), lst-'a' + 'z'+1-i);
		lst = i;
	}
	cout<<r;
	return 0;
}
