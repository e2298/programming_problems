#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<vi> vvi;

int main(){
	int a,b,n;
	cin>>a>>b;
	while(a||b){
		cin>>n;
		vii nums(n);
		for(int i = 0; i<n;i++){
			int c;
			cin>>c;
			nums[i] = ii(c,i);
		}
		sort(nums.begin(),nums.end());
		for(int i = 1; i<n;i++) nums[i].first += nums[i-1].first;
		
		auto ta = lower_bound(nums.begin(),nums.end(), ii(a,0));
		if((*ta).first == a) ta++;
		auto tb = lower_bound(nums.begin(),nums.end(),ii(b,0));
		if((*tb).first == b) tb++;
		
		if(((*(nums.end()-1)).first - (*(ta-1)).first) <= b){
			cout<<ta-nums.begin();
			vi r (ta-nums.begin());
			int j = 0;
			for(auto i = nums.begin(); i != ta; i++){
				r[j++] = (*i).second+1;
			}
			sort(r.begin(),r.end());
			for(auto i: r)cout<<' '<<i;
		}
		else if(((*(nums.end()-1)).first - (*(tb-1)).first) <= a){
			cout<<nums.end()-tb;
			vi r (nums.end()-tb);
			int j = 0;
			while(tb != nums.end()){
				r[j++] =(*tb).second+1;
				tb++;
			}
			sort(r.begin(),r.end());
			for(auto i: r)cout<<' '<<i;
		}
		else cout<<"Impossible to distribute";
		cout<<'\n';
		
		cin>>a>>b;
	}

	return 0;
}
