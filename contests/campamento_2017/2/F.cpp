#include <bits/stdc++.h>
using namespace std;

typedef pair<long, long> ii;
typedef vector<ii> vii;
typedef vector<long> vi;
typedef vector<vi> vvi;
typedef set<ii> sii;

int main(void){
	long n;
	vi nums;
	long r = 0,t;
	sii left, right;

	cin>>n;
	nums = vi(n);

	for(int i = 0; i <  n; i++){
		cin>>nums[i];
		right.insert(ii(nums[i],i));
	}
	for(int i = 0; i< n; i++){
		long c = nums[i],xx;
		int a,b = -1;
		left.insert(ii(c,i));
		right.erase(right.lower_bound(ii(c,i)));

		//cout<<left.size()<<' '<<right.size()<<'\n';

		auto it = left.lower_bound(ii(c,-1));

		
		if(it == left.begin()) a = 0;
		else{
			it--;
			a = (*it).second;
			a++;
		}

		it = right.lower_bound(ii(c,-1));


		if(it == right.begin()) b = n-1;
		else{
			xx = (*(--it)).first;
			while((it != right.begin()) && ((*(--it)).first == xx));
			it ++;
			b = (*it).second;
			b--;
		}

		cout<<c<<' '<<a<<' '<<b<<'\n';

		t = (b-a+1)*c;

		r = max(r,t);
	}

	cout<<r;

	return 0;
}
