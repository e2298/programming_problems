#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

vi bit;
int n;

void bit_inc(int i){
	for(;i < (n+2); i += i&(-i)){
		bit[i]++;
	}
}

int bit_q(int i){
	int r = 0;
	for(;i; i -= i&(-i)){
		r += bit[i];
	}
	return r;
}

int main(void){
	int t, i;
	long r;
	string tmp;
	
	cin>>t;
	while(t--){
		map<string,int> words;
		r = 0;
		cin>>n;
		bit = vi(n+5);

		for(int i = n; i; i--){
			cin>>tmp;
			words[tmp] = i;
		}
		for(int i = 0; i<n; i++){
			cin>>tmp;
			r += bit_q(words[tmp]);
			bit_inc(words[tmp]);
		}
		cout<<r<<'\n';
	}

	return 0;
}
