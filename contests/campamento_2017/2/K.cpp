#include <bits/stdc++.h>
using namespace std;

typedef vector<long> vi;
typedef vector<vi> vvi;


int main(void){
	long n;

	cin>>n;
		vvi in(n,vi(n));
		//for(auto& i:in) for(auto& j:i) cin>>j;
		for(int i = 0; i<n; i++){
			for(int j = 0; j<n; j++){
				cin>>in[i][j];
			}
		}


		for(long i = 0; i<n;i++)
		  for(long j = 1; j<n;j++)
			in[i][j] += in[i][j-1];
		for(long i = 1; i<n;i++)
		  for(long j = 0; j<n;j++)
			  in[i][j] += in[i-1][j];

		long m = in[0][0];


		for(long i = 0; i<n;i++){
			m = max(m, in[0][i]);
			m = max(m, in[i][0]);
			for(long j = 0; j<i; j++){
				long t = in[0][i] - in[0][j];
				m = max(m,t);
				t = in[i][0] - in[j][0];
				m = max(m,t);
			}
		}

		for(long i = 1; i<n; i++){
			for(long j = 1; j<n; j++){
				long t = in[i][j];
				m = max(m,t);

				for(long ii = 0; ii<i; ii++){
					t = in[i][j]-in[ii][j];
					m = max(m,t);
				}	
				for(long jj = 0; jj<j; jj++){
					t = in[i][j]-in[i][jj];
					m = max(m,t);
				}

				for(long ii = 0; ii<i;ii++){
					for(long jj = 0; jj<j; jj++){
						long t = in[i][j];
						t += in[ii][jj];
						t -= in[ii][j];
						t -= in[i][jj];
						m = max(m, t);
					}
				}
			}
		}

		cout<<m;
	

	return 0;
}
