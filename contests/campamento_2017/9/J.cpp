#include <bits/stdc++.h>
using namespace std;
typedef complex<double> cd;

#define pi 3.14159265358979323846D
#define to_rad(a) fmod(fmod((a*pi/180),(2*pi))+(2*pi),(2*pi))

int main(void){
	int t,n;
	double a,b;
	cin>>t;
	while(t--){
		cin>>n;
		cd curr;
		double angle = pi/2;
		while(n--){
			cin>>a>>b;
			angle += to_rad(a);
			curr = curr + polar(b,angle);
		}
		cout<<curr.real()<<' '<<curr.imag()<<'\n';
	}

	return 0;
}
