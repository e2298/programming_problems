#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef complex<int> cd;

int sign(int a){
	if(a){
		if(a<0) return -1;
		else return 1;
	}
	return 0;
}

int main(){
	cd p[2][2];
	int abc[2][3];	
	int n,m;
	int t;
	cin>>t;
	while(t--){
		for(int i = 0; i<2;i++){
			for(int j = 0; j<2;j++){
				cin>>n>>m;
				p[i][j] = cd(n,m);	
			}
		}
		for(int i = 0; i<2;i++){
			int a,b,c;
			cd perp;
			perp = (p[i][0]-p[i][1])* cd(0,1);
			a = perp.real();
			b = perp.imag();
			c = a*p[i][0].real() + b*p[i][0].imag();
			abc[i][0] = a;
			abc[i][1] = b;
			abc[i][2] = c;
		}

		if(arg(p[0][0]-p[0][1]) == arg(p[1][0]-p[1][1])){ 
			if(abc[0][2] == abc[1][2]) cout<<"SEGMENT";
			else cout<<"NO";
		}
		else{	
			int t,s,d;
			t = (p[1][1].real()-p[1][0].real())*(p[1][0].imag()-p[0][0].imag());
			t -= (p[1][1].imag()-p[1][0].imag())*(p[1][0].real()-p[0][0].real());
			
			s = (p[0][1].real()-p[0][0].real())*(p[1][0].imag()-p[0][0].imag());
			s -= (p[0][1].imag()-p[0][0].imag())*(p[1][0].real()-p[0][0].real());
			
			d = (p[1][1].real()-p[1][0].real())*(p[0][1].imag()-p[0][0].imag());
			d -= (p[1][1].imag()-p[1][0].imag())*(p[0][1].real()-p[0][0].real());
			
			if((d<0) && ((t >=0) || (s >=0))) cout<<"NO";
			else if (((t < 0) || (s < 0)) && (d>0))cout<<"NO";
			else{
				if(d<0){
					d *=-1;
					s *=-1;
					t *=-1;
				}
				if ((d >= t) && (d >= s)) cout<<"POINT";
				else cout<<"NO";
			}

		}cout<<'\n';

	}
	
	return 0;
}
