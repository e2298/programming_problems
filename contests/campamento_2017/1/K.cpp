#include <bits/stdc++.h>

using namespace std;

double p,q,r,s,t,u;
double e = 10E-10;

double fun(double x){
	double res = p*exp(-x);
	res += q*sin(x);
	res += r*cos(x);
	res += s*tan(x);
	res += t*(x*x);
	res += u;
	
	return res;
}

int main(void){
	while(cin>>p>>q>>r>>s>>t>>u){
		double z = fun(0), o = fun(1);
		if((z>=0.0) && (o <= 0.0)){
			double l = 0.0, h = 1.0,c = 0.5;
			double f = fun(c);
			while(fabs(f) > e){
				if(f > 0.0){
					l = c;
					c = (h-l)/2+l;
				}
				else{
					h = c;
					c = (h-l)/2+l;
				}
				f = fun(c);
			}
			char r[10];
			sprintf(r,"%1.4lf\n",c);
			cout<<r;
		}
		else cout<<"No solution\n";
	}

	return 0;
}
