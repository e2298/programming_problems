#include <bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;

vvi dp;
vi w;
vi last;

long knap(int i, long c){
	if(c == 0) return 0;
	if(c < 0) return (3L << 62);
	if(i < 0) return 0;
	if(dp[i][c]) {
		return dp[i][c];
	}
	long take = knap(i-1,c-w[i])+w[i];
	long leave = knap(i-1,c);
	if(take > leave){
		last[take] = i;
		return dp[i][c] = take;	
	}
	else return dp[i][c] = leave;
}


vi rec(long curr){
	vi r;
	while(curr){
		int t = last[curr];
		r.push_back(w[t]);
		curr -= w[t];
	}
	return r;
}

int main(void){
	long c,n,t,m;
	while(cin>>c>>n){
		w = vi(n);
		dp = vvi(n);
		m = 0;
		for(auto& i:dp){ 
			i = vi(c+1,0);
		}
		
		for(auto& i:w){
			cin>>i;
			m += i;
		}

		last = vi(m+1);

		long r= knap(n-1,c);
		vi rr = rec(r);
		for(auto i: rr) cout<<i<<' ';
		cout<<"sum:"<<r<<'\n';
	}

	return 0;
}
