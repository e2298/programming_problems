#include <bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;


vvi m;

int main(void){
	long t,d,n,a,b,c;
	cin>>t;
	while(t--){
		cin>>d>>n;
		m = vvi(1025,vi(1025));

		for(int i =0; i<n;i++){
			cin>>a>>b>>c;
			m[a][b] = c;
		}
		for(int i = 0; i< 1025; i++){
			for(int j = 1; j<1025; j++){
				m[i][j] += m[i][j-1];
			}
		}
		for(int i = 0; i < 1025; i++){
			for(int j = 1; j<1025; j++){
				m[j][i] += m[j-1][i];
			}
		}
		long mr = 0;
		long mx,my,tmp;
		mr = m[d*2][d*2];
		mx = my = d;

		for(int i = d*2+1; i < 1025; i++){
			tmp = m[i][d*2];
			tmp -= m[i-d*2-1][d*2];
			if(tmp > mr){
				mr = tmp;
				mx = i-d;
				my = d;
			}

			for(int j = d*2+1; j < 1025; j++){
				long tmp = m[i][j];
				tmp -= m[i-d*2-1][j];
				tmp -= m[i][j-d*2-1];
				tmp += m[i-d*2-1][j-d*2-1];	
				if(tmp > mr){
					mr = tmp;
					mx = i-d;
					my = j-d;
				}
			}
		} 
		cout<<mx<<' '<<my<<' '<<mr<<'\n';

	}

	return 0;
}
