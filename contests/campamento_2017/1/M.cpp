#include <bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;

set<int> r;

int main(void){
	int n;
	vi nums = {0,1,2,3,4,5,6,7,8,9};
	do{
		int t = 0;
		for(int i = 0; i < 5; i++){
			t*=10;
			t+= nums[i];
		}
		r.insert(t);
	}while(next_permutation(nums.begin(),nums.end()));

	cin>>n;
	while(n){
		bool sol = 0;
		for(auto d:r){
			int u = n*d;
			int uu = u;
			int dd = d;
			int digc=0;
			vi dig(10);
			for(int i = 0; (i<5) || uu;i++){
				dig[uu%10]++;
				dig[dd%10]++;
				uu/=10;
				dd/=10;
			}
			for(int i = 0; i<10;i++) if(dig[i] == 1) digc++;
			if(digc==10){
				cout<<setfill('0') << setw(5)<<u;
				cout<<" / "<<setfill('0') << setw(5)<<d;
				cout<<" = "<<n<<'\n';
	
				sol = 1;
			}
		}
		if(!sol)cout<<"There are no solutions for "<<n<<".\n";

		cin>>n;
		if(n)cout<<'\n';
	}
	return 0;
}
