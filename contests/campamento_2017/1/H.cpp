#include <bits/stdc++.h>
using namespace std;
typedef vector<long> vi;
typedef vector<vi> vvi;

vi segt;
vi lazy;

//l,u current bounds, p,q targets
long update(long i, long l, long u, long p, long q, long v){
	if(lazy[i] && (l == u)){
		segt[i] += lazy[i];
		lazy[i] = 0;
	}
	else if(lazy[i]){
		lazy[2*i] += lazy[i];
		lazy[2*i+1] += lazy[i];
		segt[i] += (u-l+1)*lazy[i];
		lazy[i] = 0;
	}
	if((l > q) || (u<p)) return segt[i];
	if(l == u) return segt[i] = segt[i] + v;
	if((l >= p) && (u <= q)){
		segt[i] += (u-l+1) * v;
		lazy[2*i] += v;
		lazy[2*i+1] += v;
		return segt[i];	
	}
	long cambio = u-l+1;
	cambio/=2;
	return segt[i] = 
	  update(i*2, l, l+cambio-1, p, q, v) + 
	  update(i*2+1, l+cambio, u, p, q, v);
}


long query(long i, long l, long u, long p, long q){
	if(lazy[i] && (l == u)){
		segt[i] += lazy[i];
		lazy[i] = 0;
	}
	else if(lazy[i]){
		lazy[2*i] += lazy[i];
		lazy[2*i+1] += lazy[i];
		segt[i] += (u-l+1)*lazy[i];
		lazy[i] = 0;
	}
	
	if((l > q) || (u<p)) return 0;
	if((l >= p) && (u <= q)){
		return segt[i];	
	}
	long cambio = u-l+1;
	cambio/=2;
	return
	  query(i*2, l, l+cambio-1, p, q) + 
	  query(i*2+1, l+cambio, u, p, q);

}

int main(void){
	long t,n,c,p,q,type;
	long v;
	cin>>t;
	while(t--){
		cin>>n>>c;
		if((n&(n-1))){
			n = n|(n>>16);
			n = n|(n>>8);
			n = n|(n>>4);
			n = n|(n>>2);
			n = n|(n>>1);
			n++;
		}
		
		segt = vi(n*2);
		lazy = vi(n*2);
		
		while(c--){
			cin>>type>>p>>q;
			//query
			if(type){
				cout<<query(1, 1, n, p, q)<<'\n';
			}
			//update
			else{
				cin>>v;
				update(1, 1, n, p, q, v);
			}
		}
	}

	return 0;
}
