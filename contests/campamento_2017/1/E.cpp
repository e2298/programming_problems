#include<bits/stdc++.h>
using namespace std;
	
int main(void){
	int t, w, a=0,b=0,l = 0, ia, ib;
	cin>>t;
	while(t--){
		cin>>ia>>ib;
		a+=ia;
		b += ib;
		int t = a-b;
		if(t > 0){
			if(t>l){
				l = t;
				w = 1;
			}
		}
		else{
			if(-t > l){
				l = -t;
				w = 2;
			}
		}
	}

	cout<<w<<' '<<l;

	return 0;
}
