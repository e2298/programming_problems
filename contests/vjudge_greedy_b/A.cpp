#include<bits/stdc++.h>
using namespace std;
typedef long long ll;


int s[1000];
ll w[1000];
ll t[1000];


ll c(int a, int b){
	if(w[a]*t[b] > w[b]*t[a]) return 1;	
	if(w[a]*t[b] < w[b]*t[a]) return 0;	
	return a < b;
}

int main(void){
	ll tt;
	cin>>tt;
	while(tt--){
		ll n;
		cin>>n;
		for(int i = 0; i < n; i++){
			s[i] = i;
			cin>>t[i]>>w[i];
		}
		sort(s,s+n, c);
		for(int i = 0; i < n-1; i++){
			cout<<s[i]+1<<' ';
		}
		cout<<s[n-1]+1;
		cout<<(tt?"\n\n":"\n");
	}
	return 0;
}
