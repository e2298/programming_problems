#include<bits/stdc++.h>
using namespace std;

long cost(long n){
	long r = 0;
	if(n > 1000000){
		r += (n-1000000) * 7;
		n = 1000000;
	} 
	if(n > 10000){
		r += (n-10000) * 5;
		n = 10000;
	}
	if(n > 100){
		r += (n-100)*3;
		n = 100;
	}
	r += 2*n;
	return r;
}

long wh(long cost){
	long r = 0;
	long t = 100*2 + (10000-100) * 3 + (1000000-10000) * 5;
	if(cost > t){
		r = 1000000;
		r += (cost - t)/7;
		return r;
	}
	t -= (1000000-10000) * 5;
	if(cost > t){
		r = 10000;
		r += (cost - t) / 5;
		return r;
	}
	t -= (10000-100) * 3;
	if(cost > t){
		r = 100;
		r += (cost - t) / 3;
		return r;
	}
	return cost /2;

}


int main(void){
	long a,b;
	cin>>a>>b;
	while(a){
		long tot = wh(a);	
		long up = tot/2 + 1;
		long lo = 0;
		while(up > lo){
			long curr = (up + lo) / 2;
			long c = cost(curr);
			long cc = cost(tot-curr);
			if((cc - c) > b) lo = curr + 1;
		       	else up = curr;	
		}
		cout<<cost(lo)<<'\n';

		cin>>a>>b;
	}
	return 0;
}
