#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;


int seen[300];
int dp[300];
vi graph[300];
int n;

int solve(int i, int c, int cas){
	if(seen[i] == cas) return dp[i] == c;
	seen[i] = cas;
	dp[i] = c;

	int r = 1;
	for(auto v:graph[i]){
		r = r && solve(v,c^1,cas);
	}
	return r;
}


int main(void){
	cin>>n;
	int cas = 1;
	while(n){
		for(int i = 0; i < n; i++)graph[i].clear();
		int l;
		cin>>l;
		while(l--){
			int a,b;
			cin>>a>>b;
			graph[b].push_back(a);
			graph[a].push_back(b);
		}	
			
		if(solve(0,0,cas++)) cout<<"BICOLORABLE.\n";
		else cout<<"NOT BICOLORABLE.\n";

		cin>>n;
	}	

	return 0;
}
