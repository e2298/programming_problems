#include<bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

long dp[10010];
long c[10010];
vi c_idx[10010];
int ca[100010];
int cb[100010];

long solve(int i){
	if(dp[i] != -1) return dp[i];
	long r = c[i];
	dp[i] = -2;

	for(auto idx : c_idx[i]){
		long t;
		if(dp[ca[idx]] == -2){
			t = min(c[i], c[ca[idx]]);
		}
		else{
			t = solve(ca[idx]);
		}
		if(dp[cb[idx]] == -2){
			t += min(c[i], c[cb[idx]]);
		}
		else{
			t += solve(cb[idx]);
		}
		r = min(r,t);
	}
	return dp[i] = r;
}


int main(void){
	memset(dp,-1,10010*4);
	ios::sync_with_stdio(0);
	ifstream istream;
	ofstream ostream;
	istream.open("dwarf.in");
	ostream.open("dwarf.out");
	int n, m, ci = 0;
	istream>>n>>m;
	for(int i = 1; i <=n; i++){
		istream>>c[i];
	}
	for(int i = 0; i < m; i++){
		long a,b,c;
		istream>>a>>b>>c;
		c_idx[a].push_back(ci);
		ca[ci] = b;
		cb[ci] = c;
		ci++;
	}

	ostream<<solve(1);

	return 0;
}
