#include<bits/stdc++.h>
using namespace std;

int main(void){
	int n;
	cin>>n;
	while(n){
		if(n == 1) cout<<"1\n";
		else if(n & (n-1)){
			int t = n;
			t |= t>>16;
			t |= t>>8;
			t |= t>>4;
			t |= t>>2;
			t |= t>>1;
			t++;
			t >>= 1;
			cout<< (n-t)*2<<'\n';
		}
		else cout<<n<<'\n';

		cin>>n;
	}
	return 0;
}
