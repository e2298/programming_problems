#include<bits/stdc++.h>
using namespace std;

int n;
int z[100010];
int neg[100010];
int nums[100010];

void inc(int* a, int i){
	for(; i <= n; i += i & (-i)){
		a[i]++;
	}
}

void dec(int* a, int i){
	for(; i <= n; i += i & (-i)){
		a[i]--;
	}
}

int q(int* a, int i){
	int r = 0;
	for(; i; i -= i & (-i)){
		r += a[i];	
	}
	return r;
}

int main(void){
	int k;
	while(cin>>n>>k){
		memset(z,0,100010*4);
		memset(neg,0,100010*4);
		for(int i = 1; i <= n; i++){
			int in;
			cin>>in;
			nums[i] = in;
			if(in == 0){
				inc(z,i);
			}
			else if (in < 0){
				inc(neg,i);
			}
		}
		while(k--){
			char op;
			int a,b;
			cin>>op>>a>>b;
			switch(op){
				case 'C': {
						if(!nums[a]) dec(z,a);
						else if(nums[a] < 0) dec(neg,a);

						if(b < 0) inc(neg,a);
						else if(!b) inc(z,a);

						nums[a] = b;
						break;
					  }
				case 'P':{
					 	if(q(z,b) - q(z,a-1)){
							cout<<'0';
						}
						else{
							int r = q(neg,b) - q(neg,a-1);
							if(r&1) cout<<'-';
							else cout<<'+';
						}
						break;
					 }
			}
		}
		cout<<'\n';
	}	
}
