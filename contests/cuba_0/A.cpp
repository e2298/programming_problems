#include<bits/stdc++.h>
using namespace std;

int mat[20][20];

int main(void){
	ifstream istream;
	ofstream ostream;
	istream.open("addictive.in");
	ostream.open("addictive.out");
	int w, h, c;
	istream>>h>>w>>c;
	int curr = 0;
	int ci = 0;
	for(int i = 0; i < h; i++){
		for(int j = (i & 1)? (w-1) : 0; (i & 1)? (j >= 0) : (j < w); j += (i & 1)? -1: 1){
			if(curr == 0){
				istream>>curr;
				ci++;
			}
			mat[i][j] = ci;
			curr--;
		}
	}
	for(int i = 0; i < h; i++){
		for(int j = 0; j < w; j++) ostream<<mat[i][j];
		ostream<<'\n';
	}
	return 0;
}
