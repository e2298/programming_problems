#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		int g = 1;
		int ln = 0;
		char ls = 'a';
		while(n--){
			int in;
			char inn;
			cin>>in>>inn;
			if(in == ln) g = 0;
			if(inn == ls) g = 0;
			ln = in;
			ls = inn;
		}
		if(g) cout<<"B\n";
		else cout<<"M\n";
	}

	return 0;
}
