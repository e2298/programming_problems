#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef set<int> s;
typedef vector<s> v;

v dom;
v doms;
vi ans;

int main(void){
	int cas = 1;
	int n;
	while(cin>>n){
		string in;
		dom = v(n);
		doms = v(n);
		ans = vi();
		for(int i = 0; i < n; i++){
			cin>>in;
			for(int j = 0; j < n; j++){
				if(in[j] == '1' || j ==i){
					dom[j].insert(i);
					doms[i].insert(j);
				}
			}
		}
		int duns = 0;
		while(duns < n){
			vi cant(n);
			for(int i = 0; i < n; i++){
				for(auto j: dom[i])cant[j]++;
			}		
			int maxn=0, maxc=0;
			for(int i = 0; i < n; i++){
				if(cant[i] > maxc) {
					maxc = cant[i];
					maxn = i;
				}
			}
			ans.push_back(maxn);
			for(auto i: doms[maxn]){
				if(dom[i].size()){
					duns++;
					dom[i]= s();
				}
			}
		}
		cout<<"Case "<<cas<<": "<<ans.size();
		for(auto i:ans){ cout<<' '<<i+1;}
		cout<<'\n';
		cas++;
	}
		
	return 0;
}
