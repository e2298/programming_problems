#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double d;
typedef vector<double> v;
typedef vector<int> vi;

v ci;
v c;
v ct;
int t;
int n;
double pow(double b, int e){
	if(e == 0) return 1.0;
	double r = pow(b, e/2);
	return (e&1)? b * r * r: r * r;
}

double eval(double x){
	double r = 0;
	for(int i = 0; i < c.size(); i++){
		r += pow(x, i) * c[i];
	}
	return r * acos(0) * 2.0;
}

int main(void){
	int cas = 1;
	while(cin>>n){
		ci = v(n+1);
		c = v(2*n+2);
		ct = v(2*n+1);
		for(auto& i:ci) cin>>i;
		for(int i = 0; i <= n; i++){
			for(int j = 0; j <= n; j++){
				ct[i+j] += (ci[i] * ci[j]);
			}
		}
		for(int i = 0; i < ct.size(); i++){
			c[i+1] = ct[i]/((double)i+1.0);
		}	

		double xh, xl, inc;
		cin>>xl>>xh>>inc;
		double vol = eval(xh) - eval(xl);
		printf("Case %d : %.2f\n", cas, vol);
		cas++;
		int times = vol / inc;
		if(times == 0) printf("insufficient volume");
		times = min(8, times);
		double curr = inc;

		double bas = eval(xl);
		while(times--){
			int t = 1000;
			double lo = xl;
			double hi = xh;
			while(t--){
				double cur = (lo + hi) / 2.0;
				if((eval(cur) - bas ) < curr){
					lo = cur;
				}
				else hi = cur;
			}
			if(times){
				printf("%.2f ", lo - xl);
			}
			else if(xh - lo > 0.01)
				printf("%.2f ", lo - xl);
			
			curr += inc;
		}

		printf("\n");
	}
	
	return 0;
}
