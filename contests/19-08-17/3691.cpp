#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> pii;
typedef queue<pii> qpi;

int main(void){
	int t;
	int r,c;
	int rs, cs;
	int re, ce;
	int rc, cc;
	int step;
	pii tmp;

	cin>>t;
	while(t--){
		cin>>c>>r;
		int grid[r][c];
		qpi q;

		for(int i = 0; i< r;i++)for(int j = 0; j<c;j++)
			cin>>grid[i][j];

		cin>>cs>>rs>>ce>>re;
		q.push(pii(rs,cs));
		grid[rs][cs] = 1;

		while(1){
			tmp = q.front(); 
			q.pop();
			rc = tmp.first;
			cc = tmp.second;
			//cout<<rc<<' '<<cc<<'\n';
			step = grid[rc][cc];

			if(tmp == pii(re,ce)) break;
		       	
			if(rc>0) if(!grid[rc-1][cc]){
				q.push(pii(rc-1,cc));
				grid[rc-1][cc] = step+1;
			}
			if(rc<(r-1)) if(!grid[rc+1][cc]){
				q.push(pii(rc+1,cc));
				grid[rc+1][cc] = step+1;
			}
			if(cc>0) if(!grid[rc][cc-1]){
				q.push(pii(rc,cc-1));
				grid[rc][cc-1] = step+1;
			}
			if(cc<(c-1)) if(!grid[rc][cc+1]){
				q.push(pii(rc,cc+1));
				grid[rc][cc+1] = step+1;
			}
		}

		cout<<step<<'\n';

	}

	return 0;
}
