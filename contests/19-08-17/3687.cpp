#include <bits/stdc++.h>
using namespace std;

typedef pair<int,int> pii;
typedef map<pii, int> mpi;

int main(void){
	int rs, cs;
	int r,c;
	int steps;
	cin>>rs>>cs>>c;
	
	while((rs+cs+c)!=0){
		r = 0;
		steps = 0;
		c--;

		mpi seen;
		char field[rs][cs];

		for(int i = 0; i<rs; i++)for(int j = 0; j<cs;j++)
			cin>>field[i][j];

		
		while((r>=0) && (r<rs) && (c>=0) && (c<cs) && !(seen.count(pii(r,c)))){
			seen[pii(r,c)] = steps++;
			switch (field[r][c]){
				case 'N':
					r--;
					break;
				case 'S':
					r++;
					break;
				case 'W':
					c--;
					break;
				case 'E':
					c++;
					break;
			}
		}

		if((r<0) | (r>=rs) | (c<0) | (c>=cs)){
			cout<<steps<<" step(s) to exit\n";	
		}
		else{
			cout<<seen[pii(r,c)]<<" step(s) before a loop of "<< 
			  steps-seen[pii(r,c)]<<" step(s)\n";
		}

		cin>>rs>>cs>>c;
	}
}
