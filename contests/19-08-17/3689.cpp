#include <iostream>
#include <stdio.h>
#include <cmath>
using namespace std;


int gcd(int a, int b){
	int t;
	while(b){
		t = b;
		b = a%b;
		a = t;
	}
	return a;
}


int main(void){
	int n;
	int a,b,s;
	while(scanf("%d,", &n) != EOF){
		a = 3*n;
		b = 2*n+1;
		s = gcd(a,b);
		a/= s;
		b/= s;
		if(b==1) cout<<a<<'\n';
		else cout<<a<<'/'<<b<<'\n';
	}

	return 0;
}

