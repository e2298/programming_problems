#include<bits/stdc++.h>
using namespace std;
typedef complex<long> c;
typedef vector<c> vc;

bool compare_complex(c a, c b){
	pair<long,long> aa(real(a),imag(a));
	pair<long,long> bb(real(b),imag(b));
	return aa < bb;
}

long cross_product(c a, c b){
	return real(a) * imag(b) - real(b) * imag(a);
}

int righ;
vc hull;
vc &convex_hull(vc &points){
	int idx_u = 0, idx_l = 0, n = points.size();
	hull = vc(n);vc lower(n);
	sort(points.begin(), points.end(), compare_complex);

	for(int i = 0; i<n; i++){
		//change <= to < if including collinear points
		while((idx_u > 1) && 
		  (cross_product(points[i] - hull[idx_u-2],
		  hull[idx_u-1] - hull[idx_u-2]) <= 0)){
			idx_u--;
		}
		hull[idx_u++] = points[i];
	}
	for(int i = n-1; i+1; i--){
		while((idx_l > 1) && 
		  (cross_product(points[i] - lower[idx_l-2],
		  lower[idx_l-1] - lower[idx_l-2]) <= 0)){
			idx_l--;
		}
		lower[idx_l++] = points[i];
	}
	idx_u--;
	idx_l--;
	righ = idx_u;
	int size = idx_u+idx_l;
	idx_l = 0;
	while(idx_u < size){
		hull[idx_u++] = lower[idx_l++];
	}
	hull.resize(size);
}

bool cmpdis(c a, c b, c xx, c yy){
	double d1=0,d2=0;
	
	double x1,y1,x2,y2,x,y;
	x1 = a.real();
	y1 = a.imag();
	x2 = b.real();
	y2 = b.imag();
	x = abs(x1-x2);
	y = abs(y1-y2);
	
	if(x > y){
		d1 += x-y;
		d1 += sqrt(2.0*y*y);
	}
	else{
		d1 += y-x;
		d1 += sqrt(2.0*x*x);
	}

	x1 = xx.real();
	y1 = xx.imag();
	x2 = yy.real();
	y2 = yy.imag();
	x = abs(x1-x2);
	y = abs(y1-y2);
	
	if(x > y){
		d2 += x-y;
		d2 += sqrt(2.0*y*y);
	}
	else{
		d2 += y-x;
		d2 += sqrt(2.0*x*x);
	}


	return d1 < d2;
}

void calipers(vc &points){
	//get hull and lower convex hulls
	//hull[size_u] = lower[0] and viceversa
	vc opoints(points);	
	convex_hull(points);
	c a,b;
	int size;
	double res;

	//i is index of smallest point lexicographically
	//j biggest

	size = hull.size();
	int i= 0, j = righ, k = 0;
	while(k < size){
		//hull[i] and hull[j] are a pair
		if(cmpdis(a,b, hull[i],hull[j])){
			a = hull[i];
			b = hull[j];	
		}
		k++;

		if( cross_product(hull[(j+1) % size] - hull[j], hull[(i+1) % size] - hull[i]) < 0){
			i++;		
		}
		else if( cross_product(hull[(j+1) % size] - hull[j] , hull[(i+1) % size] - hull[i]) == 0 ){
			//hull[i] and hull[j+1]
			//hull[i+1] and hull[j]
			//are a pair;
			if(cmpdis(a,b, hull[i],hull[(j+1)%size])){
				a = hull[i];
				b = hull[(j+1)%size];	
			}
			if(cmpdis(a,b, hull[(1+i)%size],hull[j])){
				a = hull[(i+1)%size];
				b = hull[j];	
			}
			i++;
			j++;
			k++;
		}
		else{
			j++;
		}
		i %= size;
		j %= size;
	}
	i = find(opoints.begin(), opoints.end(), a) - opoints.begin();
	j = find(opoints.begin(), opoints.end(), b) - opoints.begin();	
	cout<<i+1<<' '<<j+1<<'\n';
}

int main(void){
	int ss;
	cin>>ss;
	vc points(ss);
	for(auto&i:points){
		int a,b;
		cin>>a>>b;
		i = c(a,b);
	}
	calipers(points);
	return 0;
}
