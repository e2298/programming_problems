//sistema de 2 ecuaciones con n incognitas
#include <bits/stdc++.h>
using namespace std;

typedef vector<double> vi;

int main(void){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	int t, n,q;
	double qa, qb;
	double mult;
	double a1,b1,a2,b2,ap,aq,bp,bq;
	double t10,t11,t20,t21;	
	cin>>t;
	while(t--){
		cin>>n;
		if (n==1){
		       	cin>>a1>>b1;
			cin>>q;	
			while(q--){
				cin>>qa>>qb;
				if((qa==a1) && (qb==b1)) cout<<"Yes\n";
				else cout<<"No\n";
			}
		}
		else{
			vi a_in(n);
			vi b_in(n);

			for(int i = 0; i < n; i++){
				cin>>a_in[i]>>b_in[i];
			}

			cin>>q;
			while(q--){
				cin>>qa>>qb;
				vi a(a_in);
				vi b(b_in);
				for(auto& i:a) i-=qa;
				for(auto& i:b) i-=qb;

				int i=0;
				//gauss-jordan
				while((i < n) && (a[i] == 0.0)) i++;
				if(i != n){
					double fact = a[i];
					double fact_a = b[i];
					while(i < n){
						a[i] /= fact;
						b[i] -= fact_a*a[i];
						i++;
					}
				}

				i = 0;
				while((i < n) && (b[i] == 0.0)) i++;
				if(i != n){
					double fact = b[i];
					double fact_a = a[i];
					while(i < n){
						b[i] /= fact;
						a[i] -= fact_a*b[i];
						i++;
					}
				}
				int zca, zcb;
				for(i = zca = zcb = 0; i < n; i++){
					if(a[i] != 0.0) zca++;
					if(b[i] != 0.0) zcb++;
				} 

				if((zca != (1)) && (zcb != (1))) cout<< "Yes\n";
				else cout<<"No\n";
			}
			
		}
	}

	return 0;
}
