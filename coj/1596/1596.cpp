#include<bits/stdc++.h>
using namespace std;

unsigned fib[10000002];

int main(void){
	fib[1] = 1;
	for(int i = 2; i < 10000002; i++){
		fib[i] = fib[i-1] + fib[i-2];
		fib[i] %= 1000000000;
	}
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		if(n >=- 45)
			cout<<setfill('0')<<setw(9)<<fib[n]<<'\n';
		else
			cout<<fib[n]<<'\n';
	}
	

	return 0;
}
