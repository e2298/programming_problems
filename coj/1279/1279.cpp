#include <bits/stdc++.h>
using namespace std;

const int m = 1E6+5E5+10;

vector<bool> criba(m);
vector<int> suma(m);
vector<int> cant_primos(m);

void cribar(void){
	criba[0]=1;
	criba[1]=1;

	for(int i = 2; i<1E6+5E5+5;i++){
		suma[i]=suma[i-1];
		cant_primos[i]=cant_primos[i-1];
		if(!criba[i]){
			cant_primos[i]++;
			if(((i-1)%4) == 0){
				suma[i]++;
			}
			for(unsigned int j = i*i; j<1E6+5E5+5; j+=i){
				criba[j]=1;
			}
		}
	}
}

int main(void){
	int l,u;
	ios_base::sync_with_stdio(0);

	cribar();

	while(1){
		cin>>l>>u;
		if((l==-1) && (u == -1)) break;
		
		else if(u < 0 ) cout<<l<<' '<< u<< " 0 0\n";

		else if(l<0) cout<<l<<' '<<u<<' '<<' '<<cant_primos[u]<<' '<<suma[u]<<'\n';

		else cout<<l<<' '<<u<<' '<<' '<<cant_primos[u]-cant_primos[l-1] <<' '<<suma[u]-suma[l-1]<<'\n';
	}

	return 0;
}
