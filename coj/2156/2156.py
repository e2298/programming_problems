from decimal import*
comb = []
for i in range(101):
    comb.append([1])
    comb[i] += [comb[i-1][j]+comb[i-1][j-1] for j in range(1,i)]
    comb[i].append(1)

t = int(input())
for x in range(t):
    a = input().split()
    n = int(a[0])
    p = Decimal(a[1])
    q = Decimal(1-p)
    r = Decimal(0)
    getcontext().prec=50
    for i in range(n):
        if (i+n) % 2 != 0:
            r = r + p**i * q**(n-i) * comb[n][i]
    print(r)

