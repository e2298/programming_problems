#include<bits/stdc++.h>
using namespace std;

void factorizar(map<int,int>& arry,int n){
	int t = sqrt(n);
	t++;
	while((n%2)==0){
		arry[2]++;
		n>>=1;
	} 
	for(int i = 3; i < t; i++,i++){
		while((n%i)==0){
			arry[i]++;
			n/=i;
		}
	}
	
	if (n != 1) arry[n]=1;

}


int main(void){
	int t;
	map<int,int> facts[50];
	map<int,int> factsk[50];
	int ns[50];
	int ks[50];
	int r;

	cin>>t;

	for(int i =0; i<t;i++){
		cin>>ns[i]>>ks[i];
	}

	for(int i =0;i<t;i++){
		factorizar(facts[i],ns[i]);
		factorizar(factsk[i],ks[i]);

		for(auto j : factsk[i]){
			facts[i].erase(j.first);
		}
		r = ks[i]%ns[i] != 0;
		for(auto j: facts[i]){
			r*=j.second+1;
		}
		cout<<r<<"\n";
	}
	

	return 0;
}
