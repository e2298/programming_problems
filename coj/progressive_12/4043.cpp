#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int n;
int t[100005];
vi G[100005];
int roots[100005];
int tot[100005];
int mx[100005];

void solve(int i){
	int tt = t[i];
	int mxx = 0;
	for(auto j: G[i]){
		solve(j);
		tt += tot[j];
		mxx += (mx[j] > 0) ? mx[j]:0;
	}
	tot[i] = tt;
	mx[i] = max(mxx, tt);
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);

	cin>>n;
	int a,b;
	int ri = 0;
	for(int i = 0; i < n; i++){
		cin>>a>>b;
		if(a){
			a--;
			G[a].push_back(i);
		}
		else{
			roots[ri] = i;
			ri++;	
		}
		t[i] = b? 1:-1;
	}
	int r = 0;
	for(int i = 0; i < ri; i++){
		solve(roots[i]);
		r += mx[roots[i]];
	}
	cout<<r;

	return 0;
}
