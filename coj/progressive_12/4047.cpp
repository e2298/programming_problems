#include<bits/stdc++.h>
using namespace std;

typedef pair<int,int> ii;
typedef multiset<ii> mi;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);

	int t;
	cin>>t;
	mi s1, s2;
	while(t--){
		s1.clear();
		s2.clear();
		int n;
		cin>>n;
		while(n--){
			int a,b;
			cin>>a>>b;
			s1.insert(ii(a,b));
			s2.insert(ii(b,a));
		}
		int r = 0;
		while(!s1.empty()){
			ii curr = *(s1.begin());
			r++;
			s1.erase(curr);
			s2.erase(ii(curr.second, curr.first));
			while(1){
				auto tt = s1.lower_bound(ii(b,0));
				if(tt = s1.end() || *tt.first > curr.second) break;
				else{
					ii t = *tt;
					s1.erase(t);
					s2.erase(ii(t.second, t.first));
				}
			}
		}
	}

	return 0;
}
