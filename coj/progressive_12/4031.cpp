#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		string w;
		cin>>w;
		int n = w.size();
		int eq = 0;
		for(int i = 0; i < n; i++){
			eq = eq|| (w[i] == w[n-i-1]);
		}
		if(eq) cout<<"no\n";
		else cout<<"yes\n";
	}
	
	return 0;
}
