#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int c = 0;
		int n;
		int t = 0;
		cin>>n;
		for(int i =0; i < n; i++){
			char s;
			cin>>s;
			if(s == '<'){
				t += i-c;
				c++;
			}
		}
		cout<<t<<'\n';
	}

	return 0;
}
