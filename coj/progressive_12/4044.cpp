#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t=0, c, n;
	cin>>c>>n;
	vector<int> s;
	while(n--){
		int in;
		cin>>in;
		s.push_back(in);
		t += in;
	}
	sort(s.begin(), s.end(), greater<int>());
	int sc = c;
	int sz = 0;
	while(sc <= t){ sc += s[sz]; t-=s[sz]; sz++; }
	sz ++;
	cout<<sz<<'\n';
	return 0;
}
