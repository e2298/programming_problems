#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);
	int t;
	cin>>t;
	while(t--){
		ll a,b,p;
		cin>>a>>b>>p;
		int c = 0;
		while(a || b){
			c += (a & 1) ^ ( b & 1);
			a >>=1;
			b >>=1;
		}
		if(((c & 1) ^ (p & 1)) || c > p) cout<<"no\n";
		else cout<<"yes\n";
	}
	return 0;
}
