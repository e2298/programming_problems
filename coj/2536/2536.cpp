#include<bits/stdc++.h>
using namespace std;

using namespace std;

const double EPS = 1e-10;

typedef vector<int> VI;
typedef double T;
typedef vector<T> VT;
typedef vector<VT> VVT;

T GaussJordan(VVT &a, VVT &b) {
  const int n = a.size();
  const int m = b[0].size();
  VI irow(n), icol(n), ipiv(n);
  T det = 1;

  for (int i = 0; i < n; i++) {
    int pj = -1, pk = -1;
    for (int j = 0; j < n; j++) if (!ipiv[j])
      for (int k = 0; k < n; k++) if (!ipiv[k])
	if (pj == -1 || fabs(a[j][k]) > fabs(a[pj][pk])) { pj = j; pk = k; }
    ipiv[pk]++;
    swap(a[pj], a[pk]);
    swap(b[pj], b[pk]);
    if (pj != pk) det *= -1;
    irow[i] = pj;
    icol[i] = pk;

    T c = 1.0 / a[pk][pk];
    det *= a[pk][pk];
    a[pk][pk] = 1.0;
    for (int p = 0; p < n; p++) a[pk][p] *= c;
    for (int p = 0; p < m; p++) b[pk][p] *= c;
    for (int p = 0; p < n; p++) if (p != pk) {
      c = a[p][pk];
      a[p][pk] = 0;
      for (int q = 0; q < n; q++) a[p][q] -= a[pk][q] * c;
      for (int q = 0; q < m; q++) b[p][q] -= b[pk][q] * c;
    }
  }

  for (int p = n-1; p >= 0; p--) if (irow[p] != icol[p]) {
    for (int k = 0; k < n; k++) swap(a[k][irow[p]], a[k][icol[p]]);
  }

  return det;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		double pf, pb, ps;
		cin>>pf>>pb;
		ps = 100.0 - pf - pb;
		pf /= 100.0;
		pb /= 100.0;
		ps /= 100.0;

		VVT graph(n-1, VT(n-1));

		for(int i = 1; i < n-1; i++){
			graph[i][i] = ps;
			graph[i][i-1] = pb;
			graph[i-1][i] = pf;
		}
		graph[0][0] = ps + pb;
		for(int i = 0; i < n-1; i++){
			for(int j = 0; j < n-1; j++) graph[i][j] *= -1.0;
			graph[i][i] += 1.0;
		}


		VVT t(n-1, VT(1, 1.0));

		GaussJordan(graph, t);

		double res = 0.0;
		for(int i = 0; i < n-1; i++) res += graph[0][i];

		printf("%lf\n", res);

	}
	
	return 0;
}
