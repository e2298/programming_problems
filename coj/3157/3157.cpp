#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll c[100001];
ll o[100001];
ll w[100001];

int main(void){
	string s;
	int n;
	cin>>n>>s;
	for(int i = n-1; i >= 0; i--){
		c[i] = c[i+1];
		o[i] = o[i+1];
		w[i] = w[i+1];
		switch(s[i]){
			case 'C':
				c[i] += o[i];
				break;
			case 'O':
				o[i] += w[i];
				break;
			case 'W':
				w[i]++;
				break;
		}
	}
	cout<<c[0]<<'\n';

}
