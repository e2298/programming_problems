#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

ii in[100010];
int r[100010];

int main(void){
	int n;
	cin>>n;
	int t;
	for(int i = 0; i < n; i++){
		cin>>t;
		in[i] = ii(t,i);
	}
	sort(in, in+n);
	int lst = -1;
	int curr = 0;
	for(int i = 0; i < n; i++){
		if(in[i].second < lst){
			curr ^=1;
		}
		lst = in[i].second;
		r[i] = curr;
	}
	int a = 0, b = 0;
	for(int i = 0; i < n; i++){
		if(!r[i]) {cout<<i+1<<' '; a++;}
	}
	cout<<'\n';
	for(int i = 0; i < n; i++){
		if(r[i]) {cout<<i+1<<' '; b++;}
	}
	cout<<'\n';
	if(a > b) cout<<'A';
	else if(a < b) cout<<'B';
	else cout<<'E';
	cout<<'\n';

	return 0;
}
