#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

vi disj;
vi disjw;
vi enemy;
int a;

int find(int a){
	if(disj[a] == a) return a;
	return disj[a] = find(disj[a]);
}

void join(int a, int b){
	a = find(a);
	b = find(b);
	if(a == b) return;
	if(disjw[a] > disjw[b]) disj[b] = a;
	else if(disjw[a] < disjw[b]) disj[a] = b;
	else{
		disj[a] = b;
		disjw[b] ++;
	}
}
int friends(int x, int y){
	return find(x) == find(y);
}

int enemies(int x, int y){
	return enemy[find(x)] == find(y);	
}

void set_friends(int x, int y){
	if(friends(x,y)) return;
	int e1 = enemy[find(x)];
	int e2 = enemy[find(y)];

	join(x,y);
	if((e1 != -1) && (e2 != -1))
		join(e1,e2);

	if(e1 != -1){
		enemy[find(x)] = find(e1);
		enemy[find(e1)] = find(x);
	}
	else if(e2 != -1){
		enemy[find(x)] = find(e2);
		enemy[find(e2)] = find(x);
	}
}

void set_enemies(int x, int y){
	if(enemies(x,y)) return;
	if((enemy[find(x)] != -1) && (enemy[find(y)] != -1)){
		set_friends(enemy[find(x)], find(y));
		set_friends(enemy[find(y)], find(x));
	}
	else if ((enemy[find(x)] == -1) && (enemy[find(y)] == -1)){
		enemy[find(x)] = find(y);
		enemy[find(y)] = find(x);
	}
	else if (enemy[find(x)] == -1){
		set_friends(find(x), enemy[find(y)]);	
	}
	else{
		set_friends(find(y), enemy[find(x)]);	
	}
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n,q;
	while(cin>>n>>q){
		disj = vi(n+1);
		disjw = vi(n+1,1);
		enemy = vi(n+1, -1);
		for(int i = 1; i <= n; i++){
			disj[i] = i;
		}

		int c,x,y;
		while(q--){
			cin>>c>>x>>y;
			if(c == 1){
				if(enemies(x,y)) cout<<"-1\n";
				else{
					set_friends(x,y);
				}
			}
			else if(c == 2){
				if(friends(x,y)) cout<<"-1\n";
				else{
					set_enemies(x,y);
				}
			}
			else if(c == 3){
				cout<<!!friends(x,y)<<'\n';
			}
			else  cout<<!!enemies(x,y)<<'\n';
		}
	}
	return 0;
}
