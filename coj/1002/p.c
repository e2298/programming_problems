#define min(a,b,c) ((a<b?a:b)<c?(a<b?a:b):c)
#define max(a,b) (a>b?a:b)

#include <stdio.h>
#include <stdlib.h>
int main(void){
	int t;
	int n;
	int g[11][11];
	int i1,i2;
	char i[15];
	int r;

	for(i1=0;i1<11;i1++){
		g[0][i1]=0;
		g[i1][0]=0;
	}

	scanf("%d",&t);

	while(t--){
		r = 0;
		scanf("%d",&n);
		n++;
		for(i1=1;i1<n;i1++){
			scanf("%s", &i[1]);
			for(i2=1;i2<n;i2++){
				if ((i[i2]&1)^1){
					g[i1][i2] = min(g[i1-1][i2] ,g[i1][i2-1] ,g[i1-1][i2-1] )+1;
					(r<g[i1][i2]) && (r = g[i1][i2]);
				}
				else g[i1][i2] = 0;
			}
		}
		printf("%d\n", r);
	}
	return 0;
}
