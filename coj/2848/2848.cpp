#include<bits/stdc++.h>
using namespace std;
typedef vector<double> vi;

double height(long base, double r){
	if(base == 0) return 0.0;
	else if(base == 1) return r*2.0;
	else return r*2 + (base-1)*r*sqrt(3);
}

//a*b base, c height
long solve(double a, double b, double c, double rad){
	long r;
	double side = min(a,b);
	r = floor(side/(rad*2.0));
	double h = height(r,rad);
	//max possible value
	int i = 1;
	int stack = r;
	r = (r*(r+1)*(2*r+1)) / 6;
	
	while((stack != 1) && (h > c)){
		stack--;
		r -= i*i;
		h -= rad*sqrt(3);
		i++;
	}
	if(h>c) r = 0;
	
	return r;
}

int main(void){
	vi in(3);
	double r;

	while(cin>>in[0]>>in[1]>>in[2]>>r){
		sort(in.begin(),in.end());
		long res = 0;
		do{
			res = max(res, solve(in[0], in[1], in[2], r));
		}while(next_permutation(in.begin(),in.end()));
		cout<<res<<'\n';
	}

	return 0;
}
