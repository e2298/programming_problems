#include<bits/stdc++.h>
using namespace std;

typedef complex<long> cd;
typedef vector<cd> vc;
typedef vector<int> vi;


//AB hor,
//CD ver
int inter(cd a, cd b, cd c, cd d){
	if((real(a) <= real(c))
	&& (real(b) >= real(c))
	&& (imag(c) <= imag(a))
	&& (imag(d) >= imag(a)))
		return 1;
	else return 0;

}

int main(void){
	int n;
	cin>>n;
	vc p1(n),p2(n);
	vi hor,ver;
	for(int i = 0; i < n; i++){
		long x1,y1,x2,y2;
		cin>>x1>>y1>>x2>>y2;
		if(x1 == x2){
			ver.push_back(i);
			if(y1 < y2){
				p1[i] = cd(x1,y1);
				p2[i] = cd(x2,y2);
			}
			else{
				p2[i] = cd(x1,y1);
				p1[i] = cd(x2,y2);
			}
		}
		else{
			hor.push_back(i);
			if(x1 < x2){
				p1[i] = cd(x1,y1);
				p2[i] = cd(x2,y2);
			}
			else{
				p2[i] = cd(x1,y1);
				p1[i] = cd(x2,y2);
			}
		}
	}

	vi ic(n);

	for(int i = 0; i < ver.size(); i++){
		for(int j = 0; j < hor.size(); j++){
			if(inter(p1[hor[j]], p2[hor[j]], p1[ver[i]], p2[ver[i]])){
				ic[i]++;
				ic[j]++;
			}
		}
	}	

	vi t;
	for(int i = 0; i<ver.size(); i++){
		if(ic[ver[i]] < 2) t.push_back(ver[i]);
	}
	ver = t;
	ic = vi(n);

	for(int i = 0; i < ver.size(); i++){
		for(int j = 0; j < hor.size(); j++){
			if(inter(p1[hor[j]], p2[hor[j]], p1[ver[i]], p2[ver[i]])){
				ic[i]++;
				ic[j]++;
			}
		}
	}	

	t = vi();
	for(int i = 0; i<hor.size(); i++){
		if(!ic[hor[i]] ) t.push_back(hor[i]);
	}
	cout<<t.size() + ver.size();


	return 0;
}
