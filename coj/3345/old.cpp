#include<bits/stdc++.h>
using namespace std;

const double PI = acos(0) * 2;

typedef complex<double> cd;
typedef vector<cd> vc;
typedef vector<int> vi;
typedef pair<double,int> pdi;
typedef vector<pdi> vp;
typedef vector<vp> vvp;

double angleize(double angle){
	while(angle < 0) angle = 2*PI + angle;
	while(angle >= 2*PI) angle -= 2*PI;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		int b, m, s, idx;
		double angle;
		cin>>b>>m>>s>>idx>>angle;
		angle = angle * PI / 180;

		vc points(m+1);
		for(int i = 0; i<m; i++){
			int j;
			double x,y;
			cin>>j>>x>>y;
			points[j] = cd(x,y);
		}

		vvp angles(m+1, vp(m-1));
		for(int i = 1; i <= m; i++){
			int k = 0;
			for(int j = 1; j <= m; j++){
				if(j == i) continue;
				angles[i][k++] = pdi(angleize(arg(points[j] - points[i])),j);
			}
			sort(angles[i].begin(),angles[i].end());
			//for(auto h: angles[i]) cout<< h.first<<' '<<h.second<<'\n';
			//cout<<'\n';
		}
		
		cout<<b<<' ';
		int i = 0;
		int last = 0;
		while(s--){
			vp ca = angles[idx];
			double lo_angle,hi_angle;
			
			auto lo = upper_bound(ca.begin(), ca.end(), pdi(angle,100));
			if((lo != ca.end()) && ((*lo).second == last)) lo++;
			double angle2 = angle + PI;
			if(angle2 >= 2*PI) angle2 -= 2*PI;
			auto hi = upper_bound(ca.begin(), ca.end(), pdi(angle2,100));
			if((hi != ca.end()) && ((*hi).second == last)) hi++;

			if(lo == ca.end()){
			       	lo = ca.begin();
				lo_angle = PI*2 - angle + (*lo).first;
			}
			else{
				lo_angle = (*lo).first - angle;
			}
			if(hi == ca.end()){
			       	hi = ca.begin();
				hi_angle = PI*2 - angle2 + (*hi).first;
			}else{
				hi_angle = (*hi).first - angle2;
			}


			int res;
			if(lo_angle < hi_angle) res = (*lo).second;
			else res = (*hi).second;

			angle = arg( points[res] - points[idx]);
			angle = angleize(angle);

			last = idx;
			idx = res;
			
			cout<<idx<<' ';	
			//cout<<(*lo).second<<' '<<(*hi).second<<'\n';
		}
		cout<<'\n';
	}

	return 0;
}
