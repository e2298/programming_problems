#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())


vector<vector<int> >nxt;
string ss[10010];
int sizes[10010];
int dp[10010];
int solve(int i){
	if(dp[i]) return dp[i];
	int r = 0;
	for(auto j:nxt[i]){
		r = max(r, solve(j));
	}

	return dp[i] = r+1;
}

int curr;

struct corasick{
	map<char,corasick> next;
	corasick* tran[256];
	int idhoja;
	corasick *padre, *link, *linkhoja;
	char pch;
	corasick(): tran(),idhoja(-1),padre(),link(),linkhoja(){}
	void insert(const string &s, int id, int p = 0){
		if(p<sz(s)){
			corasick &ch = next[s[p]];
			tran[(int)s[p]] = &ch;
			ch.padre = this, ch.pch = s[p];
			ch.insert(s,id,p+1);
		}
		else{
			idhoja = id;
		}
	}
	corasick* get_link(){
		if(!link){
			if(!padre) link = this;
			else if(!padre->padre) link = padre;
			else  link = padre->get_link() -> get_tran(pch);
		}
		return link;
	}
	corasick* get_tran(int c){
		if(!tran[c]) tran[c] = !padre?this:this->get_link()->get_tran(c);
		return tran[c];
	}
	corasick* get_linkhoja(){
		if(!linkhoja){
			if(get_link()->idhoja >=0) linkhoja = link;
			else if(!padre) linkhoja = this;
			else linkhoja = link->get_linkhoja();
		}
		return linkhoja;
	}
	void report(int p){
		if(idhoja>=0 && idhoja != curr) {
			if(nxt[idhoja].size() && sizes[nxt[idhoja][0]] > sizes[curr]){
				nxt[idhoja] = vector<int>();
			}
			if(!nxt[idhoja].size() || sizes[nxt[idhoja][0]] == sizes[curr]){
				nxt[idhoja].push_back(curr);
			}
		}
		if(get_linkhoja()->idhoja>=0) linkhoja->report(p);
	}
	void matching(const string& s, int p = 0){
		report(p); if(p<sz(s)) get_tran(s[p])->matching(s,p+1);
	}
}cora;


int main(void){
	sizes[10000] = 1e9;
	int n;
	while(cin>>n && n){
		cora = corasick();
		nxt = vector<vector<int> >(n);
		memset(dp,0,sizeof(int)*n);
		for(int i = 0; i<n; i++){
			cin>>ss[i];
			sizes[i] = ss[i].size();
			cora.insert(ss[i],i);
		}

		for(int i = 0; i < n; i++){
			curr = i;
			cora.matching(ss[i]);
		}
		int r = 0;
		for(int i = 0; i < n; i++){
			//cout<<i<<": ";
			//for(auto j:nxt[i]){
			//	cout<<j<<' ';
			//}
			//cout<<'\n';
			r = max(r, solve(i));
		}
		cout<<r<<'\n';
	}
	return 0;
}
