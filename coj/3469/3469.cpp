#include <bits/stdc++.h>
using namespace std;


int main(void){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	int n, q, t, max;
	int c;
	cin>>n;
	
	vector<int> sum(n+1);
	sum[0]=0;

	for(int i =1; i<=n;i++){
		cin>>sum[i];
		sum[i]=sum[i-1]+sum[i];
	}

	max = sum[n];

	cin>>q;
	while(q--){
		cin>>t;
		if(t>max) cout<<"none\n";
		else{
			c = lower_bound(++sum.begin(),sum.end(),t)-sum.begin();
			cout<<c<<'\n';
			
		}
	
	}

}
