#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
typedef complex<ll> cd;

ll dp(cd a, cd b){ return a.real() * b.real() + a.imag() * b.imag();}
cd p[300];
int main(void){
	int n;
	cin>>n;
	ll r = 0ll;
	for(int i = 0; i<n;i++){
		int x,y;
		cin>>x>>y;
		p[i] = cd(x,y);
	}
	for(int i = 0; i < n; i++){
		for(int j = i+1; j < n; j++){
			for(int k = j+1; k < n; k++){
				cd a,b;
				a = p[j]-p[i];
			     	b = p[k]-p[i];
				if(!dp(a,b)){ r++; continue;};

				a = p[i]-p[j];
			     	b = p[k]-p[j];
				if(!dp(a,b)){ r++; continue;};

				a = p[i]-p[k];
			     	b = p[j]-p[k];
				if(!dp(a,b)){ r++; continue;};
			}
		}
	}
	cout<<r<<'\n';
	return 0;}
