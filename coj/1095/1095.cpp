#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;

int dad[100000];
int sz[100000];
int rec[100000];
int tots[100000];

int find(int a){
	if(dad[a] == a) return a;
	else return dad[a] = find(dad[a]);
}

void join(int a, int b){
	a = find(a);
	b = find(b);
	if(sz[a] > sz[b]){
		dad[b] = a;
		sz[a] += sz[b];
	}
	else {
		dad[a] = b;
		sz[b] += sz[a];
	}
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n,m;
		cin>>n>>m;
		for(int i = 0; i < n; i++){
			dad[i] = i;
			sz[i] = 1;
		}
		memset(rec,0,n*sizeof(int));
		memset(tots,0,n*sizeof(int));
		while(m--){
			int a,b;
			cin>>a>>b;
			a--;
			b--;
			rec[b]++;
			join(a,b);
		}
		int r = 0;
		for(int i = 0; i < n; i++){
			if(!rec[i]){
				r++;
				tots[find(i)]++;
			}
		}
		for(int i = 0; i < n; i++){
			if(dad[i] == i && !tots[i]) r++;
		}
		cout<<r<<'\n';
	}

	return 0;
}
