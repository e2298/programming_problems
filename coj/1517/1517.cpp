#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll, ll> ii;

map<string, vector<ii> > m;

int n;

ll solve(ll q){
	int can = 1;
	ll c = 0;

	for(auto k:m){
		auto v = k.second;
		ll minn = 1e10;
		int deed = 0;
		for(auto i:v){
			if(i.second >= q){
				deed = 1;
				minn = min(minn, i.first);
			}
		}
		c += minn;

		can = can && deed;
	}

	if(!can) return 1e12;
	else return c;
}


int main(void){
	int t;
	cin>>t;
	while(t--){
		m = map<string, vector<ii> >();
		ll budget;
		cin>>n>>budget;
		while(n--){
			string a,b;
			int p,q;
			cin>>a>>b>>p>>q;
			m[a].push_back(ii(p,q));
		}

		ll up = 1e9 + 10;
		ll lo = 0;
		while(lo < up){
			ll curr = (lo + up + 1) /2;
			if(solve(curr) > budget) up = curr-1;
			else lo = curr;		
		}
		cout<<lo<<'\n';

	}

	return 0;
}
