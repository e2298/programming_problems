#include <bits/stdc++.h>
using namespace std;

vector< vector<int> > graph;
vector<bool> seen;
int n;

//deepest node,depth
pair<int,int> dfs(int node, int depth = 0){
	pair<int,int> t;
	pair<int,int> r = {node,depth};
	seen[node]=true;
	
	for(auto i:graph[node]){
		if(!seen[i]){
			t = dfs(i,depth+1);
			if(t.second>r.second){ 
				r = t;
			}
		}
	}
	return r;
}

int main(void){
	int t,a,b,r,tt;
	cin>>t;
	while(t--){
		graph = vector<vector<int> >(1E6+1);
		cin>>n;
		for(int i = 1;i<n;i++){
			cin>>a>>b;
			graph[a].push_back(b);
			graph[b].push_back(a);
		}

		seen = vector<bool>(1E6+1);
		seen[0]=true;
		r = dfs(1).first;
		
		seen = vector<bool>(1E6+1);
		seen[0]=true;
		tt = dfs(r).second;

		cout<<tt+1<<'\n';
	}

	return 0;
}
