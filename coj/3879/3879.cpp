#include <bits/stdc++.h>
using namespace std;

int inp [105][3];
double dist[200][200];



int main(void){
	int n;
	int xa,ya,xb,yb,da,db;
	double t,r;
	cin>>n;
	n+=2;

	cin>>inp[0][0]>>inp[0][1];
	inp[0][2]=0;
	cin>>inp[1][0]>>inp[1][1];
	inp[1][2]=0;

	xa = inp[0][0];
	ya = inp[0][1];
	xb = inp[1][0];
	yb = inp[1][1];

	t = sqrt((xa-xb)*(xa-xb)+(ya-yb)*(ya-yb));
	dist[0][1] = t;
	dist[1][0] = t;


	for(int i = 2; i < n; i++){
		cin>>inp[i][0]>>inp[i][1]>>inp[i][2];
		for(int j = 0; j < i; j++){
			xa = inp[i][0];
			ya = inp[i][1];
			da = inp[i][2];
			xb = inp[j][0];
			yb = inp[j][1];
			db = inp[j][2];

			t = sqrt((xa-xb)*(xa-xb)+(ya-yb)*(ya-yb));
			dist[i][j] = abs(t-da);
			dist[j][i] = abs(t-db);
		}
	}

	for(int k = 0;k<n;k++)
		for(int i = 0;i<n;i++)
			for(int j = 0;j<n;j++){
				if (dist[i][j] > dist[i][k] + dist[k][j])
             				dist[i][j] = dist[i][k] + dist[k][j];
	}	
	
	cout<<fixed<<setprecision(2)<<dist[0][1]<<'\n';


	return 0;
}

