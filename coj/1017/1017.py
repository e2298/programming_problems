from __future__ import print_function
def solve(n):
    global r
    if(n != 0):
        t = n
        lg = 0
        while(t != 0):
            lg += 1
            t = t // 2
        lg -= 1
        solve( n - 2**lg)
        r = r + [3**lg]

while(True):
    n = int(input())
    if(n == 0):
        break
    global r
    r = []
    solve(n-1)
    if(len(r) != 0):
        print("{",end='')
        for j in range(len(r)-1):
            i = r[j]
            print(" %d," % (i), end='')
        print(" %d }" % r[len(r)-1])
    else:
        print("{ }")
