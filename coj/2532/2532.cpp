#include<bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

int main(void){
	int n,p,k,t;
	
	cin>>n;
	vi tt(n);
	for(auto&i:tt)cin>>i;
	cin>>p>>k;
	sort(tt.begin(),tt.end());

	int pos = 1;
	int c = 0;
	for(int i = 1; i < n; i++){
		if((tt[0] + tt[i]) <= k){
			c += tt[i] + tt[0];
		}
		else {
			pos = 0;
		}
	}
	c-=tt[0];
	if(tt[0] > k) pos = 0;
	if((p - c) < 0) pos = 0; 

	if(pos) cout<<c;
	else cout<<"-1";

	return 0;
}
