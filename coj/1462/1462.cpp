#include<bits/stdc++.h>
using namespace std;

int rem[100005];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	cin>>n;
	int r = 0;
	rem[0] = 1;
	for(int i = 1; i < 100005; i++){
		rem[i] = rem[i-1] * 10;
	}
	while(n--){
		string s;
		cin>>s;
		int m = 0;
		int i = 0;
		int j = s.size();
		do{
			j--;
			int t = s[j] - '0';
			t *= rem[i];
			m += t;	
			i++;
		}while(j);

		r += m;
	}
	cout<<(r & 127);
}
