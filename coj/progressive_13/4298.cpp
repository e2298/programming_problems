#include<bits/stdc++.h>
using namespace std;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n,m;
		cin>>n>>m;
		set<int> d;
		for(int i = 1; i<=n;i++) d.insert(i);
		while(m--){
			int t;
			cin>>t;
			d.erase(t);
		}
		for( auto i:d) cout<<i<<' ';
		cout<<'\n';
	}


	return 0;
}
