#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N = 10000000;
ll criba[N+1];
void cribar(){
	ll w[] = {4,2,4,2,4,6,2,6};
	for(ll p = 25; p <= N; p+= 10) criba[p] = 5;
	for(ll p = 9; p <= N; p+= 6) criba[p] = 3;
	for(ll p = 4; p <= N; p+= 2) criba[p] = 2;
	for(ll p = 7, cur = 0; p*p <= N; p += w[cur++&7]) if(!criba[p])
		for(ll j = p*p; j <= N; j+= (p<<1)) if(!criba[j]) criba[j] = p;
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	cribar();
	criba[1] = 1;
	criba[0] = 1;
	ll n;
	string s;
	while(cin>>n>>s){
		ll i = 0;
		ll r = 1;
		while(s[i]){
			ll curr = 0;
			ll lft = n;
			while(lft--){
				curr *= 10;
				curr += s[i]-'0';
				i++;
			}
		//	cout<<curr<<' ';
			r = r && !criba[curr];
		}	
		cout<<(r?":)\n":":(\n");
	}
	return 0;
}
