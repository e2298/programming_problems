#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int alph[256];
int al;

ll pow(ll a,ll b){
	if(!b) return 1;
	ll r = pow(a,b/2);
	return (b&1)?r*a:r;
}

ll s2(string s, int i, int l){
	if(!l) return 0;
	if(l == 1) return alph[s[i]];
	ll r = pow(al,l-1)*(alph[s[i]]-1);
	return r + s2(s, i+1, l-1);
}

ll solve(string s, int i, int l){
	if(!l) return 0;
	if(l == 1) return alph[s[i]];
	ll r = 1-pow(al,l);
	r /= 1-al;
	r--;
	r += pow(al,l-1)*(alph[s[i]]-1);
	return r + s2(s, i+1, l-1);
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		cin>>al;
		for(int i = 1; i <=al; i++){
			char c;
			cin>>c;
			alph[c] = i;
		}
		int q;
		cin>>q;
		while(q--){
			string s;
			cin>>s;
			if(al == 1) cout<<s.size()<<'\n';
			else cout<<solve(s,0,s.size())<<'\n';
		}
	}

	return 0;
}
