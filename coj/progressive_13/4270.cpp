#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int s[100005];

int main(void){
	int t;
	cin>>t;
	while(t--){
		int zs = 0;
		int i = 1;
		int n,q;
		cin>>n>>q;
		for(int j = 0; j < n; j++){
			int tt;
			cin>>tt;
			if(!tt){
				zs++;
			}
			else{
				if(zs){
					s[i] = -zs;
					zs = 0;
					i++;
				}
				s[i] = tt;
				i++;
			}
		}
		if(zs){
		       	s[i] = -zs;
			i++;
		}
		s[i] = 0;
		for(int j = 0; j < q; j++){
			int qi;
			ll r= 0;
			cin>>qi;
			int curr = 0;
			int lo = 1, up = 1;
			while(up < i){
				if(s[up] < 0){
					up++;
				}
				else{
					curr += s[up];
					while(curr > qi && lo < up){
						if(s[lo] > 0) curr -= s[lo];
						lo++;
					}
					if(curr == qi){
						r++;
						if(s[up+1] < 0){
							if(s[lo] < 0) r += (s[lo]-1) * s[up+1];
							else if(s[lo-1] < 0) r += (s[lo-1]-1) * s[up+1];
							else r += -s[up+1];
						}
						if(s[lo] < 0) r += -s[lo];
						if(s[lo-1] < 0) r += -s[lo-1];

					}
					up++;
				}
			}

			cout<<r<<' ';

		}
		cout<<'\n';
	
	}
	return 0;
}
