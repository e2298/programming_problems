#include<bits/stdc++.h>
using namespace std;

typedef unsigned long long ll;
int ffstr(ll x, ll n){
	ll s;
	while(n>1){
		s = n>>1;
		x = x &(x<<s);	
		n = n-s;
	}
	return x;
}


int main(void){
	ll m,k;
	while(cin>>m>>k){
		int mx = 0;
		while(ffstr(m, mx)){mx++;}
		mx --;
		if(mx >= k) cout<<m<<'\n';
		else{
			ll mm = ~0;	
			for(int i = 1; i <= k; i++){
				ll t = 1ll<<(i);
				t--;
				while(ffstr(t, i)){
					if(ffstr(t|m, k)){
						ll tt = (t|m) & ~((1ll<<__builtin_ctzll(t))-1);
						if(ffstr(tt, k)) mm = min(mm, tt);
						else mm = min(mm, t|m);
					}
					t<<=1;
				}
			}
			cout<<mm<<'\n';
		}

	}

	return 0;
}
