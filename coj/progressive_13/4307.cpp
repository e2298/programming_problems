#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll mod = 1000000007;

ll dp[505];
int seen[505];
ll c,v;
ll mx;
ll solve(int i, int cas){
	if(i < 1) return 0;
	if(i == 1) return v;
	if(i == 2) return v * c + v * v;
	if(seen[i] == cas) return dp[i];
	seen[i] = cas;
	
	ll t;
	ll r = v * solve(i-1, cas);
	t = r;
	r %= mod;
	ll r2 = c * v * solve(i-2,cas);
	t += r2;
	r2 %= mod;
	mx = max(t, mx); 
	
	return dp[i] = (r + r2) % mod;	
}
	

int main(void){
	int t;
	cin>>t;
	int cas = 1;
	while(t--){
		int l,k;
		cin>>c>>v>>l>>k;
		mx = 0ll;
		ll r = solve(l, cas++);
		cout<<r<<((mx >= k) || (r >= k) ? " Accepted\n":" Rejected\n");
	}
	return 0;
}
