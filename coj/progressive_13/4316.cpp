#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	ll n,s;
	while(cin>>n>>s){
		vector<ll> v(n);
		for(auto &i:v) cin>>i;
		ll c = 0;
		int b=0, e=0;
		for(int i = 0; i < n && c != s; i++){
			int dun = 0;
			while(c > s){
				c -= v[b];
				b++;
				if(c == s) dun = 1;
			}
			if(dun) break;
			c += v[i];
			e = i;
		}
		if(c > s){
			c -= v[b];
			b++;
		}
		if(c == s) cout<<b+1<<' '<<e+1<<'\n';
		else cout<<"-1\n";


	}

	return 0;
}

