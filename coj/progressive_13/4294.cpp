#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef pair<ii,ii> pp;

int height[600][600];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int w,h;
		cin>>h>>w;	
		for(int i = 0; i < h; i++) for(int j = 0; j < w; j++) cin>>height[j][i];
		priority_queue<pp, vector<pp>, greater<pp>> q;
		vector<vector<ii> > dist(600, vector<ii>(600, ii(1e9,1e9)));
		q.push(pp(ii(0,0),ii(0,h-1))); dist[0][h-1] = ii(0,0);
		ii t = (ii(w-1,0));
		while(q.size()){
			ii cost = q.top().first;
			ii pos = q.top().second;	
			//cout<<height[pos.first][pos.second]<<'\n';
			//cout<<pos.first<<' '<<pos.second<<'\n';
			int ch = height[pos.first][pos.second];
			q.pop();
			if(pos == t) break;
			ii next[4];
			for(int i = 0; i < 4; i++) next[i] = pos;
			next[0].first++;
			next[1].first--;
			next[2].second++;
			next[3].second--;
			for(int i = 0; i < 4; i++){
				ii n = next[i];
				int nh = height[n.first][n.second];
				if((n.first < 0) || (n.first >= w) || (n.second < 0) || (n.second >= h)) continue;
				if((nh-ch) > 2) continue;
				if((ch-nh) > 4) continue;
				if(nh > ch){
					if((dist[pos.first][pos.second].first + nh - ch) < dist[n.first][n.second].first){
						dist[n.first][n.second] = dist[pos.first][pos.second];
						dist[n.first][n.second].first += nh-ch;
						q.push(pp(dist[n.first][n.second], n));
					}
				}
				else{
					if((dist[pos.first][pos.second].first <= dist[n.first][n.second].first) && 
					   ((dist[pos.first][pos.second].second + ch-nh) < dist[n.first][n.second].second)){
						dist[n.first][n.second] = dist[pos.first][pos.second];
						dist[n.first][n.second].second += ch-nh;
						q.push(pp(dist[n.first][n.second], n));
					}
				}
			}
		}
		if(dist[w-1][0].first != (int)1e9){
			ii d = dist[w-1][0];
			cout<<d.first<<' '<<d.second<<'\n';
		}
		else cout<<"IMPOSSIBLE\n";


	}
	return 0;
}
