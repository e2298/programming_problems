#include<bits/stdc++.h>
using namespace std;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		string s;
		cin>>s;
		int curr = 0;
		int r = 0;
		for(int i = s.size()-1; i >= 0; i--){
			if(curr == 0){
				if(s[i] == 'a'){
					r++;
					curr ^= 1;
				}

			}
			else{
				if(s[i] == 'b'){
					r++;
					curr ^= 1;
				}
			}
		
		}
		cout<<r<<'\n';
	}

	return 0;
}
