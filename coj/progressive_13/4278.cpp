#include<bits/stdc++.h>
using namespace std;

int f[100000];

int fib(int n){
	if(f[n]) return f[n];
	else return f[n] = (fib(n-1) + fib(n-2)) % 1000000007;
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	f[0] = 1;
	f[1] = 1;
	int  n;
	while(cin>>n){
		cout<<fib(n)<<'\n';
	}
	
	return 0;
}
