#include<bits/stdc++.h>
using namespace std;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	int t;
	cin>>t;
	while(t--){
		int a,b;
		cin>>a>>b;
		int r = min(a,b)+1;
		if(a < b){
			b -= a;	
			r += (int)((double)b*log10(5.0));
		}
		else if(b < a){
			a -= b;
			r += (int)((double)a*log10(2.0));
		}
		cout<<r<<'\n';

	}


	return 0;
}
