#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;
typedef set<ii> sii;

int r = 0;
sii itn;

void solve(int x, int y, int t){
	if(t < 0) return;
	if(itn.count(ii(x,y))) return;
	itn.insert(ii(x,y));
	r++;
	solve(x+1,y,t-1);
	solve(x-1,y,t-1);
	solve(x,y+1,t-1);
	solve(x,y-1,t-1);
	return;
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	int n,m,t;
	cin>>n>>m>>t;
	vector<ii> inv(n);
	for(auto &i : inv){
		cin>>i.first>>i.second;
	}
	while(m--){
		int x,y;
		cin>>x>>y;
		itn.insert(ii(x,y));
	}
	
	for(auto i: inv){
		solve(i.first,i.second,t);
	}
	cout<<r;


	return 0;
}

