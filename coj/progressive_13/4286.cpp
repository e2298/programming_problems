#include<bits/stdc++.h>
using namespace std;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	cin>>n;
	int mn = 1000000008;
	int mx = 0;
	while(n--){
		int t;
		cin>>t;
		mn = min(mn, t) ;
		mx = max(mx, t);
		cout<<mn+mx<<'\n';
	}

	return 0;
}
