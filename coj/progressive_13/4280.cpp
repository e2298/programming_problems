#include<bits/stdc++.h>
using namespace std;

int times[60][100005];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n,a;
		cin>>n;
		for(int i =1 ; i <= n; i++){
			for(int j = 0; j < 60; j++){
				times[j][i] = times[j][i-1];
			}
			cin>>a;
			a--;
			times[a][i]++;
		}
		int q;
		cin>>q;
		while(q--){
			int l,r;
			cin>>l>>r;
			int res = 0;
			for(int j = 0; j < 60; j++){
				res += (times[j][r] - times[j][l-1]) & 1; 
			}
			cout<<res<<'\n';

		}

	}
	return 0;
}
