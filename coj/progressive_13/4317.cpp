#include<bits/stdc++.h>
using namespace std;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	string in;
	int t;
	cin>>t;
	getline(cin,in);
	while(t--){
		getline(cin, in);
		for(auto i:in){
			if(i == 'a' || i == 'e' || i == 'i' || i == 'o' || i == 'u'){
				cout<<i<<'p'<<i;	
			}
			else cout<<i;
		}	
		cout<<'\n';
	}

	return 0;
}
