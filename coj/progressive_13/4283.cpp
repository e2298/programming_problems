#include<bits/stdc++.h>
using namespace std;
typedef multiset<int> ms;


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		int n,q;
		cin>>n;
		ms s;
		while(n--){
			int t;
			cin>>t;
			s.insert(t);
		}
		cin>>q;
		while(q--){
			int p,y;
			cin>>p>>y;
			if(p == 1){
				if(s.empty()){
					cout<<"-1 100001\n";
					continue;
				}
				auto i = s.find(y);
				int x,z;
				if(i != s.end()){
					if(s.upper_bound(y) != s.end()) z = *(s.upper_bound(y));	
					else z = 100001;
					if(i != s.begin()) x = *(--i);
					else x = -1;
				}	
				else{
					i = s.lower_bound(y);	
					if(i != s.end()){
						z = *i;
						if(i != s.begin()) x = *(--i);
						else x = -1;
					}
					else{
						z = 100001;
						i = s.end();
						i--;
						x = *(i);
					}
					
				}
				cout<<x<<' '<<z<<'\n';
			}
			else if(p == 2){
				s.insert(y);		
			}
			else{
				auto i = s.find(y);
				if(i != s.end())
					s.erase(s.find(y));
			}
		}

	}
	

	return 0;
}
