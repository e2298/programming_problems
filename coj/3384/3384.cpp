#include <bits/stdc++.h>
using namespace std;

vector<vector<int> > graph;
bool visited[1005];
int n,m,r,e;


template <typename T,typename U>                                                   
std::pair<T,U> operator+(const std::pair<T,U> & l,const std::pair<T,U> & r) {   
    return {l.first+r.first,l.second+r.second};                                    
}   


//nodes,edges
pair<int,int>dfs (int node){
	pair<int,int> res;
	
	res = {1,0};
	visited[node]=true;

	for(auto i:graph[node]){
		if(!visited[i]){
			res = res + dfs(i);
		}	
		res.second++;
	}

	return res;
}

int main(void){
	int a,b;
	long res;
	pair<int,int> t;
	cin>>n>>m>>r>>e;

	graph = vector<vector<int> >(n+1);

	for(int i = 0; i<m;i++){
		cin>>a>>b;
		graph[a].push_back(b);
		graph[b].push_back(a);
	}

	res = 0;

	for(int i = 1;i<=n;i++){
		if(!visited[i]){
			t = dfs(i);
			t.second/=2;
			//cout<<i<<' '<<t.first<<' '<<t.second<<'\n';
			res += min( t.first*e, ((t.first*(t.first-1))/2-t.second)*r);


		}
	}

	cout<<res<<'\n';

}
