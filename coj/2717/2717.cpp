#include <iostream>
using namespace std;

int main(void){
	int n,l,r,t;
	cin>>t;
	while(t--){
		cin>>n>>l>>r;
		if(n<l){
			cout<<"No\n";
			continue;
		}
		while((r != l) && (n >= l)){
			if(n < r) r = n;

		}
		if(n) cout<<"No\n";
		else cout<<"Yes\n";
	}

	return 0;
}
