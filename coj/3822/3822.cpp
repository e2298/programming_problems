#include <bits/stdc++.h>
int criva[10005];
using namespace std;

void crivar(int n){
	criva[0]=1;
	criva[1]=1;
	n++;
	for(int i = 2; i<n;i++){
		if (criva[i]) continue;
		else{
			for(int j = i*i; j < 10005;j+=i) criva[j]=1;
		}
	}
}


int main(void){
	ios_base::sync_with_stdio(0);

	int n,r=0,t;
	cin>>n;

	crivar(n);
	for(int i = 2;i<=n;i++){
		t = __gcd(n,i);
		if(!criva[t])r++;
	}

	cout<<r<<"\n";

	return 0;
}
