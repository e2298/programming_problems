#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> pi;
typedef complex<int> ci;
typedef vector<ci> vc;
typedef vector<pi> vp;

int main(void){
	int n;
	cin>>n;
	vc points(n);
	for(auto &i: points){
		int a,b;
		cin>>a>>b;
		i = ci(a,b);
	}
	int r = 0;
	for(int i = 0; i < n; i++){
		vp slopes(n);	
		for(int j = 0; j < n; j++){
			if(points[j] == points[i]) continue;
			ci t = points[j] - points[i]; 
			int g = __gcd(t.real(),t.imag());
			slopes[j] = pi(t.real()/g, t.imag()/g);
		}
		sort(slopes.begin(), slopes.end());
		pi last(100000000,0);
		int ctr = 0;
		for(int j = 0; j < n; j++){
			if(j==i)continue;
			if(slopes[j] == last){
				ctr++;
			}
			else{
				last = slopes[j];
				r = max(ctr+1,r);
				ctr = 1;
			}
		}

		r = max(ctr+1,r);
	}  
	cout<<r;

	return 0;
}
