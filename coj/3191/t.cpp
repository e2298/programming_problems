#include<bits/stdc++.h>
using namespace std;

typedef pair<long, long> pii;
typedef pair<pii, long> pip;
typedef set<pip> sp;

pip s[50050];
pip e[50050];
pii d[50050];
pii ss[50050];
long r[50050];
long disj[50050];
long disjw[50050];

long find(long i){
	if(disj[i] == i) return i;
	else return disj[i] = find(disj[i]);
}

long join(long i, long j){
	i = find(i), j = find(j);
	if(i == j) return 0;
	if(disjw[i] > disjw[j]){
		disj[j] = i;
		r[i] += r[j];
		return r[i];
	}
	else if(disjw[i] < disjw[j]){
		disj[i] = j;
		r[j] += r[i];
		return r[j];
	}	
	else{
		disj[i] = j;
		r[j] += r[i];
		disjw[j]++;
		return r[j];
	}
}

int main(void){
	long n, x, y, w, h, m = 0;
	cin>>n;

	for(long i = 0; i<n; i++){
		cin>>x>>y>>w>>h;
		s[i] = pip(pii(x,y),i);
		ss[i] = pii(x,y);
		e[i] = pip(pii(x+w,y+h),i);
		d[i] = pii(w,h);
		m = max(m, r[i] = w * h);
		disj[i] = i;
		disjw[i] = 1;
	}

	sort(s, s+n);
	sort(e, e+n);
	s[n] = pip(pii(1E15,1E15),n+2);
	e[n] = pip(pii(1E15,1E15),n+2);

	long si = 0;
	long ei = 0;
	long ii = s[0].first.first;
	sp curr;
	while((ei < n)){
		long t = s[si].first.first;
		if(t == ii){
			while(s[si].first.first == t){
				curr.insert(pip(pii(s[si].first.second, s[si].first.first),s[si].second));
				si++;
			}
			auto i = curr.begin(), j = curr.begin();
			j++;
			while(j != curr.end()){
				auto tmp = j;
				long k = (*i).second;
				k = d[k].second;
				while((tmp != curr.end())&&(((*i).first.first + k) >= (*tmp).first.first)){ 
					m = max(m, join((*i).second, (*tmp).second));	
					tmp++;
				}
				i++; j++;	
			}
		}
		t = e[ei].first.first;
		if(t == ii){
			while(e[ei].first.first == t){
				long k = e[ei].second;
				curr.erase(pip(pii(ss[k].second,ss[k].first),k));
				ei++;
			}
		}
		ii = min(e[ei].first.first,s[si].first.first);
	}

	cout<<m;


	return 0;
}

