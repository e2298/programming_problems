#include<bits/stdc++.h>
using namespace std;

typedef pair<int,int> pii;

pii s[50050];
pii d[50050];
int r[50050];
int disj[50050];
int disjw[50050];

int find(int i){
	if(disj[i] == i) return i;
	else return disj[i] = find(disj[i]);
}

int join(int i, int j){
	i = find(i), j = find(j);
	if(i == j) return 0;
	if(disjw[i] > disjw[j]){
		disj[j] = i;
		r[i] += r[j];
		return r[i];
	}
	else if(disjw[i] < disjw[j]){
		disj[i] = j;
		r[j] += r[i];
		return r[j];
	}	
	else{
		disj[i] = j;
		r[j] += r[i];
		disjw[j]++;
		return r[j];
	}
}

int main(void){
	int n, x, y, w, h, m = 0;
	cin>>n;

	for(int i = 0; i<n; i++){
		cin>>x>>y>>w>>h;
		s[i] = pii(x,y);
		d[i] = pii(w,h);
		m = max(m, r[i] = w * h);
		disj[i] = i;
		disjw[i] = 1;
	}

	for(int i = 0; i < (n-1); i++){
		for(int j = i+1; j < n; j++){
			int minx,maxx,miny,maxy;
			if(s[i].first < s[j].first){
				minx = i;
				maxx = j;
			} 
			else{
				minx = j;
				maxx = i;
			}
			if(s[i].second < s[j].second){
				miny = i;
				maxy = j;
			}
			else{
				miny = j;
				maxy = i;
			}
			
			if(((s[minx].first+d[minx].first) >= s[maxx].first)
			 //&&((s[minx].first+d[minx].first) <= (s[maxx].first+d[maxx].first))		
		 	 &&((s[miny].second+d[miny].second) >= s[maxy].second)
		 	 //&&((s[miny].second+d[miny].second) <= (s[maxy].second+d[maxy].second))){
				){
				m = max(m, join(i,j));
			}

		}
	}

	cout<<m;

	return 0;
}
