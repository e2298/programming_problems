#include<bits/stdc++.h>
using namespace std;

typedef pair<int,int> cd;
typedef complex<int> ci;
typedef vector<ci> vc;
typedef set<cd> sc;

double len(ci a){
	double x = a.real();
	double y = a.imag();
	return sqrt(x*x+y*y);
}


int cross(ci a, ci b){
	return a.real() * b.imag() - b.real() * a.imag();
}

int main(void){
	int n,m,x,y;
	cin>>n>>m;
	sc border;
	vc points(m);

	while(n--){
		cin>>x>>y;
		border.insert(cd(x,y));
	}

	for(auto &i: points){
		cin>>x>>y;
		i = ci(x,y);
	}
	
	int flag = 1;
	while(flag && (border.size() > 2)){
		flag = 0;
		auto i = border.begin();
		ci a,b,c;
		a = ci((*i).first, (*i).second);i++;
		b = ci((*i).first, (*i).second);i++;
		c = ci((*i).first, (*i).second);i++;
		
		while(i != border.end()){
			//c-a, b-c, a-b;
			for(int j = 0; j<m; j++){
				ci t = points[j];
				if( (cross(c-a,t-a) < 0)
				  &&(cross(b-c,t-c) < 0)
				  &&(cross(a-b,t-b) < 0)){
					flag = 1;
					border.erase(cd(b.real(),b.imag()));
					break;
				}
			}

			if(flag) break;

			a = b;
			b = c;
			c = ci((*i).first, (*i).second);i++;
		}
		if(flag) continue;
		else{
			auto i = border.end();i--;
			b = ci((*i).first, (*i).second);i--;
			a = ci((*i).first, (*i).second);
			i = border.begin();
			c = ci((*i).first, (*i).second);i++;
			for(int j = 0; j<m; j++){
				ci t = points[j];
				if( (cross(c-a,t-a) < 0)
				  &&(cross(b-c,t-c) < 0)
				  &&(cross(a-b,t-b) < 0)){
					flag = 1;
					border.erase(cd(b.real(),b.imag()));
					break;
				}
			}
			if(flag) continue;
			else{
				a = b;
				b = c;
				c = ci((*i).first, (*i).second);
				for(int j = 0; j<m; j++){
					ci t = points[j];
					if( (cross(c-a,t-a) < 0)
					  &&(cross(b-c,t-c) < 0)
					  &&(cross(a-b,t-b) < 0)){
						flag = 1;
						border.erase(cd(b.real(),b.imag()));
						break;
					}
				}

			}
		}
	}

	double r = 0;
	auto i = border.begin();
	ci a,b;
	a = ci((*i).first, (*i).second);i++;
	b = ci((*i).first, (*i).second);i++;
	while(i != border.end()){
		r += len(a-b);
	
		a = b;
		b = ci((*i).first, (*i).second);i++;
	}
	i = border.begin();
	b = ci((*i).first, (*i).second);i++;
	r += len(a-b);

	printf("%.2lf",r);

	return 0;
}
