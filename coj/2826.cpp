#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

vector<int> finales[100010];
ll ppow[50];


ll dp[100010];
ll solve(int i){
	if(i < 0) return 1;
	if(dp[i]) return dp[i];
	ll r = 0;
	for(auto j:finales[i]){
		r += solve(i - j);
		r %= 1000000007ll;
	}
	return dp[i] = r;
}

ll get_hash(string s){
	ll r = 0;
	for(auto i:s){
		r *= 331ll;
		r += i;
	}
	return r;
}

int main(void){
	int n;
	cin>>n;
	ppow[0] = 1;
	for(int i = 1; i < 50; i++) ppow[i] = ppow[i-1] * 331ll;

	set<ll>hashes;
	for(int i = 1; i <= n; i++){
		string s;
		cin>>s;
		hashes.insert(get_hash(s));
	}
	string text;
	cin>>text;
	for(int i = 1; i <= 20 && i <= text.size(); i++){
		ll hash = 0;
		for(int j = 0; j < i && j < text.size(); j++){
			hash *= 331ll;
			hash += text[j];
		}
		if(hashes.count(hash)) finales[i-1].push_back(i);
		for(int j = i; j < text.size(); j++){
			hash *= 331ll;
			hash += text[j];
			hash -= (text[j-i]) * ppow[i];
			if(hashes.count(hash)) finales[j].push_back(i);
		}
	}	


	cout<<solve(text.size()-1);

	return 0;
}
