#include <bits/stdc++.h>
using namespace std;

map<int,int> factorizar(int n){
	map<int,int> arry;
	int t = sqrt(n);
	t++;
	while((n%2)==0){
		arry[2]++;
		n>>=1;
	}
	for(int i = 3; i < t; i++,i++){
		while((n%i)==0){
			arry[i]++;
			n/=i;
		}
	}
	
	if (n != 1) arry[n]=1;

	return arry;
}

int exp(int base, int ex){
	if(ex == 0) return 1;
	else{
		int r = exp(base,ex/2);
		r*=r;
		if(ex&1) r*=base;
		return r;
	}
}

int phi(int n){
	map<int,int> fact = factorizar(n);
	int r = 1;
	int t;

	for(auto i: fact){
		t = exp(i.first,i.second-1)*(i.first-1);
		r*=t;
	}
	return r;
}

int gcd(int a, int b){
	int t;
	while(b){
		t = b;
		b = a%b;
		a = t;
	}

	return a;
}


int main(void){
	unsigned t,n,a,b,d,tot,r;
	cin>>t;

	while(t--){
		r = 0;
		cin>>n>>a>>b;
		tot = phi(n);
		//cout<<n<<' '<<tot<<' ';
		d = b-a+1;
		r += tot*(d/n);
		d = d%n;
		for(int i = a; i<a+d;i++){
			if(gcd(i,n)==1) r++;
		}
		cout<<r<<'\n';

	}

	return 0;
}
