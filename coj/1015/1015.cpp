#include <bits/stdc++.h>

using namespace std;

typedef vector<unsigned long> vi;
typedef vector<vi> vii;

int r;
vi seen;
vii graph, shr;


void dfs(int v){
	seen[v] = 1;
	for(auto i : graph[v]){
		if(!seen[i])
			if(i != 2)
			if(shr[v][2] > shr[i][2]){
				r++;
				dfs(i);
			}
	}	
}

int main(void){
	int n, m;
	int a, b, d;

	cin>>n;
	while(n){
		cin>>m;
		n++;
		
		seen = vi(n);
		graph = vii(n);
	       	shr = vii(n);
	
		unsigned long inf = -1;
		inf /=2;

		for(int i = 1; i < n; i++){
			shr[i] = vi(n, inf);
			shr[i][i] = 0;
		}


		for(int i = 0; i < m; i++){
			cin>>a>>b>>d;

			graph[a].push_back(b);
			graph[b].push_back(a);

			shr[a][b] = shr[b][a] = d;
		}
		
		for(int k = 1; k < n; k++)
			for(int i = 1; i < n; i++)
				for(int j = 1; j < n; j++)
					shr[i][j] = min(shr[i][j],
					  shr[i][k]+shr[k][j]);

		for(auto i: shr) {
			for( auto j: i)
			cout<<j<<' '; 
			cout<<'\n';
		}

		r = 0;
		dfs(1);

		cout<<r<<'\n';
		cin>>n;
	}

	return 0;
}
