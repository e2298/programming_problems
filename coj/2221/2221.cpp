#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
vvi dp;

int solve(int a, int b, int i){
	if(!b) return 0;
	if(dp[a][b]) return dp[a][b];
	if(b == 1) return dp[a][b] = a;
	int r = 0;
	int d = b/2;
	int c = a + d;

	if(i == c){
		return dp[a][b] = i;
	}
	else if(i < c){
		r = c + solve(a,d,i);
	}
	else{
		r = c + solve(c,d,i);
	}

	if(b & 1){
		d = b/2+1;
		c = a + d;
		if(i == c){
			return dp[a][b] = i;
		}
		else if(i < c){
			r = min(r,c + solve(a,d,i));
		}
		else{
			r = min(r,c + solve(c,d,i));
		}
	}
	return dp[a][b] = r;

}

int main(void){
	int a,b;
	while(cin>>a>>b){
		dp = vvi(1010,vi(1010));
		int r = 0;
		for(int i = a; i <= b; i++){
			if(i == 31) break;
			dp = vvi(a+10,vi(b-a+10));
			r = max(r, solve(a,b-a,i));
		}
		cout<<r<<'\n';
	}

	return 0;
}
