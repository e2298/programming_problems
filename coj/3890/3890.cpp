#include <bits/stdc++.h>
using namespace std;



long long unsigned min_needed(int n){
	long long unsigned r=n;
	r+=2ll*n*n*(n+1LL);
	r-=(2LL*(2LL*n+1LL)*(n+1LL)*n)/3LL;
	return r;
}

int main(void){
	long long unsigned n,t;
	int r;
	int min = 1, max = 6E6;
	cin>>n;
	r = max/2;
	while(true){
		t = min_needed(r);
		if(t<n){

			t = min_needed(r+1);
			if(t>n){
				break;
			}
			else{
				min = r;
				r = r+((max-r)/2);
			}
		}
		else if(t>n){
			max = r;
			r = min+((r-min)/2);
		}
	       	else break;	
	}

	cout<<r<<'\n';

	return 0;
}
