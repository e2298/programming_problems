#include<bits/stdc++.h>
using namespace std;

typedef pair<int,int> pi;
typedef vector<pi> vp;
typedef complex<double> cd;
typedef vector<cd> vc;
typedef vector<int> vi;

int main(void){
	int n, r=0;
	cin>>n;
	vc points(n);
	vp closest(n);
	vi seen(n);
	
	for(auto &i:points){
		int x,y;
		cin>>x>>y;
		i = cd(x,y);
	}
	
	if(n == 2) cout<<'0';

	double d1 = 0.0,d2 = 0.0;
	int i1,i2;
	for(int i = 0; i < n; i++){
		d1 = d2 = 1E10;
		for(int j = 0; j < n; j++){
			if(i == j) continue;
			double d = abs(points[i] - points[j]);
			if(d < d1){
				d2 = d1;
				d1 = d;
				i2 = i1;
				i1 = j;
			}
			else if(d < d2){
				d2 = d;
				i2 = j;
			}
		}
		closest[i] = pi(i1,i2);
	}

	for(int i = 0; i < n; i++){
		if(seen[i]) continue;
		int j = closest[i].first;
		int a = closest[i].second;
		int b = closest[j].second;
		seen[j] = 1;
		if(abs(points[j] - points[i]) < max(abs(points[i]-points[b]), abs(points[j]-points[b]))) r++;
		else if(abs(points[j] - points[i]) < max(abs(points[i]-points[a]), abs(points[j]-points[a]))) r++;
	}
	cout<<r;
}
