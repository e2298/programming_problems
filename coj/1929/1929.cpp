#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll a,c,m,f0;
ll asum[100000001];

ll geosum(ll b, ll e){
	if(e == 0) return 1ll;
	if(e == 1) return b+1ll;
	ll r = geosum(b, e/2) * pow(b, e-e/2);
	r %= m;
	r += geosum(b,e-e/2);
	return r%m;
}

ll pow(ll b, ll e){
	if(!e) return 1;
	ll q = pow(b,e/2); q = (q*q)%m;
	return e%2? (b*q)%m:q;
}

ll solve(ll i){
	ll r = f0;
	r *= pow(a,i);
	r %= m;
	r += c*(i?geosum(a,i-1):0);
	r %= m;
	return r;
}

int main(void){
	int q;
	ll n;
	cin>>a>>c>>m>>f0>>q>>n;
	string dir;
	cin>>dir;
	int ord = 1;
	if(dir=="DESC"){
		ord = -1;
	}
	while(q--){
		ll qq;
		cin>>qq;
		cout<<qq<<' ';
		if(ord == 1){
			cout<<solve(qq)<<'\n';
		}
		else cout<<solve(n-qq)<<'\n';
	}


	return 0;
}
