#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll expmod (ll b, ll e, ll m){//O(log b)
	if(!e) return 1;
	ll q= expmod(b,e/2,m); q=(q*q) %m;
	return e %2? (b * q) %m : q;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		ll n;
		ll r;
		cin>>n;
		r = expmod(2ll, n, 1e9+7) -1ll;
		cout<<r<<'\n';
	}

	return 0;
}
