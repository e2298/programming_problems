#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main(void){
	int n, k, nmk,t,tt;
	long long r;
	vector<int> div;
	
	while(scanf("%d %d", &n,&k)!=EOF){
		div = vector<int>(200);
		nmk = n-k;
		for(int t = nmk; t<=n;t++){
			tt=t;
			for(int i = 2; tt>1;i++){
				while((tt%i)==0){
					tt/=i;
					div[i]++;
				}
			}
		}
		for(int t = 2; t<=nmk;t++){
			tt=t;
			for(int i = 2; tt>1;i++){
				while((tt%i)==0){
					tt/=i;
					div[i]--;
				}
			}
		}
		r=1;
		for(int i =2;i<200;i++){
			if (div[i])r*=div[i];
		}
		cout<<r<<endl;
	}
}
