#include <bits/stdc++.h>
using namespace std;

typedef complex<double> cd;
typedef vector<cd> vc;
typedef vector<double> vd;
typedef vector<int> vi;
typedef pair<int,int> pi;
typedef pair<double, pi> pdp;
typedef vector<pdp> vp;

vi disj, disjw;

int find(int a){
	if(disj[a] == a) return a;
	else return disj[a] = find(disj[a]);
}

void join(int a, int b){
	a = find(a);
	b = find(b);
	if(a == b) return;
	if(disjw[a] > disjw[b]){
		disj[b] = a;
	}
	else if(disjw[a] < disjw[b]){
		disj[a] = b;
	}
	else{
		disj[a] = b;
		disjw[b]++;
	}
}


int main(void){
	int t;
	scanf("%d",&t);
	while(t--){
		vc r,b;
		vd xs,ys;
		char flag = 0;
		while(flag != '\n'){
			double in;
			scanf("%lf%c",&in,&flag);
			xs.push_back(in);
		}
		flag = 0;
		while(flag != '\n'){
			double in;
			scanf("%lf%c",&in,&flag);
			ys.push_back(in);
		}
		r = vc(xs.size());
		for(int i = 0; i<xs.size(); i++){
			r[i] =  cd(xs[i],ys[i]);
		}

		flag = 0;
		xs = vd(), ys = vd();
		while(flag != '\n'){
			double in;
			scanf("%lf%c",&in,&flag);
			xs.push_back(in);
		}
		flag = 0;
		while(flag != '\n'){
			double in;
			scanf("%lf%c",&in,&flag);
			ys.push_back(in);
		}
		b = vc(xs.size());
		for(int i = 0; i<xs.size(); i++){
			b[i] =  cd(xs[i],ys[i]);
		}


		int n = b.size();
		disj = vi(n);
		disjw = vi(n);
		for(int i = 0; i < n; i++){
			disj[i] = i;
			disjw[i] = 1;
		}

		vp edges(n*n);
		int s = 0;
		for(int i = 0; i < (n-1); i++){
			for(int j = i+1; j < n; j++){
				edges[s] = (pdp(abs(b[i]-b[j]),pi(i,j)));
				s++;
			}
		}
		edges.resize(s);
		sort(edges.begin(),edges.end());
		double res = 0.0;
		for(auto i : edges){
			pi edge = i.second;
			if(find(edge.first) != find(edge.second)){
				join(edge.first,edge.second);
				res += i.first;
			}
		}

		double mm = abs(r[0]-b[0]);
		for(int i = 0; i < b.size(); i++){
			for(int j = 0; j < r.size(); j++){
				mm = min(mm, abs(b[i]-r[j]));
			}
		}
		cout<<res+mm<<'\n';

	}

	return 0;
}
