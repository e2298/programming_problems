#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

const double EPS = 1e-10;

typedef vector<int> VI;
typedef double T;
typedef vector<T> VT;
typedef vector<VT> VVT;

T GaussJordan(VVT &a, VVT &b) {
  const int n = a.size();
  const int m = b[0].size();
  VI irow(n), icol(n), ipiv(n);
  T det = 1;

  for (int i = 0; i < n; i++) {
    int pj = -1, pk = -1;
    for (int j = 0; j < n; j++) if (!ipiv[j])
      for (int k = 0; k < n; k++) if (!ipiv[k])
	if (pj == -1 || fabs(a[j][k]) > fabs(a[pj][pk])) { pj = j; pk = k; }
    ipiv[pk]++;
    swap(a[pj], a[pk]);
    swap(b[pj], b[pk]);
    if (pj != pk) det *= -1;
    irow[i] = pj;
    icol[i] = pk;

    T c = 1.0 / a[pk][pk];
    det *= a[pk][pk];
    a[pk][pk] = 1.0;
    for (int p = 0; p < n; p++) a[pk][p] *= c;
    for (int p = 0; p < m; p++) b[pk][p] *= c;
    for (int p = 0; p < n; p++) if (p != pk) {
      c = a[p][pk];
      a[p][pk] = 0;
      for (int q = 0; q < n; q++) a[p][q] -= a[pk][q] * c;
      for (int q = 0; q < m; q++) b[p][q] -= b[pk][q] * c;      
    }
  }

  for (int p = n-1; p >= 0; p--) if (irow[p] != icol[p]) {
    for (int k = 0; k < n; k++) swap(a[k][irow[p]], a[k][icol[p]]);
  }

  return det;
}

int main(void){
	int n; double p;
	cin>>n>>p;
	p /= 100.0;
	double pn = 1-p;
	int tam = (n-1)* 2 + 1;

	VVT graph(tam, VT(tam));
		
	int centro = n-1;
	for(int i = 1; i < n-1; i++){
		graph[centro + i][centro + i - 1] = pn;
		graph[centro + i - 1][centro + i] = p;
		graph[centro - i][centro - i + 1] = p;
		graph[centro - i + 1][centro - i] = pn;

	}
	for(int i = 0; i < tam; i++){
		for(int j = 0; j < tam; j++){
			graph[i][j] *= -1.0;
		}
		graph[i][i] += 1.0;
	}

	VVT nada = VVT(tam);
	GaussJordan(graph,nada);
	
	double steps = 0;
	for(int i = 0; i < tam; i++){
		steps += graph[centro][i];
	}

	double prob = graph[centro][tam-1] * p;
	printf("%.5lf %.5lf\n", steps, prob);


	return 0;
}
