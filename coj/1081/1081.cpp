#include <iostream>
#include <iomanip>
#include <complex>
using namespace std;

typedef complex<double> cd;

#define e (10E-8)

int main(void){
	double i1,i2;
	cd a,b,c,la,lb,lc;
	while(1){
		int r = 0;
		cin>>i1>>i2;
		a = cd(i1,i2);
		cin>>i1>>i2;
		b = cd(i1,i2);
		cin>>i1>>i2;
		c = cd(i1,i2);
		if((a+b+c) == cd(0,0)) break;
		la = b-a;
		lb = c-b;
		lc = a-c;

		//point
		if((a==b) && (b == c)){
			if((i1 == (double)(int)i1)&&(i2 == (double)(int)i2)){
				cout<<"   1\n";
			}
			else cout<<"   0\n";
		}


		//line
		else if((real(la)*imag(c-a) - real(c-a)*imag(la)) == 0.0) {
			double xa = real(a), ya = imag(a), xb = real(lb), yb = imag(lb);
			for(double x = 1.0; x < 100.0; x+=1){
				for(double y = 1.0; y < 100.0; y+=1 ){
					if((xa*(y-imag(a)) - (x-real(a))*ya) == 0){
						//check for other way
						if( ((xa*(x-real(a)) + ya*(y-imag(a))) >= 0) 
						  &&(((-xa)*(x-real(b)) + -(ya)*(y-imag(b))) >= 0)){
							r++;
						}
						else if( 
						  ((xb*(x-real(b)) + yb*(y-imag(b))) >= 0) 
						  &&(((-xb)*(x-real(c)) + (-yb)*(y-imag(c))) >= 0)){
							r++;
						}
					}
				}
			}
			cout<<setw(4)<<r<<'\n';

		}

		//tiangle
		else{
			if((real(la)*imag(c-a) - real(c-a)*imag(la)) > 0.0) {
				cd t = b;
				b = c;
				c = t;
				la = b-a;
				lb = c-b;
				lc = a-c;
			}


			
			for(double x = 1.0; x < 100.0; x+=1.0){
				for(double y = 1.0; y < 100.0; y +=1.0){
					int t = 0;
					if((real(la)*(y-imag(a)) - (x-real(a))*imag(la)) <=e) t++;
					if((real(lb)*(y-imag(b)) - (x-real(b))*imag(lb)) <=e) t++;
					if((real(lc)*(y-imag(c)) - (x-real(c))*imag(lc)) <=e) t++;
					if(t == 3) r++;
				}
			}
			
			cout<<setw(4)<<r<<'\n';
		}
	}

}
