#include <bits/stdc++.h>
using namespace std;

int main(void){
	int r,c,t,rl,ud;
	cin>>t;
	while(t--){
		cin>>r>>c;
		
		int m = min(r,c);
		
		if(r == c){
			if( r == 1) cout<<'R';
			else cout<<((r%2)?'R':'L');
		}
		else if( m == r) (cout<<(r%2)?'R':'L');
		else cout<< ((c%2)?'D':'U');
		
		cout<<'\n';	

	}
	return 0;
}
