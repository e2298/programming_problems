#include <bits/stdc++.h>
using namespace std;

typedef complex<long> ci;
typedef vector<ci> vc;
typedef vector<long> vi;
typedef vector<vi> vvi;
typedef pair<long, long> pii;
typedef vector<pii> vpi;


vi disj, disj_w;

bool col_inter(ci a, ci b, ci c, ci d){
	//not collinear
	if( real(b-a) * imag(c-a) - real(c-a) * imag(b-a)) return 0;

	int dot = real(b-a) * real(c-a) + imag(b-a) * imag(c-a);
	if((dot >= 0) && (dot <= norm(b-a))) return 1;
	
	
	dot = real(b-a) * real(d-a) + imag(b-a) * imag(d-a);
	if( dot < 0 ) return 0;
	else if(dot > norm(b-a)) return 0;
	else return 1;
}

bool intersection(ci a, ci b, ci c, ci d){
	ci l1 = b - a;
	ci l2 = d - c;
	int D = real(l2) * imag(l1) - real(l1) * imag(l2);
	int s,t;
	if( D == 0){
		return col_inter(a,b,c,d);
	}
	t = real(l2) * imag(c-a) - imag(l2) * real(c-a);
       	s = real(l1) * imag(c-a) - imag(l1) * real(c-a);	

	if( D < 0){
		if( (t > 0) || s > 0) return 0;
		else if( (t < D) || (s < D)) return 0;
		else return 1;
	}
	else{
		if( (t < 0) || s < 0) return 0;
		else if( (t > D) || (s > D)) return 0;
		else return 1;
	}

}


bool line_point_inter(ci a, ci b, ci c){
	if( real(b-a) * imag(c-a) - real(c-a) * imag(b-a)) return 0;

	long dot = real(b-a) * real(c-a) + imag(b-a) * imag(c-a);
	if( dot < 0 ) return 0;
	else if(dot > norm(b-a)) return 0;
	else return 1;
}


long disj_find(long a){
	if(disj[a] == a) return a;
	else return disj[a] = disj_find(disj[a]);
}

void disj_join(long a, long b){
	long aa = disj_find(a);
	long bb = disj_find(b);

	if(aa == bb) return;
	else if(disj_w[aa] > disj_w[bb]){
		disj[bb] = aa;
		disj_w[aa] += disj_w[bb];
	}
	else{
		disj[aa] = bb;
		disj_w[bb] += disj_w[aa];
	}
}


int main(void){
	long t;
	cin>>t;
	while(t--){
		long n,m,c;
		cin>>n>>m;
		vc points(n+1);
		vpi segments(m);
		vvi taken(105, vi(105));

		disj = vi(n+1);
		disj_w = vi(n+1);

		for(long i = 1; i <= n; i++){
			long x,y;
			cin>>x>>y;
			points[i] = ci(x,y);
			disj[i] = i;

			if(taken[x][y]) disj_join(taken[x][y], i);
			else taken[x][y] = i;

		}
		for(long i = 0; i < m; i++){
			long a,b;
			cin>>a>>b;
			segments[i] = pii(a,b);
			disj_join(a,b);
		}

		for(long k = 0; k < m; k++){
			ci a = points[segments[k].first];
			ci b = points[segments[k].second];
			if(real(a) > real(b)){
				ci tmp = a;
				a = b;
				b = tmp;
			}

			long xd = real(b) - real(a);
			long yd = imag(b) - imag(a);
			if(!(xd||yd)){
				if(taken[real(a)][imag(a)]) disj_join(taken[real(a)][imag(a)],segments[k].first);
				else taken[real(a)][imag(a)] = segments[k].first;
			}	
			else if(!xd){
				long mi = min(imag(a),imag(b));
				long ma = max(imag(a),imag(b));
				for(long i = mi; i <= ma; i++){
					if(taken[real(a)][i]) disj_join(taken[real(a)][i],segments[k].first);
					else taken[real(a)][i] = segments[k].first;
				}
			}
			else if(!yd){
				long mi = min(real(a),real(b));
				long ma = max(real(a),real(b));
				for(long i = mi; i <= ma; i++){
					if(taken[i][imag(a)]) disj_join(taken[i][imag(a)],segments[k].first);
					else taken[i][imag(a)] = segments[k].first;
				}
			}
			else{
				long gcd = __gcd(abs(xd),abs(yd));	
				xd /= gcd;
				yd /= gcd;
				int y = imag(a);
				for(int x = real(a); x <= real(b); x += xd, y += yd){
					if(taken[x][y]) disj_join(taken[x][y],segments[k].first);
					else taken[x][y] = segments[k].first;
				}

			}
		}
		
		cin>>c;

		//for (auto i: disj) cout<<i<<' ';

		while(c--){
			long a,b;
			cin>>a>>b;
			if(disj_find(a) == disj_find(b)) cout<<"YES\n";
			else cout<<"NO\n";
		}

	}

	return 0;
}
