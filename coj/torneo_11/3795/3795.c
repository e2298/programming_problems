#include <stdio.h>
#include <math.h>

int main(void){
	int t,n,s,r;

	scanf("%d", &t);
	while(t--){
		scanf("%d",&n);
		s = sqrt(n);
		r = 1;
		if (s*s == n) {
			r+=s;
		}
		else{
			s++;
		}
		for(int i = 2; i<s;i++){
			if(n%i == 0){
				r+=i;
				r+=n/i;
			} 
		}
		if(r<n) printf("deficient\n");
		else if (r > n) printf("abundant\n");
		else printf("perfect\n");
		
	}
}
