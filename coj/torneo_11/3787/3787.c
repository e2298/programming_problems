#include <stdio.h>
#include <math.h>

int main(void){
	int t,n,tt,ttt;
	scanf("%d", &t);
	while(t--){
		scanf("%d", &n);
		tt = sqrt(n<<1);
		ttt = tt-1;
		tt = tt*tt+tt;
		tt>>=1;
		if (tt == n){
			printf("%d\n",ttt);
		}
		else{
			printf("No solution\n");
		}
	}

	return 0;
}
