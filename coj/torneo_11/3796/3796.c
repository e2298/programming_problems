#include <stdio.h>

int main(void){
	unsigned t,n,m,k;
	unsigned long r, tt;
	scanf("%d", &t);
	while(t--){
		scanf("%d %d %d", &n, &m , &k);
		r = n | (m<<k);
		tt = m-1;
		tt<<=k;
		tt = ~tt;
		r&=tt;
		printf("%ld\n",r);
	}

	return 0;
}
