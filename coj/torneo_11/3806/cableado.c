#include <stdio.h>

int ulams[4001];
void llena_ulam(void);
int ulam(int n);

int main(void){
	llena_ulam();
	printf("{");
	for (int i =0;i<4001;i++){
		printf("%d,",ulams[i]);
	}
	printf("}");
}

void llena_ulam(void){
	ulams[1] = 1;
	ulams[2] = 2;
	ulams[3] = 3;
	for(int i = 4; i < 4001; i++){
		ulams[i] = ulam(i);
	}
}

int ulam(int n){
	int cant;
	int p = ulams[n-1];
	int t;

	//sobre p
	while(p++){
		cant = 0;
		//sobre t
		for(int i = 1; i<n;i++){
			t = ulams[i];
			for(int j = i+1; j < n; j++){
				if (t + ulams[j] == p){
					cant ++;
					break;
				}
			}

			if (cant > 1) break;
		}
		if (cant == 1){
			return p;
		}
	}
}
