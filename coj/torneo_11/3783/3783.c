#include <stdio.h>
#include <math.h>

int main(void){
	int t,n,s,r,k,tt;

	scanf("%d", &t);
	while(t--){
		scanf("%d %d",&n,&k);
		s = sqrt(n);
		r = 0;
		if ((s*s == n)) {
			if(s%k)r+=s;
		}
		else{
			s++;
		}
		for(int i = 1; i<s;i++){
			if((n%i == 0)){
				if(i%k)r+=i;
				tt = n/i;
				if (tt%k)r+=tt;
			} 
		}
		printf("%d\n", r);	
	}
}
