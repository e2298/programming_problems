#include <stdio.h>
#include <stdlib.h>

int main(void){
	int t;
	unsigned n;
	unsigned cincobits = 31;
	int i,tmp, r;
	int* numeros;
	scanf("%d", &t);
	while(t--){
		scanf("%d", &n);
		numeros = calloc(32,sizeof(int));
		i = 32;
		r = 0;
		while(i--){
			tmp = n & cincobits;
			if(!numeros[tmp]){
				numeros[tmp]++;
				r++;
				n = (n<<1)|(n>>(31));
			}
			
		}	
		free(numeros);
		r==32 && printf("yes\n");
		r==32 || printf("no\n");
	}

	return 0;
}
