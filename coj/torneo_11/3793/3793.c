#include <stdio.h>
#include <stdlib.h>

int main(void){
	int t,n,k;
	char** medicines = malloc(3010*sizeof(char*));
	for(int i = 0; i< 3010;i++){
		medicines[i]=malloc(18);
	}
	int* frequencies = malloc(3010*sizeof(int));
	int* freq2 = malloc(3010*sizeof(int));
	int m,i;
	scanf("%d",&t);
	while(t--){
		scanf("%d %d", &n, &k);
		for(int i = 0; i < n; i++){
			scanf("%s %d", medicines[i], frequencies+i);
			freq2[i] = frequencies[i];
		}
		
		while(k--){
			m = 0;
			for(i = 1; i<n; i++){
				if (frequencies[i]<frequencies[m]){
					m = i;
				}
			}
			printf("%d %s\n",frequencies[m],medicines[m]);
			frequencies[m] += freq2[m];
		}

	}

	return 0;
}
