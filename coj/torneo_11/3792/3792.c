#include <stdio.h>

int main(void){
	int n;
	long xi[10005];
	long yi[10005];
	long x,y;
	long r;
	long xx,yy,t;
	while(scanf("%d",&n)!=EOF){
		x = 0;y = 0;
		for(int i =0;i<n;i++){
			scanf("%d %d", &(xi[i]),&(yi[i]));
			x+=xi[i];
			y+=yi[i];
		}
		x = (x+n-1)/n;
		y = (y+n-1)/n;
		
		r=0;	
		for(int i = 0; i<n; i++){
			xx= xi[i]-x;
			yy= yi[i]-y;
			
			t = xx>>63;
			xx = (xx+t)^t;

			t = yy>>63;
			yy = (yy+t)^t;
			r+=xx+yy;
		}
		printf("%ld\n",r);
	}

	return 0;
}
