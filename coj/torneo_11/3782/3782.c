#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void){
	char* uno = "1 2 3 4 5";
	char* dos = "2 3 4 5 6";
	int t;
	char* tt = malloc(100);
	size_t s = 100;
	int i = 0;
	scanf("%d",&t);
	getline(&tt,&s,stdin);
	ciclo:
	while(t--){
		getline(&tt,&s,stdin);
		if(strncmp(tt,uno,9)){
			if (strncmp(tt,dos,9)){
				printf("N\n");
				goto ciclo;
			}
			printf("Y\n");
			goto ciclo;
		}
		printf("Y\n");
	}

	return 0;
}
