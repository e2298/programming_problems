#include <bits/stdc++.h>

using namespace std;

int main(void){
	int t,n,r;
	cin>>t;
	while(t--){
		cin>>n;
		r = 0;
		while(n>>++r);
		cout<<r<<'\n';
	}

	return 0;
}
