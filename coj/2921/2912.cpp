#include<bits/stdc++.h>
using namespace std;

typedef complex<double> cd;
typedef vector<cd> vc;
typedef vector<double> vd;

int main(void){
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	int n, cas = 1;
	cin>>n;
	while(n){
		cd c1,c2;
		double r1,r2;
		double x,y;
		vc h(n);
		vd d1(n+1),d2(n+1);
		int q;
		for(auto &i: h){
			cin>>x>>y;
			i = cd(x,y);
		}
		
		cin>>x>>y;
		c1 = cd(x,y);
		cin>>x>>y;
		c2 = cd(x,y);
		cin>>q;

		for(int i = 0; i<n; i++){
			d1[i] = abs(c1-h[i]);
			d2[i] = abs(c2-h[i]);
		}
		d1[n] = 100000;
		d2[n] = 100000;
		sort(d1.begin(),d1.end());
		sort(d2.begin(),d2.end());
		
		cout<<"Case "<<cas++<<":\n";
		while(q--){
			cin>>r1>>r2;
			int r = n;
			auto i = upper_bound(d1.begin(),d1.end(),r1);
			//r -= distance(d1.begin(),i);
			r -= i-d1.begin();
			i = upper_bound(d2.begin(),d2.end(),r2);
			r -= i-d2.begin();
			//r -= distance(d2.begin(),i);
		
			r = max(r,0);
			cout<<r<<'\n';
		}

		cin>>n;
	}


	return 0;
}
