#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
typedef map<ll,ll> ml;
typedef vector<int> vi;

int n;
int s,t;
ll f;
vi p;
vector<ml> g;

void augment(int v, ll mine){
	if(v==s) f = mine;
	else if(p[v] != -1){
		augment(p[v], min(mine, g[p[v]][v]));
		g[p[v]][v] -= f, g[v][p[v]] += f;
	}
}

ll flow(){
	ll mf = 0;
	do{
		f = 0;
		vector<int> used(2*n+2); queue<int>q; q.push(s);
		p = vi(2*n+2,-1);	
		while((q).size()){
			int u = q.front();q.pop();
			if(u == t) break;
			for(auto i:g[u]){
				if(i.second > 0 && !used[i.first]){
					used[i.first] = 1;
					q.push(i.first);
					p[i.first] = u;
				}
			}
		}
		augment(t, 1e10);
		mf +=f;

	}while(f);
	return mf;
}


int main(void){
	while(cin>>n){
		vi va(n);
		vi vb(n);
		for(int i = 0; i < n; i++){
			cin>>va[i]>>vb[i];
			va[i]--;
			vb[i]--;
		}
		int r = n;
		s = 2*n;
		t = 2*n + 1;

		for(int i = 0; i < n; i++){
			g = vector<ml>(2*n+2);
			int votos=0; int votantes = n-1;
			for(int j = 0; j < n; j++){
				if(j != i){
					if(va[j] == i || vb[j] == i){
						votos ++;
						votantes --;
					}
					else{
						g[j][va[j]+n] = 1;
						g[j][vb[j]+n] = 1;
						g[s][j] = 1;
					}
				}
			}
			if(votos > 0)
				for(int j = 0; j < n; j++){
					g[j+n][t] = votos-1;
				}
			if(votos > 1){	
				g[va[i]+n][t] --;
				g[vb[i]+n][t] --;
			}
			int fl = flow();
			//cout<<i+1<<' '<<votantes<<' '<<votos<<' ';
			//cout<<fl<<endl;
			if(fl == votantes) r--;
		}

		cout<<r<<'\n';
	}
}
