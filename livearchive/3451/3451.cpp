#include<bits/stdc++.h>
using namespace std;
typedef vector<string> vs;
typedef map<long, int> m;

long mod = 4294967291L;
long base = 251L;
//long base = 2147483647L;

vs strings;
m mapp;
int c;

/*
long mpow(long b, int e){
	if(e){
		long t = mpow((b * b) % mod,e>>1);
		t %= mod;
		if(e & 1){
			t *= b;
			t %= mod;
		}
		return t;
	}
	return 1;
}*/

long mpow(long b, int e){
	long r = 1;
	while(e){
		if(e & 1) {
			r *= b;
			r %= mod;
		}
		b *= b;
		b %= mod;
		e>>=1;
	}
	return r;
}

long modn(long a, long b){
	return (b + (a%b)) % b;
}

int can(int n){
	int maxx = 0;
	long lim = mpow(base, n-1);
	//while(!lim);
	for(auto i:strings){
		long hash = 0;
		for(int j = 0; j < n; j++){
			hash *= base;
			hash += i[j];
			hash %= mod;
		} 
		mapp[hash] ++;
		maxx = max(maxx, mapp[hash]);
		for(int j = n; j < i.size(); j++){
			hash -= lim * i[j-n];
			hash = modn(hash, mod);
			hash *= base;
			hash += i[j];
			hash %= mod;
			mapp[hash] ++;
			maxx = max(maxx, mapp[hash]);
		}
	}
	return (maxx>=c);
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	while(cin>>n){
		c = n;
		strings = vs(n);
		
		int up = 1000000;
		for(auto& i : strings){
			cin>>i;
			up = min(up, (int)i.size());
		}
		int lo = 0;
		while(lo < up){
			mapp = m();
			int curr = (lo + up + 1) /2;
			if(can(curr)) lo = curr;
			else up = curr-1;
		}

		cout<<up<<'\n';
	}
	return 0;
}
