#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<vii> graph;
const int MAXN = 10005;
const int LOGN = 16;
#define lg(x) (31-__builtin_clz(x))
graph G;
int n;
#define forr(i,a,b) for(int i=(a); i<(b); i++)
#define forn(i,n) forr(i,0,n)
#define dforn(i,n) for(int i=n-1; i>=0; i--)
#define forall(it,v) for(vii::iterator it=v.begin();it!=v.end();++it)

/****** segment ********************/
#define operacion(x, y) max(x, y)
const int neutro=-1;
struct RMQ{
	int sz;
	int t[4*MAXN];
	int &operator[](int p){return t[sz+p];}
	void init(int n){//O(nlgn)
		sz = 1 << (32-__builtin_clz(n));
		forn(i, 2*sz) t[i]=neutro;
	}
	void updall(){//O(n)
		dforn(i, sz) t[i]=operacion(t[2*i], t[2*i+1]);}
	int get(int i, int j){while(i > j || j >= sz || i < 0);  return get(i,j,1,0,sz);}
	int get(int i, int j, int n, int a, int b){//O(lgn)
		if(j<=a || i>=b) return neutro;
		if(i<=a && b<=j) return t[n];
		int c=(a+b)/2;
		return operacion(get(i, j, 2*n, a, c), get(i, j, 2*n+1, c, b));
	}
	void set(int p, int val){//O(lgn)
		for(p+=sz; p>0 && t[p]!=val;){
			t[p]=val;
			p/=2;
			val=operacion(t[p*2], t[p*2+1]);
		}
	}
}rmq;


/****** LCA ********************/

//f[v][k] holds the 2^k father of v
//L[v] holds the level of v
int f[MAXN][LOGN], L[MAXN];
//call before build:
void dfs(int v, int fa=-1, int lvl=0){//generate required data
	f[v][0]=fa, L[v]=lvl;
	forall(it, G[v])if((*it).first!=fa) dfs((*it).first, v, lvl+1); }

void build(){//f[i][0] must be filled previously, O(nlgn)
	dfs(0);
	for(int k = 0; k < LOGN-1; k++) for(int i = 0; i < n; i++) f[i][k+1]=f[f[i][k]][k];}

int climb(int a, int d){//O(lgn)
	if(!d) return a;
	for(int i = lg(L[a]); i >= 0; i--) if(1<<i<=d) a=f[a][i], d-=1<<i;
    return a;}

int lca(int a, int b){//O(lgn)
	if(L[a]<L[b]) swap(a, b);
	a=climb(a, L[a]-L[b]);
	if(a==b) return a;
	for(int i = lg(L[a]); i >= 0; i--) if(f[a][i]!=f[b][i]) a=f[a][i], b=f[b][i];
	return f[a][0]; }

int dist(int a, int b) {//returns distance between nodes
	return L[a]+L[b]-2*L[lca(a, b)];}


/****** HLD ********************/
int dad[MAXN];
int treesz[MAXN];

void dfs1(int v, int p=-1){//pre-dfs
	dad[v]=p;
	treesz[v]=1;
	forall(i, G[v]) if(i->first != p){
		dfs1(i->first, v);
		treesz[v]+=treesz[i->first];
	}
}

/*secuencia:
 *rmq.init(n)
 *dfs1(inicial);
 *heavylight(inicial);
 *dfs(); (o poner la llamada adentro de build)
 *build(inicial);
 *rmq[pos[v]] = peso entre dad[v] y v para todo v;
 *rmq.updall();
*/
int pos[MAXN], q;//pos[v]=posicion del nodo v en el recorrido de la dfs
//Las cadenas aparecen continuas en el recorrido!
int cantcad;
int homecad[MAXN];//dada una cadena devuelve su nodo inicial
int cad[MAXN];//cad[v]=cadena a la que pertenece el nodo
void heavylight(int v, int cur=-1, int peso = -1){
	if(cur==-1) homecad[cur=cantcad++]=v;
	pos[v]=q++;
	rmq[pos[v]]=peso;
	cad[v]=cur;
	int mx=-1;
	for(int i = 0; i < G[v].size(); i++) if(G[v][i].first!=dad[v])
		if(mx==-1 || treesz[G[v][mx].first]<treesz[G[v][i].first]) mx=i;
	if(mx!=-1) heavylight(G[v][mx].first, cur, G[v][mx].second);
	for(int i = 0; i < G[v].size(); i++) if(i!=mx && G[v][i].first!=dad[v])
		heavylight(G[v][i].first, -1, G[v][i].second);
}
int query(int an, int v){//O(logn)
	//si estan en la misma cadena:
	if(an == v) return -1;
	if(cad[an]==cad[v] && homecad[cad[an]] == an) return rmq.get(pos[an]+1, pos[v]+1);
	if(cad[an]==cad[v]) return rmq.get(pos[an]+1, pos[v]+1);
	return max(query(an, dad[homecad[cad[v]]]),
			   rmq.get(pos[homecad[cad[v]]], pos[v]+1));
}

int edgea[MAXN];
int edgeb[MAXN];


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);
	int t;
	cin>>t;
	while(t--){
		cin>>n;
		G = graph(n);
		memset(dad,0,sizeof(dad));
		memset(treesz,0,sizeof(treesz));
		memset(pos,0,sizeof(pos));
		memset(cad,0,sizeof(cad));
		memset(homecad,0,sizeof(homecad));
		memset(f,0,sizeof(f));
		memset(L,0,sizeof(L));
		rmq.init(n);
		cantcad = 0;
		q = 0;
		for(int i = 0; i < n-1; i++){
			int a,b,c;
			cin>>a>>b>>c;
			a--;
			b--;
			edgea[i] = a;
			edgeb[i] = b;
			G[a].push_back(ii(b,c));
			G[b].push_back(ii(a,c));
		}		

		dfs1(0);
		heavylight(0);
		build();
		rmq.updall();

		string in;
		while(1){
			int a,b;

			cin>>in;
			if(!in.compare("DONE")) break;
			cin>>a>>b;
			if(!in.compare("QUERY")){
				a--;
				b--;
				int l = lca(a,b);
				int r = max(query(l,a), query(l,b));
				cout<<r<<'\n';
			} 
			if(!in.compare("CHANGE")){
				int u,l;
				a--;
				if(dad[edgea[a]] == edgeb[a]){ u = edgeb[a]; l = edgea[a];}
				else if(dad[edgeb[a]] == edgea[a]){u = edgea[a]; l = edgeb[a];}
				rmq.set(pos[l], b);
			}
		}
	}
	return 0;
}
