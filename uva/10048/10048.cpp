#include<bits/stdc++.h>
using namespace std;
typedef vector<unsigned int> vi;
typedef vector<vi> vvi;

int main(void){
	int c,s,q;
	int cas = 1;
	while(cin>>c>>s>>q && (c || s || q)){
		if(cas != 1) cout<<'\n';
		vvi graph(c, vi(c, -1));
	       	while(s--){
			int x,y,z;
			cin>>x>>y>>z;
			x--;
			y--;
			graph[x][y] = z;
			graph[y][x] = z;
	       	}	
		for(int i = 0; i < c; i++)graph[i][i] = 0;		
		for(int k = 0; k < c; k++)
		for(int i = 0; i < c; i++)
		if(graph[i][k] != -1)
		for(int j = 0; j < c; j++)
		if(graph[k][j] != -1)
		graph[i][j] = min(graph[i][j], max(graph[i][k], graph[k][j]));
		
		cout<<"Case #"<<cas++<<'\n';
		while(q--){
			int a,b;
			cin>>a>>b;
			a--;
			b--;
			if(graph[a][b] != -1) cout<<graph[a][b]<<'\n';
			else cout<<"no path\n";
		}

	} 

	return 0;
}
