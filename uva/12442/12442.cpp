#include<bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

vi dp, fw, seen;

int s(int i){
	if(seen[i]){
		return dp[i];
	}
	seen[i] = 1;
	if(seen[fw[i]]) return dp[i] = 1 + dp[fw[i]]; 
	return dp[i] = 1 + s(fw[i]);
}

int main(void){
	int t;
	cin>>t;
	int cas = 1;
	while(t--){
		int a,b,n;
		cin>>n;
		n++;
		dp = vi(n);
		fw = vi(n);
		seen = vi(n);

		for(int i = 1; i < n; i++){
			cin>>a>>b;
			fw[a] = b;
		}	

		int maxn = 0, maxx = 0;

		for(int i = 1; i < n; i++){
			int t = s(i);
			if( t > maxn){
				maxn = t;
				maxx = i;
			}
		}
		for(int i = 1; i< n; i++){
			cout<<i<<' '<<dp[i]<<'\n';
		}
		cout<<"Case "<<cas<<": "<<maxx<<'\n';
		cas++;
	}

	return 0;
}
