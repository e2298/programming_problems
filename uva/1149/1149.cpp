#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int li[100000];

int main(void){
	int t;
	cin>>t;
	int tt = 0;
	while(t--){
		int n;
		cin>>n;
		int l;
		cin>>l;
		int r = 0;
		for(int i = 0; i < n; i++){
			cin>>li[i];
		}
		sort(li, li+n);
		int a=0,b=n-1;
		while(a < b){
			if(li[a]+li[b] > l) b--;
			else {a++, b--;}
			r++;
		}
		if(a == b) r++;
		if(tt) cout<<'\n';
		tt = 1;
		cout<<r<<'\n';
	}

	return 0;
}
