#include<bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

vvi dp;
string in;

int solve(int i, int j){
	if(dp[i][j]) return dp[i][j];
	if(i == j) return 0;
	if(i == (j-1)) return dp[i][j] = 1;
	if(j == (i+2)) {
		if(in[i] == in[j-1]) return dp[i][j] = 2;
		else return 1;
	}
	if(in[i] == in[j-1]) return dp[i][j] = 2 + solve(i+1,j-1);
	return dp[i][j] = max(solve(i+1,j), solve(i,j-1));
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	getline(cin,in);
	while(t--){
		getline(cin,in);
		dp = vvi(in.size()+10, vi(in.size()+10));
		cout<<solve(0, in.size())<<'\n';
	}

	return 0;
}
