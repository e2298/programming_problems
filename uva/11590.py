class trieNode:
    def __init__(self,l):
            self.fin = False
            self.children = [None, None]
            self.l = l
            self.r = 0
            self.acc = 0

    def insert(self, s,l):
        if(len(s) == 1):
            self.fin = True
            self.r += l
            return 1
        else:
            if(self.children[s[0]] is None):
                self.children[s[0]] = trieNode(self.l-1)
            has = self.children[s[0]].insert(s[1:],l)
            if has and self.fin:
                self.r -= l
                return 0 
            return has

    def get(self, s):
        if(len(s)==1):
            return self.r
        else:
            return self.children[s[0]].get(s[1:])

n,m = [int(i) for i in input().split()]
while(n != 0):
    trie = trieNode(m)
    trie.insert([ord("*")-ord("0")],2**m)
    for _ in range(n):
        inp = input()
        trie.insert([ord(i)-ord("0") for i in inp], 2**(m-len(inp)+1))
    k = int(input())
    for _ in range(k):
        inp = input()
        print(trie.get([ord(i)-ord("0") for i in inp]))
    print("")
    input()
    n,m = [int(i) for i in input().split()]
