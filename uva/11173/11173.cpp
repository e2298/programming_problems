#include <bits/stdc++.h>
using namespace std;

int main(void){
	int N, n, k, r, t;
	ios_base::sync_with_stdio(false);

	cin>>N;
	while(N--){
		cin>>n>>k;
		r = 0;
		while(k){
			t = k;
			t |= t>>16;
			t |= t>>8;
			t |= t>>4;
			t |= t>>2;
			t |= t>>1;
			t ^= t>>1;
			r |= t;
			k ^= t;
			k = t-1-k;
		}
		cout<<r<<'\n';
	}

	return 0;
}
