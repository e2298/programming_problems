#include<bits/stdc++.h>
using namespace std;

string p;
int sz;
string mat[4];
int seen[4][4];

int gud(int i, int j){
	return i < 4 && i >= 0 && j < 4 && j>=0 && !seen[i][j];
}

int diri[] = {-1,-1,0,1,1,1,0,-1};
int dirj[] = {0,1,1,1,0,-1,-1,-1};

int search(int i = 0, int j = 0, int k = 0){
	//cout<<i<<' '<<j<<endl;
	int r = 0;
	if(mat[i][j] != p[k]) return 0;
	k++;
	if(k == sz) return 1;
	seen[i][j] = 1;
	for(int l = 0; (l < 8) && !r; l++){
		if(!gud(i+diri[l],j+dirj[l])) continue;
		//cout<<diri[l]<<' '<<dirj[l]<<endl;
		r = r || search(i+diri[l], j+dirj[l],k);
	}
	seen[i][j] = 0;
	return r;
}
int points[] = {0,0,0,1,1,2,3,5,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11};

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	int cas = 1;
	while(t--){
		for(int i = 0; i < 4; i++) cin>>mat[i];
		int n;
		cin>>n;
		int r = 0;
		while(n--){
			cin>>p;
			sz = p.size();
			int found = 0;
			for(int i = 0; i < 4; i++)for(int j = 0; j < 4; j++) found = found || search(i,j,0);
			if(found) r += points[p.size()];
		}
		cout<<"Score for Boggle game #"<<cas++<<": "<<r<<'\n';
	}

	return 0;
}
