#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;
typedef map<ll,ll> ml;
typedef vector<int> vi;

vector<ml> g;
int n;
int siz;
int s,t;
ll f;
vi p;
vi pp;
ll aug; 


void augment(int v, ll mine){
	if(v==s) f = mine;
	else if(pp[v] == aug){
		augment(p[v], min(mine,g[p[v]][v]));
		g[p[v]][v] -= f; g[v][p[v]] +=f;
	}
}

ll flow(){
	ll mf = 0;
	aug = 1;
	p = vi(siz);
	pp = vi(siz);
	vi used(siz);
	do{
		f = 0;
	 	queue<int>q;q.push(s);
		while(q.size()){
			int u = q.front();q.pop();
			if(u==t)break;
			for(auto i:g[u]){
				if(i.second>0 && used[i.first] != aug){
					used[i.first] = aug;
					q.push(i.first);
					p[i.first] = u;
					pp[i.first] = aug;
				}
			}
		}
		augment(t, 1e10);
		mf += f;
		aug++;
	}while(f);
	return mf;
}

int main(void){
	int cas = 1;
	while(cin>>n && n){
		int m;
		cin>>m;
		ll target = 0;
		siz = n + 50000 + 2;
		g = vector<ml>(siz);
		s = n + 50000;
		t = n + 50001;
		vi tim(50001);
		vi rangA;
		vi rangB;
		vi as(n);
		vi bs(n);
		for(int i = 0; i < n; i++){
			int a,b,c;
			cin>>a>>b>>c;
			target += a;
			as[i] = b;
			bs[i] = c;
			g[s][i] = a;
			tim[b] = 1;
			tim[c] = 1;
		}

		int rang = 0;
		for(int i = 0; i < 50001; i++){
			if(tim[i]){
				rangA.push_back(rang);
				rangB.push_back(i-1);
				rang = i;
			}
		}
		for(int i = 0; i < rangA.size(); i++){
			g[i+n][t] = rangB[i]-rangA[i] + 1;
			g[i+n][t] *= m;
		}
		for(int i = 0; i < n; i++){
			for(int j = 0; j < rangA.size(); j++){
				if(rangB[j] >= bs[i]) break;
				if(rangA[j] >= as[i]){
					g[i][j+n] = rangB[j] - rangA[j] +1;
				}
			}
		}
		tim = vi(50001,m);
		ll last = -1;
		for(int i = 0; i < rangA.size(); i++){
			if(last >= rangA[i]){
				exit(1);
			}
			last = rangA[i];
		} 

		ll fl = flow();
		if(fl == target){
			cout<<"Case "<<cas++<<": Yes\n";
			for(int i = 0; i < n; i++){
				vi r;
				for(auto j:g[i]){
					if((j.first-n < 0) || (j.first-n >= rangA.size())) continue;
					ll org = rangB[j.first-n] - rangA[j.first-n] + 1;
					if(j.second < org){
						ll tar = org - j.second;
						int a = rangA[j.first-n];
						for(int i = a; tar; i++){
							if(i > rangB[j.first-n]) exit(1);
							if(tim[i]) {
								r.push_back(i);
								tim[i]--;
								tar--;
							}
						}
					}
				}
				vector<ii> rr;
				int a, b;
				for(int j = 0; j < r.size(); ){
					a = b = r[j];
					int k;
					for(k = j+1; k < r.size() && r[k] == b+1; k++){
						b = r[k];	
					}
					j = k;
					rr.push_back(ii(a,b));
				}

				cout<<rr.size();
				for(auto j:rr){
					cout<<" ("<<j.first<<','<<j.second+1<<')';
				}
				cout<<'\n';
			}	
		}
		else{
			cout<<"Case "<<cas++<<": No\n";
		}
	}
	return 0;
}
