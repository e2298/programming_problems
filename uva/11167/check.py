print("Read input")
all_c = {}
index = 0
while True:
    try:
        n, m = map(int,input().split())
    except:
        break
    c = {}
    all_c[index] = c
    index+=1
    c['n'] = n
    c['m'] = m
    for i in range(n):
        v, s, e = map(int, input().split())
        c[i] = { 'v' : v, 's' : s, 'e' : e}

print("Read output")
index = -1
for c in [all_c[i] for i in range(len(all_c))]:
    index+=1
    y = input()
    if y.endswith('No'):
        continue
    assert y.endswith('Yes')
    available = {i : c['m'] for i in range(50000+1)}
    for i in range(c['n']):
        lastE = -1
        total = 0
        for r in input().split()[1:]:
            s,e = map(int,r[1:-1].split(","))
            assert lastE < s, "Expected: {} < {} ".format(lastE,s)
            lastE = e
            assert s < e, "Expected {} < {}".format(s. e)
            assert c[i]['s'] <= s and  e <= c[i]['e'], "Solution outside range"
            total+=e-s
            for x in range(s,e):
                available[x]-=1
                assert available[x] >= 0, "Expected available {} in case {}"\
                       .format(x, index+1)
        assert total == c[i]['v'], "Expected water: {}. Got: {}".format(
                total,c[i]['v'])
    print("Good result")
