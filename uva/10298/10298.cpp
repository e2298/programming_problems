#include<bits/stdc++.h>
using namespace std;

string T;
string P;
int b[2000010];

void kmppre(){
	int i = 0, j = -1; b[0]=-1;
	while(i<P.size()){
		while(j >=0 && P[i] != P[j]) j = b[j];
		i++,j++;
		b[i] = j;
	}
}

int kmp(){
	int i = 1, j = 0;
	while(i < T.size()){
		while(j >= 0 && T[i] != P[j])j = b[j];
		i++, j++;
		if(j == P.size()){
			return i-j;
		}
	}
}

int main(void){
	while(cin>>P){
		if(P == ".") return 0;
		T = P+P;
		memset(b,0,P.size()*4);
		kmppre();
		cout<<(P.size()/kmp())<<'\n';
	}

}
