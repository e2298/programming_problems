#include<bits/stdc++.h>
using namespace std;

#define MAX_N 1010
#define rBOUND(x) (x<n?r[x]:0)
int sa[MAX_N], r[MAX_N], f[MAX_N], tmpsa[MAX_N],n;
string s;

void countingSort(int k){
	memset(f,0,sizeof(sa));
	for(int i = 0; i < n; i++) f[rBOUND(i+k)]++;
	int sum = 0;
	for(int i = 0; i < max(255,n); i++){
		int t = f[i];
		f[i] = sum;
		sum +=t;
	}
	for(int i = 0; i < n; i++){
		tmpsa[f[rBOUND(sa[i]+k)]++]=sa[i];
	}
	memcpy(sa,tmpsa,sizeof(sa));
}


void constructsa(){
	n = s.size();
	for(int i = 0; i < n; i++) sa[i] = i, r[i] = s[i];
	for(int k = 1; k < n; k<<=1){
		countingSort(k),countingSort(0);
		int rank,tmpr[MAX_N];
		tmpr[sa[0]]=rank=0;
		for(int i = 1; i < n; i++){
			tmpr[sa[i]]=(r[sa[i]]==r[sa[i-1]] && r[sa[i]+k] == r[sa[i-1]+k])? rank:++rank;
		}
		memcpy(r,tmpr,sizeof(r));
		if(r[sa[n-1]]==n-1)break;
	}
}

int LCP[MAX_N],phi[MAX_N],PLCP[MAX_N];
void computeLCP(){
	phi[sa[0]]=-1;
	for(int i = 1; i < n; i++)phi[sa[i]]=sa[i-1];
	int L=0;
	for(int i = 0; i < n;i++){
		if(phi[i]==-1){PLCP[i]=0;continue;}
		while(s[i+L]==s[phi[i]+L]) L++;
		PLCP[i]=L;
		L=max(L-1,0);
	}
	for(int i = 0; i < n; i++) LCP[i] = PLCP[sa[i]];
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		cin>>s;
		s += '$';
		constructsa();
		computeLCP();

		int mx = 0;
		for(int i = 1; i < s.size(); i++)mx = max(mx,LCP[i]);
		if(!mx) {
			cout<<"No repetitions found!\n";
			continue;
		}
		for(int i = 1; i < s.size(); i++)if(LCP[i] >= mx){
			int iorg = i;
			int cant = 1;
			while(i<s.size() && (LCP[i++] >= mx) && cant++);
			cout<<s.substr(sa[iorg],mx)<<' '<<cant<<'\n';
			break;
		}
	}
	return 0;
}
