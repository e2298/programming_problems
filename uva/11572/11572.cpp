#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

int main(void){
	int t,n,s,e,r,nn;
	vi in;
	map< int, bool>hp;
	cin>>t;
	while(t--){
		cin>>n;
		in = vi(n);
		hp = map<int,bool>();
		s = e = r = 0;
		
		nn = n;

		while(nn--) cin>>in[nn];

		for(;e<n;e++){
			if(hp[in[e]]){
				while(in[s]!=in[e]){
					hp[in[s]] = false;
					s++;
				}
				s++;
			}
			else {
				hp[in[e]] = true;
				r = max(r, e-s);
			}
		}

		cout<<++r<<'\n';
	}

	return 0;
}
