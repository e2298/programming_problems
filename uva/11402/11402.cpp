#include <bits/stdc++.h>
using namespace std;
vector<int> segt;
vector<bool> lazy_set;
vector<bool> lazy_clear;
vector<bool> lazy_flip;

void segmentize(){
	int n = segt.size()>>1;
	while(--n){
		segt[n] = segt[n>>1]+segt[(n>>1)+1];
	}
}
//inclusive ranges
int query(int a, int b, int curr, int curr_s, int curr_e){
	
}

int set(int a, int b, int curr, int curr_s, int curr_e){
	if(curr_s > b) return segt[curr];
	else if (curr_e < a) return segt[curr];
	else if (curr_s == curr_e){
		segt[curr] = 1;
		lazy_set[curr] = false;
		lazy_clear[curr] = false;
		lazy_flip[curr] = false;
		return 1;
	}
	else if((a>=curr_s) && (b<=curr_e)){
		segt[curr] = curr_e - curr_s + 1;
		lazy_set[curr*2] = true;
		lazy_set[curr*2+1] = true;
		lazy_clear[curr*2] = false;
		lazy_clear[curr*2+1] = false;
		lazy_clear[curr] = false;
		lazy_flip[curr*2] = false;
		lazy_flip[curr*2+1] = false;
		lazy_flip[curr] = false;
		return segt[curr];
	}
	else{
		return segt[curr] = 
		  set(a,b,curr*2,curr_s, curr_s+(curr_e-curr_s)/2) 
		  + set(a,b,curr*2+1, curr_s+(curr_e-curr_s)/2+1, curr_e);
	}	

}

int clear(int a, int b, int curr, int curr_s, int curr_e){
	
}

int flip(int a, int b, int curr, int curr_s, int curr_e){
	
}



int main(void){
	
	int T,t;
	int n;
	int m;
	int q,a,b;
	char qt;

	cin>>T;
	for(int tt = 1; tt<=T;tt++){
		vector<int> in;
		n = 0;

		cout<<"Case "<<tt<<":\n";
		cin>>m;

		while(m--){
			string s;
			string ss;
			int it = in.size();

			cin>>t;
			cin>>s;
			ss = s;
			while(--t) s+=ss;

			in.resize(in.size()+s.size());

			for(auto i:s){
				in[it] = ((int)i)-48;
				it++;
			}
		}

		
		n = in.size();
		//not a power of 2
		if(n & (n-1)){
			n|=n>>16;
			n|=n>>8;
			n|=n>>4;
			n|=n>>2;
			n|=n>>1;
			n++;
		}

		segt = vector<int>(n<<1);
		lazy_set = vector<bool>(n<<1);
		lazy_clear = vector<bool>(n<<1);
		lazy_flip = vector<bool>(n<<1);

		for(int i = 0; i<in.size();i++)segt[n+i] = in[i];
		segmentize();

		cin>>q;
		int qc = 1;
		while(q--){
			cin>>qt>>a>>b;
			//f set, e clear, i flip, s query
			switch (qt){
				case 'S':{
					cout<<'Q'<<qc<<": "<<query(a,b,0,0,n-1);
					qc++;
					break;
				}
				case 'F':{
					set(a,b,0,0,n-1);
					break;
				}
				case 'E':{
					clear(a,b,0,0,n-1);
					break;
				}
				case 'I':{
					flip(a,b,0,0,n-1);
					break;
				}
			}
		}	
	}
	return 0;
}
