#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
typedef map<ll,ll> ml;
typedef vector<ll> vi;

int n;
int s,t,c;
ll f;
vi p;
vector<ml> graph;

void augment(int v, ll mine){
	if(v==s) f = mine;
	else if(p[v] != -1){
		augment(p[v], min(mine, graph[p[v]][v]));
		graph[p[v]][v] -= f, graph[v][p[v]] += f;
	}
}

ll flow(){
	ll mf = 0;
	do{
		f = 0;
		vector<int> used(n); queue<int>q; q.push(s);
		p = vi(n,-1);	
		while((q).size()){
			int u = q.front();q.pop();
			if(u == t) break;
			for(auto i:graph[u]){
				if(i.second > 0 && !used[i.first]){
					used[i.first] = 1;
					q.push(i.first);
					p[i.first] = u;
				}
			}
		}
		augment(t, 1e10);
		mf +=f;

	}while(f);
	return mf;
}

int main(void){
	int cas = 1;
	while(cin>>n && n){
		cin>>s>>t>>c;
		s--,t--;
		graph = vector<ml>(n);
		int x,y,z;
		for(int i =0; i < c; i++){
			cin>>x>>y>>z;
			x--,y--;
			graph[x][y] +=z;
			graph[y][x] +=z;
		}
		ll f = flow();
		cout<<"Network "<<cas++<<"\nThe bandwidth is "<<f<<".\n\n";
	}

	return 0;
}
