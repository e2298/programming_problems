#include<bits/stdc++.h>
using namespace std;

long double sum[100];

int can(long double s, long double t, int b){
	long double last = 0.0,curr = s;
	b--;
	b--;
	while(b--){
		long double tmp = curr;
		curr = (curr+1.0)*2.0 - last;
		last = tmp;	
	}

	return curr <= t;
}

int main(void){
	int n;
	long double a;	

	long double ad = 0.0;
	for(int i = 1; i < 100; i++, ad += 2.0){
		sum[i] = sum[i-1] + ad;
	}
	while(cin>>n>>a){
		int b = lower_bound(sum,sum+90, a)- sum;
		if(b >= n){
			printf("0.00\n");
			continue;
		}
		long double up = 2.0;
		long double lo = 0.0;
		for(int i = 0; i < 1000 && lo < up; i++){
			long double curr = (up+lo)/2.0;
			if(can(curr, a, b)) lo = curr;
			else up = curr;
		}		
		long double last = 0;
		long double curr = 2.0 - lo;
		int i = 0;
		b++;
		while(b < n){
			long double tmp = curr;
			curr = (curr+1.0)*2.0 - last;
			last = tmp;	

			b++;
		}
		printf("%.2llf\n",curr);
	}

	return 0;
}
