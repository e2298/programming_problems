#include<bits/stdc++.h>
using namespace std;

string s1,s2;
int sz1, sz2;
int cas = 1;

int dp[200][200];
int seen[200][200];

int solve(int i, int j){
	if(i >= sz1) return 0;
	if(j >= sz2) return 0;
	if(seen[i][j] == cas) return dp[i][j];
	seen[i][j] = cas;
	if(s1[i] == s2[j]) return dp[i][j] = 1 + solve(i+1,j+1);

	return dp[i][j] = max(solve(i+1,j), solve(i,j+1));
}

int main(void){
	ios::sync_with_stdio(0);
	while(getline(cin,s1) && s1 != "#"){
		getline(cin,s2);
		sz1 = s1.size();
		sz2 = s2.size();
		printf("Case #%d: you can visit at most %d cities.\n", cas++,solve(0,0));

	}
	return 0;
}
