#include <bits/stdc++.h>
using namespace std;

int swap(int& a , int& b){
	int t;
	t = a;
	a = b;
	b = t;
	return a&b;
}

int main(void){
	int n, m, t, r;
	char c;
	
	cin>>t;
	while(t--){
		cin>>c>>n>>m;
		(n>m) && (swap(n,m));
		if(c =='r'){
			cout<<min(n,m)<<'\n';
		}
		else if (c == 'K'){
			cout<<((n+1)/2)*((m+1)/2)<<'\n';
		}
		else if (c == 'k'){
			if (n==1) {
				cout<<m<<'\n';
			}
			//both odd
			else if ((n&m)&1){
				cout<<  m*(n/2)+(m+1)/2 << '\n';
			}
			//one is even
			else if ((n|m)&1){
				//odd to n
				if(m&1) swap(n,m);
				cout << (m/2) * n << '\n';
			}
			//both even
			else {
				n = ((n+1)/2)*2;
				m = ((m+1)/2)*2;
				cout<<(n*m)/2<<'\n';
			}
		}
		else{
			cout<<min(n,m)<<'\n';
		}
	}
}
