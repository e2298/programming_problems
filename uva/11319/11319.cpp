#include<bits/stdc++.h>
using namespace std;

typedef unsigned long long ll;
typedef vector<int> VI;
typedef long double T;
typedef vector<T> VT;
typedef vector<VT> VVT;
const long double EPS = 1e-10;
int gud;

int GaussJordan(VVT &a, VVT &b) {
  const int n = a.size();
  const int m = b[0].size();
  VI irow(n), icol(n), ipiv(n);
  T det = 1;

  for (int i = 0; i < n; i++) {
    int pj = -1, pk = -1;
    for (int j = 0; j < n; j++) if (!ipiv[j])
      for (int k = 0; k < n; k++) if (!ipiv[k])
	if (pj == -1 || fabs(a[j][k]) > fabs(a[pj][pk])) { pj = j; pk = k; }
    if (fabs(a[pj][pk]) < EPS) { gud = 0; return 1; }
    ipiv[pk]++;
    swap(a[pj], a[pk]);
    swap(b[pj], b[pk]);
    if (pj != pk) det *= -1;
    irow[i] = pj;
    icol[i] = pk;

    T c = 1.0 / a[pk][pk];
    det *= a[pk][pk];
    a[pk][pk] = 1.0;
    for (int p = 0; p < n; p++) a[pk][p] *= c;
    for (int p = 0; p < m; p++) b[pk][p] *= c;
    for (int p = 0; p < n; p++) if (p != pk) {
      c = a[p][pk];
      a[p][pk] = 0;
      for (int q = 0; q < n; q++) a[p][q] -= a[pk][q] * c;
      for (int q = 0; q < m; q++) b[p][q] -= b[pk][q] * c;      
    }
  }

  for (int p = n-1; p >= 0; p--) if (irow[p] != icol[p]) {
    for (int k = 0; k < n; k++) swap(a[k][irow[p]], a[k][icol[p]]);
  }

  return 0;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		VVT mat (7, VT(7,1));
		for(int i = 1; i<= 7; i++) for(int j = 1; j < 7; j++){
			mat[i-1][j] = mat[i-1][j-1] * i;
		}
		
		VVT ans (7, VT(1));
		for(int i = 0; i < 7; i++){
			long double in;
			cin>>in;
			ans[i][0] = in;
		}
		
		VVT inp = ans;
		gud = 1;
		GaussJordan(mat, ans);

		vector<ll> vals(7);
		for(int i = 0; i < 7; i++) vals[i] = (ll)(ans[i][0]+0.5);

		for(ll i = 1ll; i <= 7ll; i++){ 
			ll in; 
			in = (ll)(inp[i-1][0] + 0.5);	
			ll mult = 1ll;

			in -= vals[0];
			for(int j = 1; j < 7; j++){
				mult *= i;
				ll t = vals[j]  * mult;
				in -= t;
			}
			if(in){
				gud = 0;
			}
		} 
		for(ll i = 8ll; i <= 1500ll; i++){ 
			ll in; 
			cin>>in;	
			ll mult = 1ll;

			in -= vals[0];
			for(int j = 1; j < 7; j++){
				mult *= i;
				ll t = vals[j]  * mult;
				in -= t;
			}
			if(in){
				gud = 0;
			}

		}

		if(gud) {
			cout<<vals[0];
			for(int i = 1; i < 7; i++)cout<<' '<<vals[i];
		}
		else cout<<"This is a smart sequence!";
		if(t >=1)cout<<'\n';
	}

	return  0;
}
