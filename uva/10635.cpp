#include<bits/stdc++.h>
using namespace std;

int s1[63000];
int cas = 1;

int sz;
unsigned arry[63000];
int tail[63000];

int lis(){
	memset(tail,0,sz*sizeof(int));
	int length = 1; // always points empty slot in tail

	tail[0] = arry[0];

	for (int i = 1; i < sz; i++) {
		// Do binary search for the element in
		// the range from begin to begin + length
		auto it = lower_bound(tail, tail+length, arry[i]);

		if (it == tail + length)
			tail[length++] = arry[i];
		else
			*it = arry[i];
	}
	return length;
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int t;
	cin>>t;
	while(t--){
		memset(s1,-1,sizeof(s1));
		int n,p,q;
		cin>>n>>p>>q;
		for(int i = 0; i <= p; i++) {
			int a;
			cin>>a;
			s1[a] = i;
		}
		for(int i = 0; i <= q; i++) {
			int a;
			cin>>a;
			arry[i]= s1[a];
		}
		sz = q+1;
		printf("Case %d: %d\n", cas++,lis());

	}
	return 0;
}
