#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int main(void){
	int n,nn;
	while(true){
		int r = 0;
		int cnt = 0;
		int t = 0;
		map<vi,int> mp;
		cin>>n;
		nn=n;
		vi rs(n);
		if(!n) break;

		while(nn--){
			vi tmp(5);
			for(int i = 0; i<5;i++)cin>> tmp[i];
			sort(tmp.begin(),tmp.end());
			mp[tmp]++;
		}

		
		for(auto i:mp){
			rs[r++] = i.second;
			cnt++;
		}
		rs.resize(cnt);
		
		sort(rs.begin(),rs.end());

		r = rs[cnt-1];

		for(int i = cnt-1; (i>-1) && (rs[i]==r);t+=rs[i--]);

		cout<<max(r,t)<<'\n';
	}
	return 0;
}
