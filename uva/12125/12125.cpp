#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef complex<double> cd;

cd points[110];
int s, t;
vector<map<int, int> > go , g;
int f;
vector<int> p;
void augment(int v, int mine){
	if(v == s) f = mine;
	else if(p[v] != -1){
		augment(p[v], min(mine, g[p[v]][v]));
		g[p[v]][v] -=f; g[v][p[v]] += f;
	}
}

int flow(){
	int mf = 0;
	do{
		f = 0;
		vector<char> used(220); queue<int> q; q.push(s);
		p = vector<int>(220,-1);
		while(q.size()){
			int u = q.front(); q.pop();
			if(u == t) break;
			for(auto i:g[u]){
				if(i.second > 0 && !used[i.first]){
					used[i.first] = 1; q.push(i.first); p[i.first] = u;
				}	
			}
		}
		augment(t, 1e8);

		mf +=f;
	}while(f);
	return mf;
}


int main(void){
	int casos;
	cin>>casos;
	while(casos--){
		int n;
		double d;
		int peng = 0;
		cin>>n>>d;
		go = vector<map<int, int> >(2*n + 2);
		s = 2*n;
		t = 2*n+1;
		for(int i = 0; i < n; i++){
			double a,b;
			int c, d;
			cin>>a>>b>>c>>d;
			points[i] = cd(a,b);
			peng += c;
			go[s][2*i] = c;
			go[2*i][2*i+1] = d;
		}
		for(int i = 0; i < n; i++){
			for(int j = i+1; j < n; j++){
				if(abs(points[i] - points[j]) <= d){
					go[2*i+1][2*j] = 1e8;
					go[2*j+1][2*i] = 1e8;	
				}		
			}
		}
		int can = 0;
		for(int i = 0; i < n; i++){
			g = go;
			g[2*i+1][t] = 1e8;
			g[2*i][t] = 1e8;

			if(flow() == peng) {
				if(can) cout<<' ';
				cout<<i;
				can = 1;
			}
		}
		if(!can) cout<<"-1\n";
		else cout<<'\n';

	}

	return 0;
}
