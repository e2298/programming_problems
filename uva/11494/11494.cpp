#include <bits/stdc++.h>
using namespace std;

int main(void){
	pair<int,int> a,b;

	while(true){
		cin>>a.first>>a.second>>b.first>>b.second;
		if(a.first == 0) break;

		if(a == b) cout<<"0\n";
		else if(a.first == b.first)cout<<"1\n";
		else if(a.second == b.second)cout<<"1\n";
		else if(abs(a.first-b.first)==abs(a.second-b.second)) cout<<"1\n";
		else cout<<"2\n";
	}

	return 0;
}
