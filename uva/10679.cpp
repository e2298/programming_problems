#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())

int r[1000];
vector<int> others[1000];
struct corasick{
	map<char,corasick> next;
	corasick* tran[256];
	int idhoja,szhoja;
	corasick *padre, *link, *linkhoja;
	char pch;
	corasick(): tran(),idhoja(-1),padre(),link(),linkhoja(){}
	void insert(const string &s, int id, int p = 0){
		if(p<sz(s)){
			corasick &ch = next[s[p]];
			tran[(int)s[p]] = &ch;
			ch.padre = this, ch.pch = s[p];
			ch.insert(s,id,p+1);
		}
		else{
			if(idhoja >= 0){
				others[idhoja].push_back(id);
			}
			else idhoja = id;
		}
	}
	corasick* get_link(){
		if(!link){
			if(!padre) link = this;
			else if(!padre->padre) link = padre;
			else  link = padre->get_link() -> get_tran(pch);
		}
		return link;
	}
	corasick* get_tran(int c){
		if(!tran[c]) tran[c] = !padre?this:this->get_link()->get_tran(c);
		return tran[c];
	}
	corasick* get_linkhoja(){
		if(!linkhoja){
			if(get_link()->idhoja >=0) linkhoja = link;
			else if(!padre) linkhoja = this;
			else linkhoja = link->get_linkhoja();
		}
		return linkhoja;
	}
	void report(int p){
		if(idhoja>=0) {
			r[idhoja] = 1;
			for(auto i:others[idhoja]) r[i] = 1;
		}
		if(get_linkhoja()->idhoja>=0) linkhoja->report(p);
	}
	void matching(const string& s, int p = 0){
		report(p); if(p<sz(s)) get_tran(s[p])->matching(s,p+1);
	}
}cora;


int main(void){
	int t;
	cin>>t;
	while(t--){
		cora = corasick();
		string tx;
		cin>>tx;
		int n;
		cin>>n;
		memset(r,0,sizeof(int)*n);
		for(int i = 0; i < n; i++){
			others[i] = vector<int>();
			string s;
			cin>>s;
			cora.insert(s,i);
		}
		cora.matching(tx);
		for(int i = 0; i < n; i++){
			cout<<(r[i]?"y\n":"n\n");
		}

	}
	return 0;
}
