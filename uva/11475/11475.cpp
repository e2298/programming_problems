#include<bits/stdc++.h>
using namespace std;
typedef vector<string> vs;
typedef map<unsigned long, int> m;

const unsigned long mod = 4294967291L;
const unsigned long base = 251L;
string in;

unsigned long mpow(unsigned long b, int e){
	unsigned long r = 1;
	while(e){
		if(e & 1) {
			r *= b;
			r %= mod;
		}
		b *= b;
		b %= mod;
		e>>=1;
	}
	return r;
}

int main(void){
	while(cin>>in){	
		int i = in.size()-1;
		int maxx = in.size()-1;
		unsigned long hashf = 0, hashb = 0;
		unsigned long p = 1;

		while(i > -1){
			hashf *= base;
			hashf += in[i];
			hashf %= mod;
			
			hashb += p*in[i];
			hashb %= mod;
			p *= base;
			p %= mod;

			if(hashb == hashf){
				maxx = i;
			}
			i--;
		}
		i = in.size();
		in.resize(i+maxx);
		for(int j = maxx-1; j > -1; j--,i++){
			in[i] = in[j];
		}

		cout<<in<<'\n';
	}

	return 0;
}
