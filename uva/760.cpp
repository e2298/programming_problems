#include<bits/stdc++.h>
using namespace std;

#define MAX_N 1000
#define rBOUND(x) (x<n?r[x]:0)
int sa[MAX_N], r[MAX_N], f[MAX_N], tmpsa[MAX_N],n;
string s;

void countingSort(int k){
	memset(f,0,sizeof(sa));
	for(int i = 0; i < n; i++) f[rBOUND(i+k)]++;
	int sum = 0;
	for(int i = 0; i < max(255,n); i++){
		int t = f[i];
		f[i] = sum;
		sum +=t;
	}
	for(int i = 0; i < n; i++){
		tmpsa[f[rBOUND(sa[i]+k)]++]=sa[i];
	}
	memcpy(sa,tmpsa,sizeof(sa));
}


void constructsa(){
	n = s.size();
	for(int i = 0; i < n; i++) sa[i] = i, r[i] = s[i];
	for(int k = 1; k < n; k<<=1){
		countingSort(k),countingSort(0);
		int rank,tmpr[MAX_N];
		tmpr[sa[0]]=rank=0;
		for(int i = 1; i < n; i++){
			tmpr[sa[i]]=(r[sa[i]]==r[sa[i-1]] && r[sa[i]+k] == r[sa[i-1]+k])? rank:++rank;
		}
		memcpy(r,tmpr,sizeof(r));
		if(r[sa[n-1]]==n-1)break;
	}
}

int LCP[MAX_N],phi[MAX_N],PLCP[MAX_N];
void computeLCP(){
	phi[sa[0]]=-1;
	for(int i = 1; i < n; i++)phi[sa[i]]=sa[i-1];
	int L=0;
	for(int i = 0; i < n;i++){
		if(phi[i]==-1){PLCP[i]=0;continue;}
		while(s[i+L]==s[phi[i]+L]) L++;
		PLCP[i]=L;
		L=max(L-1,0);
	}
	for(int i = 0; i < n; i++) LCP[i] = PLCP[sa[i]];
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	string s1, s2;
	int dun = 0;
	while(cin>>s1>>s2){
		if(dun) cout<<'\n';
		dun = 1;
		s = s1;
		s += '#';
		s += s2;
		s += '$';
		constructsa();
		computeLCP();
		int n = s.size();
		int m = s1.size();
		int mx = 0;
		for(int i = 0; i < n-1; i++){
			if((sa[i] < m && sa[i+1] > m) || (sa[i] >m && sa[i+1] < m)) mx = max(mx, LCP[i+1]);
		}
		//for(int i = 0; i < n; i++){
		//	cout<<s.substr(sa[i])<<'\n';
		//}
		if(mx == 0){
			cout<<"No common sequence.\n";
			continue;
		}
		int brk = 1;
		set<string> r;
		for(int i = 1; i < n; i++){
			if((LCP[i] == mx) && ((sa[i] < m && sa[i-1] > m) || (sa[i] >m && sa[i-1] < m))){
				if(brk)
					r.insert(s.substr(sa[i],mx));
				brk = 0;
			}
			else brk = 1;
		}
		for(auto i:r)cout<<i<<'\n';
	}
	return 0;
}
