#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll digs[300];
int sz;
ll dp[300];
unsigned lim = 1<<31;
ll solve(int i){
	if(i >= sz) return 0;
	if(dp[i] != -1) return dp[i];
	ll num = digs[i];
	ll r = num;
	int j = i+1;
	while(num < lim && j < sz){
		r = max(r, solve(j)+num);
		num *= 10ll;
		num += digs[j++];
	}
	if(num<lim)r = max(r, solve(j)+num);

	return dp[i] = r;
}

int main(void){
	int t;
	cin>>t;
	while(t--){
		string s;
		cin>>s;
		sz = s.size();
		for(int i = 0; i < sz; i++)digs[i] = s[i]-'0';
		memset(dp,-1,sizeof(dp));
		cout<<solve(0)<<'\n';
		//for(int i = 0; i < sz; i++)cout<<dp[i]<<' ';
		//cout<<'\n';
	}

	return 0;
}
