#include<bits/stdc++.h>
using namespace std;
typedef pair<unsigned, unsigned> pi;
typedef vector<int> v;
typedef vector<pi> vi;
typedef vector<vi> vvi;

vvi graph;

//s = tam y obj
int spfa(int s){
	v d(s<<1, 1000000000);
	v in(s<<1);
	s--;
	int par = 0;
	d[0] = 0;

	queue<pi> q;
	q.push(pi(par,0));
	in[0] = 1;
	while(!q.empty()){
		pi t = q.front();
		q.pop();
		par = t.first;
		int curr = t.second;
		in[(curr<<1) + par] = 0;
		for( auto i : graph[curr]){
			int w = i.second;			
			int nodo = i.first;
			if((d[(curr<<1)+par] + w) < d[(nodo<<1)+(par^1)]){
				d[(nodo<<1)+(par^1)] = 	(d[(curr<<1)+par] + w);
				if(!in[(nodo<<1)+(par^1)]){
					in[(nodo<<1) + (par^1)] = 1;
					q.push(pi( par^1, nodo));
				}
			}
		}
	}
	return d[s<<1];
}


int main(void){
	int c,v;
	while(cin>>c>>v){
		graph = vvi(c);
		int a,b,w;
		while(v--){
			cin>>a>>b>>w;
			a--;
			b--;
			graph[a].push_back(pi(b,w));
			graph[b].push_back(pi(a,w));
		
		}
		unsigned int r = spfa(c);

		if(r == 1000000000) cout<<"-1\n";
		else cout<<r<<'\n';
	}
	return 0;
}
