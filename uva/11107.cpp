#include<bits/stdc++.h>
using namespace std;

#define MAX_N 101000
#define rBOUND(x) (x<n?r[x]:0)
int sa[MAX_N], r[MAX_N], f[MAX_N], tmpsa[MAX_N],n;
int owner[MAX_N];
string s;

void countingSort(int k){
	memset(f,0,sizeof(sa));
	for(int i = 0; i < n; i++) f[rBOUND(i+k)]++;
	int sum = 0;
	for(int i = 0; i < max(255,n); i++){
		int t = f[i];
		f[i] = sum;
		sum +=t;
	}
	for(int i = 0; i < n; i++){
		tmpsa[f[rBOUND(sa[i]+k)]++]=sa[i];
	}
	memcpy(sa,tmpsa,sizeof(sa));
}


void constructsa(){
	n = s.size();
	for(int i = 0; i < n; i++) sa[i] = i, r[i] = s[i];
	for(int k = 1; k < n; k<<=1){
		countingSort(k),countingSort(0);
		int rank,tmpr[MAX_N];
		tmpr[sa[0]]=rank=0;
		for(int i = 1; i < n; i++){
			tmpr[sa[i]]=(r[sa[i]]==r[sa[i-1]] && r[sa[i]+k] == r[sa[i-1]+k])? rank:++rank;
		}
		memcpy(r,tmpr,sizeof(r));
		if(r[sa[n-1]]==n-1)break;
	}
}

int LCP[MAX_N],phi[MAX_N],PLCP[MAX_N];
void computeLCP(){
	phi[sa[0]]=-1;
	for(int i = 1; i < n; i++)phi[sa[i]]=sa[i-1];
	int L=0;
	for(int i = 0; i < n;i++){
		if(phi[i]==-1){PLCP[i]=0;continue;}
		while(s[i+L]==s[phi[i]+L]) L++;
		PLCP[i]=L;
		L=max(L-1,0);
	}
	for(int i = 0; i < n; i++) LCP[i] = PLCP[sa[i]];
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int dun = 0;
	int n;
	while(cin>>n && n){
		if(dun) cout<<'\n';
		dun = 1;
		string in;
		s = "";
		for(int i = 0; i < n; i++){
			cin>>in;
			for(int j = 0; j < in.size(); j++) in[j] += 3;
			for(int j = 0; j < in.size(); j++) owner[s.size()+j] = i;
			s += in + (char)(i+1);
		}
		constructsa();
		computeLCP();
		int mx = 0;
		for(int i = 1; i < s.size(); i++){
			if(owner[sa[i]]  != owner[sa[i-1]]) mx = max(mx, LCP[i]);
		}
		for(int i = 0; i < s.size(); i++) s[i]-=3;
		//for(int i = 0; i < s.size(); i++){
		//	cout<<sa[i]<<endl;
		//	cout<<s.substr(sa[i])<<endl;
		//}
		if(mx == 0){
			cout<<"?\n";
			continue;
		}
		set<string> r;
		for(int i = 1; i < s.size(); i++){
			if((LCP[i] == mx)){
				set<int> owners;
				owners.insert(owner[sa[i-1]]);
				for(; i < s.size() && LCP[i] == mx; i++){
					owners.insert(owner[sa[i]]);
				}
				if((owners.size() * 2) > n) r.insert(s.substr(sa[i-1], LCP[i-1]));
			}
		}
		if(r.empty()){
			cout<<"?\n";
			continue;
		}

		for(auto i:r)cout<<i<<'\n';
	}
	return 0;
}
