#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

int main(void){
	int n;
	bool r;
	vi nums;
	vb fl;
	

	while(cin>>n){
		nums = vi(n);
		fl = vb(n);
		r = true;

		for(int i = 0; i < n; i++){
			cin>>nums[i];
		}

		for(int i = 0; i<(n-1); i++){
			fl[abs(nums[i]-nums[i+1])] = 1;
		}

		for(int i = 1; i < n; i++){
			r &= fl[i];
		}

		if (r) cout<<"Jolly\n";
		else cout<<"Not jolly\n";

	}


	return 0;
}
