#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef deque<int> di;
typedef pair<int, int> ii;

ii qs[300005];
ii rs[300005];


int main(void){
	int n,qu;
	cin>>n>>qu;
	di q;
	int mx = 0;
	for(int i = 0; i < n; i++){
		int t;
		cin>>t;
		mx = max(mx, t);
		q.push_back(t);
	}
	for(int i = 0; i < qu; i++){
		cin>>qs[i].first;
		qs[i].second = i;
	}
	sort(qs,qs+qu);
	
	int i = 1;
	int j = 0;
	while(q.front() != mx){
		int a = q.front();
		q.pop_front();
		int b = q.front();
		q.pop_front();
		if(a > b){
			q.push_front(a);
			q.push_back(b);
		}
		else{
			q.push_front(b);
			q.push_back(a);
		}
		if(qs[j].first == i){
			rs[qs[j].second].first = a;			
			rs[qs[j].second].second = b;			
			j++;
		}

		i++;
	}
	for(; j < qu; j++){
		int qr = qs[j].first - i;		
		qr %= (n-1);
		qr++;
		rs[qs[j].second].first = q.front();
		rs[qs[j].second].second = q[qr];
	}
	for(i = 0; i < qu; i++) cout<<rs[i].first<<' '<<rs[i].second<<'\n';

	
	return 0;
}
