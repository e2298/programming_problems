#include<bits/stdc++.h>
using namespace std;

int s[1000];
int d[1000];

int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		cin>>s[i];		
	}
	sort(s,s+n);

	int r = 1;
	int c = 0;
	for(int i = 2; i < s[n-1]+s[n-2]; i++){
		c++;
		int curr = 0;
		for(int j = 0; j < n-1; j++){
			if(d[j] == c) continue;
			auto rs = lower_bound(s+j+1, s+n, abs(s[j]-i));
			int dn = 0;
			while(rs < s+n && *rs == abs(s[j]-i)){
				if(d[rs-s] != c){
					dn = 1;
					d[rs-s] = c;
					break;
				}
				rs++;
			}
			if(dn) curr++; 
		}
		r = max(r, curr);
	}
	cout<<r;

	return 0;
}
