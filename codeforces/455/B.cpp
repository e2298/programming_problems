#include<iostream>
#include<vector>
using namespace std;

typedef vector<int> vi;

int main(void){
	int n,r=0;
	cin>>n;
	vi cnt(n+1);
	for(int i = 1; i<=n; i++){
		for(int j = 1; (j+i-1) <= n; j++){
			for(int k = j; k<j+i;k++){
				cnt[k]++;
			}
		}	
	}	

	for(auto i:cnt) r = max(i,r);

	cout<<r;

	return 0;
}
