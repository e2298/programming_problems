#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef multiset<ll> ms;
typedef map<ll,ll> mi;
typedef pair<ll,ll> ii;
typedef vector<ii> vii;


ll logc(ll n){
	if(n&(n-1)){
		n |= n>>16;
		n |= n>>8;
		n |= n>>4;
		n |= n>>2;
		n |= n>>1;
		n++;
	}
	return __builtin_ctz(n);
}


int main(void){
	cin.tie(0);
	ios::sync_with_stdio(0);	
	ll n,i;
	cin>>n>>i;
	i*=8;
	mi m;
	for(ll i = 0; i < n; i++){
		ll t;
		cin>>t;
		m[t]++;
	}
	ll curr = 0;
	ll mn = n+1;
	ll ns = i/n;
	ll drop = m.size() - (1ll<<ns);

	auto l = m.begin();
	auto r = m.end();
	if(drop <= 0 || ns > 31) {
		cout<<0;
		return 0;
	}
	for(ll i = 0; i < drop; i++, l++){
		curr += (*l).second;
	}
	mn = curr;
	l--;
	r--;
	for(ll i = 1; i <= drop; i++, l--, r--){
		curr += (*r).second;
		curr -= (*l).second;
		mn = min(mn, curr);
	}
	cout<<mn;

}

