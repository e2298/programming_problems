#include<bits/stdc++.h>
using namespace std;

int dp [51][51][51][51];
char in[50][51];

int solve(int a, int b, int x, int y){
	if(a == x || b == y) return 0;
	if(x-a == 1 && y-b == 1) return in[a][b] == '#';
	if(dp[a][b][x][y]) return dp[a][b][x][y];
	
	int r = min(x-a, y-b);

	int d, e, f, g;
	d = solve(a,b, x, b+1);
	e = solve(a,b, a+1, y);
	f = solve(x-1, b, x, y);
	g = solve(a, y-1, x, y);

}



int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		cin>>in[i];
	}	

	return 0;
}
