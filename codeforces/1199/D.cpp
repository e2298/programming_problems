#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef multiset<int> ms;

int ps[200000];
int qs[200001];
int ls[200000];


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n,q;
	cin>>n;
	for(int i = 0; i < n; i++) cin>>ps[i];
	cin>>q;
	for(int i = 1; i <= q; i++){
		int t, p, x;
		cin>>t;
		if(t==1){
			cin>>p>>x;
			ps[--p] = x;
			ls[p] = i;			
		}
		else{
			cin>>x;
			qs[i] = x;
		}
	}
	int mx = qs[q];
	for(int i = q-1; i; i--){
		mx = qs[i] = max(qs[i], mx);
	}
	qs[0] = mx;
	
	for(int i = 0; i < n; i++){
		cout<<max(ps[i], qs[ls[i]])<<' ';		
	}

	return 0;		
}
