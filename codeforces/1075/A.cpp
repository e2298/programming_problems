#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(NULL);

	ll n;
	cin>>n;
	ll x,y;
	cin>>x>>y;
	ll d1 = max(x,y);
	d1--;
	ll d2 = min(x,y);
	d2 = n - d2;
	if(d1 <= d2) cout<<"White\n";
	else cout <<"Black\n";

	return 0;
}
