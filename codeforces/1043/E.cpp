#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> ii;

ll a[300005];
ll b[300005];
ll r[300005];
int ca=0, cb=0;
ll sa=0, sb=0;
ii da[300005];
ii db[300005];
ll suma[300005];
ll sumb[300005];

int main(void){
	int n,m;
	cin>>n>>m;
	for(int i = 0; i < n; i++){
		cin>>a[i]>>b[i];
		if(a[i] < b[i]){
			sa+=a[i];
			da[ca] = ii(b[i] - a[i], i);
			ca++;
		}
		else{
		       	sb += b[i];
			db[cb] = ii(a[i] - b[i], i);
			ca++;
		}
	}

	sort(da, da+ca);
	sort(db, db+cb);
	suma[0] = a[da[0].second];
	sumb[0] = b[db[0].second];
	for(int i = 1; i < ca; i++) suma[i] = suma[i-1] + a[da[i].second];
	for(int i = 1; i < cb; i++) sumb[i] = sumb[i-1] + b[db[i].second];
	

	for(int i = 0; i < n; i++){
		if(a[i] < b[i]){
			r[i] += sb + cb*a[i];		
		}
		else{
			r[i] += sa + ca*b[i];	
		}
	}

	for(int i = 0; i < m; i++){
		int aa,bb;
		cin>>aa>>bb;
		aa--;
		bb--;
		ll m = min(a[aa] + b[bb], a[bb]+b[aa]);
		r[aa] -= m;
		r[bb] -= m;
	}

	for(int i = 0; i < n; i++) cout<<r[i]<<' ';

	return 0;
}
