#include <bits/stdc++.h>

using namespace std;

int main() {
  int a, b, c, j = 1;
  cin >> a;
  c = a;
  vector<int> nums(a+1), res(a);
  nums[0] = 0;
  while(a--){
    cin >> b;
    nums[j] = b;
    j++;
  }
  res[0] = nums[1];
  for(int i = 2;i<c+1;i++){
    res[i-1] = nums[i] - nums[i-1];
  }
  int temp = c;
  bool can = true;
  vector<int> sum;
  while(c){
    can = true;
    for(int i = 1;i<=temp;i++){
      if(res[(i-1)%c]+nums[i-1] != nums[i]) {can = false;}
    }
    if(can){
      sum.push_back(c);
    }
    c--;
  }
  cout << sum.size() << "\n";
  for(int i = sum.size()-1;i>-1;i--){
    cout << sum[i] << ' ';
  }cout << "\n";
  return 0;
}
