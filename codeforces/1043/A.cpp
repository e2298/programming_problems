#include <bits/stdc++.h>

using namespace std;

int main() {
  int input, a, cant;
  int b = 0;
  int max = 0;
  cin >> input;
  cant = input;
  while(input--){
    cin >> a;
    b+=a;
    max = max > a ? max : a;
  }
  int res = cant*max;
  while((res-b) <= b){
    max++;
    res += cant;
  }
  cout << max << "\n";
  return 0;
}
