#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll l[100001][2];
char s[100001];

int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < 2*n; i++){
		int a;
		cin>>a;
		if(s[a]) l[a][1] = i;
		else {
			s[a] = 1;
			l[a][0] = i;
		}
	}	
	int a=0,b=0;
	ll dis = 0;
	int curr = 1;
	while(curr <= n){
		ll o1, o2;
		o1 = abs(a - l[curr][0]) + abs(b - l[curr][1]);
		o2 = abs(a - l[curr][1]) + abs(b - l[curr][0]);
		if(o1 < o2){
			dis += o1;
			a = l[curr][0];
			b = l[curr][1];
		}
		else{
			dis += o2;
			a = l[curr][1];
			b = l[curr][0];
		}
		curr++;
	}
	cout<<dis;

	return 0;
}
