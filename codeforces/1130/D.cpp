#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int cand[5000];
int closest[5000];

int main(void){
	int n,m;
	cin>>n,m;
	for(int i = 0; i < n; i++) closest[i] = 1000000;
	while(m--){
		int a,b;
		cin>>a>>b;
		cand[a] ++;
		closest[a] = min(closest[a], a < b? a-b : n-a + b);
	}


	return 0;
}
