#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int n, p=0, neg = 0, z = 0;
	cin>>n;
	int m = n;
	while(m--){
		int i;
		cin>>i;
		if(i >0) p++;
		if(i == 0) z++;
		if( i < 0) neg++;
	}
	if(p >= (n+1)/2) cout<<1;
	else if(neg >= (n+1)/2) cout<<-1;
	else cout<<0;

	return 0;
}
