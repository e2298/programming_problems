#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;
typedef queue<ii> que;

int xi, yi, xd, yd;
int grid[51][51];
int seen[51][51];
int seen2[51][51];
int n;

int bfs(int x, int y){
	int r = 1000000000;
	memset(seen2, 0, 51*51*sizeof(int));
	que q;
	q.push(ii(xd,yd));
	seen2[xd][xd] = 1;
	while (!q.empty()){
		ii curr = q.front();
		seen2[curr.first][curr.second] = 1;
		q.pop();
		int d = (curr.first - x) * (curr.first - x) + (curr.second - y) * (curr.second - y);
		r = min(r, d);
		//cout<<x<<' '<<y<<' '<<curr.first<<' '<<curr.second<<' '<<d<<'\n';
		ii nxt;
	        nxt = curr;
		nxt.first++;
		if(nxt.first <= n && !seen2[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	        nxt = curr;
		nxt.first--;
		if(nxt.first > 0 && !seen2[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	        nxt = curr;
		nxt.second--;
		if(nxt.second > 0 && !seen2[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	        nxt = curr;
		nxt.second++;
		if(nxt.second <= n && !seen2[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	}

	return r;
}

int main(void){
	cin>>n;
	cin>>xi>>yi>>xd>>yd;
	for(int i = 1; i <= n; i++){
		for(int j = 1; j <=n; j++){
			char in;
			cin>>in;
			grid[i][j] = in-'0';
		}
	}

	que q;

	q.push(ii(xi,yi));
	seen[xi][xi] = 1;
	int r = 1000000000;
	while (!q.empty()){
		ii curr = q.front();
		seen[curr.first][curr.second] = 1;
		q.pop();
		ii nxt;
		r = min (r, bfs(curr.first,curr.second));
	        nxt = curr;
		nxt.first++;
		if(nxt.first <= n && !seen[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	        nxt = curr;
		nxt.first--;
		if(nxt.first > 0 && !seen[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	        nxt = curr;
		nxt.second--;
		if(nxt.second > 0 && !seen[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	        nxt = curr;
		nxt.second++;
		if(nxt.second <= n && !seen[nxt.first][nxt.second] && !grid[nxt.first][nxt.second]){
			q.push(nxt);
			seen[nxt.first][nxt.second] = 1;
		}
	}
	cout<<r;

	return 0;
}
