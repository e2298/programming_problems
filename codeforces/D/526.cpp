#include<bits/stdc++.h>
using namespace std;
#define INF 1e9
#define foru(i,a,b) for(int i=(a); i<(b); i++)
#define forue(i,a,b) for(int i=(a); i<=(b); i++)
#define forb(i,n) for(int i=n-1; i>=0; i--)
#define forall(it,v) for(auto it=v.begin();it!=v.end();++it)
#define pb push_back
#define fst first
#define snd second
#define pt(x) cout << x << "\n"
#define sz(c) ((int)c.size())
#define add(a, b, w) edges[a].pb(make_pair(b, w))
typedef long long int ll;
typedef vector<ll> vi;
typedef pair<int, ll> ii;
typedef vector<ii> vii;

vii graph [300001];
int seen[300001];
ll gain[300001];
ll m=0;

ll solve(int i){
	if(seen[i]) return 0;
	seen[i] = 1;
	ll m1 = 0;
	ll m2 = 0;
	for(auto j: graph[i]){
		ll t = solve(j.first);
		t -= j.second;
		if(t > m1){
			m2 = m1;
			m1 = t;
		}	
		else if(t > m2){
			m2 = t;
		}
	}
	m = max(m, m1+m2+gain[i]);

	return m1 + gain[i];
}

int main(){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		cin>>gain[i];
	}
	n--;
	for(int i = 0; i < n; i++){
		int a,b;
		ll c;
		cin>>a>>b>>c;
		a--;
		b--;
		graph[a].push_back(ii(b,c));
		graph[b].push_back(ii(a,c));
	}
	ll r = solve(0);
	cout<<max(r,m)<<'\n';
	return 0;
}
