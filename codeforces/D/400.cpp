#include<bits/stdc++.h>
#define INF (1ll<<60l)
using namespace std;
typedef long long ll;

ll cant[510];
vector<vector<ll> > gg;
int comp[100010];

void solve(int i, int c){
	if(!comp[i]){
		comp[i] =c;
		for(auto j:gg[i]) solve(j,c);
	}
}


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n,m,kk;
	cin>>n>>m>>kk;
	ll last = 0;
	for(int i = 0; i < kk; i++){
		ll a;
		cin>>a;
		cant[i] = a+last;
		last = cant[i];	
	}
	
	vector<vector<ll> > g (kk,vector<ll>(kk, INF));
	gg = vector<vector<ll> > (n+1);
	while(m--){
		ll a,b,c;
		cin>>a>>b>>c;
		if(!c){
			gg[a].push_back(b);
			gg[b].push_back(a);
		}
		a = lower_bound(cant,cant+kk,a) - cant;
		b = lower_bound(cant,cant+kk,b) - cant;
		g[a][b] = min(g[a][b],c);
		g[b][a] = min(g[b][a],c);
	}

	int curr = 1;
	int c = 1;
	for(int i = 1; i<=n;i++)solve(i,curr++);

	int idx = 1;
	for(int i = 0; i < kk; i++){
		ll nn = cant[i];
		curr = comp[idx];
		while(idx <= nn) c = (curr==comp[idx++])&&c;
	}
	if(c){
		for(int i = 0; i < kk; i++) g[i][i] = 0ll;
		for(int k = 0; k < kk; k++) {
			for(int i = 0; i < kk; i++){
				if(g[i][k] != INF) {
					for(int j = 0; j < kk; j++){
						if(g[k][j] != INF){ 
							g[i][j]  = min(g[i][j], g[i][k] + g[k][j]);
						}
					}
				}
			}
		}
		cout<<"Yes\n";
		for(int i = 0; i < kk; i++){
			for(int j = 0; j < kk; j++){
				if(g[i][j] != INF) cout<<g[i][j]<<' ';
				else cout<<"-1 ";
			}
			cout<<'\n';
		}	
	}
	else cout<<"No\n";

	return 0;
}
