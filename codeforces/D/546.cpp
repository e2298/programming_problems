#include<bits/stdc++.h>
using namespace std;

int r[5000010];
int criba[5000010];

void cribar(){
	int m = 5000010;
	int w[] = {4,2,4,2,4,6,2,6};
	for(int p = 25; p <m; p+= 10) criba[p] = 5;
	for(int p = 9; p <m; p+= 6) criba[p] = 3;
	for(int p = 4; p <m; p+= 2) criba[p] = 2;
	for(int p = 7, curr = 0; p*p < m; p+= w[curr++&7])if(!criba[p])
		for(int j = p*p; j < m; j+= (p<<1)) if(!criba[j]) criba[j] = p;

	for(int i = 2; i < m; i++){
		if(criba[i]){
			int t = i/criba[i];
			r[i] = r[t];
		}
		r[i]++;
	}
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	cribar();

	for(int i = 1; i < 5000010; i++) r[i] += r[i-1];

	int t;
	cin>>t;
	while(t--){
		int a,b;
		cin>>a>>b;
		cout<<r[a]-r[b]<<'\n';
	}

	return 0;
}
