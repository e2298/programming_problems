#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll dp[100010];
ll ddp[100010];
ll k;
ll solve(int i){
	if( i < 1) return 0;
	if(dp[i]) return dp[i];
	if(i < k) return dp[i] = 1;
	if(i == k) return dp[i] =2;
	return dp[i] = (solve(i-1) + solve(i-k)) % 1000000007ll;
}

int main(void){
	int n;
	cin>>n>>k;
	for(int i = 1; i < 100005; i++){
		ddp[i] = ddp[i-1] + solve(i);
		ddp[i] %= 1000000007ll;
	}
	while(n--){
		int a,b;
		cin>>a>>b;
		ll r = ddp[b] - ddp[a-1];
		if(r < 0ll) r += 1000000007ll;	
		cout<<r<<'\n';
	}
}
