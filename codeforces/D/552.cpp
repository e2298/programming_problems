#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,ii> iii;

map<iii,ll> times;
ii points[2010];


int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		int a,b;
		cin>>a>>b;
		points[i] = ii(a,b);
	}

	for(int i = 0; i < n; i++){
		for(int j = i+1; j < n; j++){
			int dx = points[i].first - points[j].first;
			int dy = points[i].second - points[j].second;
			ii m,b;
			if(dx){
				if(dy){
					m = ii(dy,dx);
					b = ii(dx*points[i].first - dy*points[i].second,dx);
					if(m.second < 0){
						m.first *=-1;
						m.second*=-1;
					}
					if(b.second < 0){
						b.first *=-1;
						b.second*=-1;
					}
					int g = __gcd(b.first,b.second);
					b.first/=g;
					b.second/=g;
					g = __gcd(m.first,m.second);
					m.first/=g;
					m.second/=g;
				}
				else{
					m.first = m.second = 0;
					b = ii(points[i].second,0);
				}
			}
			else{
				m = ii(0,0);
				b = ii(points[i].first, 1000);
			}
			times[iii(m,b)]++;
		}
	}
	ll r = n * (n-1) * (n-2);
	r/= 6ll;

	for(auto i:times){
		ll t = i.second;
		/*cout<<i.first.first.first<<' '<<i.first.first.second<<' ';
		cout<<i.first.second.first<<' '<<i.first.second.second<<' ';
		cout<<t<<'\n';
		*/
		t = sqrt(8*t+1)+1;
		t/= 2ll;
		r -= (t * (t-1) * (t-2))/6ll;
	}
	cout<<r<<'\n';

	return 0;
}
