#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef complex<int> cd;
typedef pair<int,int> ii;

cd points[2010];
cd l[2010];
int idx[110][110];

cd curr;
bool cmc(cd a, cd b){
	a = a - curr;
	b = b - curr;
	return real(a) * imag(b) - imag(a) * real(b) > 0;
}

bool cm(cd a, cd b){
	pair<ll,ll> aa(a.real(),a.imag());
	pair<ll,ll> bb(b.real(),b.imag());
	return aa < bb;
}

int main(void){
	ll n;
	cin>>n;
	for(int i = 0; i < n; i++){
		int a,b;
		cin>>a>>b;
		points[i] = cd(a,b);
		idx[a][b]= i;
	}
	if(n < 3) cout<<"0\n", exit(0);
	
	ll r = n;
	r *= n-1ll;
	r *= n-2ll;
	r /= 6ll;

	int cl = 0;
	for(int i = 0; i < n; i++){
		curr = points[i];
		for(int j = 0; j < n; j++) l[j]=points[j];
		l[i] = l[n-1];

		sort(l,l+n-1,cmc);
		for(int j = 0 ;j < n-1; j++) cout<<l[j]<<' ';
		cout<<'\n';

		for(int j = 1; j < n-1; j++){
			cd a = l[j];
			cd b = l[j-1];	
			a = a - curr;
			b = b - curr;
			if(!(a.real()*b.imag()-a.imag()*b.real())){
				ii aa(l[j].real(),l[j].imag());
				ii bb(curr.real(),curr.imag());
				{
					ll c = 1ll;
					int k;
					for(k = j+1; k < n-1; k++){
						b = l[k]- curr;
						if(a.real()*b.imag()-a.imag()*b.real()){
							break;
						}
						else{
							c++;
							aa = ii(l[k].real(),l[j].imag());
							cout<<l[k]<<' ';
						}
					}
					j = k;
					r-= (c * (c+1) * (2*c + 4ll))/12ll;
				}
			}
		}
	}
	cout<<r<<'\n';


	return 0;
}
