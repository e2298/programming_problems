#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll in[100010];


int main(void){
	int n;
	cin>>n;
	for(int i= 0; i < n; i++){
		cin>>in[i];
	}
	sort(in, in+n);

	ll curr=0ll;
	int r = 0;
	for(int i = 0; i < n; i++){
		if(curr > in[i]) r++;
		else curr += in[i];
	}
	cout<<n-r;

	return 0;
}
