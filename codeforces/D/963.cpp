#include<bits/stdc++.h>
using namespace std;
#define sz(s) (s.size())

string ss[100001];
int ks[100001];
vector<int> matches[100001];

struct corasick{
	map<char, corasick> next;//trie, sirve para limpiar
	corasick* tran[256];//transiciones del automata
	int idhoja, szhoja;//id de la hoja o 0 si no lo es
	//link lleva al sufijo mas largo, linkhoja lleva al mas largo pero que es hoja (o a la raiz si no hay)
	corasick *padre, *link, *linkhoja;
	char pch;//caracter que conecta con padre
	corasick(): tran(),  idhoja(-1), padre(), link(), linkhoja() {}
	void insert(const string &s, int id, int p=0){//id>=0!!!
		if(p<sz(s)){
			corasick &ch=next[s[p]];
			tran[(int)s[p]]=&ch;
			ch.padre=this, ch.pch=s[p];
			ch.insert(s, id, p+1);
		}
		else idhoja=id, szhoja=sz(s);
	}
	corasick* get_link() {
		if(!link){
			if(!padre) link=this;//es la raiz
			else if(!padre->padre) link=padre;//hijo de la raiz
			else link=padre->get_link()->get_tran(pch);
		}
		return link; }
	corasick* get_tran(int c) {
		if(!tran[c]) tran[c] = !padre? this : this->get_link()->get_tran(c);
		return tran[c];	}
	corasick *get_linkhoja(){
		if(!linkhoja){
			if(get_link()->idhoja>=0) linkhoja = link;
			else if(!padre) linkhoja=this;//es la raiz
			else linkhoja=link->get_linkhoja();
		}
		return linkhoja; }
	void report(int p){
		if(idhoja>=0) matches[idhoja].push_back(p);
		if(get_linkhoja()->idhoja>=0) linkhoja->report(p); }
	void matching(const string &s, int p=0){
		report(p); if(p<sz(s)) get_tran(s[p])->matching(s, p+1);	}
}cora;

int main(void){
	cora = corasick();
	string t;
	cin>>t;
	int n;
	cin>>n;
	for(int i = 1; i <= n; i++){
		cin>>ks[i]>>ss[i];
		cora.insert(ss[i],i);
	}
	cora.matching(t);
	for(int i = 1; i <= n; i++){
		if(ks[i] > matches[i].size()) cout<<"-1\n";
		else {
			int sz = ss[i].size() + matches[i][ks[i]-1] - matches[i][0];
			int mn = sz;

			for(int j = ks[i]; j < matches[i].size(); j++){
				sz += matches[i][j] - matches[i][j-1];
				sz -= matches[i][j-ks[i]+1] - matches[i][j-ks[i]];	
				mn = min(mn, sz);
			}			
			cout<<mn<<'\n';
		}
	}

	return 0;
}
