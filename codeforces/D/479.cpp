#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll nums[100010];

int main(void){
	ll n,l,x,y;
	cin>>n>>l>>x>>y;
	for(int i = 0; i < n; i++)cin>>nums[i];
	int j = 0;
	for(int i = lower_bound(nums,nums+n,x) - nums; i < n; i++){
		if(nums[i] - nums[j] == x){ x = 0; break;}	
		if(nums[i] - nums[j] > x)j++, i--;
	}
	j = 0;
	for(int i =lower_bound(nums,nums+n,y) - nums; i < n; i++){
		if(nums[i] - nums[j] == y){ y = 0; break;}	
		if(nums[i] - nums[j] > y)j++, i--;
	}

	if(x || y){
		if(!x){
			cout<<"1\n"<<y<<'\n';
		}
		else if(!y){
			cout<<"1\n"<<x<<'\n';
		}
		else{
			ll c = 0;
			
			for(int i = 0; i < n; i++){
				ll cu = nums[i] + y;
				if(cu <= l){
				if(*lower_bound(nums,nums+n,cu+x) == cu+x){c = cu; break;}
				if(*lower_bound(nums,nums+n,cu-x) == cu-x){c = cu; break;}
				}
				cu = nums[i] - y;
				if(cu >= 0){
				if(*lower_bound(nums,nums+n,cu+x) == cu+x){c = cu; break;}
				if(*lower_bound(nums,nums+n,cu-x) == cu-x){c = cu; break;}
				}
			}

			if(c){
				cout<<"1\n"<<c<<'\n';	
			}
			else cout<<"2\n"<<x<<' '<<y<<'\n';
		}
	}
	else cout<<"0\n";

	return 0;
}
