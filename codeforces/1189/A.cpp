#include<bits/stdc++.h>
using namespace std;

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n;
	string s;
	cin>>n>>s;
	int ones = 0;
	for(auto i:s) if(i=='1') ones++;
	if(ones*2 == n){
		cout<<2<<'\n';
		cout<<s[0]<<' ';
		for(int i = 1; i < n; i++)cout<<s[i];
		cout<<'\n';
	}
	else{
		cout<<"1\n"<<s<<'\n';
	}

	return 0;
}
