#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef deque<ll> di;
typedef pair<ll, ll> ii;

ii qs[300005];
ii rs[300005];


int main(void){
	ll n,qu;
	cin>>n>>qu;
	di q;
	ll mx = 0;
	for(ll i = 0; i < n; i++){
		ll t;
		cin>>t;
		mx = max(mx, t);
		q.push_back(t);
	}
	for(ll i = 0; i < qu; i++){
		cin>>qs[i].first;
		qs[i].second = i;
	}
	sort(qs,qs+qu);
	
	ll i = 1;
	ll j = 0;
	while(q.front() != mx){
		ll a = q.front();
		q.pop_front();
		ll b = q.front();
		q.pop_front();
		if(a > b){
			q.push_front(a);
			q.push_back(b);
		}
		else{
			q.push_front(b);
			q.push_back(a);
		}
		while(qs[j].first == i){
			rs[qs[j].second].first = a;			
			rs[qs[j].second].second = b;			
			j++;
		}

		i++;
	}
	for(; j < qu; j++){
		ll qr = qs[j].first - i;		
		qr %= (n-1);
		qr++;
		rs[qs[j].second].first = q.front();
		rs[qs[j].second].second = q[qr];
	}
	for(i = 0; i < qu; i++) cout<<rs[i].first<<' '<<rs[i].second<<'\n';

	
	return 0;
}
