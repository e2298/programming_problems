#include<bits/stdc++.h>
using namespace std;

int main(void){
	unsigned long n,k;
	cin>>n>>k;
	priority_queue<int> pq;

	int i = 0;
	while(n){
		if(n & 1) pq.push(i);
		i++;
		n>>=1;
	}

	if(pq.size() <= k){
		k -= pq.size();
		while(k){
			int t = pq.top();pq.pop();
			t--;
			pq.push(t);
			pq.push(t);
			k--;
		}
		cout<<"Yes\n";
		while(!pq.empty()){
			cout<<pq.top()<<' ';
			pq.pop();
		}
	}
	else cout<<"No";
	return 0;
}
