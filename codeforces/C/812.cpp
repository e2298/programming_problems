#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<ll> vi;

ll as[100010];

int k;

int main(void){
	ll n, s;
	cin>>n>>s;

	for(int i = 0; i < n; i++){cin>>as[i];}

	ll lo = 0, up = n;
	ll r;
	while(lo < up){
		ll curr;
		curr = (lo + up + 1ll) / 2ll;
		vi t(n);
		for(ll i = 0ll; i < n; i++){
			t[i] = as[i] + curr * (i+1ll);
		}
		sort(t.begin(), t.end());
		r = 0;
		for(int i = 0; i < curr; i++){
			r += t[i];
		}
		if(r > s){
			up = curr-1ll;	
		}
		else{
			lo = curr;
		}
	}
	vi t(n);
	for(ll i = 0ll; i < n; i++){
		t[i] = as[i] + lo * (i+1ll);
	}
	sort(t.begin(), t.end());
	r = 0;
	for(int i = 0; i < lo; i++){
		r += t[i];
	}
	cout<<lo<<' '<<r<<'\n';

	return 0;
}
