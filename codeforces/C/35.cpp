#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

int g[2010][2010];

int main(void){
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n,m;
	cin>>n>>m;
	ii currv;
	queue<ii> q;
	int k;
	cin>>k;
	while(k--){
		int a,b;
		cin>>a>>b;
		q.push(ii(a,b));
		g[a][b] = 1;
		currv=ii(a,b);
	}
	while(q.size()){
		ii curr = q.front();q.pop();
		if(curr.first < 1 || curr.first > n || curr.second < 1 || curr.second > m) continue;
		currv = curr;
		int x,y;
		
		x = curr.first+1;
		y = curr.second;
		if(!g[x][y]){
			g[x][y] = 1;
			q.push(ii(x,y));
		}
		x = curr.first-1;
		y = curr.second;
		if(!g[x][y]){
			g[x][y] = 1;
			q.push(ii(x,y));
		}
		x = curr.first;
		y = curr.second+1;
		if(!g[x][y]){
			g[x][y] = 1;
			q.push(ii(x,y));
		}
		x = curr.first;
		y = curr.second-1;
		if(!g[x][y]){
			g[x][y] = 1;
			q.push(ii(x,y));
		}
	}

	cout<<currv.first<<' '<<currv.second<<'\n';

	return 0;
}
