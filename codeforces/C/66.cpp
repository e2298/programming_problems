#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	string in;
	cin>>in;
	ll n;
	if(in.size() >= 19){
		if(in.size() > 19) cout<<"BigInteger";
		else if(in > "9223372036854775807")cout<<"BigInteger";
		else cout<<"long";
		return 0;
	}
	stringstream i(in);
	i>>n;
	if(n <= 127) cout<<"byte";
	else if(n <= 32767) cout<<"short";
	else if(n <= 2147483647) cout<<"int";
	else cout<<"long";

	return 0;
}
