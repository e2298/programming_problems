#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int n,k;
	ll c=1;
	deque<int> q1,q2;
	cin>>n>>k;
	while(k--){
		int i;
		cin>>i;
		q1.push_back(i);
	}
	cin>>k;
	while(k--){
		int i;
		cin>>i;
		q2.push_back(i);
	}
	for(int i = 1; i<=n; i++) c*=i;
	ll t = c;
	while(q1.size() && q2.size() && c--){
		int n1 = q1.front(); q1.pop_front();
		int n2 = q2.front(); q2.pop_front();
		if(n1>n2){
			q1.push_back(n2);
			q1.push_back(n1);
		}
		else{
			q2.push_back(n1);
			q2.push_back(n2);
		}
	}
	if(!q1.size()){ cout<< t-c<<' '<<2;}
	else if(!q2.size()){ cout<< t-c<<' '<<1;}
	else cout<<"-1";
	return 0;
}
