#include<bits/stdc++.h>
using namespace std;

int digits[11];
vector<vector<int> > factors = {{}, {}, {}, {}, {2,2}, {}, {2,3}, {},
       	{2,2,2}, {3,3}};
int res[11];

int main(void){
	int n; cin>>n;
	char a;
	while(n--){
		cin>>a;
		for(int i = 1; i<= a-'0'; i++) digits[i]++;
	}
	for(int i = 9; i > 3; i--){
		if(!digits[i]) continue;
		if(factors[i].size()){
			while(digits[i] > digits[i+1]){
				digits[i]--;
				for(auto j:factors[i]){
					digits[j]++;
				}
			}	
		}
	}
	int last = 0;
	for(int i = 9; i > 1; i--){
		res[i] = digits[i]-last;
		last = digits[i];
	}
	for(int i = 9; i > 1; i--){
		while(res[i]--)cout<<i;
	}
	

	return 0;
}
