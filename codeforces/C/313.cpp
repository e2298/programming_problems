#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll in[(int)2e6+10];

int main(void){
	cin.tie(0);
	ios::sync_with_stdio(0);
	int n;
	cin>>n;
	int t = n;
	ll lg = 0;
	while(t != 1) {
		t>>=2;
		lg++;
	}
	lg++;
	for(int i = 1; i <= n; i++) cin>>in[i];
	sort(in+1,in+n+1, greater<int>());
	int pow = 1;	
	ll r = 0;
	for(int i = 1; i <= n; i++){
		r += in[i]*lg;
	       	if(i == pow){
			pow <<=2;
			lg--;
		}	
	}
	cout<<r<<'\n';

	return 0;
}
