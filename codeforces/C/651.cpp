#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll>ii;

ll xs[200010];
ll ys[200010];

int main(void){
	int n;
	ll x,y;
	cin>>n;
	ll r = 0;
	map<ii,ll> seen;
	for(int i = 0; i < n; i++){
		cin>>x>>y;
		xs[i] = x;
		ys[i] = y;
		seen[ii(x,y)]++;
	}
	for(auto i:seen){
		ll n = i.second;
		n *= n-1ll;
		n/= 2ll;
		r-=n;
	}
	sort(xs,xs+n);
	sort(ys,ys+n);
	
	ll* p = xs;
	while(p < xs+n){
		ll* t = p;
		while(t < xs+n){
			if(*t != *p) break;
			t++;
		}
		ll c = t-p; 
		c = c * (c-1ll);
		c /= 2ll;
		r += c;
		p = t;
	}
	p = ys;
	while(p < ys+n){
		ll* t = p;
		while(t < ys+n){
			if(*t != *p) break;
			t++;
		}
		ll c = t-p; 
		c = c * (c-1ll);
		c /= 2ll;
		r += c;
		p = t;
	}
	cout<<r;

	return 0;
}
