#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int in[100010];

int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++)cin>>in[i];
	sort(in, in+n);	
	int a=0,b;
	int r = 0;
	b = upper_bound(in,in+n,in[0]) - in;
	for(; a < n;){
		int* lo = in+a;
		int* up = upper_bound(in+a,in+n, in[a]);
		int cant = up - lo;
		r += cant;
		a += cant;
		int lb = -1;
		while(cant > 0){
			if(b == lb) break;
			lb = b;
			lo = in+b;
			up = upper_bound(in+b,in+n,in[b]);
			int cant2 = up - lo;
			if(cant2 <= cant){
				cant -= cant2;
				b += cant2;
			}
			else{
				b += cant;
				cant = 0;
			}
		}	
		if(cant > 0) r -= cant;
	}
	cout<<r;
}
