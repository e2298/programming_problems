#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<ll> vi;

int main(void){
	int n;
	cin>>n;
	ll last = 1e10;
	int can = 1;
	while(n--){
		ll a,b,c;
		cin>>a>>b;
		c = max(a,b);
		if(c > last){
			c = min(a,b);
			if(c > last) can = 0;
			else last = c;
		}
		else last = c;
	}

	if(can) cout<<"YES";
	else cout<<"NO";	

	return 0;
}
