#include<bits/stdc++.h>
using namespace std;

int main(void){
	int t;
	cin>>t;
	while(t--){
		vector<int> c(3);
		cin>>c[0]>>c[1]>>c[2];
		sort(c.begin(), c.end());
		if(c[2] > c[0] + c[1] + 1) cout<<"No\n";
		else cout<<"Yes\n";
	}

	return 0;
}
