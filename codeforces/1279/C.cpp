#include<bits/stdc++.h>
using namespace std;
struct fenwick{
	static const int sz = 100005;
	int t[sz];
	void adjust(int p, int v){
		p++;
		for(int i = p; i < sz; i += (i&-i)) t[i] += v;
	}
	int sum(int p){
		p++;
		int s = 0;
		for(int i = p; i; i-=(i&-i)) s+=t[i];
		return s;
	}
	void clear(){
		memset(t, 0, sizeof(t));
	}
} after, before;

int pos[100005];


int main(void){
	int t;
	cin>>t;
	while(t--){
		after.clear();
		before.clear();
		int n,m;
		cin>>n>>m;
		for(int i = 0; i < n; i++){
			int a;
			cin>>a;
			pos[a] = i;
		}
		int r = 0;
		while(m--){
			int a;
			cin>>a;
			int s = pos[a];
			s -= before.sum(pos[a]);
			if(after.sum(n-pos[a])) s = 0;
			r += 2*s + 1;
			before.adjust(pos[a], 1);
			after.adjust(n-pos[a], 1);
		}
		cout<<r<<'\n';

	}

	return 0;
}
