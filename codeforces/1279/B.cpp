#include<bits/stdc++.h>
using namespace std;

int a[100005];

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n,s;
		cin>>n>>s;
		a[n] = s + 10;
		a[n+1] = s+10;
		for(int i = 0; i < n; i++){
			cin>>a[i];
		}	
		int i = 0;
		int sum = 0;
		while(sum <= s && i < n){
			sum += a[i];
			i++;
		}
		if( sum <= s){
			cout<<"0\n";
			continue;
		}
		int mx = 0;
		for(int j = 0; j < i-1; j++){
			if(a[j] > a[mx]) mx = j;
		}
		if(sum - a[mx] + a[i] + a[i-1] <= s) cout<<mx+1<<'\n';
		else if (sum - a[i-1] + a[i] + a[i+1] <= s) cout<<i<<'\n';
		else cout<<"0\n";
	}

	return 0;
}
