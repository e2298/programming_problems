#include<bits/stdc++.h>
using namespace std;

int m[200][200];
int rows[200], columns[200];

int main(void){
	int r,c;
	cin>>r>>c;
	for(int i = 0; i < r; i++) for(int j = 0; j < c; j++){
		char in;
		cin>>in;
		m[i][j] = (in == 'W')?0: 1;
	}
	for(int i = 0; i < r; i++) {
		int can = 0;
		for(int j = 0; j < c; j++){
			can += m[i][j];
		}
		rows[i] = can;
	}
	for(int j = 0; j < c; j++) {
		int can = 0;
		for(int i = 0; i < r; i++){
			can += m[i][j];
		}
		columns[j] = can;
	}

	int s, e,rr,cc;
	for(s = 0; !rows[s];s++);
	for(e = s; rows[e];e++);
	e--;
	rr = (e+s)/2;
	for(s = 0; !columns[s];s++);
	for(e = s; columns[e];e++);
	e--;
	cc = (e+s)/2;
	cout<<++rr<<' '<<++cc;
}
