#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;

int abs(int a){
	if(a & ( 1 << 31)) return -a;
	return a;
}

int main(void){
	int n, s;
	int r;
	vi a;

	cin>>n;
	a = vi(n);
	
	r = 500;

	for(int i = 0; i< n; i++) cin>>a[i];

	for(int i = 0; i < n; i++){
		for(int j = i; j < n; j++){
			s = 0;
			for(int k = i; k<=j; k++){
				s+=a[k];
			}
			r = min(r, 2*abs(180-s));
		}
	}
	if(r == 360) r = 0;

	cout<< r;

	return 0;
}
