#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
typedef vector<int> vi;
vi primes;

int N = 1000000;
int criba[1000001];
void crearcriba(){
	int w[] = {4,2,4,2,4,6,2};
	for(int p = 25; p <= N; p+=10) criba[p] = 5;
	for(int p = 9; p <= N; p+=6) criba[p] = 3;
	for(int p = 4; p <= N; p+=2) criba[p] = 2;
	for(int p = 25; p <= N; p+=10) criba[p] = 5;
	for(int p=7,curr=0; p*p<=N; p+=w[curr++&7]) if(!criba[p])
		for(int j = p*p; j <= N; j+=(p<<1)) criba[j] = p;
}

int is_prime(ll n){
	int r = 1;
	if(n%6 != 1 && n%6 != 5){
		return 0;
	}
	for(auto i:primes){
		if(n%i == 0){
			r = 0;
			break;
		}
	}
	return r;
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	crearcriba();
	for(int i = 2; i <= N; i++){
		if(!criba[i]) primes.push_back(i);
	}

	ll n;
	while(cin>>n){
		if(n&1){
			if(n <= primes[primes.size()-1]){
				auto i = lower_bound(primes.begin(), primes.end(), n);
				if(i != primes.end() && *i == n){
					cout<<"1\n";
					continue;
				}	
				i = lower_bound(primes.begin(), primes.end(), n-2);
				if(i != primes.end() && *i == n-2){
					cout<<"2\n";
					continue;
				}	
				cout<<"3\n";
				continue;
			}	
			if(is_prime(n)){
				cout<<"1\n";
			}
			else if(is_prime(n-2)){
				cout<<"2\n";
			}
			else cout<<"3\n";

		}
		else{
			if(n == 2) cout<<1<<'\n';
			else cout<<2<<'\n';
		}
	}


	return 0;
}
