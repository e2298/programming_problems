#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;

ll mod = 1000000007;
ll dp[201][40000];
int seen[201][40000];


ll solve(ll i, ll p){
	if(p == 0) return 1;
	if(p < 0 || i == 0) return 0;
	if(seen[i][p]) return dp[i][p];
	ll r = solve(i-1,p-i);
	r += solve(i-1,p);
	r %= mod;
	seen[i][p] = 1;
	return dp[i][p] = r;
	
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	int q = 0;
	cin>>q;
	while(q--){
		ll n,p;
		cin>>n>>p;
		cout<<solve(n,p)<<'\n';
	}

	return 0;
}

