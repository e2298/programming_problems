#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;


int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	ll x,y;
	while(cin>>x>>y && x){
		ll n = x + y - 1;
		ll m = n * (n-1) / 2;
		if(n&1){
			m += y;
		}	
		else m+= x;
		cout<<m<<'\n';
	}
	return 0;
}
