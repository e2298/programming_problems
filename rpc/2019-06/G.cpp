#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;
vi primes;

ll gcd(ll a, ll b ){return a?gcd(b%a, a):b;}

ll mulmod(ll a, ll b, ll c){
	ll x = 0, y = a%c;
	while(b > 0){
		if(b % 2 == 1) x = (x+y) %c;
		y = (y*2) % c;
		b /= 2;
	}
	return x%c;
}

ll expmod(ll b, ll e, ll m){
	if(!e) return 1;
	ll q = expmod(b, e/2, m); q = mulmod(q,q,m);
	return e%2? mulmod(b,q,m):q;
}

bool es_primo_prob(ll n, int a){
	if(n == a) return true;
	ll s =0, d = n-1;
	while(d%2 == 0) s++,d/=2;
	ll x = expmod(a,d,n);
	if((x == 1) || (x+1 == n)) return true;
	for(int i = 0; i < s-1; i++){
		x = mulmod(x,x,n);
		if(x==1) return false;
		if(x+1 == n) return true;
	}
	return false;
}

bool is_prime(ll n){
	if(n ==1) return false;
	const int ar[] = {2,3,5,6,11,13,17,19,23};
	for(int j = 0; j < 9; j++) if(!es_primo_prob(n,ar[j])) return false;
	return true;
}

int N = 1000000;
int criba[1000001];
void crearcriba(){
	int w[] = {4,2,4,2,4,6,2};
	for(int p = 25; p <= N; p+=10) criba[p] = 5;
	for(int p = 9; p <= N; p+=6) criba[p] = 3;
	for(int p = 4; p <= N; p+=2) criba[p] = 2;
	for(int p = 25; p <= N; p+=10) criba[p] = 5;
	for(int p=7,curr=0; p*p<=N; p+=w[curr++&7]) if(!criba[p])
		for(int j = p*p; j <= N; j+=(p<<1)) criba[j] = p;
}

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);
	ll n;
	crearcriba();
	for(int i = 2; i <= N; i++){
		if(!criba[i]) primes.push_back(i);
	}

	while(cin>>n){
		if(n&1){
			if(n <= primes[primes.size()-1]){
				auto i = lower_bound(primes.begin(), primes.end(), n);
				if(i != primes.end() && *i == n){
					cout<<"1\n";
					continue;
				}	
				i = lower_bound(primes.begin(), primes.end(), n-2);
				if(i != primes.end() && *i == n-2){
					cout<<"2\n";
					continue;
				}	
				cout<<"3\n";
				continue;
			}	
			if(is_prime(n)){
				cout<<"1\n";
			}
			else if(is_prime(n-2)){
				cout<<"2\n";
			}
			else cout<<"3\n";

		}
		else{
			if(n == 2) cout<<1<<'\n';
			else cout<<2<<'\n';
		}
	}


	return 0;
}
