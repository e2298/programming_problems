#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
typedef pair<int,int> ii;
typedef pair<int,ii> iii;

iii ev[2*1000];
ii ts[1000];

int main(void){
	ios::sync_with_stdio(0);
	cin.tie(0);

	int t;
	cin>>t;
	while(t--){
		int b, n, t1, t2;
		cin>>b>>b>>n;
		for(int i = 0; i < n; i++){
			cin>>b>>b>>t1>>b>>b>>t2;
			ts[i] = ii(t1,t2);
			ev[2*i] = iii(t1,ii(0,i));
			ev[2*i+1] = iii(t2,ii(1,i));
		}		
		sort(ev, ev+2*n);
		int curr = 0;
		int mx = 0;
		for(int i = 0; i < 2*n; i++){
			if(ts[ev[i].second.second].first == ev[i].first) curr++;
			else curr--;
			mx = max(mx, curr);
		}
		cout<<mx<<'\n';
	}


	return 0;
}
