#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())
typedef long long ll;
#define MOD 1000000007ll

ll expmod(ll b, ll e){
	if(!e) return 1ll;
	ll r = expmod(b,e/2);
	r = (r*r)%MOD;
	return (e&1)?(r*b)%MOD:r;
}

int main(void){
	ll a,b,k,c;
	cin>>a>>b>>k>>c;
	if(a == b) cout<<k*(c==a)<<'\n';
	else if(c == a || c == b) cout<<(k*expmod(2,k-1))%MOD<<'\n';
	else cout<<"0\n";
	return 0;
}
