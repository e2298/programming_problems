#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())
typedef long long ll;
typedef complex<ll> cl;
typedef pair<ll,ll> ii;

bool cmp(cl a, cl b){
	return ii(a.real(),a.imag()) < ii(b.real(),b.imag());
}

ii conv(cl a){
	return ii(a.real(),a.imag());
}

cl points[1000];

int main(void){
	int n;
	cin>>n;
	set<ii>p;
	for(int i = 0; i < n; i++){
		ll x,y;
		cin>>x>>y;
		points[i] = cl(x,y);
		p.insert(conv(points[i]));
	}
	set<ii> seen;
	sort(points,points+n,cmp);
	ll r= 0;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if(j == i)continue;
			cl dif = points[i]-points[j];
			if(!seen.count(conv(dif))){
				int kul = 1;
				for(int k = 0; k < n; k++){
					if(!(p.count(conv(points[k]+dif))+p.count(conv(points[k]-dif)))){
						kul = 0;
						break;
					}
				}
				if(kul) {
					seen.insert(conv(dif));
				}
			}
		}
	}
	cout<<sz(seen)-seen.count(ii(0,0))<<'\n';
	return 0;
}
