#include<bits/stdc++.h>
using namespace std;
#define sz(c) (c.size())
typedef pair<int,int> ii;
typedef long long ll;
int a[300000];
ii msks[300000];
int n;


int main(void){
	cin>>n;
	string s;
	cin>>s;
	int msk = 0;
	int r = 0;
	for(int i = 0; i < n; i++){
		msk ^= 1<<(s[i]-'a');
		a[i] = msk;
		msks[i] = ii(msk,i);
		if(!(msk&(msk-1))){
			r = i+1;
		}
	}
	sort(msks,msks+n);
	//for(int i = 0; i < n; i++){
	//	cout<<msks[i].first<<' '<<msks[i].second<<'\n';
	//}
	for(int i = 1; i < n; i++){
		int tgt;
		for(msk = 1<<('t'-'a'); msk; msk>>=1){
			tgt = a[i-1] ^msk;
			auto j = upper_bound(msks,msks+n, ii(tgt,1000000000));
			j--;
			if(j != msks-1 && j->first == tgt){
				r = max(r, abs(i-j->second)+1);
			}
			//for(auto j = lower_bound(msks,msks+n,ii(tgt,-1)); j != msks+n && j->first == tgt; j++){
			//	r = max(r, abs(i-j->second)+1);
			//}
		}
		tgt = a[i-1];
		auto j = upper_bound(msks,msks+n, ii(tgt,100000000));
		j--;
		if(j != msks-1 && j->first == tgt){
			r = max(r, abs(i-j->second)+1);
		}
		//for(auto j = lower_bound(msks,msks+n,ii(tgt,-1)); j != msks+n && j->first == tgt; j++){
		//	r = max(r, abs(i-j->second)+1);
		//}
	}
	cout<<r<<'\n';

	return 0;
}
