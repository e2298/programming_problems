#include<bits/stdc++.h>
using namespace std;
typedef long long ll;


int main(void){
	string line;
	ll r= 0;
	while(getline(cin,line)){
		if(line[0] == '|') r+= 42ll*(ll)line.size();
		else{
			ll num = 0;
			int i = 0;
			while(line[i] != ','){
				num *= 10ll;
				num += line[i]-'0';
				i++;
			}
			i+=2;
			ll times = 0;
			while(line[i]){
				i++;
				times++;
			}
			times = max(times,1ll);
			r += times * num;
		}		
	}
	if(r%10 < 1){
		r -= r%10;
	}
	else {
		r-= r%10;
		r+=10;
	}
	cout<<r<<",-\n";
	return 0;
}
