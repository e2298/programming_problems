#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int times[26];

int main(void){
	int fst = 26;
	string s;
	cin>>s;
	for(auto i:s){
		times[i-'a']++;
		fst = min(fst,i-'a');
	}
	int cool = 1;
	for(int i = fst+1; i < 26; i++){
		if(times[i]>0)
			cool = cool && ((times[fst]&1) == (times[i]&1));
	}	
	if(!cool) cout<<2<<'\n';
	else cout<<(times[fst]&1)<<'\n';

	return 0;
}
