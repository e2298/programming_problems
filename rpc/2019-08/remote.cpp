#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int werks[10];

int main(void){
	int n;
	cin>>n;
	while(n--){
		int t;
		cin>>t;
		werks[t] = 1;
	}

	int mnd=10000;
	int target;
	cin>>target;
	if(!werks[0]) mnd = target;
	for(int i =1; i < 1000; i++){
		int t = i;
		int can = 1;
		while(t){
			if(werks[t%10]) can = 0;
			t/=10;
		}
		if(can) if(((int)abs(target - i)) < mnd){
			mnd = (int)(abs(target-i));
		}
	}
	cout<<mnd<<'\n';

	return 0;	
}
