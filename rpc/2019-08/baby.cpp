#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(void){
	int n;
	cin>>n;
	int good = 1;
	for(int i = 0; i < n; i++){
		string in;
		cin>>in;
		if(in[0] >= '0' && in[0] <= '9'){
			if(atoi(in.c_str()) != i+1) good = 0;
		}
	}	
	cout<<(good?"makes sense":"something is fishy");
	cout<<'\n';

	return 0;
}
