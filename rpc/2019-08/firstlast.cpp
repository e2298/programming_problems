#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int where[100001];
int a[100000];
int seen[100000];

int main(void){
	int n;
	cin>>n;
	where[n] = -1;
	for(int i = 0; i < n; i++){
		cin>>a[i];
		a[i]--;
		where[a[i]] = i;
	}
	int mx = 0;
	for(int i = 0; i < n; i++){
		if(seen[i]) continue;
		int c = 1;
		int j = where[a[i]+1];
		int lst = i;
		while(j > lst){
			seen[j] = 1;
			c++;
			lst = j;
			j = where[a[j]+1];
		}
		mx = max(mx, c);
	}
	cout<<n-mx<<'\n';
		

	return 0;
}
