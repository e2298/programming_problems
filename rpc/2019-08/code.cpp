#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<int> vi;

int d[366];

int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		int t;
		cin>>t;
		d[t]=1;
	}
	set<int> actv;
	int r = 0;
	for(int i = 1; i <= 365; i++){
		if(d[i]) actv.insert(i);
		int dty = 0;
		cout<<i<<' ';
		for(auto j:actv){
			cout<<j<<' ';
			dty += i-j;
		}
		cout<<dty<<'\n';
		if(dty + actv.size() >= 20){
			r++;
			actv.clear();
		}
	}
	if(actv.size()) r++;
	cout<<r<<'\n';


	return 0;
}
