#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

#define MAXP  (int)1e7
int criba[MAXP+1];
void cribar(){
	int w[] = {4,2,4,2,4,6,2,6};
	for(int p = 25; p <= MAXP; p+=10) criba[p] = 5;
	for(int p = 9; p <= MAXP; p+=6) criba[p] = 3;
	for(int p = 4; p <= MAXP; p+=2) criba[p] = 2;
	for(int p = 7,cur =0; p*p <= MAXP; p+=w[cur++&7]) if(!criba[p]) for(int j = p*p; j <= MAXP; j+=(p<<1)) if(!criba[j]) criba[j]=p;
}

int main(void){
	cribar();
	int n;
	scanf("%d\n",&n);
	while(n--){
		int a=0,b=0;
		char c = 0;
		int cnt=0;
		int pot1 = 0, pot2 = 0;
		while(scanf("%c",&c) && c != ' '){
			if(c == '.') cnt++;
			else{
				a *= 10;
				a += c-'0';
				if(cnt) pot1++;
			}
		}
		cnt = 0;
		while(scanf("%c",&c) != EOF && c != '\n'){
			if(c == '.') cnt++;
			else{
				b *= 10;
				b += c-'0';
				if(cnt) pot2++;
			}
		}
		if(pot1 < pot2){
			pot1 = pot2-pot1;
			while(pot1--) a*=10;
		}
		else if(pot2 < pot1){
			pot1 -= pot2;
			while(pot1--) b*=10;
		}
		cout<<a<<' '<<b<<'\n';

		int g = __gcd(a,b);
		a/=g;
		b/=g;
		//if(criba[a] || criba[b]) cout<<"impossible"<<'\n';
		//else cout<<a<<' '<<b<<'\n';

	}

	return 0;
}
