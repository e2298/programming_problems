#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

int a[1000];
int b[1000];

int main(void){
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		cin>>a[i];
	}
	for(int i = 0; i < n; i++){
		cin>>b[i];
	}
	if(a[0] > b[0]) cout<<"0\n";
	else{
		int diff = b[0] - a[0];
		for(int i = 0; i < n; i++){
			a[i] += diff;
		}

		int r = 0;
		for(int i = 0; i < n && !r; i++){
			if(a[i] > b[i]) r = 1;
			else if(a[i] < b[i]) r = -1;
		}
		if( r == -1) cout<<diff+1<<'\n';
		else cout<<diff<<'\n';
	}
	return 0;
}
