#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

vii g[1000];
int dp[1000];

int solve(int i){
	if(dp[i]) return dp[i];
	if(!g[i].size()) return 0;

	int mx = 0;
	for(auto j:g[i]){
		mx = max(mx, solve(j.first)+j.second);
	}
	return dp[i] = mx;
}

int main(void){
	int n,m;
	cin>>n>>m;
	while(m--){
		int a,b,c;
		cin>>a>>b>>c;
		g[a-1].push_back(ii(b-1,c));
	}
	int mx = 0;
	for(int i = 0; i < n; i++) mx = max(mx, solve(i));

	cout<<mx<<'\n';

	return 0;
}
