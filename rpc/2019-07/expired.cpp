#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;

#define MAXP  (int)2e7
int criba[MAXP+1];
void cribar(){
	int w[] = {4,2,4,2,4,6,2,6};
	for(int p = 25; p <= MAXP; p+=10) criba[p] = 5;
	for(int p = 9; p <= MAXP; p+=6) criba[p] = 3;
	for(int p = 4; p <= MAXP; p+=2) criba[p] = 2;
	for(int p = 7,cur =0; p*p <= MAXP; p+=w[cur++&7]) if(!criba[p]) for(int j = p*p; j <= MAXP; j+=(p<<1)) if(!criba[j]) criba[j]=p;
}

int main(void){
	cribar();
	int n;
	cin>>n;
	criba[1] = 1;
	while(n--){
		int a=0,b=0;
		char c = 0;
		int cnt=0;
		int pot1 = 0, pot2 = 0;
		string s;
		cin>>s;
		for(auto c:s){
			if(c == '.') cnt++;
			else{
				a *= 10;
				a += c-'0';
				if(cnt) pot1++;
			}
		}
		cnt = 0;
		cin>>s;
		for(auto c:s){
			if(c == '.') cnt++;
			else{
				b *= 10;
				b += c-'0';
				if(cnt) pot2++;
			}
		}
		if(pot1 < pot2){
			pot1 = pot2-pot1;
			while(pot1--) a*=10;
		}
		else if(pot2 < pot1){
			pot1 -= pot2;
			while(pot1--) b*=10;
		}
		//cout<<a<<' '<<b<<'\n';

		int g = __gcd(a,b);
		a/=g;
		b/=g;
		if((a | b) == 1) cout<<"2 2\n";
		else if(criba[a] || criba[b]) cout<<"impossible"<<'\n';
		else cout<<a<<' '<<b<<'\n';

	}

	return 0;
}
