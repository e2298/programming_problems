#include<bits/stdc++.h>
using namespace std;

typedef pair<int,int>ii;

char seen[5001][5001];
unsigned dist[5001][5001];

int main(void){
	int n,q;
	cin>>n>>q;

	memset(dist, -1, 5001*5001*4);

	while(n--){
		int x,y;
		cin>>x>>y;
		memset(seen,0, 5001*5001);
		int curr=0;
		int ls=1;
		int nls=0;
		queue<ii> q;
		q.push(ii(x,y));
		while(!q.empty()){
			ii t = q.front();
			q.pop();
			x = t.first;
			y = t.second;
			ls--;
			dist[x][y] = curr;
			if(!ls){
				ls = nls;
				nls = 0;
				curr++;
			}
			if(x < 0 || x > 5000 || y < 0 || y > 5000) continue;
			int nx, ny;
			int nd = curr+1;
			int xx[] = {-1, 0, 1, -1, 1, -1, 0, 1};
			int yy[] = {1, 1, 1, 0, 0, -1, -1, -1};
			for(int i = 0; i < 8; i++){
				nx = x + xx[i];
				ny = y + yy[i];
				if(nx >= 0 && nx <= 5000 && ny >= 0 && ny <= 5000 && !seen[nx][ny] && dist[nx][ny] > nd)
					{q.push(ii(nx,ny)); nls++; seen[nx][ny] = 1;}
			}


		}

	}
	while(q--){
		int x,y;
		cin>>x>>y;
		cout<<dist[x][y]<<'\n';
	}
	

	return 0;
}
