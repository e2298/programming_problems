#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

ll DP[300002][12];
ll num[300005];
int n, m;

ll solve(int index, int c) {
    if (index == n) {
        return 0;
    }
    if (DP[index][c] != -1) return DP[index][c];
    ll best = 0;
    if (c == m){
        best = solve(index+1, 1) + num[index];
    }
    best = max(best, solve(index+1, min(c+1,m)));
    //cout << index << " " << c << " " << best << endl;
    return DP[index][c] = best;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    memset(DP, -1, sizeof DP);

    cin >> n >> m;
    for (int i = 0; i < n; i++){
        cin >> num[i];
    }

    cout << solve(0,0) << endl;

}
