#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

#define MAX_V 3005
#define INF (1e9)
int SRC, SNK;

map<int,int> G[MAX_V];
int f, p[MAX_V];

void augment(int v, int minE){
	if(v == SRC) f = minE;
	else if(p[v] != -1){
		augment(p[v], min(minE, G[p[v]][v]));
		G[p[v]][v] -=f, G[v][p[v]] += f;
	}
}

ll maxflow(){
	ll Mf = 0;
	do{
		f = 0;
		char used[MAX_V]; queue<int> q; q.push(SRC);
		memset(used,0,sizeof(used));
		memset(p,-1,sizeof(p));
		while(q.size()){
			int u = q.front(); q.pop();
			if(u == SNK) break;
			for(auto it:G[u]){
				if(it.second > 0 && !used[it.first])
					used[it.first] = true, q.push(it.first), p[it.first] = u;
			}
		}
		augment(SNK, INF);
		Mf += f;
	}while(f);
	return Mf;
}

int main(void){
	int m,n,k;
	cin>>m>>n>>k;
	SRC = m + n;
	SNK = SRC+1;
	for(int i =0; i < k; i++){
		int a,b;
		cin>>a>>b;
		a--;
		b--;
		G[a][b+m] = 1;
	}
	for(int i = 0; i < m; i++){
		G[SRC][i] = 1;
	}
	for(int i = 0; i < n; i++){
		G[i+m][SNK] = 1;
	}
	int r = maxflow();
	int mx = 0;
	for(int i = 0; i < m ; i++){
		int cur = 0;
		for(auto it:G[i]){
			int c = it.first;
			int w = it.second;
			if(c >= m && c < m+n && w > 0 && G[c][SNK] > 0) cur++;	
		}
		mx = max(mx, cur);
	}
	r += min(mx,2);
	cout<<r<<'\n';


	return 0; 
}
