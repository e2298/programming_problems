#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<ll> vi;
#define tipo ll
struct F{
	static const int sz = 100005;
	tipo t[sz];
	void adjust(int p, tipo v){
		for(int i = p; i < sz; i+=(i&-i)) t[i]+=v;
	}
	tipo sum(int p){
		tipo s = 0;
		for(int i = p; i; i-=(i&-i)) s+=t[i];
		return s;
	}
	tipo sum(int a, int b){return sum(b)-sum(a);}
};

F fenwick;

vi G[100005];
vi u[100005];
int utype[100005];
ll uvalue[100005];


void solve(int i){
	ll multi = 0;
	ll tot = 0;
	ll lastsum = 0;
	for(auto up:u[i]){
		switch (utype[up]){
			case 2:
				tot += fenwick.sum(lastsum, up)*multi;
				lastsum = up;
				multi = uvalue[up];
				break;
			case 3:
				fenwick.adjust(up, uvalue[up]);
				break;
			case 4:
				tot += fenwick.sum(lastsum, up)*multi;
				lastsum = up;
				uvalue[up] = tot;
				break;
		}
	}
	for(auto j:G[i]){
		solve(j);
	}
	for(auto up:u[i]){
		if(utype[up] == 3)
			fenwick.adjust(up, -uvalue[up]);
	}

}

int main(void){
	int n,s;
	cin>>n>>s;
	utype[0] = 2;
	uvalue[0] = s;
	u[1].push_back(0);
	int emp = 1;
	for(int i = 1; i <= n; i++){
		int type,target;
		ll value;
		cin>>type>>target;
		utype[i] = type;
		switch (type){
			case 1:
				emp++;
				utype[i] = 2;
				uvalue[i] = s;
				G[target].push_back(emp);
				u[emp].push_back(i);
				break;
			case 2:
				cin>>uvalue[i];
				u[target].push_back(i);
				break;
			case 3:
				cin>>uvalue[i];
				u[target].push_back(i);
				break;
			case 4:
				u[target].push_back(i);
				break;
				
		}
	}
	solve(1);
	for(int i =0 ; i <= n; i++){
		if(utype[i] == 4) cout<<uvalue[i]<<'\n';
	}
	

	return 0;
}
