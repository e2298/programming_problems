#include <bits/stdc++.h>

using namespace std;
typedef long long ll;

ll x[1000005];
ll y[1000005];

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n;
    cin >> n;

    ll exterior = 0;

    for (int i = 0; i < n; i++){
        cin >> x[i] >> y[i];
    }

    for (int i = 0; i < n; i++){
        exterior += __gcd(llabs(x[i]-x[(i+1)%n]),llabs(y[i]-y[(i+1)%n]));
    }

    ll area = 0;
    for (int i = 0; i < n; i++){
        area+=x[i]*y[(i+1)%n];
    }
    for (int i = 0; i < n; i++){
        area-=x[(i+1)%n]*y[i];
    }

    area = llabs(area)/2;

    cout << (area + 1 - exterior/2) << "\n";


}
