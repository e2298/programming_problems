#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

vi g1[200];
vi g2[200];
map<string,int> codes;
int tgt;
vi seen;

int True(int i){
	if(i == tgt) return 1;
	int r = 0;
	for(auto j:g1[i]){
		if(!seen[j]){
			seen[j] = 1;
			r = r || True(j);
		}
	}
	return r;
}


int main(void){
	int n,m;
	scanf("%d %d\n", &n, &m);
	int curr=0;
	while(n--){
		char a[40];
		char b[40];
		scanf("%s are worse than %s\n", a, b);
		string aa(a);
		string bb(b);
		if(codes.find(aa) == codes.end()) codes[aa] = curr++;
		if(codes.find(bb) == codes.end()) codes[bb] = curr++;
		g1[codes[aa]].push_back(codes[bb]);
		g2[codes[bb]].push_back(codes[aa]);
	}
	while(m--){
		char a[40];
		char b[40];
		scanf("%s are worse than %s\n", a, b);
		string aa(a);
		string bb(b);
		if(codes.find(aa) == codes.end()) codes[aa] = curr++;
		if(codes.find(bb) == codes.end()) codes[bb] = curr++;
		tgt = codes[bb];
		seen = vi(200);
		if(True(codes[aa])){
			cout<<"Fact\n";
			continue;
		}	
		seen = vi(200);
		tgt = codes[aa];
		if(True(codes[bb])){
			cout<<"Alternative Fact\n";
			continue;
		}	
		cout<<"Pants on Fire\n";
	}
	
}
