#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<ll> vi;
#define INF (1ll<<50)
ll n, m, t;
ll cost[2000];
ll tim[2000];
vi g[2000];

ll dp[2000][2000];

ll solve(ll i, ll l){
	if(l < 0) return INF; 
	if(l == 0 && i != 0) return INF;
	if(i == 0 && l == 0) return dp[i][l] = 0ll;
	if(dp[i][l]) return dp[i][l];
	ll r = cost[i] + solve(i, l-tim[i]);
	for(auto j:g[i]){
		r = min(r, cost[j] + solve(j, l-t-tim[j]));
	}
	return dp[i][l] = r;
}


int main(void){
	ll x;
	cin>>x;
	cin>>n>>m>>t;
	for(ll i = 0; i < m; i++){
		ll a,b;
		cin>>a>>b;
		a--;
		b--;
		g[a].push_back(b);
		g[b].push_back(a);
	}
	for(ll i = 0; i < n; i++){
		cin>>tim[i]>>cost[i];
	}
	if(cost[0] +  solve(0, x-tim[0]) >= INF) cout<<"It is a trap.\n";
	else cout<<cost[0] + solve(0,x-tim[0])<<'\n';

	return 0;
}
