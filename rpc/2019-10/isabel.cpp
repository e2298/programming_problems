#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int dp[200];
vector<int> sizes({6,2,5,4,7});
vector<int> sz({2,5,4,7,6});
int solve(int k){
	if(k <= 0) return 0;
	if(dp[k]) return dp[k];
	int m = 500;
	for(auto i:sizes){
		m = min(m, solve(k-i));		
	}
	return dp[k] = m+1;
}
map<int,int> chars;

int main(void){
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		if(n == 1){
			cout<<"1\n";
			continue;
		}
		if(n == 2){
			cout<<"2\n";
			continue;
		}
		chars = map<int,int>();
		solve(n+1);
		int curr = n+1;
		for(auto i:sz){
			if(dp[curr-i]+1 == dp[curr]){
				chars[i]++;
				curr -= i;
				break;
			}
		}
		while(curr > 0){
			if(dp[curr] == 1){
				for(auto i:sizes){
					if(i >= curr) {
						chars[i]++;
						curr -= i;
						break;
					}
				}
			}
			else{
				for(auto i:sizes){
					if(dp[curr-i]+1 == dp[curr]){
						chars[i]++;
						curr -= i;
						break;
					}
				}
			}
		}
		if(chars[6]){
			if(chars[2]){
				cout<<'1';
				chars[2]--;
			}
			else if(chars[5]){
				cout<<'2';
				chars[5]--;
			}
			else if(chars[4]){
				cout<<'4';
				chars[4]--;
			}
			else{
				if(curr == 0){
					cout<<'6';
				}
				else{
					curr += 6;
					if(curr <= 2) cout<<'1';
					else cout<<'2';
				}
				chars[6]--;
			}
		}
		while(chars[6]--) cout<<'0';
		while(chars[2]--) cout<<'1';
		while(chars[5]--) cout<<'2';
		while(chars[4]--) cout<<'4';
		while(chars[7]--) cout<<'8';
		cout<<'\n';
	}
	return 0;
}
