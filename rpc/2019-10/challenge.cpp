#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

#define MOD 1000000007ll

struct mat{
	static const int sz = 26;
	ll m[sz][sz];
	mat(){
		for(int i = 0; i < sz; i++){
			for(int j = 0; j < sz; j++){
				m[i][j] = 0;
			}
		}
	}
	ll* operator[](ll i){
		return m[i];
	}
	mat operator*(mat b){
		mat r;
		for(ll i = 0; i < sz; i++){
			for(ll j = 0; j < sz; j++){

				for(ll k = 0; k < sz; k++){
					r[i][j] += m[i][k] * b[k][j];
					r[i][j] %= MOD;
				}
			}
		}
		return r;
	}
};

mat ident(){
	mat r;
	for(int i = 0; i < r.sz; i++){
		r[i][i] = 1;
	}
	return r;
}

mat exp(mat b, ll e){
	if(e == 0) return ident();
	mat r = exp(b,e/2);
	r = r*r;
	if(e&1) return r * b;
	else return r;
}

char hasp[26];
ll times[26];

int main(void){
	int t;
	cin>>t;
	while(t--){
		memset(hasp,0,sizeof(hasp));
		memset(times,0,sizeof(times));
		string s;
		int k;
		ll n;
		cin>>s>>k>>n;
		for(auto i:s){
			times[i-'a']++;
		}
		char a;
		string p;
		mat init;
		while(k--){
			cin>>a>>p;
			a-= 'a';
			hasp[a] = 1;
			for(auto i: p){
				init[i-'a'][a]++;
			}
		}
		for(int i = 0; i < 26; i++){
			if(!hasp[i])init[i][i]++;
		}
		mat ex = exp(init,n);
		ll r = 0;
		for(int i = 0; i < 26; i++){
			for(int j = 0; j < 26; j++){
				r += times[j] * ex[i][j];
				r %= MOD;
			}
		}
		cout<<r<<'\n';

	}

	return 0;
}
