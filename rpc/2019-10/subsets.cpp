#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> ii;

int A[1000005];
int queries[105];
ll res[105];

int main(void){
	int n,q;
	cin>>n>>q;
	for(int i = 0; i < n; i++){
		cin>>A[i];
	}
	for(int i = 0; i < q; i++){
		cin>>queries[i];
	}
	sort(A,A+n);
	for(int i = 1; i < n; i++){
		for(int j = 0; j < q; j++){
			int tgt = queries[j];
			if(tgt > A[i])
				res[j] +=  min(upper_bound(A,A+n,tgt-A[i]), A+i)-A;
		}
	}
	for(int i = 0; i < q; i++)cout<<res[i]<<'\n';
	return 0;
}
