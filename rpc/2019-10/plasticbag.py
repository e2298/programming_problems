MOD = 4300000013
def inv(n):
    return pow(n,MOD-2, MOD)

fen = []
n = 0
def adjust(p,v):
    i = p
    while(i < n):
        fen[i] += v
        i += (i&-i)

def sum(p):
    i = p
    s = 0
    while(i != 0):
        s += fen[i]
        i -= (i&-i)
    return s


t = int(input())
for _ in range(t):
    [n,m] = [int(i) for i in input().split()]
    fen = [0]*(n+1)
    ways = 1
    records = 0
    times = [0] * (10**4+5);
    for _ in range(m):
        line = input().split()
        if(line[0] == 'Q'):
            print(sum(int(line[2])) - sum(int(line[1])-1))
        elif (line[0] == 'W'):
            print(ways)
            pass
        else:
            d= int(line[1])
            b= int(line[2])
            adjust(d,b)
            times[b] += 1
            records += 1
            ways = ways * records
            ways = ways * inv(times[b])
            ways = ways % MOD





