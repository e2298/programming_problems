#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
#define MOD 4300000013ll
ll expmod(ll b, ll e){
	if(!e) return 1;
	ll q = expmod(b,e/2); q = (q*q)%MOD;
	return e%2? (b*q)%MOD:q;
}

static const int sz=100002;
ll t[sz];
void adjust(int p, ll v){
	for(int i = p; i < sz; i+=(i&-1)) t[i] += v;
}
ll sum(int p){
	ll s = 0;
	for(int i = p; i; i-=(i&-1)) s+=t[i];
	return s;
}


ll invmod[100005];

int main(void){
	invmod[1] = 1;
	for(int i = 2; i < 100005; i++){
		invmod[i] = expmod(i,MOD-2);
	}
	int tt;
	cin>>tt;
	while(tt--){
		memset(t, 0, sizeof(t));
		ll ways = 1;
		ll records = 0;
		map<int, ll> times;
		int n,m;
		ll p1,p2;
		cin>>n>>m;
		while(m--){
			char type;
			cin>>type;
			switch (type){
				case 'Q':
					cin>>p1>>p2;
					cout<<sum(p2)-sum(p1-1)<<'\n';
					break;
				case 'W':
					cout<<ways<<'\n';
					break;
				case 'U':
					cin>>p1>>p2;
					adjust(p1, p2);
					times[p2]++;
					ways *= ++records;
					ways %= MOD;
					ways *= invmod[times[p2]];
					ways %= MOD;
			}
		}
	}

	return 0;
}
