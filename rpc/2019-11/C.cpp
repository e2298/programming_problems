#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
typedef pair<int,int> ii;

#define INF (1<<30)

int grid[10002][1000];
int dist[10002][1000];

int r,c;
int gud(int i, int j){
	return i >= 0 && i <= r && j >= 0 && j < c;
}

int main(){
	ll k;
	cin>>r>>c>>k;
	int m = 0;
	for(int i = 0; i < r; i++){
		for(int j = 0; j < c; j++){
			cin>>grid[i+1][j];
			m = max(m, grid[i+1][j]);
		}
	}
	r++;
	for(int j= 0; j < c; j++){
		grid[0][j] = INF;
		grid[r][j] = INF;
	}
	int lo = 0;
	int up = m+1;
	while(lo < up){
		int limit = (lo + up+1)/2;
		deque<ii> Q;
		for(int i = 0; i < r; i++){
			for(int j = 0; j < c; j++) dist[i][j] = INF;
		}
		for(int j = 0; j < c; j++) dist[r][j] = 0;
		for(int j = 0; j < c; j++) Q.push_back(ii(r,j));
		while(Q.size()){
			ii c = Q.front(); Q.pop_front();
			int i = c.first;
			int j = c.second;
			if(gud(i-1,j) && dist[i-1][j] > dist[i][j] + (grid[i-1][j] < limit)){
				dist[i-1][j] = dist[i][j] + (grid[i-1][j] < limit);
				if(grid[i-1][j] < limit) Q.push_back(ii(i-1,j));
				else Q.push_front(ii(i-1,j));
			}
			if(gud(i+1,j) && dist[i+1][j] > dist[i][j] + (grid[i+1][j] < limit)){
				dist[i+1][j] = dist[i][j] + (grid[i+1][j] < limit);
				if(grid[i+1][j] < limit) Q.push_back(ii(i+1,j));
				else Q.push_front(ii(i+1,j));
			}
			if(gud(i,j-1) && dist[i][j-1] > dist[i][j] + (grid[i][j-1] < limit)){
				dist[i][j-1] = dist[i][j] + (grid[i][j-1] < limit);
				if(grid[i][j-1] < limit) Q.push_back(ii(i,j-1));
				else Q.push_front(ii(i,j-1));
			}
			if(gud(i,j+1) && dist[i][j+1] > dist[i][j] + (grid[i][j+1] < limit)){
				dist[i][j+1] = dist[i][j] + (grid[i][j+1] < limit);
				if(grid[i][j+1] < limit) Q.push_back(ii(i,j+1));
				else Q.push_front(ii(i,j+1));
			}
		}
		int res = INF;
		for(int j = 0; j < c; j++){
			res = min(res, dist[0][j]);
		}
		if(res <= k){
			lo = limit;	
		}
		else up = limit-1;
	}	
	cout<<lo<<'\n';
	
	return 0;
}
