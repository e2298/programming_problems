MOD = 1000000007

input()
toks = input().split()

op = 0
stack = []
val = 0
for tok in toks:
    if(tok.isdigit()):
        if(op == 1):
            val = val * int(tok)
        else:
            val += int(tok)
        val = val % MOD 
    elif(tok == "("):
        op = op ^ 1
        stack.append(val)
        if(op == 1):
            val = 1
        else:
            val = 0;
    else:
        op = op ^ 1
        if(op == 1):
            val = val * stack.pop()
        else:
            val += stack.pop()
        val = val % MOD

print(val)
