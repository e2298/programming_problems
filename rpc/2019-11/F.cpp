//RPC 19/10/2019
//Problema F
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int main(){
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	string s;
	cin >> s;
	int r = 4;
	vector<string> uno({"m","e","o","w"});
	vector<string> dos({"me", "eo", "ow", "emo", "moe", "oew", "ewo", "emwo"});
	vector<string> tres({"meo", "eow", "emow", "moew", "mewo", "mow", "mew"});
	vector<string> cuatro{("meow")};
	for (auto i : uno){
		if(s.find(i) != string::npos){
			r = 3; 
			break;
		}
	}
	for (auto i : dos){
		if(s.find(i) != string::npos){
			r = 2; 
			break;
		}
	}
	for(int i = 0; i < s.size(); ++i){
		if(s[i] == 'm'){
			if( (i+1) < s.size() && s[i+1] == 'e'){
				if(i+3 < s.size() && s[i+3] == 'o'){
					if(i+4 < s.size() && s[i+4] == 'w'){
						r = 1;
					}
				}
				if(i+3 < s.size() && s[i+3] == 'w'){
					r = 1;
				}
			}else{
				if((i+2) < s.size() && s[i+2] == 'e'){
					if( (i+3 < s.size() && s[i+3] == 'w')){ //mxew
						r = 2;
					}else{
						if(i+4 < s.size() && s[i+4] == 'w'){ //mxexw
							r = 2;
						}
					}
				}else{
					if((i+2) < s.size() && s[i+2] == 'o'){
						if((i+3 < s.size() && s[i+3] == 'w')){
							r = 1;
						}
					}
				}
			}
		}else{
			if(s[i] == 'e'){
				if(i+1 < s.size() && s[i+1] == 'm'){
					if(i+3 < s.size() && s[i+3] == 'w'){
						r = 2;
					}
				}
			}
		}
	}
	for (auto i : tres){
		if(s.find(i) != string::npos){
			r = 1; 
			break;
		}
	}
	for (auto i : cuatro){
		if(s.find(i) != string::npos){
			r = 0; 
			break;
		}
	}
	cout << r << endl;
	
	return 0;
}
