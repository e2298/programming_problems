#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll solve(ll l, ll n){
	if(l %n) return 1 + solve(l, n-(l%n));
	else return 1;
}


int main(void){
	ll l,n;
	cin>>l>>n;
	cout<<solve(l,n)<<'\n';

	return 0;
}
