//RPC 19/10/2019
//Problema I
#include <bits/stdc++.h>
using namespace std;
typedef long long unsigned ll; //Tiene que ser asi
//NO se puede typedef ll
//unsigned ll     NO se puede
vector<int> input;

int main(){
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	ll n;
	cin >> n;
	input.assign(n, 0);
	ll izq = 0;
	ll der = 0;
	for(ll i = 0; i < n; ++i){
		cin >> input[i];
		der += input[i];
	}
	ll respuesta = 0;
	for(int i = 0; i < n; ++i){
		ll miRespuesta = 0;
		der -= input[i];
		izq += (input[i] * input[i]);
		miRespuesta = (izq * der );
		if( miRespuesta > respuesta){
			respuesta = miRespuesta;
		}
	}
	cout << respuesta << "\n";
	return 0;
}
