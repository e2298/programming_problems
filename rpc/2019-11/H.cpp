#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

int mat[11*11][11*11];
int m[11*11][11*11];

int valid(int i, int j){
	switch (j){
		case 0:
			return  i  < 6;
		case 1:
			return  i  < 7;
		case 2:
			return  i  < 8;
		case 3:
			return  i  < 9;
		case 4:
			return  i  < 10;
		case 5:
			return  i  < 11;
		case 6:
			return i > 0 && i < 11;
		case 7:
			return i > 1 && i < 11;
		case 8:
			return i > 2 && i < 11;
		case 9:
			return i > 3 && i < 11;
		case 10:
			return i > 4 && i < 11;
		default:
			return false;
	}
}

int idx(int i, int j){ return 11*i + j;}

void add(int a, int b, int i, int j){
	if(valid(i,j)) mat[idx(a,b)][idx(i,j)] = mat[idx(i,j)][idx(a,b)] = 1;
}


int main(void){
	for(int i = 0; i < 11; i++){
		for(int j = 0; j < 11; j++){
			if(!valid(i,j)) continue;
			for(int k = 1; k < 11; k++){
				add(i,j,i+k,j);
				add(i,j,i,j+k);
				add(i,j,i+k,j+k);
			}
		}
	}
	for(int i = 0; i < 11*11; i++) for(int j = 0; j < 11*11; j++) for(int k = 0; k < 11*11; k++) m[i][j] += mat[i][k] * mat[k][j];
	char srci,desti;
	int srcj,destj;
	cin>>srci>>srcj>>desti>>destj;
	srci -= 'a';
	desti -= 'a';
	srcj--;
	destj--;
	if(srci > 5){
		srcj += srci -5;
	}
	if(desti > 5){
		destj += desti -5;
	}
	cout<<m[idx(desti,destj)][idx(srci,srcj)]<<'\n';


	return 0;
}
