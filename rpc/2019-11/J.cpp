#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll avg[100][100];
ll ril[100][100];

int main(void){
	memset(ril,-1,sizeof(ril));
	int n;
	cin>>n;
	for(int i = 0; i < n; i++){
		ll mn = 1<<60;
		for(int j = 0; j < n; j++){
			cin>>avg[i][j];
			if(i != j && avg[i][j] != -1ll)
				mn = min(mn, avg[i][j]);
		}
		if(mn != -1ll){
			for(int j = 0; j < n; j++){
				if(i != j && avg[i][j] == mn) ril[i][j] = avg[i][j];
			}
		}
	}
	return 0;
}
