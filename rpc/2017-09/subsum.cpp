#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

int main(void){
	int n,s;
	vi sum;
	while(cin>>n>>s){
		if(n==0){
			cout<<"-1\n";
			continue;
		}

		sum = vi(n+1);
		for(int i = 1; i<=n; i++){
			cin>>sum[i];
			sum[i]+=sum[i-1];
		}	
		int i;	
		for(i = 1; i <=n; i++){
			int t = lower_bound(sum.begin() + (i), sum.end(), s+sum[i-1]) - sum.begin();
			if((sum[t]-sum[i-1]) == s) {
				cout<<i<<' '<<t<<'\n';
				break;
			}
		}
		if(i >n) cout<<"-1\n";
	}

	return 0;
}
