#include <stdio.h>

int main(void){
	long n;
	long k;
	int bit;
	while(scanf("%ld %ld", &n, &k) != EOF){
		bit = 0;
		while(n>>(bit++));
		bit--;
		if(bit == 0){
			printf("%ld\n", (((long)1)<<k)-1);
			break;
		}
		bit--;

		long t = ((long)1)<<bit;
		long mask = (((long)1)<<k)-1;
		long maskk = mask;

		while((t|mask)<n) mask<<=1;
		
		if(((t|mask) > (maskk<<(bit-k+1))) && ((maskk<<(bit-k+1)) > n)){
			printf("%ld\n", (maskk<<(bit-k+1)));
		}
		else if((t<<1)<(t|mask)){
			t |= (((long)1)<<k)-1;
			printf("%ld\n",t);
		}
		else printf("%ld\n", t|(mask));
	}

	return 0;
}
