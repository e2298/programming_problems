#include <stdio.h>

int main(void){
	int t, n;
	scanf("%d ", &t);

	while(t--){
		scanf("%d ", &n);
		if(n>2) n = (n-2)*3+1;
		printf("%d\n", n);
	}

	return 0;
}
