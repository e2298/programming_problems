#include <stdio.h>
#include <stdlib.h>

char out[1000];

int main(void){
	int n;
	size_t s;
	scanf("%d ", &n);
	while(n--){
		char* in = NULL;
		s = 0;
		getline(&in,&s,stdin);
		
		int j = 0;
		for(int i = 0; in[i]; i++){
			switch(in[i]){
				case 'a':{
					out[j++]='a';
			       		out[j++]='p';
					out[j++]='a';
					break;
				}case 'e':{
					out[j++]='e';
			       		out[j++]='p';
					out[j++]='e';
					break;
				}case 'i':{
					out[j++]='i';
			       		out[j++]='p';
					out[j++]='i';
					break;
				}case 'o':{
					out[j++]='o';
			       		out[j++]='p';
					out[j++]='o';
					break;
				}case 'u':{
					out[j++]='u';
			       		out[j++]='p';
					out[j++]='u';
					break;
				}
				default:{
					out[j++]=in[i];	     
				}
			}	
		}
		out[--j] = '\0';
		printf("%s\n", out);
		free(in);
	}
}
