#include<bits/stdc++.h>
using namespace std;

int main(void){
	int h,w;
	cin>>h>>w;
	double cnt = 0;
	double sum = 0;
	for(int i = 0; i < h-1; i++){
		for(int j = 0; j < w; j++){
			char c;
			cin>>c;
			if(c != '.'){
				sum += j;
				cnt += 1;
			} 
		}
	}
	int lst, fst = -1;
	for(int j = 0; j < w; j++){
		char c;
		cin>>c;
		if(c!= '.'){
			if(fst == -1) fst = j;
			lst = j;
			sum += j;
			cnt += 1;
		}
	}
	
	sum /= cnt;
	if(sum < fst) cout<<"left\n";
	else if((int)sum > lst || (double)(lst+1) == sum) cout<<"right\n";
	else cout<<"balanced\n";

	return 0;
}
