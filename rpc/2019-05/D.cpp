#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll bit[100015];
int n;

void bit_inc(ll i, ll a){
	for(;i < (100015); i += i&(-i)){
		bit[i]+=a;
	}
}

ll bit_q(ll i){
	ll r = 0;
	for(;i; i -= i&(-i)){
		r += bit[i];
	}
	return r;
}

int main(void){
	int n,q;
	cin>>n>>q;
	for(int i = 0; i < n; i++){
		ll t;
		cin>>t;
		bit_inc(i+1, t);
	}
	for(int i = 0; i < q; i++){
		int j;
		ll t;
		cin>>j>>t;
		j++;
		ll lst = bit_q(j) - bit_q(j-1);
		bit_inc(j, t-lst);
		ll tgt = bit_q(n)/2;
		int lo = 1;
		int up = n;
		while( lo < up){
			int curr = (lo + up+1) /2;
			ll a,b,c;
			a = bit_q(curr-1);
			b = bit_q(n) - bit_q(curr);
			c = (bit_q(curr) - bit_q(curr-1)) &1;
			if(a < b) a += c;
			else b += c;
			if(a < b){
				lo = curr;		
			}
			else up = curr-1;
			
		}
		ll mn = 1000000000000;
		int r;
		for(int off = -1; off <= 1; off++){
			int curr = lo + off;
			ll a,b,c;
			a = bit_q(max(0,curr-1));
			b = bit_q(n) - bit_q(curr);
			c = (bit_q(curr) - bit_q(max(curr-1,0))) &1;
			if(a < b) a += c;
			else b += c;
			if(abs(a-b) < mn){
				r = lo + off;
				mn = abs(a-b);
			}	
		}
			
		cout<<r-1<<'\n';
	}
	return 0;
}
