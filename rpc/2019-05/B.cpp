#include<bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

int dad[1000000];
int w[1000000];
int grid[1000][1000];
int ln[1000][1000];
int n,m;

int is_in(int a, int b){
	return a <n && a >=0 && b < m && b >= 0;
}

int find(int a){
	if(dad[a] == a) return a;
	else return dad[a] = find(dad[a]);
}

void join(int a, int b){
	a = find(a);
	b = find(b);
	if(a == b) return;
	if(w[a] > w[b]) {
		dad[b] = a;
		w[a] += w[b];
	}
	else {
		dad[a] = b;
		w[b] += w[a];
	}
}

int main(void){
	int c;
	cin>>c>>n>>m;
	for(int i = 0; i < n*m; i++){
		dad[i] = i;
		w[i] = 1;
	}
	int cnt = 0;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			ln[i][j] = cnt;
			cnt++;
			char cc;
			cin>>cc;
			if(cc == '.'){
				grid[i][j] = 1;
				if(is_in(i,j-1) && grid[i][j-1]){
				       	join(ln[i][j-1], ln[i][j]);
				}
				int ii, jj;
				ii = i-1;
				jj = j-1 + (i&1);
				if(is_in(ii, jj) && grid[ii][jj]){
				       	join(ln[i][j], ln[ii][jj]);
				}
				jj = j + (i&1);
				if(is_in(ii, jj) && grid[ii][jj]){
					join(ln[i][j], ln[ii][jj]);
				}
			}	
		}
	}
	vi sz;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(grid[i][j] && dad[ln[i][j]] == ln[i][j]){
				sz.push_back(w[ln[i][j]]);
			}	
		}
	}
	sort(sz.begin(), sz.end(), greater<int>());

	int r = 0;
	int i = 0;
	while(c > 0){
		c -= sz[i];
		r++;
		i++;
	}
	cout<<r<<'\n';

	return 0;
}
