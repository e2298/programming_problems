#include<bits/stdc++.h>
using namespace std;
typedef long long ll;

ll inp[256];
ll out[256];


int main(void){
	string in;
	cin>>in;
	ll n;
	cin>>n;
	ll num = 0;
	char lst = 0;
	int m = in.size();
	for(int i = 0; i < m; i++){
		if(in[i] >= 'A' && in[i] <= 'Z'){
			if(num){
				inp[lst] += n*num;
				lst = in[i];
				num = 0;
			}
			else{
				inp[lst] += n;
				lst = in[i];
			}
		}
		else{
			num *= 10;
			num += in[i]-'0';
		}
	}
	if(num) inp[lst] += n*num;
	else inp[lst] += n;
	cin>>in;
	num = 0;
	lst = 0;
	m = in.size();
	for(int i = 0; i < m; i++){
		if(in[i] >= 'A' && in[i] <= 'Z'){
			if(num){
				out[lst] += num;
				lst = in[i];
				num = 0;
			}
			else{
				out[lst] += 1;
				lst = in[i];
			}
		}
		else{
			num *= 10;
			num += in[i]-'0';
		}
	}
	out[0] = 0;
	if(num) out[lst] += num;
	else out[lst] += 1;
	ll mn = 100000000000000;
	for(int i = 0; i < 256; i++){
		if(out[i]){
			mn = min(mn, inp[i]/out[i]);
		}	
	}
	cout<<mn<<'\n';

	return 0;
}
