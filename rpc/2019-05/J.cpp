#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define sum(a,b) (((b)*((b)+1))/2-((a)*((a)-1))/2)

ll m;

ll cool(ll n){
	ll lo = m;
	ll up = n;
	while(lo < up){
		ll curr = (up+lo)/2;
		if(sum(m,curr-1) < sum(curr+1,n)){
			lo = curr+1;
		}
		else up = curr;
	}

	if(sum(m,lo-1) == sum(lo+1,n)) return lo;
	else return 0;
}

int main(void){
	cin>>m;

	/*ll lo = m+1;
	ll up = 10000000;
	while(lo < up){
		ll curr = lo + up;
		curr >>= 1;
		if(cool(curr)){
			up = curr;
		}
		else lo = curr+1;
	}
	cout<<m<<' '<<cool(lo)<<' '<<lo<<'\n';
	*/
	for(int i = m+2; ; i++){
		if(cool(i)){
			cout<<m<<' '<<cool(i)<<' '<<i<<'\n';
			break;
		}
	}
	return 0;
}
