from math import gcd
def tot(n):
    r = 0
    for i in range(n):
        if(gcd(i,n) == 1):
            r += 1
    return r

def main():
    ins = input().split()
    n = int(ins[0])
    m = int(ins[1])
    c = int(ins[2])

    mod = 1000000007

    k = pow(c,n*n,mod)
    n = m

    r = 0
    for i in range(1,n+1):
        if(n%i == 0):
            r += tot(i) * pow(k,n//i,mod)
            r = r % mod
    r = r * pow(n,mod-2,mod)
    r = r % mod
    print(r)

main()

