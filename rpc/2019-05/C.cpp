#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;
typedef pair<int, ii> iii;
typedef priority_queue<iii, vector<iii>, greater<iii>> pq;
typedef set<ii> sii;

int sock[500000];
int cabl[500000];


int main(void){
	int n,m;
	cin>>n>>m;

	for(int i = 0; i < n; i++){
		cin>>sock[i];
	}
	for(int i = 0; i < m; i++){
		cin>>cabl[i];
	}
	sort(cabl, cabl+n);
	pq q;
	sii s;
	for(int i = 1; i < n; i++){
		q.push(iii(sock[i] - sock[i-1], ii(i-1, i)));
		s.insert(ii(i-1,i));
	}
	int can = 1;

	for(int i = 0; i < m; i++){
		if(q.empty() || q.top().first > cabl[i]){
			can = 0;
			break;
		}
		ii t = q.top().second;
		q.pop();
		s.erase(t);
		int a = t.first;
		int b = t.second;
		if(a && (b < (n-1))){
			int o;
			if(sock[a] - sock[a-1] < sock[b+1] - sock[b]){
				o = 1;
			}	
			else o = 0;
			if((o || s.count(ii(a,b+1))) && !s.count(ii(a-1,b))){
				q.push(iii(sock[b] - sock[a-1], ii(a-1, b)));
				s.insert(ii(a-1,b));
			}
			else if (!s.count(ii(a,b+1))){
				q.push(iii(sock[b+1] - sock[a], ii(a, b+1)));
				s.insert(ii(a,b+1));
			}
		}
		else {
			if(a){
				if(!s.count(ii(a-1,b))){
					q.push(iii(sock[b] - sock[a-1], ii(a-1, b)));
					s.insert(ii(a-1,b));
				}
			}
			if(b < (n-1)){
				if (!s.count(ii(a,b+1))){
					q.push(iii(sock[b+1] - sock[a], ii(a, b+1)));
					s.insert(ii(a,b+1));
				}
			}
		}
	}

	cout<<(can?"yes":"no")<<'\n';

	return 0;
}
